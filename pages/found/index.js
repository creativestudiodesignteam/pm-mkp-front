import React, { useEffect, useState } from "react"
import propTypes from "prop-types"
import Head from "next/head"
import Main from "../../src/components/Main"
import { BoardedWrapBox_styled } from "../../src/components/BoardedWrapBox"
import {
    Alert,
    Button,
    Col,
    Pagination,
    PaginationItem,
    PaginationLink,
    Row,
} from "reactstrap"
import ProductCard from "../../src/components/home/ProductCard"
import styled from "styled-components"
import {compareBy, fromServer, hasKey} from "../../src/assets/helpers"
import { useRouter } from "next/router"
import _ from "underscore"
import ClipLoader from "react-spinners/ClipLoader"

const GridComponent = styled.div`
    & {
        display: grid;
        gap: 0rem;
        grid-template-columns: repeat(auto-fill, minmax(215px, 1fr));
        grid-auto-rows: minmax(max-content, 320px);
        width: 100%;
    }
`
const Ul = styled.ul`
    & {
        list-style: none;
        padding-top: 2rem;
        padding-left: 0.5rem;
    }
`
const _interface = {
    id: 24,
    name: "Xiaomi Redmi Note 5A Prime",
    status: true,
    categories: {
        id: 13,
        name: "Eletrônicos",
        status: true,
    },
    thumb:
        "https://devs.portaldamaria.com/files/bfecb3ba0dea9c8f2cb6fbd63e622cfc",
    infos: {
        minPrice: 123000,
        maxPrice: 234500,
        amount_stock: "65",
    },
}

function Component({ context: { user, token }, dispatch, products, ...props }) {
    const [state, setState] = useState(products)
    const [category, setCategory] = useState(false)
    const [alert, setAlert] = useState(true)
    const [loaderState, setLoaderState] = useState(false)
    const router = useRouter()
    const toggle = () => {
        setAlert(!alert)
    }
    let categories = products.map(({ categories }) => ({ ...categories }))
    const [currentPage, setCurrentPage] = useState(0)
    const pageSize = 9
    const pagesCount = Math.ceil(state.length / pageSize)
    const handleClick = (e, index) => {
        e.preventDefault()
        setCurrentPage(index)
    }
    useEffect(() => {
        setLoaderState(true)

        console.log({ products })
        if (products.length < 1) {
            setAlert(true)
        }
        setState(products)
        setCategory(false)
        setLoaderState(false)
    }, [router.query, products])
    return (
        <>
            <Head>
                <title>Portal da Maria </title>
                <link rel="icon" href={"/favicon.ico"} />
                <meta name="format-detection" content="telephone=no" />
                <meta
                    httpEquiv="Content-Type"
                    content="text/html; charset=UTF-8"
                />
                <meta
                    name="viewport"
                    content="width=device-width, initial-scale=1, shrink-to-fit=no"
                />
                <meta httpEquiv="X-UA-Compatible" content="IE=edge" />
                <link
                    href="https://fonts.googleapis.com/css?family=Montserrat%7CRoboto:100,300,400,500,700,900%7CRoboto+Condensed:100,300,400,500,700"
                    rel="stylesheet"
                />
            </Head>
            <Main>
                {loaderState && (
                    <div className="spinnerLoader">
                        <ClipLoader
                            size={150}
                            color={"var(--red-1)"}
                            loading={loaderState}
                        />
                    </div>
                )}
                <Row>
                    {products.length === 0 && (
                        <>
                            <Col md="12" className="w-100 mb-4">
                                <Alert
                                    color="primary"
                                    isOpen={alert}
                                    toggle={toggle}
                                    style={{
                                        opacity: 1,
                                        color: "white",
                                        backgroundColor: "var(--red-1)",
                                        padding: "2rem",
                                        fontSize: "16px",
                                        textAlign: "center",
                                        width: "100%",
                                    }}
                                >
                                    Nenhum resultado encontrado para{" "}
                                    {props.query.text}
                                </Alert>
                            </Col>
                            <Col xs="12" className="w-100">
                                <Button
                                    outline
                                    size="lg"
                                    className="redButtonOutline100"
                                    onClick={(e) => {
                                        setLoaderState(true)
                                        router.push("/")
                                    }}
                                >
                                    VOLTAR PARA A HOME
                                </Button>
                            </Col>
                        </>
                    )}
                    {products.length > 0 && (
                        <>
                            <Col xs={12} md={4} lg={3}>
                                <BoardedWrapBox_styled className="sider-bar-category border bottom-margin-default">
                                    <p className="title-siderbar bold">
                                        Categorias{" "}
                                    </p>
                                    {!!categories.length && (
                                        <Ul>
                                            {_.uniq(
                                                _.sortBy(categories, "name"),
                                                function(
                                                    { name },
                                                    key,
                                                    codigo
                                                ) {
                                                    return name
                                                }
                                            )?.map(({ id, name, status }) => (
                                                <li
                                                    key={id}
                                                    className={`mb-5 pointer ${
                                                        !status ? "d-none" : ""
                                                    }`}
                                                    onClick={(event) => {
                                                        event.preventDefault()
                                                        setState(
                                                            products.filter(
                                                                (s) =>
                                                                    Number(
                                                                        s
                                                                            .categories
                                                                            .id
                                                                    ) ===
                                                                    Number(id)
                                                            )
                                                        )
                                                        setCategory(name)
                                                        setCurrentPage(0)
                                                        return false
                                                    }}
                                                >
                                                    <p className="text_dark_2">
                                                        {name}
                                                    </p>
                                                </li>
                                            ))}
                                            <li
                                                className={`mb-5 pointer `}
                                                onClick={(event) => {
                                                    event.preventDefault()
                                                    setState(products)
                                                    setCategory(false)
                                                    setCurrentPage(0)
                                                    return false
                                                }}
                                            >
                                                <p className="text_red">
                                                    Ver todos
                                                </p>
                                            </li>
                                        </Ul>
                                    )}
                                </BoardedWrapBox_styled>
                            </Col>
                            <Col xs={12} md={8} lg={9}>
                                <Col lg="12" className="barCategory p-0">
                                    <Row>
                                        <Col lg="8">
                                            <p className="titleCategory">
                                                {category && (
                                                    <>
                                                        Busca em&nbsp;
                                                        <b>{category}</b>
                                                    </>
                                                )}

                                                {!category &&
                                                    "Busca em todas as categorias"}
                                            </p>
                                        </Col>
                                        <Col
                                            lg="4"
                                            className="rightCategoryBar"
                                        >
                                            <br />
                                            <p className=" float-left">
                                                Mostrando{" "}
                                                {currentPage * pageSize + 1}–
                                                {(currentPage + 1) * pageSize >
                                                state.length
                                                    ? state.length
                                                    : (currentPage + 1) *
                                                      pageSize}{" "}
                                                de {state.length} resultados
                                            </p>
                                        </Col>
                                    </Row>
                                </Col>
                                <GridComponent>
                                    {state
                                        .slice(
                                            currentPage * pageSize,
                                            (currentPage + 1) * pageSize
                                        )
                                        ?.map(
                                            ({
                                                id,
                                                name,
                                                status,
                                                thumb,
                                                infos: { minPrice, maxPrice },
                                            }) => (
                                                <ProductCard
                                                    className={
                                                        !status ? "d-none" : ""
                                                    }
                                                    key={id}
                                                    id={id}
                                                    thumb={{ url: thumb }}
                                                    name={name}
                                                    infos={{
                                                        minPrice,
                                                        maxPrice,
                                                    }}
                                                    little={false}
                                                />
                                            )
                                        )}
                                </GridComponent>
                            </Col>
                            <Col
                                xs={{ size: 12 }}
                                md={{ size: 8, offset: 4 }}
                                lg={{ size: 9, offset: 3 }}
                                className="allignerCenter"
                            >
                                <Pagination aria-label="Page navigation example">
                                    <PaginationItem disabled={currentPage <= 0}>
                                        <PaginationLink
                                            className="buttonPagination"
                                            first
                                            onClick={(e) => handleClick(e, 0)}
                                            href="#"
                                        >
                                            Começo
                                        </PaginationLink>
                                    </PaginationItem>

                                    {[...Array(pagesCount)].map((page, i) => (
                                        <PaginationItem
                                            active={i === currentPage}
                                            key={i}
                                        >
                                            <PaginationLink
                                                className={
                                                    i === currentPage
                                                        ? `
                                                              buttonPaginationActive
                                                          `
                                                        : `
                                                              buttonPagination
                                                          `
                                                }
                                                onClick={(e) =>
                                                    handleClick(e, i)
                                                }
                                                href="#"
                                            >
                                                {i + 1}
                                            </PaginationLink>
                                        </PaginationItem>
                                    ))}

                                    <PaginationItem
                                        disabled={currentPage >= pagesCount - 1}
                                    >
                                        <PaginationLink
                                            className="buttonPagination"
                                            onClick={(e) =>
                                                handleClick(e, pagesCount - 1)
                                            }
                                            href="#"
                                        >
                                            Fim
                                        </PaginationLink>
                                    </PaginationItem>
                                </Pagination>
                            </Col>
                        </>
                    )}
                </Row>
            </Main>
        </>
    )
}

Component.propTypes = {
    context: propTypes.object.isRequired,
    dispatch: propTypes.func.isRequired,
}
export const getServerSideProps = async ({ res, query }) => {
    if (!!query && hasKey(query, "text")) {
        const products = await fromServer(`/search?content=${query.text}`)
        return {
            props: { query, products },
        }
    } else {
        res.setHeader("location", "/")
        res.statusCode = 302
        res.end()
        return {
            props: {},
        }
    }
}
export default Component
