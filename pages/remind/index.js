import React, { useEffect } from "react"
import Head from "next/head"
import Main from "../../src/components/Main"
import { Button } from "reactstrap"
import {
    AuthenticationContainer,
    InputDefault,
    LinkTo,
} from "../../src/components"
import { elm, email_blur, fromServer, hasKey } from "../../src/assets/helpers"
import { withRouter } from "next/router"
import { MySwal } from "../../src/assets/sweetalert"

function Index({ context: { token }, router }) {
    useEffect(() => {
        if (!!token) {
            router.push("/login")
        }
    }, [token])
    const recovery_access = async (e) => {
        e.preventDefault()
        const { value: email } = elm("[name=email]")
        if (!!email) {
            const response = await fromServer(
                "/forgot_password",
                false,
                "POST",
                { email }
            )
            if (hasKey(response, "error")) {
                await MySwal.fire(
                    "Não foi possivel recuperar seu acesso",
                    response?.error?.message,
                    "error"
                )
            } else {
                await MySwal.fire(response?.message, "", "success")
                await router.push("/login")
            }
        }
    }
    return (
        <>
            <Head>
                <title>Portal da Maria | Recuperar acesso</title>
                <link rel="icon" href={"/favicon.ico"} />
                <meta name="format-detection" content="telephone=no" />
                <meta
                    httpEquiv="Content-Type"
                    content="text/html; charset=UTF-8"
                />
                <meta
                    name="viewport"
                    content="width=device-width, initial-scale=1, shrink-to-fit=no"
                />
                <meta httpEquiv="X-UA-Compatible" content="IE=edge" />
                <script src={"//code.jquery.com/jquery-3.2.1.slim.min.js"} />

                <link
                    href="//fonts.googleapis.com/css?family=Montserrat%7CRoboto:100,300,400,500,700,900%7CRoboto+Condensed:100,300,400,500,700"
                    rel="stylesheet"
                />
            </Head>
            <Main
                _className={"mt-5"}
                _BreadCrumb={[
                    {
                        node_text: "login",
                        href: "/login",
                    },
                    {
                        node_text: "esqueci minha senha",
                        href: "/remind",
                    },
                ]}
            >
                <AuthenticationContainer
                    className="justify-content-center mx-auto mt-5 w-50"
                    maxW={"100%"}
                    minW={"max-content"}
                    onSubmit={recovery_access}
                >
                    <h1 className={"h1 font-weight-bold mb-3"}>
                        Recuperação de senha
                    </h1>
                    <div className="d-flex flex-column justify-content-center mb-2">
                        <InputDefault
                            name="email"
                            label={"E-mail"}
                            type={"email"}
                            placeholder={"Informe seu e-mail*"}
                            onBlur={email_blur}
                        />
                    </div>
                    <div className="d-flex flex-row justify-content-start align-items-center">
                        <Button
                            size={"lg"}
                            className={
                                "rounded-0 border-0  px-5 py-4 btn-custom actively mr-5"
                            }
                            type="submit"
                        >
                            ENVIAR
                        </Button>
                        <LinkTo
                            _href="/login"
                            className={"text-muted  m-0 my-auto"}
                        >
                            <u className={"text-dark"}>
                                <b>Voltar</b>
                            </u>
                        </LinkTo>
                    </div>
                </AuthenticationContainer>
            </Main>
        </>
    )
}

export default withRouter(Index)
