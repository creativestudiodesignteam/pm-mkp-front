/*global FormData*/
import React, { useEffect, useState } from "react"
import propTypes from "prop-types"
import Head from "next/head"
import dynamic from "next/dynamic"
import Main from "../../src/components/Main"
import { Button, Col, Row } from "reactstrap"
import { useRouter } from "next/router"
import {
    add_invalid,
    dataURItoBlob,
    elm,
    elmAll,
    formatPriceToServer,
    fromServer,
    OBJ,
} from "../../src/assets/helpers"
import { getToken, isAuthenticated } from "../../src/assets/auth"
import { dispatchFunctions } from "../../src/store"
import Feedback from "../../src/components/product/Feedback"
import { MySwal, Toast } from "../../src/assets/sweetalert"
import { Img, LinkTo } from "../../src/components"
import ClipLoader from "react-spinners/ClipLoader"

const Tabs = dynamic(() => import("../../src/components/Tabs"), {
    ssr: false,
    loading: function Loading() {
        return (
            <div className="spinnerLoader">
                <ClipLoader size={150} color={"var(--red-1)"} loading={true} />
            </div>
        )
    },
})
const initial_state = {
    current_subcategory: [],
    current_category: {},
    categories: [],
    sub_categories: [],
}
const initial_grid = {
    optionsNames: [],
    selectedList: [],
    sheetOption: [],
    images: [],
    rows: [{ id: 1, attrs: [], newImages: [] }],
    sheetOptions: [],
}
const FALSE = false
const index = ({ context, dispatch }) => {
    const Router = useRouter()
    const { user, token } = context
    const [thumb, setThumb] = useState("")
    const [state, setState] = useState(initial_state)
    const [grid, changeGrid] = useState(initial_grid)
    const [isFeedBack, setIsFeedBack] = useState(false)
    const [authentication, setAuthentication] = useState(FALSE)
    const [condition, setCondition] = useState("")
    const [packing, setPacking] = useState(1)
    const [loaderState, setLoaderState] = useState(false)

    async function initPageProps(_token) {
        const categories = await fromServer("/categories", _token)
        if (categories) {
            const { id: current_ID } = categories[0]
            const url = `/subcategories/${current_ID}`
            const sub_categories = await fromServer(url, _token)
            sub_categories &&
                setState({
                    ...state,
                    current_category: categories[0],
                    categories,
                    sub_categories,
                })
        }
        const options = await fromServer("/options", _token)
        if (options) {
            const optionsNames = options.map(({ options }) => options)
            changeGrid({ ...grid, optionsNames })
        }
    }

    useEffect(() => {
        setLoaderState(true)
        if (token) {
            !authentication && setAuthentication(true)
            initPageProps(token).then(() => console.log("load page"))
            setLoaderState(false)
        } else {
            const user = isAuthenticated(token)
            if (!user) {
                Router.push("/login").then(() => {})
            } else if (token === "") {
                dispatch(dispatchFunctions.login({ user, token: getToken() }))
                initPageProps(getToken()).then(() =>
                    console.log("load page after re store")
                )
                setLoaderState(false)
            }
        }
        setLoaderState(false)
    }, [])

    async function save(e) {
        e.preventDefault()
        setLoaderState(true)
        const btn = elm(".save[type=submit]")
        btn.disabled = true
        btn.innerHTML = `Carregando ... <div class="spinner-border text-light" role="status"><span class="sr-only">Loading...</span></div>`
        let forms = []
        grid.rows.forEach((row) => {
            let formData = new FormData()
            row.newImages.forEach((base64Url) => {
                formData.append("files", dataURItoBlob(base64Url))
            })
            forms = [...forms, { id: row.id, formData }]
        })
        const gallery = forms.map(async (data) => {
            return {
                id: data.id,
                data: await fromServer(
                    "/gallery",
                    token,
                    "post",
                    data.formData
                ),
            }
        })
        const response = await Promise.all(gallery)
        const _formData = new FormData()
        let file = dataURItoBlob(thumb[0].dataURL)
        _formData.append("file", file)
        const _thumb = await fromServer("/files", token, "POST", _formData)
        if (OBJ.hasKey(_thumb, "error")) {
            btn.disabled = false
            setLoaderState(false)
        } else {
            let detailsPayload = {}
            if (Number(packing) === 1) {
                detailsPayload = {
                    delivery_time: elm("[name=delivery_time]")?.value || "",
                    length: elm("[name=length]")?.value || "",
                    height: elm("[name=height]")?.value || "",
                    width: elm("[name=width]")?.value || "",
                    weight:
                        String(
                            Number(Number(elm("[name=weight]")?.value) / 1000)
                        ) || "",
                    situation: elm("[name=situation]")?.value || "",
                    type_packing: Number(packing),
                }
            } else if (Number(packing) === 2) {
                detailsPayload = {
                    delivery_time: elm("[name=delivery_time]")?.value || "",
                    length: elm("[name=length]")?.value || "",
                    diameter: elm("[name=diameter]")?.value || "",
                    weight:
                        String(
                            Number(Number(elm("[name=weight]")?.value) / 1000)
                        ) || "",
                    situation: elm("[name=situation]")?.value || "",
                    type_packing: Number(packing),
                }
            } else if (Number(packing) === 3) {
                detailsPayload = {
                    delivery_time: elm("[name=delivery_time]")?.value || "",
                    length: elm("[name=length]")?.value || "",
                    width: elm("[name=width]")?.value || "",
                    weight:
                        String(
                            Number(Number(elm("[name=weight]")?.value) / 1000)
                        ) || "",
                    situation: elm("[name=situation]")?.value || "",
                    type_packing: Number(packing),
                }
            }
            const payload = {
                products: {
                    name: elm("[name=name]")?.value || "",
                    description: elm("[name=description]")?.value || "",
                    brand: elm("[name=brand]")?.value || "",
                    model: elm("[name=model]")?.value || "",
                    sku: elm("[name=sku]")?.value || "",
                    fk_thumb: _thumb.id,
                    fk_categories: state.current_category.id,
                    condition,
                },
                subcategories: state.current_subcategory.map(
                    (sub_category) => ({
                        fk_subcategories: sub_category.id,
                    })
                ),
                details: detailsPayload,
                grids: grid.rows.map((_row) => ({
                    price: formatPriceToServer(
                        elm(`[name=price-${_row.id}]`)?.value || "0",
                        [",", "."]
                    ),
                    amount: elm(`[name=amount-${_row.id}]`).value,
                    options_grids: _row.attrs,
                    gallery: response
                        .filter((r) => r.id === _row.id)[0]
                        .data.map((res) => ({
                            fk_files: res.id,
                        })),
                })),
                datasheet: grid.sheetOption.map((obj) => {
                    const [name, id, description] = Object.values(obj)
                    return { name, description }
                }),
            }

            const data = await fromServer("/products", token, "POST", payload)
            if (OBJ.hasKey(data, "error")) {
                await MySwal.fire(
                    "Erro ao cadastrar",
                    `<div style="text-align: center;">${data?.error}</div>`,
                    "error"
                )
                btn.disabled = false
                setLoaderState(false)
            } else {
                await MySwal.fire(
                    payload?.products?.name,
                    '<div style="text-align: center;">Produto Cadastrado</div>',
                    "success"
                )
                setIsFeedBack(true)
                setLoaderState(false)
            }
        }
        btn.disabled = false
        btn.innerHTML = "SALVAR"
        setLoaderState(false)
        return false
    }

    const goBack = () => {
        setLoaderState(true)
    }

    return (
        <>
            <Head>
                <title>Portal da Maria | Cadastrar Produto</title>
                <link rel="icon" href={"/favicon.ico"} />
                <meta name="format-detection" content="telephone=no" />
                <meta
                    httpEquiv="Content-Type"
                    content="text/html; charset=UTF-8"
                />
                <meta
                    name="viewport"
                    content="width=device-width, initial-scale=1, shrink-to-fit=no"
                />
                <meta httpEquiv="X-UA-Compatible" content="IE=edge" />
                <link
                    href="https://fonts.googleapis.com/css?family=Montserrat%7CRoboto:100,300,400,500,700,900%7CRoboto+Condensed:100,300,400,500,700"
                    rel="stylesheet"
                />
            </Head>
            <Main
                _BreadCrumb={[
                    {
                        node_text: "Minha conta",
                        href: "/account",
                    },
                    {
                        node_text: "Cadastrar Produto",
                        href: "/product",
                    },
                ]}
            >
                {loaderState && (
                    <div className="spinnerLoader">
                        <ClipLoader
                            size={150}
                            color={"var(--red-1)"}
                            loading={loaderState}
                        />
                    </div>
                )}
                {(isFeedBack && (
                    <Feedback
                        isFeedBack={isFeedBack}
                        setIsFeedBack={setIsFeedBack}
                        setLoaderState={setLoaderState}
                    />
                )) || (
                    <form className="m-0 p-0" onSubmit={save}>
                        <div className=" w-75 mx-auto displayFlex showAboveTablet">
                            <Button
                                type="submit"
                                className="ml-auto mr-2 btn-lg btn-save rounded-0 border-0 px-3 py-3 save"
                            >
                                SALVAR
                            </Button>
                            <LinkTo
                                _href={`/account/[feature]`}
                                query={{ token }}
                                as={`/account/product`}
                            >
                                <div className={"delete-icon"} onClick={goBack}>
                                    <Img
                                        image_name_with_extension="add_icon.png"
                                        alt="insert a new product"
                                    />
                                </div>
                            </LinkTo>
                        </div>

                        <Tabs
                            setCondition={setCondition}
                            condition={condition}
                            state={state}
                            setState={setState}
                            grid={grid}
                            changeGrid={changeGrid}
                            user={user}
                            token={token}
                            dispatch={dispatch}
                            thumb={thumb}
                            setThumb={setThumb}
                            packing={packing}
                            setPacking={setPacking}
                        />
                        <Row className="w-100">
                            <Col xs="12" className="allignerEnd" />
                        </Row>
                        <div className="w-75 mx-auto displayFlex">
                            <Button
                                type="submit"
                                className="ml-auto mr-2 btn-lg btn-save rounded-0 border-0 px-3 py-3 save"
                            >
                                SALVAR
                            </Button>
                            <LinkTo
                                _href={`/account/[feature]`}
                                query={{ token }}
                                as={`/account/product`}
                            >
                                <div className={"delete-icon"} onClick={goBack}>
                                    <Img
                                        image_name_with_extension="add_icon.png"
                                        alt="insert a new product"
                                    />
                                </div>
                            </LinkTo>
                        </div>
                    </form>
                )}
            </Main>
        </>
    )
}
index.propTypes = {
    context: propTypes.object.isRequired,
    dispatch: propTypes.func.isRequired,
}
export default index
