import React, { useState } from "react"
import Head from "next/head"
import { InputDefault, Main } from "../../src/components"
import { Button, Col, Form, Row } from "reactstrap"
import {
    defaultListener,
    elm,
    fromServer,
    OBJ,
    onKeyUpTelephone,
} from "../../src/assets/helpers/index"
import { MySwal } from "../../src/assets/sweetalert"
import { useRouter } from "next/router"

const Contact = () => {
    const router = useRouter()
    const [contact, setContact] = useState({
        name: "",
        tel: "",
        email: "",
        message: "",
    })
    const send = async (e) => {
        e.preventDefault()
        const btn = elm(".buttonSave")
        btn.disabled = true
        btn.innerHTML = `Carregando ... <div class="spinner-border text-light" role="status"><span class="sr-only">Loading...</span></div>` //ENVIAR MENSAGEM
        const contactResponse = await fromServer(
            "/contact",
            "",
            "POST",
            contact
        )
        if (!OBJ.hasKey(contactResponse, "error")) {
            await MySwal.fire("Mensagem enviada com sucesso", ``, "success")
            btn.disabled = false
            await router.push("/")
        } else {
            await MySwal.fire(
                "Erro ao enviar mensagem, tente novamente",
                `${contactResponse.error}`,
                "error"
            )
            btn.disabled = false
        }
        btn.innerHTML = `ENVIAR MENSAGEM`
    }

    return (
        <>
            <Head>
                <title>Portal da Maria | Contato</title>
                <link rel="icon" href="/favicon.ico" />
                <meta name="format-detection" content="telephone=no" />
                <meta
                    httpEquiv="Content-Type"
                    content="text/html; charset=UTF-8"
                />
                <meta
                    name="viewport"
                    content="width=device-width, initial-scale=1, shrink-to-fit=no"
                />
                <meta httpEquiv="X-UA-Compatible" content="IE=edge" />
                <link
                    href="https://fonts.googleapis.com/css?family=Montserrat%7CRoboto:100,300,400,500,700,900%7CRoboto+Condensed:100,300,400,500,700"
                    rel="stylesheet"
                />
            </Head>
            <Main
                _BreadCrumb={[
                    {
                        node_text: "Contato",
                        href: "/contact",
                        query: {},
                    },
                ]}
            >
                <Row className="allignerCenter mb-4">
                    <Col md="6">
                        <h1>Enviar uma mensagem</h1>
                    </Col>
                </Row>
                <Row className="allignerCenter">
                    <Col md={{ size: 6 }}>
                        <Form onSubmit={send}>
                            <Row form className="mb-4">
                                <Col md={12}>
                                    <InputDefault
                                        name={"name"}
                                        type={"text"}
                                        placeholder={"Nome*"}
                                        _required
                                        initial_value={contact.name}
                                        onChange={defaultListener(
                                            setContact,
                                            contact
                                        )}
                                    />
                                </Col>
                                <Col md={12}>
                                    <InputDefault
                                        name={"tel"}
                                        type={"tel"}
                                        placeholder={"Número de telefone*"}
                                        _required
                                        initial_value={contact.tel}
                                        onKeyUp={onKeyUpTelephone}
                                        options={{
                                            delimiters: [" ", "-"],
                                            blocks: [2, 5, 4],
                                        }}
                                        onChange={defaultListener(
                                            setContact,
                                            contact
                                        )}
                                    />
                                </Col>
                                <Col md={12}>
                                    <InputDefault
                                        name={"email"}
                                        type={"email"}
                                        placeholder={"Seu email*"}
                                        _required
                                        initial_value={contact.email}
                                        onChange={defaultListener(
                                            setContact,
                                            contact
                                        )}
                                    />
                                </Col>
                                <Col md={12}>
                                    <InputDefault
                                        name={"message"}
                                        type={"textarea"}
                                        placeholder={"Mensagem*"}
                                        _required
                                        _maxLength={10000}
                                        styleInput={{
                                            fontSize: "14px",
                                            backgroundColor: "var(--gray-1)",
                                        }}
                                        initial_value={contact.message}
                                        onChange={defaultListener(
                                            setContact,
                                            contact
                                        )}
                                    />
                                </Col>
                                <Col md="4" className="mt-4">
                                    <Button
                                        type="submit"
                                        size="lg"
                                        className="buttonSave"
                                    >
                                        ENVIAR MENSAGEM
                                    </Button>
                                </Col>
                            </Row>
                        </Form>
                    </Col>
                </Row>
            </Main>
        </>
    )
}

export default Contact
