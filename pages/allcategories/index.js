import React, { useEffect, useState } from "react"
import Head from "next/head"
import Main from "../../src/components/Main"
import { LinkTo } from "../../src/components"
import { Alert, Col, ListGroup, ListGroupItem, Row } from "reactstrap"
import { awaitable, fromServer } from "../../src/assets/helpers"
import { useRouter } from "next/router"
import ClipLoader from "react-spinners/ClipLoader"

const AllCategories = () => {
    const [categories, setCategories] = useState([])
    const [alert, setAlert] = useState(false)
    const toggle = () => setAlert(!alert)
    const router = useRouter()
    const [loaderState, setLoaderState] = useState(false)

    useEffect(() => {
        setLoaderState(true)
        awaitable(async () => {
            if (!categories.length) {
                const data = await fromServer("/categories/all", "", "get")
                if (data.length > 0) {
                    setCategories([...categories, ...data])
                    setLoaderState(false)
                } else {
                    setAlert(true)
                    console.log("fail to load categories", { data })
                    setLoaderState(false)
                }
            }
            setLoaderState(false)
        })
    }, [])
    return (
        <>
            <Head>
                <title>Portal da Maria | Todas as Categorias</title>
                <link rel="icon" href={"/favicon.ico"} />
                <meta name="format-detection" content="telephone=no" />
                <meta
                    httpEquiv="Content-Type"
                    content="text/html; charset=UTF-8"
                />
                <meta
                    name="viewport"
                    content="width=device-width, initial-scale=1, shrink-to-fit=no"
                />
                <meta httpEquiv="X-UA-Compatible" content="IE=edge" />
                <link
                    href="https://fonts.googleapis.com/css?family=Montserrat%7CRoboto:100,300,400,500,700,900%7CRoboto+Condensed:100,300,400,500,700"
                    rel="stylesheet"
                />
            </Head>
            <Main>
                {loaderState && (
                    <div className="spinnerLoader">
                        <ClipLoader
                            size={150}
                            color={"var(--red-1)"}
                            loading={loaderState}
                        />
                    </div>
                )}
                {categories.length < 1 && (
                    <Row>
                        <Col lg="12">
                            <Alert
                                color="primary"
                                isOpen={alert}
                                toggle={toggle}
                                style={{
                                    opacity: 1,
                                    color: "white",
                                    backgroundColor: "var(--red-1)",
                                    padding: "2rem",
                                    fontSize: "16px",
                                    textAlign: "center",
                                }}
                            >
                                Não foi possivel carregar as categorias,
                                recarregue a página!
                            </Alert>
                        </Col>
                    </Row>
                )}
                {categories.map((category, index) => {
                    return (
                        <>
                            <Row style={{ marginBottom: "3rem" }} key={index}>
                                <Col lg="12">
                                    <h1
                                        className="bg-red text-white p-1"
                                        style={{ width: "max-content" }}
                                    >
                                        {" "}
                                        {category.name}
                                    </h1>
                                </Col>
                                <Col lg="3">
                                    <ListGroup
                                        flush
                                        style={{ boxShadow: "none" }}
                                    >
                                        {category.subcategories
                                            .slice(
                                                0,
                                                category.subcategories.length /
                                                    4
                                            )
                                            .map((subcategory, index) => {
                                                return (
                                                    <LinkTo
                                                        _href="/category"
                                                        query={{
                                                            id_sub:
                                                                subcategory.id,
                                                            id_cat: category.id,
                                                        }}
                                                        onClick={() =>
                                                            setLoaderState(true)
                                                        }
                                                    >
                                                        <ListGroupItem
                                                            key={index}
                                                            className="linkSub"
                                                        >
                                                            {subcategory.name}
                                                        </ListGroupItem>
                                                    </LinkTo>
                                                )
                                            })}
                                    </ListGroup>
                                </Col>
                                <Col lg="3">
                                    <ListGroup
                                        flush
                                        style={{ boxShadow: "none" }}
                                    >
                                        {category.subcategories
                                            .slice(
                                                category.subcategories.length /
                                                    4,
                                                2 *
                                                    (category.subcategories
                                                        .length /
                                                        4)
                                            )
                                            .map((subcategory, index) => {
                                                return (
                                                    <LinkTo
                                                        _href="/category"
                                                        query={{
                                                            id_sub:
                                                                subcategory.id,
                                                            id_cat: category.id,
                                                        }}
                                                        onClick={() =>
                                                            setLoaderState(true)
                                                        }
                                                    >
                                                        <ListGroupItem
                                                            key={index}
                                                            className="linkSub"
                                                        >
                                                            {subcategory.name}
                                                        </ListGroupItem>
                                                    </LinkTo>
                                                )
                                            })}
                                    </ListGroup>
                                </Col>
                                <Col lg="3">
                                    <ListGroup
                                        flush
                                        style={{ boxShadow: "none" }}
                                    >
                                        {category.subcategories
                                            .slice(
                                                2 *
                                                    (category.subcategories
                                                        .length /
                                                        4),
                                                3 *
                                                    (category.subcategories
                                                        .length /
                                                        4)
                                            )
                                            .map((subcategory, index) => {
                                                return (
                                                    <LinkTo
                                                        _href="/category"
                                                        query={{
                                                            id_sub:
                                                                subcategory.id,
                                                            id_cat: category.id,
                                                        }}
                                                        onClick={() =>
                                                            setLoaderState(true)
                                                        }
                                                    >
                                                        <ListGroupItem
                                                            key={index}
                                                            className="linkSub"
                                                        >
                                                            {subcategory.name}
                                                        </ListGroupItem>
                                                    </LinkTo>
                                                )
                                            })}
                                    </ListGroup>
                                </Col>
                                <Col lg="3">
                                    <ListGroup
                                        flush
                                        style={{ boxShadow: "none" }}
                                    >
                                        {
                                            <>
                                                {category.subcategories
                                                    .slice(
                                                        3 *
                                                            (category
                                                                .subcategories
                                                                .length /
                                                                4),
                                                        category.subcategories
                                                            .length
                                                    )
                                                    .map(
                                                        (
                                                            subcategory,
                                                            index
                                                        ) => {
                                                            return (
                                                                <LinkTo
                                                                    _href="/category"
                                                                    query={{
                                                                        id_sub:
                                                                            subcategory.id,
                                                                        id_cat:
                                                                            category.id,
                                                                    }}
                                                                    onClick={() =>
                                                                        setLoaderState(
                                                                            true
                                                                        )
                                                                    }
                                                                >
                                                                    <ListGroupItem
                                                                        key={
                                                                            index
                                                                        }
                                                                        className="linkSub"
                                                                    >
                                                                        {
                                                                            subcategory.name
                                                                        }
                                                                    </ListGroupItem>
                                                                </LinkTo>
                                                            )
                                                        }
                                                    )}
                                            </>
                                        }
                                    </ListGroup>
                                </Col>
                                <Col sm={"12"}>
                                    <hr />
                                </Col>
                            </Row>
                        </>
                    )
                })}
            </Main>
        </>
    )
}

export default AllCategories
