import React, { useEffect, useState } from "react"
import propTypes from "prop-types"
import Head from "next/head"
import {
    awaitable,
    fromServer,
    OBJ,
    productsParseFromServer,
} from "../../src/assets/helpers"
import { getToken, isAuthenticated, LogIn, logOut } from "../../src/assets/auth"
import { links, valid_path } from "../../src/components/account/store"
import dynamic from "next/dynamic"
import { useRouter } from "next/router"
import ClipLoader from "react-spinners/ClipLoader"

const Dashboard = dynamic(() => import("./../../src/components/account/"), {
    ssr: false,
    loading: function Loading() {
        return (
            <div className="spinnerLoader">
                <ClipLoader size={150} color={"var(--red-1)"} loading={true} />
            </div>
        )
    },
})

function Component({
    context: { user, token },
    dispatch,
    params: { feature },
}) {
    const [products, setProducts] = useState([])
    const Router = useRouter()
    useEffect(() => {
        awaitable(async () => {
            if (!products.length && token) {
                const data = await fromServer("/products", token)
                if (
                    !OBJ.hasKey(data, "error") &&
                    data.length &&
                    OBJ.hasKey(data[0], "name") &&
                    OBJ.hasKey(data[0], "id")
                ) {
                    setProducts(data.map(productsParseFromServer))
                } else {
                    console.log("fail to load produts", { data })
                }
            }
        })
    })
    
    return (
        <>
            <Head>
                <title>Portal da Maria | {valid_path[feature]}</title>
                <link rel="icon" href={"/favicon.ico"} />
                <meta name="format-detection" content="telephone=no" />
                <meta
                    httpEquiv="Content-Type"
                    content="text/html; charset=UTF-8"
                />
                <meta
                    name="viewport"
                    content="width=device-width, initial-scale=1, shrink-to-fit=no"
                />
                <meta httpEquiv="X-UA-Compatible" content="IE=edge" />
                <link
                    href="https://fonts.googleapis.com/css?family=Montserrat%7CRoboto:100,300,400,500,700,900%7CRoboto+Condensed:100,300,400,500,700"
                    rel="stylesheet"
                />
            </Head>
            <Dashboard
                token={token}
                products={products}
                setProducts={setProducts}
                node_text={valid_path[feature]}
                user={user}
                links={links}
                feature={feature}
            />
        </>
    )
}

Component.propTypes = {
    context: propTypes.object.isRequired,
    dispatch: propTypes.func.isRequired,
    params: propTypes.object.isRequired,
}

export async function getStaticPaths() {
    const paths = Object.keys(valid_path).map((v) => ({
        params: { feature: String(v) },
    }))
    return {
        paths,
        fallback: false,
    }
}

export async function getStaticProps({ params }) {
    return { props: { params } }
}

export default Component
