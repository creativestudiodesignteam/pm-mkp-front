import React, {useContext, useEffect, useState} from "react"
import propTypes from "prop-types"
import Head from "next/head"
import {fromServer, hasKey, productsParseFromServer,} from "../../src/assets/helpers"
import {links, valid_path} from "../../src/components/account/store"
import dynamic from "next/dynamic"
import {Context} from "../../src/store"
import {userByToken} from "../../src/assets/auth"
import ClipLoader from "react-spinners/ClipLoader"

const Dashboard = dynamic(() => import("./../../src/components/account/"), {
    ssr: false,
    loading: function Loading() {
        return (
            <div className="spinnerLoader">
                <ClipLoader size={150} color={"var(--red-1)"} loading={true}/>
            </div>
        )
    },
})

function Component({...props}) {
    const {token: serverToken} = props
    const [products, setProducts] = useState([])
    const [{token, user},] = useContext(Context)
    useEffect(() => {
        if (!!props?.data && !products.length && !!props?.data.length) {
            setProducts(props.data.map(productsParseFromServer))
        } else {
            console.log("fail to load produts", props)
        }
    }, [props.data])

    return (
        <>
            <Head>
                <title>Portal da Maria | {valid_path["request"]}</title>
                <link rel="icon" href={"/favicon.ico"}/>
                <meta name="format-detection" content="telephone=no"/>
                <meta
                    httpEquiv="Content-Type"
                    content="text/html; charset=UTF-8"
                />
                <meta
                    name="viewport"
                    content="width=device-width, initial-scale=1, shrink-to-fit=no"
                />
                <meta httpEquiv="X-UA-Compatible" content="IE=edge"/>
                <link
                    href="https://fonts.googleapis.com/css?family=Montserrat%7CRoboto:100,300,400,500,700,900%7CRoboto+Condensed:100,300,400,500,700"
                    rel="stylesheet"
                />
            </Head>
            <Dashboard
                setProducts={setProducts}
                products={products}
                node_text="Informações cadastrais"
                user={user}
                links={links}
                feature={"myaccount"}
                token={token || serverToken}
            />
        </>
    )
}

Component.propTypes = {
    context: propTypes.object.isRequired,
    dispatch: propTypes.func.isRequired,
}
export const getServerSideProps = async ({res, query}) => {
    if (!hasKey(query, "token") || !query?.token) {
        res.setHeader("location", "/login")
        res.statusCode = 302
        res.end()
        return {
            props: {},
        }
    }
    const {token} = query
    if (!userByToken(token)) {
        res.setHeader("location", "/login")
        res.statusCode = 302
        res.end()
    }
    const data = await fromServer("/products", token)
    if (!hasKey(data, "error")) {
        return {
            props: {token, data},
        }
    } else {
        console.log(data.error)
    }
    return {
        props: {token},
    }
}
export default Component
