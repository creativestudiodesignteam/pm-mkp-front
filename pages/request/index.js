import React, { useEffect, useState } from "react"
import Head from "next/head"
import Main from "../../src/components/Main"
import { Button, Col, Row } from "reactstrap"
import Aside from "../../src/components/Aside"
import { links } from "../../src/components/account/store"
import { BoardedWrapBox_styled } from "../../src/components/BoardedWrapBox"
import Img from "../../src/components/Img"
import Cards from "react-credit-cards"
import {
    awaitable,
    brlFormat,
    disable_btn_and_put_innerHTML,
    elm,
    fromServer,
    hasKey,
} from "../../src/assets/helpers/index"
import { MySwal } from "../../src/assets/sweetalert"
import { useRouter } from "next/router"
import { BsThreeDotsVertical } from "react-icons/bs"
import Swal from "sweetalert2"
import withReactContent from "sweetalert2-react-content"

function Component({
    context: { user, token },
    dispatch,
    serverToken,
    request,
    ...props
}) {
    const [asideToggle, setAsideToggle] = useState(false)
    const toggle = () => setAsideToggle(!asideToggle)
    const router = useRouter()
    const { progress } = request
    useEffect(() => {
        awaitable(async () => {
            const _lastStep = Math.max(...progress.map(({ step }) => step))
            console.log({ request }, _lastStep, _lastStep === 2)
            if (!request || hasKey(request, "error")) {
                await MySwal.fire("Pedido não encontrado", "", "error")
                await router.push("/account/[feature]", "/account/request")
            }
        })
    }, [])
    const lastStep = Math.max(...progress.map(({ step }) => step))
    const firstStep = progress.find(({ step }) => step === 0)
    const secondStep = progress.find(({ step }) => step === 1)
    const thirdStep = progress.find(({ step }) => step === 2)
    const forthStep = progress.find(({ step }) => step === 3)

    function progressClass(_step) {
        switch (_step) {
            case 0:
                return "w-25"
            case 1:
                return "w-50"
            case 2:
                return "w-75"
            case 3:
                return "w-100;"
            default:
                return "w-0"
        }
    }

    function castIfIsDate(date) {
        if (String(date).includes("z") || String(date).includes("T")) {
            const _date = new Date(String(date)).toLocaleDateString("pt-Br", {
                month: "numeric",
                day: "numeric",
                year: "numeric",
                hour: "numeric",
                minute: "numeric",
                second: "numeric",
            })
            const [__date, time] = _date.split(" ")
            return `${__date} às ${time}`
        }
        return date
    }

    const opt = {
        showCloseButton: true,
        width: "40rem",
        height: "50vh",
        padding: "5rem",
        customClass: {
            popup: "popup-class",
            confirmButton:
                "btn btn-outline-light dark2ButtonOutline100 buttonCancel",
            cancelButton:
                "btn btn-outline-light redButtonOutline100 buttonCancel",
        },
        showCancelButton: true,
        title: "Deseja realmente cancelar sua compra? Essa ação é irreversível",
        focusConfirm: true,
        confirmButtonText: "SIM",
        cancelButtonText: "NÃO",
        preConfirm: async (e) => {
            disable_btn_and_put_innerHTML(".buttonCancel")
            const data = await fromServer(
                `/checkout/${request.id}`,
                token,
                "DELETE"
            )
            if (!data || hasKey(data, "error")) {
                MySwal.fire(`${data.error.message}`, "", "error")
            } else {
                MySwal.fire("Pedido cancelado com sucesso", "", "success")
                router.reload()
            }
            return data
        },
    }
    const Modal = withReactContent(Swal).mixin(opt)

    return (
        <>
            <Head>
                <title>Portal da Maria</title>
                <link rel="icon" href={"/favicon.ico"} />
                <meta name="format-detection" content="telephone=no" />
                <meta
                    httpEquiv="Content-Type"
                    content="text/html; charset=UTF-8"
                />
                <meta
                    name="viewport"
                    content="width=device-width, initial-scale=1, shrink-to-fit=no"
                />
                <meta httpEquiv="X-UA-Compatible" content="IE=edge" />
                <link
                    href="https://fonts.googleapis.com/css?family=Montserrat%7CRoboto:100,300,400,500,700,900%7CRoboto+Condensed:100,300,400,500,700"
                    rel="stylesheet"
                />
            </Head>
            <Main
                _BreadCrumb={[
                    {
                        node_text: "Login",
                        href: "/login",
                    },
                    {
                        node_text: "Minha Conta",
                        href: `/account`,
                    },
                    {
                        node_text: "Pedidos",
                        href: `/request`,
                    },
                ]}
            >
                <Row>
                    {
                        <>
                            <Col
                                xs="12"
                                className={`showTabletAndLess ${
                                    asideToggle ? "" : "mb-4"
                                }`}
                            >
                                <Button
                                    className="primary w-100"
                                    style={{
                                        backgroundColor: "#e3171b",
                                        fontSize: "2rem",
                                    }}
                                    onClick={toggle}
                                >
                                    Menu
                                </Button>
                            </Col>
                            {!!asideToggle && (
                                <Col xs="12" className="mb-4">
                                    <Aside
                                        token={token}
                                        user={user}
                                        links={links}
                                        feature={"request"}
                                        setAsideToggle={setAsideToggle}
                                        asideToggle={asideToggle}
                                    />
                                </Col>
                            )}
                        </>
                    }
                </Row>
                <Row className="m-0 flex-nowrap">
                    <div className="showAboveTablet">
                        <Aside
                            token={token}
                            user={user}
                            links={links}
                            feature={"request"}
                            setAsideToggle={setAsideToggle}
                            asideToggle={false}
                        />
                    </div>

                    <div id="dashboard" className="flex flex-column w-100">
                        <Row className="w-100 p-0 m-0 allignerBetween text_dark">
                            <Col xs={12} className="p-0">
                                <BoardedWrapBox_styled className="mb-5">
                                    <Row className="p-0 m-0 align-items-start text_dark px-2 py-4">
                                        <Col xs={"12"} md={6}>
                                            <h2 className="mb-4 text_dark h1 m-0 mb-2 bold">
                                                Pedido número {request?.hash}
                                            </h2>
                                            <h3
                                                className={
                                                    "h3 mb-5 h2 m-0 mb-4 bold"
                                                }
                                            >
                                                Resumo do pedido
                                            </h3>
                                            <div className="mb-4 w-100 h-100 d-flex flex-row align-items-center justify-content-between mb-2">
                                                <p className="m-0 textCardAddressRequest">
                                                    Produto
                                                </p>{" "}
                                                <p className="m-0 textCardAddressRequest">
                                                    {brlFormat(
                                                        request?.price,
                                                        true
                                                    )}
                                                </p>
                                            </div>
                                            <div className="mb-2 w-100 h-100 d-flex flex-row align-items-center justify-content-between mb-2">
                                                <p className="m-0 textCardAddressRequest">
                                                    Envio
                                                </p>{" "}
                                                <p className="m-0 textCardAddressRequest">
                                                    {brlFormat(
                                                        request?.shipping
                                                            ?.value,
                                                        true
                                                    )}
                                                </p>
                                            </div>
                                            <hr
                                                className={"my-4"}
                                                style={{
                                                    border:
                                                        "1px solid #232323!important",
                                                    opacity: "0.7",
                                                }}
                                            />
                                            <div className="w-100 h-100 d-flex flex-row align-items-center justify-content-between textCardAddressRequest">
                                                <p>Total</p>
                                                <p>
                                                    {brlFormat(
                                                        request?.total,
                                                        true
                                                    )}
                                                </p>
                                            </div>
                                        </Col>
                                        <Col xs={"12"} md={6}>
                                            <div className=" w-100 h-100 d-flex flex-column align-items-center justify-content-center">
                                                <Img
                                                    className={
                                                        "mb-2 imageRelated"
                                                    }
                                                    alt={
                                                        "product name like a alt"
                                                    }
                                                    image_name_with_extension={
                                                        request?.products
                                                            ?.thumb !== null
                                                            ? request?.products
                                                                  ?.thumb
                                                            : "250x250.png"
                                                    }
                                                    outSide={
                                                        request?.products
                                                            ?.thumb !== null
                                                            ? request?.products
                                                                  ?.thumb
                                                            : ""
                                                    }
                                                />
                                                <h5
                                                    className={
                                                        "m-0 my-2 textCardAddressRequest"
                                                    }
                                                >
                                                    {request?.products?.name}
                                                </h5>
                                                <p className="m-0 h2 text_red">
                                                    {" "}
                                                    {request?.amount} unidade(s)
                                                </p>
                                            </div>
                                        </Col>
                                    </Row>
                                    {!request.canceled_at && lastStep < 3 && (
                                        <div
                                            className="absolute d-flex flex-column align-items-end"
                                            style={{
                                                top: "1rem",
                                                right: "1rem",
                                            }}
                                        >
                                            <BsThreeDotsVertical
                                                className="pointer"
                                                size="2rem"
                                                onClick={(event) => {
                                                    event.preventDefault()
                                                    elm(
                                                        `#cancel-${request.id}`
                                                    ).classList.toggle("d-none")
                                                }}
                                            />
                                            <button
                                                id={`cancel-${request.id}`}
                                                style={{
                                                    width: "10rem",
                                                }}
                                                className="bg-white d-none p-3 border text_dark text-center mr-3"
                                                onClick={async (e) => {
                                                    e.preventDefault()
                                                    const {
                                                        value,
                                                    } = await Modal.fire(opt)
                                                    console.log({ value })
                                                }}
                                            >
                                                Cancelar compra
                                            </button>
                                        </div>
                                    )}
                                </BoardedWrapBox_styled>
                                <BoardedWrapBox_styled className="mb-5">
                                    <Row className="p-0 m-0 align-items-start text_dark px-2 py-4">
                                        <Col xs={12}>
                                            <h2 className="mb-4 text_dark h1 m-0 mb-2 bold">
                                                Pedido número {request?.hash}
                                            </h2>
                                            <div
                                                className="mb-4 p-2 w-100"
                                                style={{
                                                    border: "1px solid #DEDEDE",
                                                }}
                                            >
                                                <div
                                                    className={`${
                                                        request?.canceled_at
                                                            ? "w-100"
                                                            : progressClass(
                                                                  lastStep
                                                              )
                                                    } bg_red`}
                                                    style={{ height: "2rem" }}
                                                />
                                            </div>
                                            <div
                                                className={`w-100 h-100 d-flex flex-row align-items-center justify-content-start`}
                                            >
                                                {!!firstStep && (
                                                    <p
                                                        className={`w-25  text-center 
                                                        ${
                                                            request.canceled_at
                                                                ? "text_red bold"
                                                                : lastStep === 0
                                                                ? "text_green bold"
                                                                : "text-muted"
                                                        }`}
                                                        style={{
                                                            fontSize: "1.5rem",
                                                        }}
                                                    >
                                                        {firstStep.situation}
                                                        <br />
                                                        {castIfIsDate(
                                                            firstStep.date
                                                        )}
                                                    </p>
                                                )}
                                                {!!secondStep && (
                                                    <p
                                                        className={`w-25  text-center ${
                                                            lastStep === 1
                                                                ? "text_green bold"
                                                                : "text-muted"
                                                        }`}
                                                        style={{
                                                            fontSize: "1.5rem",
                                                        }}
                                                    >
                                                        {secondStep.situation}
                                                        <br />
                                                        {castIfIsDate(
                                                            secondStep.date
                                                        )}
                                                    </p>
                                                )}
                                                {!!thirdStep && (
                                                    <p
                                                        className={`w-25  text-center ${
                                                            lastStep === 2
                                                                ? "text_green bold"
                                                                : "text-muted"
                                                        }`}
                                                        style={{
                                                            fontSize: "1.5rem",
                                                        }}
                                                    >
                                                        {thirdStep.situation}
                                                        <br />
                                                        {castIfIsDate(
                                                            thirdStep.date
                                                        )}
                                                    </p>
                                                )}
                                                {!!forthStep && (
                                                    <p
                                                        className={`w-25 text-center ${
                                                            lastStep === 3
                                                                ? "text_green bold"
                                                                : "text-muted"
                                                        }`}
                                                        style={{
                                                            fontSize: "1.5rem",
                                                        }}
                                                    >
                                                        {forthStep.situation}
                                                        <br />
                                                        {castIfIsDate(
                                                            forthStep.date
                                                        )}
                                                    </p>
                                                )}
                                            </div>
                                        </Col>
                                    </Row>
                                </BoardedWrapBox_styled>
                            </Col>
                        </Row>
                        <Row className="w-100 p-0 m-0 align-items-start text_dark">
                            <Col xs={12} lg={5} className="mr-auto p-0 pl-lg-0">
                                {Number(request?.payments?.payment_method) ===
                                    1 && (
                                    <BoardedWrapBox_styled
                                        className="mb-4 justify-content-start px-5 py-25"
                                        style={{ height: "35rem" }}
                                    >
                                        <h2 className="mb-4 text_dark h1 m-0 mb-4 bold text-left">
                                            Pagamento
                                        </h2>
                                        <p className="m-0 mb-5 textCardAddressRequest">
                                            {request?.payments?.situations}
                                        </p>
                                        <h3 className="m-0 mb-2">
                                            Pagamento por boleto
                                        </h3>
                                        <a
                                            className="text_red"
                                            target="_blank"
                                            href={request?.payments?.bankSlip}
                                        >
                                            Clique aqui para visualizar
                                        </a>
                                    </BoardedWrapBox_styled>
                                )}
                                {Number(request?.payments?.payment_method) ===
                                    2 && (
                                    <BoardedWrapBox_styled
                                        className="mb-4 justify-content-start px-5 py-25"
                                        style={{ height: "35rem" }}
                                    >
                                        <h2 className="mb-4 text_dark h1 m-0 mb-4 bold text-left">
                                            Pagamento
                                        </h2>
                                        <p className="m-0 mb-5 textCardAddressRequest">
                                            {request?.payments?.situations}
                                        </p>
                                        <Cards
                                            cvc={"000"}
                                            expiry={"XX/XXXX"}
                                            focused={""}
                                            name={
                                                request?.payments
                                                    ?.holder_name || ""
                                            }
                                            number={
                                                request?.payments
                                                    ?.first_digits || ""
                                            }
                                            acceptedCards={[
                                                "visa",
                                                "mastercard",
                                                "elo",
                                                "hipercard",
                                                "amex",
                                                "dinersclub",
                                                "discover",
                                            ]}
                                            placeholders={{
                                                name: "nome no cartão",
                                                valid: "valido até",
                                            }}
                                            locale={{ valid: "valido até" }}
                                        />
                                    </BoardedWrapBox_styled>
                                )}
                            </Col>
                            <Col xs={12} lg={5} className="ml-auto p-0 pr-lg-0">
                                <BoardedWrapBox_styled
                                    className="mb-4 justify-content-start px-5 py-25"
                                    style={{ height: "35rem" }}
                                >
                                    <h2 className=" text_dark h1 ml-0 mr-0 mt-0 mb-6 bold text-left">
                                        Endereço de entrega
                                    </h2>
                                    <Row>
                                        <Col xs="12">
                                            <p className={"h2 m-0 mb-2 bold"}>
                                                {request?.addresses?.name}
                                            </p>
                                            <p className="m-0 mb-2 textCardAddressRequest">
                                                {request?.addresses?.street}
                                            </p>
                                            <p className="m-0 mb-2 textCardAddressRequest">
                                                {request?.addresses?.number}{" "}
                                                {request?.addresses
                                                    ?.complement &&
                                                    request?.addresses
                                                        ?.complement.length >
                                                        0 &&
                                                    "," +
                                                        request?.addresses
                                                            ?.complement}
                                            </p>
                                            <p className="m-0 mb-2 textCardAddressRequest">
                                                {
                                                    request?.addresses
                                                        ?.neighborhood
                                                }
                                            </p>
                                            <p className="m-0 mb-2 textCardAddressRequest">
                                                {request?.addresses?.city} -{" "}
                                                {request?.addresses?.state}
                                            </p>
                                            <p className="m-0 mb-2 textCardAddressRequest">
                                                CEP{" "}
                                                {request?.addresses?.postcode}
                                            </p>
                                        </Col>
                                    </Row>
                                </BoardedWrapBox_styled>
                            </Col>
                        </Row>
                    </div>
                </Row>
            </Main>
        </>
    )
}

export const getServerSideProps = async ({ query }) => {
    if (!!query && hasKey(query, "id") && hasKey(query, "token")) {
        const { id, token } = query
        const request = await fromServer(`/sales/detail/${id}`, token)
        return {
            props: { request },
        }
    }
    return {
        props: {},
    }
}
export default Component
