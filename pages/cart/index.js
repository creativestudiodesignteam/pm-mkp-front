import React, {useContext, useEffect, useState} from "react"
import Head from "next/head"
import Main from "../../src/components/Main"
import Cart from "../../src/components/shoppingCart/Cart"
import {awaitable, cartParseFromServer, fromServer, hasKey, tryCatch} from "../../src/assets/helpers"
import {Context, dispatchFunctions} from "../../src/store";
import {__storage} from "../../src/assets/auth";

const index = (props) => {
    const [state, setState] = useState([])
    const [context, dispatch] = useContext(Context)
    useEffect(function () {
        awaitable(async () => {
            if (
                hasKey(props, "cart") ||
                props.serverToken ||
                props.context.token
            ) {
                const serverCart = await fromServer(
                    "/carts",
                    props.serverToken || props.context.token
                )
                const cart_ = hasKey(props, "cart") ? props.cart : serverCart
                const _cart = cart_.map(cartParseFromServer)
                setState(_cart)
            } else {
                const backup = __storage((storage) => JSON.parse(storage.getItem('cart')), [])
                setState(backup.map(cartParseFromServer))
            }
        })
    }, [])
    useEffect(() => {
        console.log('[state]')
        dispatch(dispatchFunctions.raw({...context, cart: state, cart_length: state.length}))
    }, [state])
    return (
        <>
            <Head>
                <title>Portal da Maria | Carrinho de Compras</title>
                <link rel="icon" href={"/favicon.ico"}/>
                <meta name="format-detection" content="telephone=no"/>
                <meta
                    httpEquiv="Content-Type"
                    content="text/html; charset=UTF-8"
                />
                <meta
                    name="viewport"
                    content="width=device-width, initial-scale=1, shrink-to-fit=no"
                />
                <meta httpEquiv="X-UA-Compatible" content="IE=edge"/>
                <link
                    href="https://fonts.googleapis.com/css?family=Montserrat%7CRoboto:100,300,400,500,700,900%7CRoboto+Condensed:100,300,400,500,700"
                    rel="stylesheet"
                />
            </Head>
            <Main
                _BreadCrumb={[
                    {
                        node_text: "Carrinho de Compras",
                        href: "/cart",
                    },
                ]}
            >
                <Cart
                    state={state}
                    setState={setState}
                    token={props.serverToken || props.context.token}
                />
            </Main>
        </>
    )
}
export const getServerSideProps = async ({res, query}) => {
    if (hasKey(query, "token") && query.token.length) {
        const {token} = query
        const cart = await fromServer("/carts", token, "get")
        if (hasKey(cart, "error")) {
            const {error} = cart
            console.log("fail to load carts")
        } else {
            return {
                props: {cart, serverToken: token},
            }
        }
    }
    return {
        props: {},
    }
}
export default index
