import React, {useEffect} from "react";
import Head from "next/head";
import Main from "../../src/components/Main";
import {Button, Col, Row} from "reactstrap";
import {Img, LinkTo} from "../../src/components";
import {BoardedWrapBox_styled} from "../../src/components/BoardedWrapBox";
import style from "./index.module.css";
import {
    disable_btn_and_put_innerHTML,
    elm,
    enable_btn_and_put_innerHTML,
    fromServer,
    hasKey,
    toFixed,
} from "../../src/assets/helpers";
import {MySwal} from "../../src/assets/sweetalert";
import {useRouter} from 'next/router'

const Index = ({context: {token}, id, token: serverToken, data}) => {
    const router = useRouter()
    const options = data?.products?.options_grids
        ?.map(({options: {name: key}, options_names: {name: value}}) => ({
            key,
            value,
        }))
        .reduce(
            (accumulator, currentValue, idx, arr) =>
                `${accumulator} ${currentValue?.key}: ${currentValue?.value} ${
                    idx < arr.length - 1 ? "|" : ""
                } `,
            ""
        );
    useEffect(() => {
        console.log({token, id, serverToken, data})
    }, [token, id, serverToken, data])
    return (
        <>
            <Head>
                <title>Portal da Maria | Sobre o portal</title>
                <link rel="icon" href={"/favicon.ico"}/>
                <meta name="format-detection" content="telephone=no"/>
                <meta httpEquiv="Content-Type" content="text/html; charset=UTF-8"/>
                <meta
                    name="viewport"
                    content="width=device-width, initial-scale=1, shrink-to-fit=no"
                />
                <meta httpEquiv="X-UA-Compatible" content="IE=edge"/>
                <link
                    href="https://fonts.googleapis.com/css?family=Montserrat%7CRoboto:100,300,400,500,700,900%7CRoboto+Condensed:100,300,400,500,700"
                    rel="stylesheet"
                />
            </Head>
            <Main
                _BreadCrumb={[
                    {
                        node_text: "Minha conta",
                        href: "/about",
                        query: {token: token || serverToken},
                    },
                    {
                        node_text: "Pedidos",
                        href: "/account/[request]",
                        as: "/account/request",
                        query: {token: token || serverToken},
                    },
                    {
                        node_text: "Informar NF-e",
                        href: "/nf",
                        query: {token: token || serverToken},
                    },
                ]}
            >
                <Row className="mb-7">
                    <Col>
                        <h1 className="m-0 p-0 text_dark_2">Informar NF-e</h1>
                        <h5 className="m-0 p-0 text_dark_2">
                            Anexe a NF-e no formato XML.
                        </h5>
                    </Col>
                </Row>
                <div className="w-100 mx-auto d-flex flex-row align-items-centers  mb-4">
                    <Button
                        id={"btn_save"}
                        className="ml-auto mr-2 btn-lg btn-save rounded-0 border-0 px-3 py-3 save"
                        onClick={async (e) => {
                            e.preventDefault();
                            disable_btn_and_put_innerHTML("#btn_save");
                            const form = elm("#formFile");
                            const xml = elm("#xml");
                            if (xml.value) {
                                const formData = new FormData(form);
                                const response = await fromServer(
                                    "/sales/invoice",
                                    token || serverToken,
                                    "POST",
                                    formData
                                );
                                console.log({response});
                                const update = await fromServer(
                                    `/sales/nfe/${id}`,
                                    token || serverToken,
                                    "patch",
                                    {
                                        fk_invoices: response.id,
                                    }
                                );
                                !!response?.error ||
                                (!!update?.error &&
                                    (await MySwal.fire(
                                        "Ops",
                                        response?.error?.message +
                                        "<br/>" +
                                        update?.error?.message,
                                        "error"
                                    )));
                                !response?.error &&
                                !update?.error &&
                                (await MySwal.fire("Ok", "Upload concluido", "success"));
                                console.log({response, update});
                                await router.push('/account')
                            } else {
                                await MySwal.fire("Ops", "Arquivo não identificado", "warning");
                            }
                            form.reset();
                            elm(
                                "#feedback"
                            ).innerHTML = ` Arraste um arquivo ou <b class='text_red'>envie do seu computador</b>`;
                            enable_btn_and_put_innerHTML("#btn_save");
                            return false;
                        }}
                    >
                        SALVAR
                    </Button>
                    <LinkTo _href={`/account`} query={{token: token || serverToken}}>
                        <div className={"delete-icon"}>
                            <Img
                                image_name_with_extension="add_icon.png"
                                alt="insert a new product"
                            />
                        </div>
                    </LinkTo>
                </div>
                <Row>
                    <Col xs={12}>
                        <BoardedWrapBox_styled style={{minWidth: "auto"}}>
                            <div
                                className="d-flex flex-row align-items-center justify-content-start p-5"
                                style={{minWidth: "auto"}}
                            >
                                <Col className="m-0 p-0 w-100">
                                    <Col xs={12} md={7}>
                                        <p>
                                            Pedido {data?.code} | {data?.buyer?.name} |{" "}
                                            {data?.buyer?.doc}
                                        </p>
                                        <Row
                                            className="d-flex flex-row align-items-center"
                                            xs={4}>
                                            <Col xs={12} md={4}>
                                                {!!data?.products?.thumb && (
                                                    <Img
                                                        image_name_with_extension={data?.products?.thumb}
                                                        outSide
                                                        alt={data?.products?.name}
                                                    />
                                                )}
                                            </Col>
                                            <Col xs={12} md={6}>
                                                <p
                                                    className={`m-0 ${(!!data?.products?.thumb &&
                                                        "ml-4") ||
                                                    ""}  p-0 `}
                                                >
                                                    {data?.products?.name}
                                                    <br/> {options} <br/>
                                                    {data?.amount} unidades <br/>
                                                    {`  R$ ${toFixed(data?.price)}`}
                                                </p>
                                            </Col>
                                        </Row>
                                    </Col>
                                    <Col
                                        xs={12}
                                        md={5}
                                        className="pointer ml-md-auto"
                                        style={{marginTop: "2rem"}}
                                    >
                                        <div className={style.dashedBoarded}>
                                            <form
                                                id="formFile"
                                                encType="multipart/form-data"
                                                autoComplete="off"
                                                className="w-100"
                                            >
                                                <input
                                                    className={style.input}
                                                    type="file"
                                                    id={"xml"}
                                                    name={"file"}
                                                    placeholder={
                                                        "Arraste um arquivo ou envie do seu computador"
                                                    }
                                                    onChange={(e) => {
                                                        e.preventDefault();
                                                        if (!e.target.value) {
                                                            elm(
                                                                "#feedback"
                                                            ).innerHTML = ` Arraste um arquivo ou <b class='text_red'>envie do seu computador</b>`;
                                                        } else {
                                                            elm(
                                                                "#feedback"
                                                            ).innerHTML = `<b class='text_green text-center '>Arquivo carregado</b>`;
                                                        }
                                                    }}
                                                />
                                                <p id="feedback" className="p-0 m-0 w-100 text-center">
                                                    Arraste um arquivo ou{" "}
                                                    <b className="text_red">envie do seu computador</b>
                                                </p>
                                            </form>
                                        </div>
                                    </Col>
                                </Col>
                            </div>
                        </BoardedWrapBox_styled>
                    </Col>
                </Row>
            </Main>
        </>
    );
};
export const getServerSideProps = async ({res, query}) => {
    if (
        !!query &&
        hasKey(query, "id") &&
        !!query?.id &&
        hasKey(query, "token") &&
        !!query?.token
    ) {
        const {id, token} = query;
        const data = await fromServer(`/sales/nfe/${id}`, token);
        if (!!data && !hasKey(data, "error")) {
            return {
                props: {id, token, data},
            };
        } else {
            hasKey(data, "error") && console.error(data.error);
            res.setHeader("location", "/account");
            res.statusCode = 302;
            res.end();
            return {
                props: {},
            };
        }
    } else {
        res.setHeader("location", "/account");
        res.statusCode = 302;
        res.end();
        return {
            props: {},
        };
    }
};
Index.propTypes = {};
export default Index;
