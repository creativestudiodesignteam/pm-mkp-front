import React from "react"
import Head from "next/head"
import { Main } from "../src/components/"
import { elm, fromServer, hasKey } from "../src/assets/helpers"
import { Row } from "reactstrap"
import GoodDeals from "../src/components/home/GoodDeals"
import propTypes from "prop-types"
import dynamic from "next/dynamic"
import { useRouter } from "next/router"
import ClipLoader from "react-spinners/ClipLoader"

const Banner = dynamic(() => import("../src/components/home/Banner"), {
    ssr: false,
    loading: function Loading() {
        return (
            <div className="spinnerLoader">
                <ClipLoader size={150} color={"var(--red-1)"} loading={true} />
            </div>
        )
    },
})
const Eletronicos = dynamic(
    () => import("../src/components/home/Eletronicos"),
    {
        ssr: false,
        loading: function Loading() {
            return (
                <div className="spinnerLoader">
                    <ClipLoader
                        size={150}
                        color={"var(--red-1)"}
                        loading={true}
                    />
                </div>
            )
        },
    }
)
const Eletrodomesticos = dynamic(
    () => import("../src/components/home/Eletrodomesticos"),
    {
        ssr: false,
        loading: function Loading() {
            return (
                <div className="spinnerLoader">
                    <ClipLoader
                        size={150}
                        color={"var(--red-1)"}
                        loading={true}
                    />
                </div>
            )
        },
    }
)
const Fashion = dynamic(() => import("../src/components/home/Fashion"), {
    ssr: false,
    loading: function Loading() {
        return (
            <div className="spinnerLoader">
                <ClipLoader size={150} color={"var(--red-1)"} loading={true} />
            </div>
        )
    },
})
const HealthBeauty = dynamic(
    () => import("../src/components/home/HealthBeauty"),
    {
        ssr: false,
        loading: function Loading() {
            return (
                <div className="spinnerLoader">
                    <ClipLoader
                        size={150}
                        color={"var(--red-1)"}
                        loading={true}
                    />
                </div>
            )
        },
    }
)
const MotherBaby = dynamic(() => import("../src/components/home/MotherBaby"), {
    ssr: false,
    loading: function Loading() {
        return (
            <div className="spinnerLoader">
                <ClipLoader size={150} color={"var(--red-1)"} loading={true} />
            </div>
        )
    },
})
const AutoMoto = dynamic(() => import("../src/components/home/AutoMoto"), {
    ssr: false,
    loading: function Loading() {
        return (
            <div className="spinnerLoader">
                <ClipLoader size={150} color={"var(--red-1)"} loading={true} />
            </div>
        )
    },
})

function Home({
    mobile,
    electric,
    fashion,
    healthBeauty,
    motherBaby,
    autoMoto,
    query,
    ...props
}) {
    const router = useRouter()
    const showBoxCateHomeByID = (id, element_general, category) => (e) => {
        const idsFood = [
            "#confectionery",
            "#milk-cream",
            "#dry-food",
            "#vegetables",
            "#drinks",
        ]
        const idsMobile = [
            "#smart-phone",
            "#tablet",
            "#smart-watch",
            "#case",
            "#gadget",
        ]
        const idsElectric = [
            "#television",
            "#laptop",
            "#camera",
            "#audio",
            "#accessories",
        ]
        e.preventDefault()
        const element = elm(element_general)
        element.style.transition = "opacity 500ms linear"
        element.classList.remove("active-box-category")
        element.style.display = "none"
        element.style.opacity = 0
        if (category === "food") {
            idsFood.map((elem) => {
                if (elem !== id) {
                    elm(elem).style.transition = "opacity 500ms linear"
                    elm(elem).style.display = "none"
                    elm(elem).style.opacity = 0
                }
            })
        } else if (category === "mobile") {
            idsMobile.map((elem) => {
                if (elem !== id) {
                    elm(elem).style.transition = "opacity 500ms linear"
                    elm(elem).style.display = "none"
                    elm(elem).style.opacity = 0
                }
            })
        } else if (category === "electric") {
            idsElectric.map((elem) => {
                if (elem !== id) {
                    elm(elem).style.transition = "opacity 500ms linear"
                    elm(elem).style.display = "none"
                    elm(elem).style.opacity = 0
                }
            })
        }
        elm(id).style.transition = "opacity 500ms linear"
        elm(id).style.display = "block"
        elm(id).style.opacity = 1
    }

    return (
        <>
            <Head>
                <title>Portal da Maria</title>
                <link rel="icon" href={"/favicon.ico"} />
                <meta
                    httpEquiv="Content-Type"
                    content="text/html; charset=UTF-8"
                />
                <meta
                    name="viewport"
                    content="width=device-width, initial-scale=1, shrink-to-fit=no"
                />
                <meta httpEquiv="X-UA-Compatible" content="IE=edge" />

                <link
                    href="https://fonts.googleapis.com/css?family=Montserrat%7CRoboto:100,300,400,500,700,900%7CRoboto+Condensed:100,300,400,500,700"
                    rel="stylesheet"
                />
            </Head>
            <Main>
                <Banner />
                <GoodDeals />
                <Eletronicos products={mobile} />
                <Eletrodomesticos products={electric} />
                <Row>
                    <Fashion products={fashion} />
                    <HealthBeauty products={healthBeauty} />
                </Row>
                <Row>
                    <MotherBaby products={motherBaby} />
                    <AutoMoto products={autoMoto} />
                </Row>
            </Main>
        </>
    )
}

Home.propTypes = {
    mobile: propTypes.array.isRequired,
    electric: propTypes.array.isRequired,
}
export const getServerSideProps = async ({ query, res }) => {
    try {
        const [
            mobile,
            electric,
            fashion,
            healthBeauty,
            motherBaby,
            autoMoto,
        ] = [
            await fromServer(`/home/${13}/4`, "get"),
            await fromServer(`/home/${12}/4`, "get"),
            await fromServer(`/home/${26}/6`, "get"),
            await fromServer(`/home/${34}/6`, "get"),
            await fromServer(`/home/${6}/6`, "get"),
            await fromServer(`/home/${4}/6`, "get"),
        ]
        if (
            hasKey(mobile, "error") ||
            hasKey(electric, "error") ||
            hasKey(fashion, "error") ||
            hasKey(healthBeauty, "error") ||
            hasKey(motherBaby, "error") ||
            hasKey(autoMoto, "error")
        ) {
            console.log("fail to load carts", {
                mobile,
                electric,
                fashion,
                healthBeauty,
                motherBaby,
                autoMoto,
            })
        } else {
            return {
                props: {
                    query,
                    mobile,
                    electric,
                    fashion,
                    healthBeauty,
                    motherBaby,
                    autoMoto,
                },
            }
        }
    } catch (e) {
        console.log("deu merda ", e)
        return {
            props: {
                query: {},
                mobile: [],
                electric: [],
                fashion: [],
                healthBeauty: [],
                motherBaby: [],
                autoMoto: [],
            },
        }
    }
    return { props: { query } }
}

export default Home
