import React from "react"
import App from "next/app"
import {ThemeProvider} from "styled-components"
import FullFooter from "../src/components/FullFooter"
import "react-image-gallery/styles/css/image-gallery.css"
import "../src/assets/css/build/icon-font-linea.css"
import "../src/assets/css/build/bootstrap.min.css"
import "../src/assets/css/build/bootstrap-theme.min.css"
import "../src/assets/css/build/style.css"
import "../src/assets/css/build/effect.css"
import "../src/assets/css/build/font-awesome.min.css"
import "../src/assets/css/build/home.css"
import "../src/assets/css/build/responsive.css"
import "../src/assets/css/build/index.css"
import "slick-carousel/slick/slick.css"
import "slick-carousel/slick/slick-theme.css"
import "bootstrap/dist/css/bootstrap.min.css"
import "animate.css/animate.min.css"
import "react-credit-cards/es/styles-compiled.css"
import {Context} from "../src/store"
import dynamic from "next/dynamic"

import ClipLoader from "react-spinners/ClipLoader"

const FullHeader = dynamic(() => import("../src/components/FullHeader"), {
    ssr: false,
    loading: function Loading() {
        return (
            <div className="spinnerLoader">
                <ClipLoader size={150} color={"var(--red-1)"} loading={true}/>
            </div>
        )
    },
})
const Store = dynamic(() => import("../src/store"), {
    ssr: false,
    loading: function Loading() {
        return (
            <div className="spinnerLoader">
                <ClipLoader size={150} color={"var(--red-1)"} loading={true}/>
            </div>
        )
    },
})
const theme = {}

class MyApp extends App {
    render() {
        const {Component, pageProps} = this.props

        function Children(props) {
            const [context, dispatch] = props
            const mergedProps = {...pageProps, context, dispatch}
            return (
                <>
                    <FullHeader {...mergedProps} />
                    <Component {...mergedProps} />
                    <FullFooter {...mergedProps} />
                </>
            )
        }

        return (
            <ThemeProvider theme={theme}>
                <Store>
                    <Context.Consumer>{Children}</Context.Consumer>
                </Store>
            </ThemeProvider>
        )
    }
}

export default MyApp
