/*global FormData*/
import React, { useEffect, useState } from "react"
import {
    Button,
    Col,
    Container,
    CustomInput,
    FormGroup,
    Label,
    Modal,
    ModalBody,
    ModalHeader,
    Row,
    Table,
} from "reactstrap"
import dynamic from "next/dynamic"
import Stepper from "react-stepper-horizontal"
import Head from "next/head"
// import Main from "../../src/components/Main"
import styles from "./index.module.css"
import { InputDefault, LinkTo, SelectDefault } from "../../src/components"
import {
    _password_blur,
    bank_account_types,
    birthday,
    cepBlur,
    cnpj,
    cnpj_blur,
    code_banks,
    cpf_blur,
    cpf_rg,
    dataURItoBlob,
    dateChecker,
    disable_btn_and_put_innerHTML,
    elm,
    email_blur,
    enable_btn_and_put_innerHTML,
    fromServer,
    hasEmptyNodeForm,
    hasKey,
    modalidades,
    obrigatory_blur,
    onKeyUpTelephone,
    onlyNumber,
    postcode,
    ramos,
    size_blur,
    state_address,
    verifyCNPJ,
    verifyEmail,
} from "../../src/assets/helpers"
import { MySwal } from "../../src/assets/sweetalert"
import { useRouter } from "next/router"
import Img from "../../src/components/Img"
import PhotoUploader from "./../../src/components/photoUploader/"
import ClipLoader from "react-spinners/ClipLoader"
import useForceUpdate from "use-force-update"

const Main = dynamic(() => import("../../src/components/Main"), {
    ssr: false,
    loading: function Loading() {
        return (
            <div className="spinnerLoader">
                <ClipLoader size={150} color={"var(--red-1)"} loading={true} />
            </div>
        )
    },
})
const SelectCustom = dynamic(
    () => import("../../src/components/SelectCustom"),
    {
        ssr: false,
        loading: function Loading() {
            return (
                <div className="spinnerLoader">
                    <ClipLoader
                        size={150}
                        color={"var(--red-1)"}
                        loading={true}
                    />
                </div>
            )
        },
    }
)
const steps = [
    { title: "Dados da Loja" },
    { title: "Localização" },
    { title: "Dados Bancários" },
    { title: "Dados Responsável" },
    { title: "Finalização" },
]

const NewSign = () => {
    const forceUpdate = useForceUpdate()
    const router = useRouter()
    const [isOk, setIsOK] = useState(false)
    const [modal, setModal] = useState(false)
    const toggle = () => setModal(!modal)
    const [currentStep, setCurrentStep] = useState(0)
    const [ramosPayload, setRamosPayload] = useState([])
    const [payload, setPayload] = useState({
        login: "",
        password: "",
        confirmPassword: "",
        cnpj: "",
        fantasy_name: "",
        reason_social: "",
        state_register: "",
        telephone: "",
        telephone_r: "",
        fundacao: "",
        qty_f: "",
        site: "",
        nameAddress: "",
        postcode: "",
        state_address: "",
        city: "",
        neighborhood: "",
        street: "",
        number: "",
        county_register: "",
        bank_code: "",
        account_type: "",
        bank_agency: "",
        bank_agency_digit: "",
        bank_account: "",
        bank_account_digit: "",
        name_completo: "",
        email: "",
        cpf: "",
        telefone_pessoal: "",
        cargo: "",
        modalidade: "",
    })
    const [thumb, setThumb] = useState("")
    /*useEffect(() => {
        console.log({payload})
    }, [payload])*/

    function defaultListener(e) {
        e.preventDefault()
        setPayload({ ...payload, [e.target.name]: e.target.value })
    }

    const onSelectRamos = (ramosSelected) => {
        let selected = ramosSelected.map((ramo) => ramo.name)
        setRamosPayload(selected)
    }

    function currentChange(idx, hasSelect) {
        return async (e) => {
            e.preventDefault()
            if (payload.qty_f === "") {
                setPayload({ ...payload, ["qty_f"]: "0" })
            }
            if (idx === 1 && hasSelect) {
                if (ramosPayload?.length < 1) {
                    await MySwal.fire(
                        "Nenhum canal de vendas foi selecionado",
                        "",
                        "error"
                    )
                    return false
                }

                if (payload?.modalidade === "") {
                    await MySwal.fire(
                        "Nenhuma modalidade foi selecionada",
                        "",
                        "error"
                    )
                    return false
                }
                if (!(await verifyCNPJ("#input-cnpj"))) {
                    return false
                }
                if (!(await verifyEmail(payload.login))) {
                    return false
                }
            } else if (idx === 3) {
                if (payload.bank_agency_digit === "") {
                    setPayload({
                        ...payload,
                        ["bank_agency_digit"]: 0,
                    })
                }

                if (payload?.bank_code === "") {
                    await MySwal.fire(
                        "Nenhum banco foi selecionado",
                        "",
                        "error"
                    )
                    return false
                }
                if (payload?.account_type === "") {
                    await MySwal.fire(
                        "Nenhum tipo de conta foi selecionado",
                        "",
                        "error"
                    )
                    return false
                }
            }
            if (hasEmptyNodeForm(`.verify input, input.verify`)) {
                await MySwal.fire(
                    "Campos obrigatórios não preenchidos",
                    "Verifique se há algum campo em vermelho",
                    "error"
                )
                return false
            }
            setCurrentStep(idx)
        }
    }

    function currentChangePrev(idx) {
        return async (e) => {
            e.preventDefault()
            setCurrentStep(idx)
        }
    }

    const finish = async (e) => {
        e.preventDefault()
        disable_btn_and_put_innerHTML(".btn_save")
        let formData = new FormData()
        let avatar = false
        let newPayload = {}
        if (!!thumb?.length) {
            let file = await dataURItoBlob(thumb)
            formData.append("file", file)
            avatar = await fromServer("/files", false, "POST", formData)
        }
        !!avatar?.error?.message &&
            (await MySwal.fire(avatar?.error?.message, "", "error"))
        if (!!avatar.id) {
            newPayload = { ...newPayload, fk_avatar: avatar.id }
        }
        newPayload = {
            ...newPayload,
            password: payload.password,
            confirmPassword: payload.confirmPassword,
            email: payload.login,

            type: true,
            data: {
                cnpj: payload.cnpj,
                county_register: payload.county_register,
                fantasy_name: payload.fantasy_name,
                reason_social: payload.reason_social,
                state_register: payload.state_register,
                telephone_commercial: payload.telephone,
                telephone_whatsapp: payload.telephone_r,
                data_fundacao: payload.fundacao,
                qty_funcionarios: payload.qty_f,
                site: payload.site,
                modalidade: payload.modalidade,
                addresses: {
                    name: payload.nameAddress,
                    postcode: payload.postcode,
                    state: payload.state_address,
                    city: payload.city,
                    neighborhood: payload.neighborhood,
                    street: payload.street,
                    number: payload.number,
                },
                BankData: {
                    Bank: {
                        Code: payload.bank_code,
                    },
                    AccountType: {
                        Code: payload.account_type,
                    },
                    BankAgency: payload.bank_agency,
                    BankAgencyDigit: payload.bank_agency_digit,
                    BankAccount: payload.bank_account,
                    BankAccountDigit: payload.bank_account_digit,
                },
                responsible: {
                    name: payload.name_completo,
                    cpf: payload.cpf,
                    email: payload.email,
                    telephone: payload.telefone_pessoal,
                    cargo: payload.cargo,
                },
                ramos: ramosPayload,
            },
        }
        try {
            const data = await fromServer("/users", "", "POST", newPayload)
            if (!hasKey(data, "error")) {
                enable_btn_and_put_innerHTML(".btn_save", "Finalizar")
                await MySwal.fire(
                    "Loja cadastrada",
                    "Verifique seu email para ativar a conta",
                    "success"
                )
                setCurrentStep(4)
            } else {
                enable_btn_and_put_innerHTML(".btn_save", "Finalizar")
                await MySwal.fire(
                    "Não foi possivel cadastrar esta loja",
                    data.error?.message,
                    "error"
                )
            }
        } catch (e) {
            enable_btn_and_put_innerHTML(".btn_save", "Finalizar")
            await MySwal.fire(
                "Não foi possivel cadastrar esta loja",
                e?.message,
                "error"
            )
        }
        enable_btn_and_put_innerHTML(".btn_save", "Finalizar")
        return false
    }

    return (
        <>
            <Head>
                <title>Portal da Maria | Cadastro</title>
                <link rel="icon" href={"/favicon.ico"} />
                <meta name="format-detection" content="telephone=no" />
                <meta
                    httpEquiv="Content-Type"
                    content="text/html; charset=UTF-8"
                />
                <meta
                    name="viewport"
                    content="width=device-width, initial-scale=1, shrink-to-fit=no"
                />
                <meta httpEquiv="X-UA-Compatible" content="IE=edge" />
                <link
                    href="https://fonts.googleapis.com/css?family=Montserrat%7CRoboto:100,300,400,500,700,900%7CRoboto+Condensed:100,300,400,500,700"
                    rel="stylesheet"
                />
            </Head>
            <Main>
                <Row className={`rowMainNewSign  m-0 mb-4`}>
                    <Col xs="12" className="mb-4 showTabletAndLess">
                        <Stepper
                            steps={steps}
                            activeStep={currentStep}
                            activeColor={"#e3171b"}
                            completeColor={"#e3171b"}
                            activeTitleColor={"#e3171b"}
                            completeTitleColor={"#e3171b"}
                            completeBarColor={"#e3171b"}
                            size={30}
                            circleFontSize={15}
                            titleFontSize={12}
                            defaultTitleOpacity={"5"}
                            activeTitleOpacity={"1"}
                        />
                    </Col>
                    <Col xs="12" className="mb-4 showAboveTablet">
                        <Stepper
                            steps={steps}
                            activeStep={currentStep}
                            activeColor={"#e3171b"}
                            completeColor={"#e3171b"}
                            activeTitleColor={"#e3171b"}
                            completeTitleColor={"#e3171b"}
                            completeBarColor={"#e3171b"}
                            size={60}
                            circleFontSize={20}
                            titleFontSize={17}
                            defaultTitleOpacity={"5"}
                            activeTitleOpacity={"1"}
                        />
                    </Col>
                </Row>

                {Number(currentStep) === 0 && (
                    <Row className={`mb-4 mx-auto rowStepsNewSign`}>
                        <>
                            <Col xs="12">
                                <PhotoUploader
                                    label={"Logo da sua empresa"}
                                    listener={(imageList) => {
                                        if (!!imageList[0]?.dataURL) {
                                            setThumb(imageList[0].dataURL)
                                        } else {
                                            setThumb("")
                                        }
                                    }}
                                    defaultValue={
                                        !!thumb.length
                                            ? [{ dataURL: thumb }]
                                            : []
                                    }
                                    maxNumber={1}
                                    maxFileSize={5 * 1024 * 1024}
                                    acceptType={["jpg", "png", "jpeg"]}
                                />
                            </Col>
                            <Col xs="12">
                                <InputDefault
                                    has_label
                                    className="verify"
                                    label={"CNPJ*"}
                                    name={"cnpj"}
                                    _id={"cnpj"}
                                    type={"text"}
                                    initial_value={payload.cnpj}
                                    _required
                                    onChange={defaultListener}
                                    options={{
                                        delimiters: [".", ".", "/", "-"],
                                        blocks: [2, 3, 3, 4, 2],
                                    }}
                                    onKeyUp={cnpj}
                                    onBlur={cnpj_blur}
                                />
                            </Col>
                            <Col xs="12">
                                <InputDefault
                                    has_label
                                    className="verify"
                                    label={"E-mail Comercial *"}
                                    name={"login"}
                                    _id={"login"}
                                    type={"email"}
                                    initial_value={payload.login}
                                    _required
                                    onBlur={email_blur}
                                    onChange={defaultListener}
                                    small={
                                        "O email deve ser único entre fornecedores e clientes!"
                                    }
                                />
                            </Col>
                            <Col xs="12">
                                <InputDefault
                                    has_label
                                    className="verify"
                                    label={"Razão Social*"}
                                    name={"reason_social"}
                                    _id={"reason_social"}
                                    type={"text"}
                                    initial_value={payload.reason_social}
                                    _required
                                    onBlur={obrigatory_blur}
                                    onChange={defaultListener}
                                />
                            </Col>
                            <Col xs="12">
                                <InputDefault
                                    has_label
                                    className="verify"
                                    label={"Nome Fantasia*"}
                                    name={"fantasy_name"}
                                    _id={"fantasy_name"}
                                    type={"text"}
                                    initial_value={payload.fantasy_name}
                                    _required
                                    onBlur={obrigatory_blur}
                                    onChange={defaultListener}
                                />
                            </Col>
                            <Col xs="12">
                                <InputDefault
                                    has_label
                                    className="verify"
                                    label={"Telefone Comercial*"}
                                    name={"telephone"}
                                    _id={"telephone"}
                                    type={"tel"}
                                    initial_value={payload.telephone}
                                    _required
                                    onKeyUp={onKeyUpTelephone}
                                    onChange={defaultListener}
                                />
                            </Col>
                            <Col xs="12">
                                <InputDefault
                                    has_label
                                    label={"Telefone Whatsapp"}
                                    name={"telephone_r"}
                                    _id={"telephone_r"}
                                    type={"tel"}
                                    initial_value={payload.telephone_r}
                                    onKeyUp={onKeyUpTelephone}
                                    onBlur={(e) => size_blur(e, 12, 13, 2)}
                                    options={{
                                        delimiters: [" ", "-"],
                                        blocks: [2, 5, 4],
                                    }}
                                    _required
                                    onChange={defaultListener}
                                />
                            </Col>
                            <Col xs="12">
                                <InputDefault
                                    has_label
                                    label={"Inscrição Municipal"}
                                    name={"county_register"}
                                    _id={"county_register"}
                                    type={"text"}
                                    initial_value={payload.county_register}
                                    _required
                                    onChange={defaultListener}
                                />
                            </Col>
                            <Col xs="12">
                                <InputDefault
                                    has_label
                                    label={"Inscrição Estadual"}
                                    name={"state_register"}
                                    _id={"state_register"}
                                    type={"text"}
                                    initial_value={payload.state_register}
                                    _required
                                    onChange={defaultListener}
                                />
                            </Col>
                            <Col xs="12">
                                <InputDefault
                                    has_label
                                    label={"Data de fundação"}
                                    name={"fundacao"}
                                    _id={"fundacao"}
                                    type={"text"}
                                    // placeholder={"XX/XX/XXXX"}
                                    initial_value={payload.fundacao}
                                    onKeyUp={birthday}
                                    onChange={birthday}
                                    onBlur={async (e) => {
                                        const flag = await dateChecker(e)
                                        if (flag) {
                                            setPayload({
                                                ...payload,
                                                fundacao: elm("[name=fundacao]")
                                                    .value,
                                            })
                                        } else {
                                            setPayload({
                                                ...payload,
                                                fundacao: "",
                                            })
                                        }
                                        return flag
                                    }}
                                    options={{
                                        delimiters: ["/", "/"],
                                        blocks: [2, 2, 4],
                                    }}
                                    _required
                                />
                            </Col>
                            <Col xs="12">
                                <InputDefault
                                    has_label
                                    label={"Quantidade de funcionários"}
                                    name={"qty_f"}
                                    _id={"qty_f"}
                                    type={"number"}
                                    initial_value={payload.qty_f}
                                    _required
                                    onChange={defaultListener}
                                />
                            </Col>
                            <Col xs="12" />
                            <Col xs="12">
                                <FormGroup>
                                    <Label for="ramo">Canal de vendas*</Label>
                                    <SelectCustom
                                        id={"ramo"}
                                        name={"ramo"}
                                        values={ramos}
                                        selectedValues={ramosPayload.map(
                                            (ramo, idx) => ({
                                                id: idx,
                                                name: ramo,
                                            })
                                        )}
                                        onSelect={onSelectRamos}
                                        onRemove={onSelectRamos}
                                    />
                                </FormGroup>
                            </Col>
                            <Col xs="12">
                                <SelectDefault
                                    name={"modalidade"}
                                    id={"modalidade"}
                                    has_label
                                    label={"Modalidade de comércio*"}
                                    initial_value={payload.modalidade}
                                    values={modalidades.map((m) => {
                                        if (
                                            String(
                                                payload.modalidade
                                            ).toUpperCase() ===
                                            String(m.name).toUpperCase()
                                        ) {
                                            return {
                                                ...m,
                                                selected: true,
                                            }
                                        } else {
                                            return m
                                        }
                                    })}
                                    listener={(e) => {
                                        setPayload({
                                            ...payload,
                                            ["modalidade"]:
                                                e.target.selectedOptions[0]
                                                    .value,
                                        })
                                    }}
                                    _required
                                />
                            </Col>
                            <Col xs="6" className="mb-4">
                                <Button
                                    className="buttonSave"
                                    style={{ width: "100%" }}
                                    onClick={currentChange(1, true)}
                                >
                                    Próximo
                                </Button>
                            </Col>
                            <Col xs="12">
                                <LinkTo _href="/login">
                                    <div
                                        className={
                                            "border bg-white text_light_gray text-center p-5 fix"
                                        }
                                    >
                                        <p className={`m-0`}>
                                            Já tem cadastro? Clique aqui para
                                            fazer login
                                        </p>
                                    </div>
                                </LinkTo>
                            </Col>
                        </>
                    </Row>
                )}
                {Number(currentStep) === 1 && (
                    <Row className={`mb-4 mx-auto rowStepsNewSign`}>
                        <>
                            <Col xs="12">
                                <InputDefault
                                    has_label
                                    className="verify"
                                    label={"De onde é este endereço?*"}
                                    name={"nameAddress"}
                                    _id={"nameAddress"}
                                    type={"text"}
                                    initial_value={payload.nameAddress}
                                    _required
                                    onChange={defaultListener}
                                    onBlur={obrigatory_blur}
                                />
                            </Col>
                            <Col xs="12">
                                <InputDefault
                                    has_label
                                    className="verify"
                                    label={"CEP*"}
                                    name={"postcode"}
                                    _id={"postcode"}
                                    type={"text"}
                                    initial_value={payload.postcode}
                                    _required
                                    onChange={defaultListener}
                                    onBlur={async (e) => {
                                        const result = await cepBlur(e, {})
                                        if (result === false) {
                                            /*console.log("result false", {
                                                result,
                                            })*/
                                            setPayload({
                                                ...payload,
                                                street: "",
                                                city: "",
                                                state_address: "",
                                                neighborhood: "",
                                                postcode: "",
                                            })
                                            elm("[name=postcode]").value = ""
                                            elm(
                                                "[name=postcode]"
                                            ).classList.add("invalid")
                                            elm(
                                                "[name=postcode]"
                                            ).classList.remove("valid")
                                            elm("[name=postcode]").placeholder =
                                                "CEP INVALIDO !"
                                        } else {
                                            setPayload({
                                                ...payload,
                                                street: result?.street || "",
                                                city: result?.city || "",
                                                state_address:
                                                    result?.state || "",
                                                neighborhood:
                                                    result?.neighborhood || "",
                                            })
                                            if (
                                                !!elm("[name=state_address]") &&
                                                result?.state
                                            ) {
                                                elm(
                                                    "[name=state_address]"
                                                ).value = result?.state
                                            }
                                        }
                                    }}
                                    onKeyUp={postcode}
                                    options={{
                                        delimiters: ["-"],
                                        blocks: [5, 3],
                                    }}
                                />
                            </Col>
                            <Col xs="12">
                                <InputDefault
                                    has_label
                                    className="verify"
                                    label={"Endereço*"}
                                    name={"street"}
                                    _id={"street"}
                                    type={"text"}
                                    uncontrolled={true}
                                    _value={payload.street}
                                    _required
                                    _handle={defaultListener}
                                    onBlur={obrigatory_blur}
                                />
                            </Col>
                            <Col xs="12">
                                <InputDefault
                                    has_label
                                    className="verify"
                                    label={"Bairro*"}
                                    name={"neighborhood"}
                                    _id={"neighborhood"}
                                    type={"text"}
                                    uncontrolled={true}
                                    _value={payload.neighborhood}
                                    _required
                                    _handle={defaultListener}
                                    onBlur={obrigatory_blur}
                                />
                            </Col>
                            <Col xs="12">
                                <InputDefault
                                    has_label
                                    className="verify"
                                    label={"Número*"}
                                    name={"number"}
                                    _id={"number"}
                                    type={"number"}
                                    uncontrolled={true}
                                    _value={payload.number}
                                    _required
                                    _handle={defaultListener}
                                    onBlur={obrigatory_blur}
                                />
                            </Col>
                            <Col xs="12">
                                <InputDefault
                                    has_label
                                    className="verify"
                                    label={"Cidade*"}
                                    name={"city"}
                                    _id={"city"}
                                    type={"text"}
                                    uncontrolled={true}
                                    _value={payload.city}
                                    _required
                                    _handle={defaultListener}
                                    onBlur={obrigatory_blur}
                                />
                            </Col>
                            <Col xs="12">
                                <SelectDefault
                                    has_label
                                    label={"Estado*"}
                                    className="verify"
                                    id={"state_address"}
                                    name={"state_address"}
                                    placeholder={"Selecione o seu estado"}
                                    initial_value={payload.state_address}
                                    _required
                                    values={state_address.map((e) =>
                                        String(e.id).toUpperCase() ===
                                        String(
                                            payload.state_address
                                        ).toUpperCase()
                                            ? {
                                                  ...e,
                                                  selected: true,
                                              }
                                            : e
                                    )}
                                    listener={(e) => {
                                        setPayload({
                                            ...payload,
                                            state_address:
                                                e.target.selectedOptions[0]
                                                    .value,
                                        })
                                    }}
                                />
                            </Col>
                            <Col xs="12">
                                <InputDefault
                                    has_label
                                    label={"Complemento"}
                                    name={"complement"}
                                    _id={"complement"}
                                    type={"text"}
                                    initial_value={payload.complement}
                                    onChange={defaultListener}
                                />
                            </Col>
                            <Col xs="12">
                                <InputDefault
                                    has_label
                                    label={"Referências"}
                                    name={"references"}
                                    _id={"references"}
                                    type={"text"}
                                    initial_value={payload.references}
                                    onChange={defaultListener}
                                />
                            </Col>
                            <Col xs="12">
                                <Row
                                    className="m-0 full-width"
                                    style={{
                                        display: "flex",
                                        justifyContent: "space-between",
                                    }}
                                >
                                    <Col
                                        xs="6"
                                        style={{
                                            padding: 0,
                                            paddingRight: "1rem",
                                        }}
                                    >
                                        <Button
                                            className="buttonSave"
                                            style={{ width: "50%" }}
                                            onClick={currentChangePrev(0)}
                                        >
                                            Anterior
                                        </Button>
                                    </Col>
                                    <Col
                                        xs="6"
                                        style={{
                                            padding: 0,
                                            paddingLeft: "1rem",
                                        }}
                                    >
                                        <Button
                                            className="buttonSave"
                                            style={{ width: "50%" }}
                                            onClick={currentChange(2, true)}
                                        >
                                            Próximo
                                        </Button>
                                    </Col>
                                </Row>
                            </Col>
                        </>
                    </Row>
                )}
                {Number(currentStep) === 2 && (
                    <Row className={`mb-4 mx-auto rowStepsNewSign`}>
                        <>
                            <Col xs="12">
                                <h1>3 - Dados Bancários</h1>
                            </Col>
                            <Col xs="12">
                                <SelectDefault
                                    _required
                                    className="verify"
                                    name={"bank_code"}
                                    id={"bank_code"}
                                    has_label
                                    label={"Código do banco*"}
                                    initial_value={payload.bank_code}
                                    values={code_banks.map((code) => {
                                        if (payload.bank_code === code.value) {
                                            return {
                                                id: code.value,
                                                name:
                                                    Number(code.value) !== 0
                                                        ? `${code.value} - ${
                                                              code.label
                                                          }`
                                                        : code.label,
                                                selected: true,
                                            }
                                        } else {
                                            return {
                                                id: code.value,
                                                name:
                                                    Number(code.value) !== 0
                                                        ? `${code.value} - ${
                                                              code.label
                                                          }`
                                                        : code.label,
                                            }
                                        }
                                    })}
                                    listener={(r) => {
                                        setPayload({
                                            ...payload,
                                            ["bank_code"]:
                                                r.target.selectedOptions[0]
                                                    .value,
                                        })
                                    }}
                                />
                            </Col>
                            <Col xs="12">
                                <SelectDefault
                                    _required
                                    className="verify"
                                    name={"account_type"}
                                    id={"account_type"}
                                    has_label
                                    label={"Tipo da conta*"}
                                    initial_value={payload.account_type}
                                    values={bank_account_types.map((type) => {
                                        if (payload.account_type === type.id) {
                                            return {
                                                id: type.id,
                                                name: type.name,
                                                selected: true,
                                            }
                                        } else {
                                            return {
                                                id: type.id,
                                                name: type.name,
                                            }
                                        }
                                    })}
                                    listener={(r) => {
                                        setPayload({
                                            ...payload,
                                            ["account_type"]:
                                                r.target.selectedOptions[0]
                                                    .value,
                                        })
                                    }}
                                />
                            </Col>
                            <Col xs="12">
                                <InputDefault
                                    className="verify"
                                    _id={"bank_agency"}
                                    name={"bank_agency"}
                                    type={"text"}
                                    has_label
                                    onKeyUp={onlyNumber}
                                    label={"Agência*"}
                                    onBlur={(e) => {
                                        onlyNumber(e)
                                        obrigatory_blur(e)
                                    }}
                                    onChange={defaultListener}
                                    initial_value={payload.bank_agency}
                                    _minLength={`${"0001".length}`}
                                    _maxLength={`${"0001".length}`}
                                    _required
                                />
                            </Col>
                            <Col xs="12">
                                <InputDefault
                                    _id={"bank_agency_digit"}
                                    name={"bank_agency_digit"}
                                    type={"text"}
                                    has_label
                                    label={"Dígito da agência"}
                                    small="Caso não possua, insira 0"
                                    initial_value={payload.bank_agency_digit}
                                    onChange={defaultListener}
                                    _minLength={`${"0001".length}`}
                                    _maxLength={`${"0001".length}`}
                                />
                            </Col>
                            <Col xs="12">
                                <InputDefault
                                    className="verify"
                                    _id={"bank_account"}
                                    name={"bank_account"}
                                    type={"text"}
                                    has_label
                                    label={"Conta*"}
                                    onKeyUp={onlyNumber}
                                    initial_value={payload.bank_account}
                                    onChange={defaultListener}
                                    onBlur={(e) => {
                                        onlyNumber(e)
                                        obrigatory_blur(e)
                                    }}
                                    _minLength={`${"5628628".length}`}
                                    _maxLength={`${"5628628".length}`}
                                    _required
                                />
                            </Col>
                            <Col xs="12">
                                <InputDefault
                                    className="verify"
                                    _id={"bank_account_digit"}
                                    name={"bank_account_digit"}
                                    type={"text"}
                                    has_label
                                    label={"Dígito da conta*"}
                                    initial_value={payload.bank_account_digit}
                                    onChange={defaultListener}
                                    onBlur={obrigatory_blur}
                                    _minLength={`${"9".length}`}
                                    _maxLength={`${"9".length}`}
                                    _required
                                />
                            </Col>
                            <Col xs="12">
                                <Row
                                    className="m-0 full-width"
                                    style={{
                                        display: "flex",
                                        justifyContent: "space-between",
                                    }}
                                >
                                    <Col
                                        xs="6"
                                        style={{
                                            padding: 0,
                                            paddingRight: "1rem",
                                        }}
                                    >
                                        <Button
                                            className="buttonSave"
                                            style={{ width: "50%" }}
                                            onClick={currentChangePrev(1)}
                                        >
                                            Anterior
                                        </Button>
                                    </Col>
                                    <Col
                                        xs="6"
                                        style={{
                                            padding: 0,
                                            paddingLeft: "1rem",
                                        }}
                                    >
                                        <Button
                                            className="buttonSave"
                                            style={{ width: "50%" }}
                                            onClick={currentChange(3, true)}
                                        >
                                            Próximo
                                        </Button>
                                    </Col>
                                </Row>
                            </Col>
                        </>
                    </Row>
                )}
                {Number(currentStep) === 3 && (
                    <Row className={`mb-4 mx-auto rowStepsNewSign`}>
                        <Modal
                            isOpen={modal}
                            toggle={toggle}
                            scrollable
                            fade={false}
                            className={`modal-lg modal-dialog modal-dialog-centered ${
                                styles.modalAddress
                            }`}
                        >
                            <ModalHeader
                                className={styles.modalHeader}
                                toggle={toggle}
                            >
                                <h1>Termos de uso</h1>
                            </ModalHeader>
                            <ModalBody>
                                <Container>
                                    <Row>
                                        <Col
                                            xs="12"
                                            style={{
                                                fontWeight: 300,
                                                fontSize: "18px",
                                                lineHeight: "35px",
                                                margin: "0 auto",
                                                float: "none",
                                                textAlign: "justify",
                                                marginTop: "3rem",
                                            }}
                                        >
                                            <p>
                                                Estes Termos e condições gerais
                                                aplicam-se ao uso dos serviços
                                                oferecidos pelo Fhibra Comercial
                                                Ltda., empresa devidamente
                                                inscrita no CNPJ/MF sob o n°
                                                10.729.483/0001-13, situada na
                                                Rua Doutor Virgílio do
                                                Nascimento, 445, CEP
                                                03.027-020,Brás, São Paulo,
                                                doravante nominada PORTAL DA
                                                MARIA, por meio da plataforma
                                                wwww.portaldamaria.com.
                                            </p>
                                            <p>
                                                Qualquer pessoa, doravante
                                                nominada Usuário, que pretenda
                                                utilizar os serviços do PORTAL
                                                DA MARIA deverá aceitar os
                                                Termos e condições gerais e
                                                todas as demais políticas e
                                                princípios que o regem.
                                            </p>
                                            <p>
                                                A aceitação destes Termos e
                                                condições gerais é absolutamente
                                                indispensável à utilização da
                                                plataforma PORTAL DA MARIA.{" "}
                                            </p>
                                            <p>
                                                O Usuário deverá ler,
                                                certificar-se de haver entendido
                                                e aceitar todas as condições
                                                estabelecidas nos Termos e
                                                condições gerais e nas Políticas
                                                de privacidade, assim como nos
                                                demais documentos a eles
                                                incorporados por referência,
                                                antes de seu cadastro como
                                                Usuário do PORTAL DA MARIA.{" "}
                                            </p>
                                            <p>
                                                O PORTAL DA MARIA não é
                                                fornecedor de quaisquer produtos
                                                ou serviços anunciados na
                                                plataforma. O PORTAL DA
                                                MARIApresta um serviço
                                                consistente na oferta de uma
                                                plataforma na internet que
                                                fornece espaços para que
                                                Usuários anunciantes/potenciais
                                                vendedores anunciem, oferecendo
                                                à venda, os seus próprios
                                                produtos e serviços para que
                                                eventuais interessados na compra
                                                dos itens, os
                                                Usuários/potenciais compradores,
                                                possam adquirir produtos e
                                                serviçosda área têxtil.{" "}
                                            </p>
                                            <p>
                                                Os Usuários
                                                anunciantes/potenciais
                                                vendedores somente poderão
                                                anunciar produtos ou serviços
                                                que possam vender e que tenham
                                                em estoque, estabelecendo
                                                diretamente os termos do anúncio
                                                e todas as suas características,
                                                além de imagens dos produtos
                                                oferecidos;{" "}
                                            </p>
                                            <p>
                                                Para utilizar os serviços do
                                                PORTAL DA MARIAo Usuário deve
                                                aceitar, expressamente, a
                                                Política de Privacidade e
                                                Confidencialidade da Informação,
                                                que contém informações claras e
                                                completas sobre coleta, uso,
                                                armazenamento, tratamento e
                                                proteção dos dados pessoais dos
                                                Usuários e visitantes do PORTAL
                                                DA MARIA. O Vendedor ou
                                                Prestador de Serviço deverá
                                                efetuar um cadastro único,
                                                criando um LOGIN e SENHA que são
                                                pessoais e intransferíveis.
                                                PORTAL DA MARIA não se
                                                responsabiliza pelo uso
                                                inadequado e divulgação destes
                                                dados para terceiros. O PORTAL
                                                DA MARIA, nem quaisquer de seus
                                                empregados ou prepostos
                                                solicitará, por qualquer meio,
                                                físico ou eletrônico, que seja
                                                informada sua senha;{" "}
                                            </p>
                                            <p>
                                                {" "}
                                                O PORTAL DA MARIA pode recusar
                                                qualquer solicitação de
                                                cadastro, advertir, suspender,
                                                temporária ou definitivamente, a
                                                conta de um usuário; Não é
                                                permitido anunciar produtos
                                                expressamente proibidos pela
                                                legislação vigente ou pelos
                                                Termos e condições gerais de uso
                                                do site, que não possuam a
                                                devida autorização específica de
                                                órgãos reguladores competentes,
                                                ou que violem direitos de
                                                terceiros;{" "}
                                            </p>
                                            <p>
                                                O PORTAL DA MARIA não se
                                                responsabiliza pela veracidade
                                                das avaliações recebidas pelos
                                                usuários. Só assumindo o
                                                controle do envio e recebimento.{" "}
                                            </p>

                                            <p
                                                style={{
                                                    fontWeight: "bold",
                                                }}
                                            >
                                                01 - Objeto{" "}
                                            </p>
                                            <p>
                                                Os serviços objeto dos presentes
                                                Termos e condições gerais
                                                consistem em (i) ofertar e
                                                hospedar espaços nos Sites para
                                                que os Usuários anunciem à venda
                                                seus próprios produtos e/ou
                                                serviços e (ii) viabilizar o
                                                contato direto entre Usuários
                                                vendedores/prestadores de
                                                serviço e Usuários interessados
                                                em adquirir os produtos e
                                                serviços anunciados, por meio da
                                                plataforma. PORTAL DA MARIA,
                                                portanto, possibilita aos
                                                Usuários se contatarem e
                                                negociarem entre si diretamente,
                                                sem qualquer intervenção do
                                                PORTAL DA MARIA, na negociação
                                                ou na concretização dos
                                                negócios. Desta forma,
                                                ressalta-se que o PORTAL DA
                                                MARIA não fornece quaisquer
                                                produtos ou serviços anunciados
                                                pelos Usuários da plataforma e
                                                nem se responsabiliza pela
                                                qualidade, quantidade,
                                                apresentação ou qualquer similar
                                                relacionado aos produtos
                                                oferecidos pelos Usuários
                                                vendedores.{" "}
                                            </p>
                                            <p
                                                style={{
                                                    fontWeight: "bold",
                                                }}
                                            >
                                                02 - Capacidade para
                                                cadastrar-se{" "}
                                            </p>
                                            <p>
                                                Os serviços do PORTAL DA MARIA
                                                estão disponíveis para as
                                                pessoas físicas e jurídicas que
                                                tenham capacidade legal para
                                                contratá-los e prestá-los.{" "}
                                            </p>
                                            <p>
                                                É proibido o cadastro de
                                                Usuários que não tenham
                                                capacidade civil (com relação a
                                                pessoas físicas), bem como de
                                                Usuários que tenham sido
                                                suspensos do PORTAL DA MARIA,
                                                temporária ou definitivamente,
                                                sem prejuízo da aplicação das
                                                sanções legais previstas no
                                                Código Civil Brasileiro,
                                                notadamente, art. 166, I; 171, I
                                                e 180.{" "}
                                            </p>
                                            <p
                                                style={{
                                                    fontWeight: "bold",
                                                }}
                                            >
                                                03 - Cadastro{" "}
                                            </p>
                                            <p>
                                                Apenas será confirmado o
                                                cadastramento do Usuário que
                                                preencher todos os campos
                                                obrigatórios do cadastro, com
                                                informações exatas, precisas e
                                                verdadeiras. O Usuário declara e
                                                assume o compromisso de
                                                atualizar os dados inseridos em
                                                seu cadastro (“Dados Pessoais”)
                                                sempre que for necessário. Além
                                                disso, o Usuário
                                                fornecedor/prestador de serviço
                                                deverá ter cadastro no SINTEGRA,
                                                e estar ativo na Receita
                                                Federal.{" "}
                                            </p>
                                            <p>
                                                Ao se cadastrar no PORTAL DA
                                                MARIA, o Usuário poderá utilizar
                                                todos os serviços
                                                disponibilizados na plataforma
                                                conforme descrito no site do
                                                PORTAL DA MARIA, declarando,
                                                para tanto, ter lido,
                                                compreendido e aceitado os
                                                respectivos Termos e Condições
                                                de uso de cada um destes
                                                serviços que passam a fazer
                                                parte integrante destes Termos e
                                                condições gerais quando
                                                concluído o cadastro.{" "}
                                            </p>
                                            <p>
                                                O Usuário acessará sua conta
                                                através de e-mail (ou login) e
                                                senha e compromete-se a não
                                                informar a terceiros esses
                                                dados, responsabilizando-se
                                                integralmente pelo uso que deles
                                                seja feito.{" "}
                                            </p>
                                            <p>
                                                O PORTAL DA MARIA não se
                                                responsabiliza pela correção dos
                                                Dados Pessoais inseridos por
                                                seus Usuários. Os Usuários
                                                garantem e respondem, em
                                                qualquer caso, civil e
                                                criminalmente pela veracidade,
                                                exatidão e autenticidade, dos
                                                Dados Pessoais cadastrados.{" "}
                                            </p>
                                            <p>
                                                O PORTAL DA MARIA se reserva o
                                                direito de recusar qualquer
                                                solicitação de cadastro e de
                                                suspender um cadastro
                                                previamente aceito, que esteja
                                                em desacordo com as políticas e
                                                regras dos presentes Termos e
                                                condições gerais.{" "}
                                            </p>
                                            <p>
                                                O anunciante reconhece que é o
                                                único responsável pela
                                                veracidade e pelo conteúdo
                                                disponibilizado no seu anúncio,
                                                isentando o PORTAL DA MARIA de
                                                qualquer responsabilidade nesse
                                                sentido, bem como declara, nos
                                                termos da lei.{" "}
                                            </p>
                                            <p>
                                                O PORTAL DA MARIA poderá, a seu
                                                exclusivo critério, realizar as
                                                buscas que julgar necessárias
                                                para apurar dados incorretos ou
                                                inverídicos bem como solicitar
                                                dados adicionais e documentos
                                                que estime serem pertinentes a
                                                fim de conferir os Dados
                                                Pessoais informados.{" "}
                                            </p>
                                            <p>
                                                Caso o PORTAL DA MARIA decida
                                                checar a veracidade dos dados
                                                cadastrais de um Usuário e se
                                                constate haver entre eles dados
                                                incorretos ou inverídicos, ou
                                                ainda caso o Usuário se furte ou
                                                se negue a enviar os documentos
                                                requeridos, o PORTAL DA MARIA
                                                poderá suspender temporariamente
                                                ou definitivamente a conta, sem
                                                prejuízo de outras medidas que
                                                entender necessárias e
                                                oportunas.{" "}
                                            </p>
                                            <p>
                                                Havendo a aplicação de quaisquer
                                                das sanções acima referidas,
                                                automaticamente serão cancelados
                                                os anúncios do respectivo
                                                Usuário, não lhe assistindo, por
                                                essa razão, qualquer indenização
                                                ou ressarcimento.{" "}
                                            </p>
                                            <p>
                                                O Usuário compromete-se a
                                                notificar o PORTAL DA MARIA
                                                imediatamente, e por meio
                                                seguro, a respeito de qualquer
                                                uso não autorizado de sua conta,
                                                bem como seu acesso não
                                                autorizado por terceiros. O
                                                Usuário será o único responsável
                                                pelas operações efetuadas em sua
                                                conta, uma vez que o acesso só
                                                será possível mediante a
                                                inclusão da senha, que deverá
                                                ser de conhecimento e
                                                propriedade exclusiva do
                                                Usuário.{" "}
                                            </p>
                                            <p>
                                                Em nenhuma hipótese será
                                                permitida a cessão, venda,
                                                aluguel ou outra forma de
                                                transferência da conta. Também
                                                não se permitirá a manutenção de
                                                mais de um cadastro por uma
                                                mesma pessoa, ou ainda a criação
                                                de novos cadastros por pessoas
                                                cujos cadastros originais tenham
                                                sido suspensos temporária ou
                                                definitivamente por infrações às
                                                políticas do PORTAL DA MARIA.{" "}
                                            </p>
                                            <p
                                                style={{
                                                    fontWeight: "bold",
                                                }}
                                            >
                                                04 - Modificações dos Termos e
                                                condições gerais{" "}
                                            </p>
                                            <p>
                                                O PORTAL DA MARIA poderá
                                                alterar, a qualquer tempo, estes
                                                Termos e condições gerais,
                                                visando seu aprimoramento e
                                                melhoria dos serviços prestados.
                                            </p>
                                            <p>
                                                Os novos Termos e condições
                                                gerais entrarão em vigor 10
                                                (dez) dias após sua publicação
                                                nos Sites. No prazo de 5 (cinco)
                                                dias contados a partir da
                                                publicação da nova versão, o
                                                Usuário deverá comunicar-se por
                                                e-mail caso não concorde com os
                                                termos alterados. Nesse caso, o
                                                vínculo contratual deixará de
                                                existir, desde que não haja
                                                contas ou dívidas em aberto. Não
                                                havendo manifestação no prazo
                                                estipulado, entender-se-á que o
                                                Usuário aceitou os novos Termos
                                                e condições gerais de uso e o
                                                contrato continuará vinculando
                                                as partes.
                                            </p>
                                            <p>
                                                As alterações não vigorarão em
                                                relação às compras já realizadas
                                                antes de sua publicação,
                                                permanecendo, nestes casos,
                                                vigente a redação anterior.{" "}
                                            </p>
                                            <p
                                                style={{
                                                    fontWeight: "bold",
                                                }}
                                            >
                                                05 - Produtos anunciados{" "}
                                            </p>
                                            <p
                                                style={{
                                                    fontWeight: "bold",
                                                }}
                                            >
                                                5.1 Anúncios/ofertas{" "}
                                            </p>
                                            <p>
                                                O Usuário poderá anunciar a
                                                venda de produtos ou serviços em
                                                suas respectivas categorias e
                                                subcategorias. Os anúncios devem
                                                conter textos, descrições,
                                                fotos, vídeos, tempo de entrega
                                                e outras informações relevantes
                                                do produto ou serviço oferecido,
                                                sempre que tal prática não
                                                violar nenhum dispositivo
                                                previsto em lei, neste contrato,
                                                nas demais políticas do PORTAL
                                                DA MARIA.{" "}
                                            </p>
                                            <p>
                                                O produto ou serviço oferecido
                                                pelo Usuário vendedor deve ser
                                                descrito com clareza, contendo
                                                todas as características
                                                relevantes. Presumir-se-á que,
                                                mediante a inclusão do anúncio
                                                no PORTAL DA MARIA, o Usuário
                                                manifesta a intenção e declara
                                                possuir o direito de vender o
                                                produto ou oferecer o serviço,
                                                além de dispor do produto para
                                                entrega imediata. Os anúncios
                                                devem conter de forma destacada,
                                                caso necessário, a descrição de
                                                todos os tributos incidentes
                                                sobre a transação em estrita
                                                observância à legislação
                                                tributária aplicável.{" "}
                                            </p>
                                            <p>
                                                O usuário deve inserir a
                                                quantidade de produtos que
                                                deseja vender e o preço por
                                                unidade. Se possuir várias
                                                unidades de um mesmo produto que
                                                não quer ou não possa vender
                                                separadamente, ou seja, um
                                                conjunto, o usuário vendedor
                                                deve inserir o preço do
                                                conjunto, estando ciente de que
                                                a tarifa de venda será cobrada
                                                pelo valor do conjunto
                                                anunciado, ainda que o usuário
                                                decida vender, posteriormente à
                                                publicação do anúncio, uma ou
                                                mais unidades, de forma isolada.{" "}
                                            </p>
                                            <p>
                                                Não é permitido fazer
                                                publicidade de outros meios de
                                                pagamentos que não sejam
                                                expressamente disponibilizados
                                                nos Sites. Caso o Usuário
                                                anunciante infrinja o disposto
                                                nesta cláusula, o PORTAL DA
                                                MARIA poderá editar o anúncio ou
                                                solicitar ao Usuário que o faça,
                                                ou remover o respectivo anúncio,
                                                não sendo, neste caso, estornada
                                                qualquer quantia paga ou devida
                                                relativa ao anúncio suprimido.{" "}
                                            </p>
                                            <p
                                                style={{
                                                    fontWeight: "bold",
                                                }}
                                            >
                                                5.1.2 Processamento de pagamento{" "}
                                            </p>
                                            <p>
                                                Todos os pagamentos efetuados
                                                serão de incumbência de um
                                                Gateway de Pagamento
                                                terceirizado, sendo todas as
                                                informações de natureza
                                                financeira administradas e
                                                armazenadas por essas empresas,
                                                sem interferência de qualquer
                                                natureza por parte do PORTAL DA
                                                MARIA. Ao terceirizar esse
                                                serviço para empresas
                                                reconhecidas e especializadas na
                                                área de operações financeiras
                                                pela internet, pretendemos
                                                oferecer e propiciar, além de
                                                opções personalizadas de
                                                pagamento, maior segurança às
                                                transações online.{" "}
                                            </p>
                                            <p>
                                                Os prazos para liberação do
                                                serviço serão contados à partir
                                                da liberação de crédito do
                                                Gateway de Pagamento, sendo ela
                                                ainda responsável por todo o
                                                armazenamento de dados pessoais
                                                e bancários do usuário.{" "}
                                            </p>
                                            <p>
                                                Está absolutamente vedada a
                                                possibilidade de transmissão de
                                                dados entre Usuários.{" "}
                                            </p>
                                            <p>
                                                A informação referente à
                                                localização do usuário, no
                                                momento do pedido, será de uso
                                                restrito do PORTAL DA MARIA.{" "}
                                            </p>
                                            <p
                                                style={{
                                                    fontWeight: "bold",
                                                }}
                                            >
                                                5.2 Inclusão de imagens,
                                                fotografias e vídeos{" "}
                                            </p>
                                            <p>
                                                O usuário pode incluir imagens,
                                                fotografias e vídeos do produto
                                                oferecido sempre que estas
                                                correspondam exatamente ao
                                                produto anunciado, com exceção
                                                aos bens intangíveis.{" "}
                                            </p>
                                            <p>
                                                Poderá ainda, incluir fotografia
                                                de serviços similar
                                                anteriormente já prestado, que
                                                corresponda ao anúncio do
                                                serviço oferecido na plataforma.{" "}
                                            </p>
                                            <p>
                                                O PORTAL DA MARIA poderá retirar
                                                dos Sites anúncios que contenham
                                                imagens, fotografias ou vídeos
                                                que não estejam de acordo com o
                                                Termos e condições gerais de uso
                                                e demais políticas de utilização
                                                da Plataforma, atentem contra o
                                                pudor assim como de acordo com
                                                sua conveniência.{" "}
                                            </p>
                                            <p>
                                                Caso haja a inclusão ou vídeo de
                                                objetos, pessoas etc. que sejam
                                                vedadas pelos termos de uso, e
                                                ainda sejam contra o pudor, bons
                                                costumes, desabonem ou sejam de
                                                conteúdo abusivo, a conta do
                                                usuário poderá ser
                                                desativada/excluída.{" "}
                                            </p>
                                            <p
                                                style={{
                                                    fontWeight: "bold",
                                                }}
                                            />
                                            <p>5.3 Produtos Proibidos </p>
                                            <p>
                                                É vedado aos usuários anunciar à
                                                venda ou comprar produtos que
                                                sejam proibidos ou violem a
                                                legislação vigente, bem como
                                                sejam considerados produtos
                                                proibidos pelo site.{" "}
                                            </p>
                                            <p>
                                                Serviços e produtos ainda de
                                                ordem ilegal, que violem
                                                direitos autorais e/ou marcas
                                                registradas, que incitem a
                                                violência (intolerância racial,
                                                religiosa, gênero), serviços e
                                                produtos pornográficos,
                                                pirataria, que atendem contra a
                                                ordem moral e o pudor serão
                                                imediatamente excluídos, podendo
                                                ainda o PORTAL DA MARIA excluir
                                                a conta do Usuário/vendedor, sem
                                                prejuízo de outras sanções ou
                                                ações por parte do PORTAL DA
                                                MARIA, como por exemplo demandas
                                                judiciais em casos de danos
                                                sofridos pelo PORTAL DA MARIA.{" "}
                                            </p>
                                            <p
                                                style={{
                                                    fontWeight: "bold",
                                                }}
                                            >
                                                06 - Privacidade da Informação{" "}
                                            </p>
                                            <p>
                                                Toda informação ou Dado Pessoal
                                                do Usuário é armazenado em
                                                servidores de grandes
                                                provedores. Salvo com relação às
                                                informações que são publicadas
                                                nos Sites, o PORTAL DA MARIA
                                                adotará todas as medidas
                                                possíveis para manter a
                                                confidencialidade e a segurança
                                                das informações sigilosas, porém
                                                não se responsabilizará por
                                                eventuais prejuízos que sejam
                                                decorrentes da divulgação de
                                                tais informações por parte de
                                                terceiros que utilizem as redes
                                                públicas ou a internet,
                                                subvertendo os sistemas de
                                                segurança para acessar as
                                                informações de Usuários.{" "}
                                            </p>
                                            <p>
                                                O Usuário expressamente autoriza
                                                que suas informações e dados
                                                pessoais sejam compartilhados
                                                pelo PORTAL DA MARIA com as
                                                demais empresas integrantes do
                                                grupo econômico, parceiros
                                                comerciais, autoridades e
                                                pessoas físicas ou jurídicas que
                                                aleguem ter sido lesadas por
                                                Usuários.{" "}
                                            </p>
                                            <p>
                                                A Política de Privacidade e
                                                Confidencialidade da Informação
                                                que integra os presentes termos
                                                de uso e condições gerais,
                                                tratará da coleta e
                                                armazenamento dos dados
                                                pessoais.{" "}
                                            </p>
                                            <p
                                                style={{
                                                    fontWeight: "bold",
                                                }}
                                            >
                                                07 - Obrigações dos Usuários{" "}
                                            </p>
                                            <p
                                                style={{
                                                    fontWeight: "bold",
                                                }}
                                            >
                                                7.1 Obrigações do Usuário
                                                comprador.{" "}
                                            </p>
                                            <p>
                                                Os usuários interessados em
                                                comprar produtos ou contratar
                                                serviços anunciados por
                                                vendedores no PORTAL DA MARIA
                                                devem manifestar seu interesse
                                                no período que o anúncio estiver
                                                ativo.{" "}
                                            </p>
                                            <p>
                                                Ao manifestar o interesse em
                                                algum produto, o Usuário
                                                comprador obriga-se a atender às
                                                condições de vendas descritas no
                                                anúncio.{" "}
                                            </p>
                                            <p>
                                                Os Usuários compradores deverão
                                                atentar-se no ato da compra, às
                                                informações sobre as
                                                qualificações do usuário
                                                vendedor, preço, prazo de
                                                entrega, descrição do serviço ou
                                                produto, eventuais serviços ou
                                                produtos adicionais, e forma de
                                                pagamento que deverão ser apenas
                                                as previstas nos presentes
                                                termos.{" "}
                                            </p>
                                            <p>
                                                Pertence ao Usuário comprador a
                                                responsabilidade quanto a
                                                informar qualquer erro, não
                                                funcionar, ou não esteja de
                                                acordo com a descrição fornecida
                                                pelo Usuário vendedor, ou ainda
                                                as exigências encaminhadas ao
                                                Usuário vendedor no ato da
                                                contratação.{" "}
                                            </p>
                                            <p>
                                                As ofertas de produto ou serviço
                                                só terão validade se realizadas
                                                nos sites do PORTAL DA MARIA.{" "}
                                            </p>
                                            <p>
                                                Tributos: O PORTAL DA MARIA não
                                                se responsabiliza pelas
                                                obrigações tributárias que
                                                recaiam sobre as atividades dos
                                                Usuários da Plataforma. Assim
                                                como estabelece a legislação
                                                pertinente em vigor, o Usuário
                                                comprador deverá exigir nota
                                                fiscal do Usuário vendedor em
                                                suas negociações, salvo se o
                                                Usuário vendedor estiver
                                                realizando uma venda eventual e
                                                não se enquadre no conceito
                                                legal de comerciante/empresário
                                                quanto aos bens postos em
                                                negociação.{" "}
                                            </p>
                                            <p>
                                                O Usuário potencial comprador,
                                                antes de decidir pela compra,
                                                deverá atentar-se às informações
                                                sobre o preço, às formas de
                                                pagamento, à disponibilidade, à
                                                forma e ao prazo de entrega dos
                                                produtos e serviços.{" "}
                                            </p>
                                            <p
                                                style={{
                                                    fontWeight: "bold",
                                                }}
                                            >
                                                7.2. Obrigações do Usuário
                                                vendedor/prestador de serviço{" "}
                                            </p>
                                            <p>
                                                O Usuário vendedor deve ter
                                                capacidade legal para vender o
                                                produto ou prestar o serviço
                                                anunciado. O Usuário vendedor
                                                obriga-se a entrar em contato
                                                com o Usuário comprador para
                                                efetivar a transação sempre que
                                                tenha recebido uma manifestação
                                                de interesse na compra do item
                                                anunciado. Apenas nos seguintes
                                                casos excepcionais, o Usuário
                                                vendedor poderá não efetivar a
                                                venda se houve evidente erro de
                                                digitação ao cadastrar preço ou
                                                quantidade do produto anunciado.{" "}
                                            </p>
                                            <p>
                                                O Usuário vendedor ou prestador
                                                de serviço se obriga a realizar
                                                as modificações/trocas
                                                solicitadas pelo Usuário
                                                comprador no prazo descrito
                                                inicialmente no anúncio e na
                                                página do anúncio, caso o
                                                serviço fornecido não esteja nos
                                                parâmetros pré-estabelecidos ou
                                                ainda em conformidade com o
                                                anúncio.{" "}
                                            </p>
                                            <p>
                                                O Usuário vendedor/prestador de
                                                serviço tem a obrigação de
                                                cumprir o prazo descrito no ato
                                                da contratação do serviço, sendo
                                                que caso não entregue o serviço
                                                ou produto no período avençado,
                                                o Usuário comprador poderá
                                                cancelar o pedido, tendo
                                                resguardado o direito de
                                                ressarcimento do valor pago.{" "}
                                            </p>
                                            <p>
                                                O Usuário vendedor/prestador de
                                                serviço obriga-se a entrar em
                                                contato com o Usuário comprador
                                                para efetivar a transação sempre
                                                que tenha recebido uma
                                                manifestação de interesse na
                                                compra do item anunciado. Apenas
                                                nos seguintes casos
                                                excepcionais, o Usuário vendedor
                                                poderá não efetivar a venda se
                                                houve evidente erro de digitação
                                                ao cadastrar preço ou quantidade
                                                do produto anunciado.{" "}
                                            </p>
                                            <p>
                                                O Usuário vendedor/prestador de
                                                serviçoobriga-se a responder,
                                                manter contato com o Usuário
                                                comprador durante a negociação,
                                                no andamento do cumprimento do
                                                serviço contratado, e
                                                posteriormente, caso o serviço
                                                contratado não tenha atendido as
                                                especificações por parte do
                                                Usuário comprador, devendo o
                                                Usuário vendedor responder todos
                                                os questionamentos e dúvidas
                                                pertinentes.{" "}
                                            </p>
                                            <p>
                                                O Usuário vendedor/prestador de
                                                serviço deve, em cumprimento à
                                                legislação brasileira vigente,
                                                além de demonstrar informações
                                                claras e ostensivas a respeito
                                                de quaisquer restrições à
                                                aquisição do produto ou serviço,
                                                apontar sempre em seus anúncios
                                                as características essenciais do
                                                produto ou do serviço, incluídos
                                                os riscos à saúde e à segurança
                                                dos consumidores. Deve ser
                                                igualmente apontado pelo
                                                Usuário/prestador de serviço
                                                vendedor no anúncio quaisquer
                                                despesas adicionais que devam
                                                ser pagas pelo Usuário comprador
                                                para aquisição do produto ou
                                                serviço, tais como despesas de
                                                entrega ou seguros.
                                            </p>
                                            <p>
                                                Também deverão ser apontados por
                                                todos os Usuários
                                                vendedores/prestador de serviço
                                                enquadrados na definição legal
                                                de fornecedor, os meios
                                                adequados para que os Usuários
                                                compradores exerçam seus
                                                direitos de arrependimento em
                                                relação aos produtos oferecidos
                                                em seus anúncios.
                                            </p>
                                            <p>
                                                Ao PORTAL DA MARIA se reserva o
                                                direito de requerer, de acordo
                                                com os critérios que estime
                                                pertinentes, que determinados
                                                produtos ou serviços, bem como
                                                Usuários vendedores somente
                                                anunciem seus bens e serviços na
                                                plataforma mediante a utilização
                                                dos Serviços de gerenciamento de
                                                pagamento da plataforma e/ou
                                                outras ferramentas
                                                disponibilizadas pelo PORTAL DA
                                                MARIA para cobrança do bem
                                                vendido e das tarifas pela
                                                utilização dos serviços,
                                                importando eventualmente no
                                                pagamento de tarifas aplicáveis
                                                pela utilização destes.{" "}
                                            </p>
                                            <p>
                                                Quando uma negociação se
                                                concretizar, o Usuário vendedor
                                                deverá, sempre que e conforme
                                                previsto, pagar ao PORTAL DA
                                                MARIA, em contrapartida aos
                                                serviços descritos na cláusula
                                                1, o valor correspondente a um
                                                percentual do preço anunciado,
                                                conforme classificação e
                                                descrição da tabela do item 15,
                                                que trata das “Tarifas e
                                                faturamento – Pagamentos”.{" "}
                                            </p>
                                            <p>
                                                Em virtude do PORTAL DA MARIA
                                                não figurar como parte nas
                                                negociações de compra e venda de
                                                serviços e produtos que se
                                                realizam entre os Usuários, a
                                                responsabilidade por todas as
                                                obrigações decorrentes, sejam
                                                fiscais, trabalhistas,
                                                consumeristas ou de qualquer
                                                outra natureza, será
                                                exclusivamente do Usuário
                                                vendedor. Assim, o Usuário
                                                vendedor declara e reconhece que
                                                na hipótese do PORTAL DA MARIA,
                                                ou qualquer empresa do grupo vir
                                                a ser demandada judicialmente ou
                                                tenha contra ela uma reclamação
                                                dos órgãos de proteção ao
                                                consumidor, os valores relativos
                                                às condenações, acordos,
                                                despesas processuais e
                                                honorários advocatícios
                                                dispendidos pela empresa serão
                                                de responsabilidade do Usuário
                                                vendedor/prestador de serviço
                                                que deu causa. Por não figurar
                                                como parte nas negociações de
                                                compra e venda que se realizam
                                                entre os Usuários, o PORTAL DA
                                                MARIA também não pode obrigar o
                                                Usuário vendedor a honrar sua
                                                obrigação ou efetivar a
                                                negociação.{" "}
                                            </p>
                                            <p>
                                                O Usuário vendedor deverá ter em
                                                mente que, na medida em que atue
                                                como um fornecedor de produtos
                                                e/ou serviços de forma continua
                                                ou eventual, sua oferta o
                                                vincula, nos termos do artigo 30
                                                do Código de Defesa do
                                                Consumidor e do artigo 429 do
                                                Código Civil, cujo cumprimento
                                                pode ser exigido judicialmente
                                                pelo Usuário comprador.{" "}
                                            </p>
                                            <p>
                                                Por razões de segurança, a conta
                                                do Usuário poderá ser suspensa,
                                                a critério do PORTAL DA MARIA,
                                                caso este suspeite de qualquer
                                                ilegitimidade, fraude ou
                                                qualquer outro ato contrário às
                                                disposições dos presentes Termos
                                                e condições gerais de uso ou
                                                ainda até a apuração e
                                                verificação de (i) questões
                                                relativas à idoneidade do
                                                Usuário; (ii) legalidade das
                                                negociações realizadas; (iii)
                                                reclamações pendentes.{" "}
                                            </p>
                                            <p>
                                                Tributos: o PORTAL DA MARIA não
                                                se responsabiliza pelas
                                                obrigações de natureza
                                                tributária que incidam sobre os
                                                negócios realizados entre
                                                Usuário comprador e Usuário
                                                vendedor. Assim, o Usuário
                                                vendedor que atue de forma
                                                contínua, nos termos da lei em
                                                vigor, responsabilizar-se-á pela
                                                integralidade das obrigações
                                                oriundas de suas atividades,
                                                notadamente pelos tributos
                                                incidentes.{" "}
                                            </p>
                                            <p
                                                style={{
                                                    fontWeight: "bold",
                                                }}
                                            >
                                                08 – Política de devolução{" "}
                                            </p>
                                            <p>
                                                O usuário comprador/consumidor
                                                possui o direito de desistir da
                                                compra no prazo de 7 (sete) dias
                                                a contar da sua assinatura ou do
                                                ato do recebimento do produto,
                                                quando for o caso, dentro do
                                                prazo previsto, o usuário
                                                vendedor deverá aceitar o
                                                respectivo pedido de devolução e
                                                arcar com todos os custos
                                                envolvidos para tanto, conforme
                                                disposto na Legislação de Defesa
                                                do Consumidor e nestes Termos e
                                                condições.
                                            </p>
                                            <p>
                                                Recebida a comunicação do
                                                direito de arrependimento no
                                                prazo retro mencionado, o
                                                usuário fornecedor deverá
                                                imediatamente (em menos de 24
                                                horas) iniciar o procedimento de
                                                devolução de pagamentos e das
                                                mercadorias.
                                            </p>
                                            <p>
                                                O arrependimento implica a
                                                rescisão dos contratos
                                                acessórios, sem qualquer ônus ao
                                                usuário consumidor.{" "}
                                            </p>
                                            <p>
                                                O PORTAL DA MARIA não se
                                                responsabiliza pelo procedimento
                                                de desistência de compra de
                                                produtos por parte do usuário
                                                consumidor, devendo todas as
                                                devoluções serem procedidas pelo
                                                mesmo meio utilizado para a
                                                entrega do produto.{" "}
                                            </p>
                                            <p>
                                                Caso o Usuário fornecedor não
                                                efetue o custeio da devolução, o
                                                PORTAL DA MARIA irá efetuar a
                                                retenção de valores a serem
                                                recebidos pelo Usuário
                                                fornecedor até o valor
                                                suficiente a reembolsar todos os
                                                valores despendidos pelo Usuário
                                                consumidor.{" "}
                                            </p>
                                            <p>
                                                O Usuário fornecedor deverá
                                                imediatamente entrar em contato
                                                com a instituição financeira
                                                responsável pela transação,
                                                quando for o caso, para que não
                                                haja lançamento da compra
                                                cancelada na fatura do usuário
                                                consumidor, sob pena de não o
                                                fazendo, o PORTAL DA MARIA
                                                estará autorizado a efetuar
                                                todas e quaisquer retenções
                                                objetivando a devolução dos
                                                valores despendidos pelo Usuário
                                                comprador.
                                            </p>
                                            <p
                                                style={{
                                                    fontWeight: "bold",
                                                }}
                                            >
                                                09 - Práticas vedadas{" "}
                                            </p>
                                            <p>
                                                Os Usuários não poderão, entre
                                                outras atitudes previstas nestes
                                                Termos e condições gerais e seus
                                                anexos: a) manipular os preços
                                                dos produtos anunciados; b)
                                                interferir nas negociações entre
                                                outros Usuários; c) anunciar
                                                produtos proibidos pelas
                                                políticas do PORTAL DA MARIA e
                                                pela legislação; d) agredir,
                                                caluniar, injuriar ou difamar
                                                outros Usuários.{" "}
                                            </p>
                                            <p>
                                                Estes tipos de comportamento
                                                poderão ser sancionados com o
                                                cancelamento do anúncio, ou com
                                                a suspensão da sua conta como
                                                Usuário do PORTAL DA MARIA, sem
                                                prejuízo das ações legais que
                                                possam ocorrer pela configuração
                                                de delitos ou contravenções ou
                                                os prejuízos civis que possam
                                                causar aos Usuários compradores,
                                                ao PORTAL DA MARIA ou terceiros.{" "}
                                            </p>
                                            <p
                                                style={{
                                                    fontWeight: "bold",
                                                }}
                                            >
                                                10 - Violação no sistema ou da
                                                base de dados{" "}
                                            </p>
                                            <p>
                                                Não é permitida a utilização de
                                                nenhum dispositivo, software ou
                                                outro recurso que venha a
                                                interferir nas atividades e
                                                operações da PORTAL DA MARIA,
                                                bem como nos anúncios,
                                                descrições, contas ou seus
                                                bancos de dados. Qualquer
                                                intromissão, tentativa ou
                                                atividade que viole ou contrarie
                                                as leis de direito de
                                                propriedade intelectual e/ou as
                                                proibições estipuladas nestes
                                                Termos e condições gerais de
                                                uso, tornarão o responsável
                                                passível das ações legais
                                                pertinentes, bem como das
                                                sanções aqui previstas, sendo
                                                ainda responsável pelas
                                                indenizações por eventuais danos
                                                causados.{" "}
                                            </p>
                                            <p
                                                style={{
                                                    fontWeight: "bold",
                                                }}
                                            >
                                                11 - Sanções{" "}
                                            </p>
                                            <p>
                                                Sem prejuízo de outras medidas
                                                cabíveis, o PORTAL DA MARIA
                                                poderá advertir, suspender,
                                                temporária ou definitivamente, a
                                                conta de um Usuário, cancelar os
                                                seus anúncios, iniciando as
                                                ações legais cabíveis e/ou
                                                suspendendo a prestação de seus
                                                serviços se: a) o Usuário não
                                                cumprir qualquer dispositivo
                                                destes Termos e condições
                                                gerais; b) se descumprir com
                                                seus deveres de Usuário; c) se
                                                praticar atos fraudulentos ou
                                                dolosos; d) se não puder ser
                                                verificada a identidade do
                                                Usuário ou se qualquer
                                                informação fornecida por ele
                                                estiver incorreta; e) se o
                                                PORTAL DA MARIAentender que os
                                                anúncios ou qualquer atitude do
                                                Usuário tenham causado algum
                                                dano a terceiros ou ao próprio
                                                PORTAL DA MARIA ou tenham a
                                                potencialidade de assim o fazer;
                                                f) se houver omissão, ou sejam
                                                informados dados inverídicos
                                                para utilização da plataforma do
                                                PORTAL DA MARIA; g) caso não
                                                haja concordância com os termos
                                                de uso e condições gerais, e
                                                diferentemente do que dispõe o
                                                item 04 destes termos, não seja
                                                informado a discordância através
                                                do endereço de e-mail do PORTAL
                                                DA MARIA, e difamar, denegrir,
                                                ou fazer qualquer comentários em
                                                sites, redes sociais que
                                                desabonem o PORTAL DA MARIA,
                                                poderão ter sua conta suspensa
                                                ou desativada imediatamente.{" "}
                                            </p>
                                            <p>
                                                A suspensão em geralmente será
                                                pelo prazo de 30 (trinta) dias,
                                                assim como a suspensão de
                                                anúncio e pagamentos.{" "}
                                            </p>
                                            <p>
                                                Nos casos suspensão da conta do
                                                usuário, todos os anúncios
                                                ativos e/ou ofertas realizadas
                                                serão automaticamente
                                                cancelados. O PORTAL DA MARIA
                                                reserva-se o direito de, a
                                                qualquer momento e a seu
                                                exclusivo critério, solicitar o
                                                envio de documentação pessoal
                                                e/ou exigir que um Usuário.{" "}
                                            </p>
                                            <p
                                                style={{
                                                    fontWeight: "bold",
                                                }}
                                            >
                                                12 - Responsabilidades{" "}
                                            </p>
                                            <p>
                                                O PORTAL DA MARIA se
                                                responsabiliza por eventuais
                                                danos causados aos seus Usuários
                                                por defeitos ou vícios relativos
                                                exclusivamente à prestação do
                                                seu serviço de hospedagem e
                                                veiculação de anúncios na
                                                plataforma, desde que o PORTAL
                                                DA MARIA tenha dado causa aos
                                                referidos defeitos ou vícios.{" "}
                                            </p>
                                            <p>
                                                É de responsabilidade do Usuário
                                                o conteúdo por ele gerado, de
                                                modo que caso o serviço
                                                ofertado, bem como sua
                                                descrição, envolvendo vídeos e
                                                fotos, trate-se de plágio, viole
                                                direitos autorais, propriedade
                                                intelectual, o Usuário será
                                                única e exclusivamente
                                                responsabilizado pelas violações
                                                e danos decorrentes, posto que o
                                                PORTAL DA MARIA não tem a
                                                responsabilidade em verificar se
                                                o conteúdo disponibilizado pelo
                                                Usuário viola direitos autorais
                                                etc.{" "}
                                            </p>
                                            <p>
                                                O PORTAL DA MARIA não se
                                                responsabiliza pelo serviço ou
                                                produto ofertado pelo Usuário
                                                vendedor/prestador de serviço,
                                                sendo assim, no ato da
                                                negociação do serviço, cabe ao
                                                Usuário vendedor fornecer todas
                                                as informações, serviços e
                                                valores adicionais, sendo que
                                                compete ao Usuário comprador ler
                                                e interpretar a descrição do
                                                serviço, preços embutidos, e
                                                quando da finalização do pedido.
                                                O PORTAL DA MARIA não se
                                                responsabiliza por vícios ou
                                                defeitos técnicos e/ou
                                                operacionais por dolo ou culpa
                                                de terceiros.{" "}
                                            </p>
                                            <p>
                                                O PORTAL DA MARIA não é o
                                                proprietário dos produtos ou
                                                prestador dos serviços
                                                anunciados pelos Usuários nos
                                                Sites, não guarda a posse desses
                                                itens e não realiza as ofertas
                                                de venda, tampouco intervém na
                                                entrega dos produtos.{" "}
                                            </p>
                                            <p>
                                                O PORTAL DA MARIA não se
                                                responsabiliza, por conseguinte,
                                                pela existência, quantidade,
                                                qualidade, estado, integridade
                                                ou legitimidade dos produtos
                                                oferecidos, adquiridos ou
                                                alienados pelos Usuários, assim
                                                como pela capacidade para
                                                contratar dos Usuários ou pela
                                                veracidade dos dados pessoais
                                                por eles inseridos em seus
                                                cadastros. O PORTAL DA MARIA não
                                                outorga garantia por vícios
                                                ocultos ou aparentes nas
                                                negociações entre os Usuários.
                                                Cada Usuário conhece e aceita
                                                ser o único responsável pelos
                                                produtos que anuncia ou pelas
                                                ofertas que realiza.{" "}
                                            </p>
                                            <p>
                                                O PORTAL DA MARIA não será
                                                responsável pelo efetivo
                                                cumprimento das obrigações
                                                assumidas pelos Usuários. O
                                                Usuário reconhece e aceita ao
                                                realizar negociações com outros
                                                Usuários ou terceiros o que faz
                                                por sua conta e risco,
                                                reconhecendo o PORTAL DA MARIA
                                                como mero fornecedor de serviços
                                                de disponibilização de espaço
                                                virtual para anúncios de
                                                produtos e serviços ofertados
                                                por terceiros. Em nenhum caso o
                                                PORTAL DA MARIA será responsável
                                                pelo lucro cessante ou por
                                                qualquer outro dano e/ou
                                                prejuízo que possa sofrer devido
                                                às negociações realizadas ou não
                                                realizadas por meio dos sites
                                                decorrentes da conduta de outros
                                                usuários.{" "}
                                            </p>
                                            <p>
                                                Nos casos em que um ou mais
                                                Usuários ou algum terceiro
                                                inicie qualquer tipo de
                                                reclamação ou ação legal contra
                                                outro ou mais Usuários, todos e
                                                cada um dos Usuários envolvidos
                                                nas reclamações ou ações eximem
                                                de toda responsabilidade o
                                                PORTAL DA MARIA e os seus
                                                diretores, gerentes, empregados,
                                                agentes, sócios, representantes
                                                e procuradores.{" "}
                                            </p>
                                            <p>
                                                Os Usuários anunciantes
                                                autorizam expressamente, ao se
                                                cadastrarem na plataforma, a
                                                serem chamados aos eventuais
                                                processos judiciais para
                                                responderem pelos defeitos
                                                causados pelos seus produtos
                                                e/ou serviços anunciados,
                                                inclusive mediante
                                                reembolso/regresso de todos os
                                                valores porventura pagos pelo
                                                PORTAL DA MARIA.{" "}
                                            </p>
                                            <p
                                                style={{
                                                    fontWeight: "bold",
                                                }}
                                            >
                                                13 - Alcance dos serviços{" "}
                                            </p>
                                            <p>
                                                Estes Termos e condições gerais
                                                de uso não geram nenhum contrato
                                                de sociedade, mandato, franquia
                                                ou relação de trabalho entre o
                                                PORTAL DA MARIA e o Usuário. O
                                                Usuário manifesta ciência de que
                                                o PORTAL DA MARIA não é parte de
                                                nenhuma da negociação realizada
                                                entre Usuários, nem possui
                                                controle algum sobre a
                                                existência, qualidade, segurança
                                                ou legalidade dos produtos ou
                                                serviços anunciados pelos
                                                Usuários, sobre a veracidade ou
                                                exatidão dos anúncios elaborados
                                                pelos usuários e sobre a
                                                capacidade dos Usuários para
                                                negociar. O PORTAL DA MARIA não
                                                pode assegurar o êxito de
                                                qualquer negociação realizada
                                                entre Usuários, tampouco
                                                verificar a identidade ou dos
                                                Dados Pessoais dos usuários.{" "}
                                            </p>
                                            <p>
                                                O PORTAL DA MARIA não garante a
                                                veracidade da publicação de
                                                terceiros que apareça em seu
                                                site e não será responsável pela
                                                correspondência ou contratos que
                                                o Usuário realize com terceiros.{" "}
                                            </p>
                                            <p
                                                style={{
                                                    fontWeight: "bold",
                                                }}
                                            >
                                                14 - Falhas no sistema{" "}
                                            </p>
                                            <p>
                                                O PORTAL DA MARIA não se
                                                responsabiliza por qualquer
                                                dano, prejuízo ou perda sofridos
                                                pelo Usuário em razão de falhas
                                                na internet, no sistema ou no
                                                servidor utilizado pelo usuário,
                                                decorrentes de condutas de
                                                terceiros, caso fortuito ou
                                                força maior. O PORTAL DA MARIA
                                                também não será responsável por
                                                qualquer vírus que possa atacar
                                                o equipamento do usuário em
                                                decorrência do acesso,
                                                utilização ou navegação na
                                                internet ou como consequência da
                                                transferência de dados,
                                                arquivos, imagens, textos,
                                                vídeos ou áudio.{" "}
                                            </p>
                                            <p
                                                style={{
                                                    fontWeight: "bold",
                                                }}
                                            >
                                                15 - Tarifas e faturamento -
                                                Pagamentos{" "}
                                            </p>
                                            <p>
                                                Será sempre cobrada uma Tarifa
                                                de Venda de Produtos que somente
                                                será paga ao PORTAL DA
                                                MARIAquando a negociação se
                                                concretizar.{" "}
                                            </p>
                                            <p>
                                                Os valores de comissão do PORTAL
                                                DA MARIA serão os seguintes:{" "}
                                            </p>
                                            <div className="allignerCenter">
                                                <Table
                                                    bordered
                                                    className="w-50"
                                                >
                                                    <thead>
                                                        <tr>
                                                            <th
                                                                width="100%"
                                                                colSpan="2"
                                                                style={{
                                                                    backgroundColor:
                                                                        "lightgrey",
                                                                }}
                                                            >
                                                                TABELA DE
                                                                COMISSÃO DO
                                                                PORTAL DA MARIA
                                                            </th>
                                                        </tr>
                                                        <tr>
                                                            <th
                                                                width="50%"
                                                                style={{
                                                                    backgroundColor:
                                                                        "lightgrey",
                                                                }}
                                                            >
                                                                CATEGORIA{" "}
                                                            </th>
                                                            <th
                                                                width="50%"
                                                                style={{
                                                                    backgroundColor:
                                                                        "lightgrey",
                                                                }}
                                                            >
                                                                VAREJO
                                                            </th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <tr>
                                                            <td
                                                                className="text-capitalize"
                                                                width="50%"
                                                            >
                                                                Tecidos
                                                            </td>
                                                            <td width="50%">
                                                                15%
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td
                                                                className="text-capitalize"
                                                                width="50%"
                                                            >
                                                                Artesanato
                                                                pronto
                                                            </td>
                                                            <td width="50%">
                                                                15%
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td
                                                                className="text-capitalize"
                                                                width="50%"
                                                            >
                                                                Roupa pronta
                                                            </td>
                                                            <td width="50%">
                                                                15%
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td
                                                                className="text-capitalize"
                                                                width="50%"
                                                            >
                                                                Serviços
                                                            </td>
                                                            <td width="50%">
                                                                15%
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td
                                                                className="text-capitalize"
                                                                width="50%"
                                                            >
                                                                Calçados
                                                            </td>
                                                            <td width="50%">
                                                                15%
                                                            </td>
                                                        </tr>
                                                    </tbody>
                                                </Table>
                                            </div>
                                            <p>
                                                Os valores/percentuais estarão
                                                sempre disponíveis no site do
                                                PORTAL DA MARIA e poderão ser
                                                alterados sem qualquer aviso,
                                                cabendo aos Usuários Vendedores
                                                estarem cientes da Tarifa de
                                                Venda de Produtos vigente na
                                                data do anúncio.
                                            </p>
                                            <p>
                                                Os valores serão liberados aos
                                                Usuários
                                                fornecedores/prestadores de
                                                serviços apenas após a
                                                confirmação por parte do Usuário
                                                cliente acerca do recebimento da
                                                mercadoria e finalização do
                                                serviço prestado.{" "}
                                            </p>
                                            <p>
                                                O PORTAL DA MARIA se reserva o
                                                direito de tomar as medidas
                                                judiciais e extrajudiciais
                                                pertinentes para receber os
                                                valores devidos.{" "}
                                            </p>
                                            <p
                                                style={{
                                                    fontWeight: "bold",
                                                }}
                                            >
                                                16 - Propriedade Intelectual e
                                                links{" "}
                                            </p>
                                            <p>
                                                O uso comercial da expressão
                                                "PORTAL DA MARIA" como marca,
                                                nome empresarial ou nome de
                                                domínio, bem como os conteúdos
                                                das telas relativas aos serviços
                                                doPORTAL DA MARIA assim como os
                                                bancos de dados, redes, arquivos
                                                que permitem ao Usuário acessar
                                                e usar a sua Conta são
                                                propriedade do PORTAL DA MARIA e
                                                estão protegidos pelas leis e
                                                tratados internacionais de
                                                direito autoral, marcas,
                                                patentes, modelos e desenhos
                                                industriais. O uso indevido e a
                                                reprodução total ou parcial dos
                                                referidos conteúdos são
                                                proibidos, salvo a autorização
                                                prévia e expressa por escrito do
                                                PORTAL DA MARIA.{" "}
                                            </p>
                                            <p>
                                                O Usuário declara e garante que
                                                é titular, possui a devida
                                                autorização do(s) titular(es) de
                                                direito(s) de propriedade
                                                intelectual ou que, de outra
                                                forma, pode anunciar no PORTAL
                                                DA MARIA, oferecer produtos e/ou
                                                serviços anunciados ou
                                                declarar-se loja oficial de
                                                determinada marca, sendo o único
                                                responsável pelo conteúdo das
                                                suas publicações.{" "}
                                            </p>
                                            <p>
                                                O Usuário assume total
                                                responsabilidade por todos os
                                                prejuízos, diretos e indiretos,
                                                inclusive indenizações, lucros
                                                cessantes, honorários
                                                advocatícios e demais encargos
                                                judiciais e extrajudiciais que o
                                                PORTAL DA MARIAseja obrigado a
                                                incorrer em virtude de ato ou
                                                omissão do Usuário.{" "}
                                            </p>
                                            <p>
                                                Se o PORTAL DA MARIA receber
                                                alguma reclamação ou
                                                questionamento de terceiros (por
                                                exemplo, dos titulares de
                                                marca), o PORTAL DA MARIA poderá
                                                remover o seu anúncio e aplicar
                                                as sanções cabíveis.{" "}
                                            </p>
                                            <p>
                                                17 - Da ausência de vínculo
                                                empregatício entre as partes{" "}
                                            </p>
                                            <p>
                                                Tanto o fornecedor/vendedor e
                                                prestador de serviços estão
                                                cientes que na presente relação
                                                não há qualquer vínculo
                                                empregatício entre as partes,
                                                posto que ausente todos os
                                                requisitos, quais sejam
                                                pessoalidade, subordinação,
                                                onerosidade e habitualidade.{" "}
                                            </p>
                                            <p>
                                                No que diz respeito ao vendedor,
                                                o mesmo é designado para
                                                realizar a intermediação entre o
                                                fornecedor e a plataforma do
                                                PORTAL DA MARIA e, sendo que não
                                                há pessoalidade, posto que pode
                                                ocorrer a indicação de qualquer
                                                vendedor cadastrado na
                                                plataforma do PORTAL DA MARIA,
                                                assim como não haverá
                                                subordinação.{" "}
                                            </p>
                                            <p>
                                                A mesma situação ocorre para o
                                                entregador das mercadorias, e as
                                                costureiras contactadas pelos
                                                Usuários.{" "}
                                            </p>
                                            <p>
                                                O vendedor, a costureirae o
                                                entregador prestarão serviço por
                                                intermédia do PORTAL DA MARIA em
                                                caráter autônomo.{" "}
                                            </p>
                                            <p>
                                                Quanto ao fornecedor, ficam as
                                                partes cientes de que os
                                                funcionários e prepostos do
                                                PORTAL DA MARIA não possuem
                                                qualquer vínculo com o
                                                fornecedor, e os funcionários e
                                                prepostos do fornecedor não
                                                possuem qualquer vínculo com o
                                                PORTAL DA MARIA.{" "}
                                            </p>
                                            <p
                                                style={{
                                                    fontWeight: "bold",
                                                }}
                                            >
                                                18 – Da entrega{" "}
                                            </p>
                                            <p>
                                                O PORTAL DA MARIA não se
                                                responsabilizará pela entrega
                                                dos produtos/peças adquiridas,
                                                lembrando que o PORTAL DA MARIA
                                                é mero intermediador entre
                                                usuários fornecedores e
                                                compradores.{" "}
                                            </p>
                                            <p>
                                                A entrega ficará a cargo de
                                                terceiro
                                                (parceiro/intermediador) que
                                                será indicado logo na
                                                finalização da compra através da
                                                plataforma do PORTAL DA MARIA.{" "}
                                            </p>
                                            <p>
                                                O Usuário comprador terá a opção
                                                de escolher se a entrega
                                                ocorrerá através do terceiro
                                                indicado pelo site, devendo se
                                                atentar aos valores cobrados
                                                pelo frete, ou se a entrega
                                                ocorrerá de outra forma.{" "}
                                            </p>
                                            <p>
                                                A taxa de entrega sofrerá
                                                variações de acordo com o
                                                deslocamento a ser realizado e
                                                pelo tamanho do produto.{" "}
                                            </p>
                                            <p
                                                style={{
                                                    fontWeight: "bold",
                                                }}
                                            >
                                                19 - Legislação aplicável e Foro
                                                de eleição{" "}
                                            </p>
                                            <p>
                                                Todos os itens destes Termos e
                                                condições gerais de uso são
                                                regidos pelas leis vigentes na
                                                República Federativa do Brasil.
                                                Para todos os assuntos
                                                referentes à interpretação, ao
                                                cumprimento ou qualquer outro
                                                questionamento relacionado a
                                                estes Termos e condições gerais
                                                de uso, as partes concordam em
                                                se submeter ao Foro de São
                                                Paulo, Estado de São Paulo,
                                                Brasil, com exceção de
                                                reclamações apresentadas por
                                                Usuários que se enquadrem no
                                                conceito legal de consumidores,
                                                que poderão submeter tais
                                                reclamações ao foro de seu
                                                domicílio.{" "}
                                            </p>
                                        </Col>
                                    </Row>
                                </Container>
                            </ModalBody>
                        </Modal>

                        <>
                            <Col xs="12">
                                <h1>4 - Dados do Responsável</h1>
                            </Col>
                            <Col xs="12">
                                <InputDefault
                                    has_label
                                    className="verify"
                                    label={"Nome Completo*"}
                                    name={"name_completo"}
                                    _id={"name_completo"}
                                    type={"text"}
                                    initial_value={payload.name_completo}
                                    _required
                                    onChange={defaultListener}
                                    onBlur={obrigatory_blur}
                                />
                            </Col>
                            <Col xs="12">
                                <InputDefault
                                    has_label
                                    className="verify"
                                    label={"E-mail comercial*"}
                                    name={"email"}
                                    _id={"email"}
                                    type={"text"}
                                    initial_value={payload.email}
                                    _required
                                    onChange={defaultListener}
                                    onBlur={email_blur}
                                />
                            </Col>
                            <Col xs="12">
                                <InputDefault
                                    has_label
                                    className="verify"
                                    label={"CPF*"}
                                    name={"cpf"}
                                    _id={"cpf"}
                                    type={"text"}
                                    initial_value={payload.cpf}
                                    onBlur={cpf_blur}
                                    onKeyUp={cpf_rg}
                                    _minLength={14}
                                    _maxLength={14}
                                    options={{
                                        delimiters: [".", ".", "-"],
                                        blocks: [3, 3, 3, 2],
                                    }}
                                    _required
                                    onChange={defaultListener}
                                />
                            </Col>
                            <Col xs="12">
                                <InputDefault
                                    has_label
                                    className="verify"
                                    label={"Telefone*"}
                                    name={"telefone_pessoal"}
                                    _id={"telefone_pessoal"}
                                    type={"tel"}
                                    initial_value={payload.telefone_pessoal}
                                    onKeyUp={onKeyUpTelephone}
                                    onBlur={(e) =>
                                        obrigatory_blur(e, 12, 13, 2)
                                    }
                                    options={{
                                        delimiters: [" ", "-"],
                                        blocks: [2, 5, 4],
                                    }}
                                    _required
                                    onChange={defaultListener}
                                />
                            </Col>
                            <Col xs="12">
                                <InputDefault
                                    has_label
                                    className="verify"
                                    label={"Cargo*"}
                                    name={"cargo"}
                                    _id={"cargo"}
                                    type={"text"}
                                    initial_value={payload.cargo}
                                    _required
                                    onChange={defaultListener}
                                    onBlur={obrigatory_blur}
                                />
                            </Col>
                            <Col xs="12">
                                <InputDefault
                                    has_label
                                    className="verify"
                                    label={"Senha*"}
                                    name={"password"}
                                    _id={"password"}
                                    type={"password"}
                                    initial_value={payload.password}
                                    _required
                                    onChange={defaultListener}
                                    onBlur={(e) => obrigatory_blur(e, 6)}
                                />
                            </Col>
                            <Col xs="12">
                                <InputDefault
                                    has_label
                                    className="verify"
                                    label={"Confirmar senha*"}
                                    name={"confirmPassword"}
                                    _id={"confirmPassword"}
                                    type={"password"}
                                    initial_value={payload.confirmPassword}
                                    _required
                                    onChange={defaultListener}
                                    onBlur={_password_blur}
                                />
                            </Col>
                            <Col xs="12">
                                <FormGroup>
                                    <CustomInput
                                        onClick={(e) => {
                                            e.target.checked = !isOk
                                            //console.log(e.target.checked)
                                            setIsOK(!isOk)
                                            return true
                                        }}
                                        bsSize="lg"
                                        type="checkbox"
                                        id="termsOfUSe"
                                        label={"Li e aceito os"}
                                    >
                                        &thinsp;
                                        <Button
                                            color="link"
                                            className={`${
                                                styles.textTermofUse
                                            } p-0 m-0`}
                                            onClick={() => setModal(true)}
                                        >
                                            termos de uso
                                        </Button>
                                    </CustomInput>
                                </FormGroup>
                            </Col>
                            <Col xs="12">
                                <Row
                                    className="m-0 full-width"
                                    style={{
                                        display: "flex",
                                        justifyContent: "space-between",
                                    }}
                                >
                                    <Col
                                        xs="6"
                                        style={{
                                            padding: 0,
                                            paddingRight: "1rem",
                                        }}
                                    >
                                        <Button
                                            className="buttonSave"
                                            style={{ width: "50%" }}
                                            onClick={currentChangePrev(2)}
                                        >
                                            Anterior
                                        </Button>
                                    </Col>
                                    <Col
                                        xs="6"
                                        style={{
                                            padding: 0,
                                            paddingLeft: "1rem",
                                        }}
                                    >
                                        <Button
                                            className="buttonSave btn_save"
                                            style={{ width: "50%" }}
                                            onClick={finish}
                                            disabled={!isOk}
                                        >
                                            Finalizar
                                        </Button>
                                    </Col>
                                </Row>
                            </Col>
                        </>
                    </Row>
                )}

                {Number(currentStep) === 4 && (
                    <>
                        <Row
                            className={`mb-4 mx-auto showAboveTablet ${
                                styles.feedbackSign
                            }`}
                        >
                            <Col
                                md="12"
                                className={styles.cardPrincipal}
                                style={{ height: "20rem" }}
                            >
                                <Row className="mb-4 w-100 mt-5">
                                    <Col md="12" className={styles.aligner}>
                                        <Img
                                            className={styles.imgMary}
                                            image_name_with_extension={
                                                "Mary-newsign.png"
                                            }
                                            alt={"Seja Bem vindo"}
                                        />
                                    </Col>
                                </Row>
                                <Row>
                                    <Col md="12" className={styles.aligner}>
                                        <h1 className={styles.textOne}>
                                            Sua loja foi registrada com sucesso!
                                        </h1>
                                    </Col>
                                </Row>
                                <Row className="mb-4">
                                    <Col
                                        md={{ size: 11, offset: 1 }}
                                        className={styles.aligner}
                                    >
                                        <h4 className={styles.textTwo}>
                                            Acessando seu perfil você pode
                                            inserir seus produtos e configurar
                                            suas
                                        </h4>
                                    </Col>
                                    <Col
                                        md={{ size: 11, offset: 1 }}
                                        className={styles.aligner}
                                    >
                                        <h4 className={styles.textTwo}>
                                            formas de parcelamento e valores dos
                                            produtos
                                        </h4>
                                    </Col>
                                </Row>
                                <Row className="mb-5">
                                    <Col md="12" className={styles.aligner}>
                                        <Button
                                            className={styles.buttonLeft}
                                            size="lg"
                                            onClick={(e) => {
                                                e.preventDefault()
                                                router.push("/login")
                                            }}
                                        >
                                            ENTRAR
                                        </Button>
                                    </Col>
                                </Row>
                            </Col>
                        </Row>
                    </>
                )}

                {Number(currentStep) === 4 && (
                    <>
                        <Row
                            className={`mb-5 mx-auto showTabletAndLess ${
                                styles.feedbackSign
                            }`}
                        >
                            <Col
                                sm={{ size: 11, offset: 1 }}
                                xs={{ size: 11, offset: 1 }}
                                className={styles.cardPrincipal}
                                style={{ height: "20rem" }}
                            >
                                <Row className="mb-4 w-100">
                                    <Col md="12" className={styles.aligner}>
                                        <Img
                                            className={styles.imgMaryMobile}
                                            image_name_with_extension={
                                                "Mary-newsign.png"
                                            }
                                            alt={"Seja Bem vindo"}
                                        />
                                    </Col>
                                </Row>
                                <Row>
                                    <Col md="12" className={styles.aligner}>
                                        <h3 className={styles.textOne}>
                                            Sua loja foi registrada com sucesso!
                                        </h3>
                                    </Col>
                                </Row>
                                <Row className="mb-4">
                                    <Col
                                        md={{ size: 11, offset: 1 }}
                                        className={styles.aligner}
                                    >
                                        <h4 className={styles.textTwo}>
                                            Acessando seu perfil você pode
                                            inserir
                                        </h4>
                                    </Col>
                                    <Col
                                        md={{ size: 11, offset: 1 }}
                                        className={styles.aligner}
                                    >
                                        <h4 className={styles.textTwo}>
                                            seus produtos e configurar suas
                                            formas
                                        </h4>
                                    </Col>
                                    <Col
                                        md={{ size: 11, offset: 1 }}
                                        className={styles.aligner}
                                    >
                                        <h4 className={styles.textTwo}>
                                            de parcelamento e valores dos
                                            produtos
                                        </h4>
                                    </Col>
                                </Row>
                                <Row className="mb-5">
                                    <Col md="12" className={styles.aligner}>
                                        <Button
                                            className={styles.buttonLeft}
                                            size="lg"
                                            onClick={async (e) => {
                                                e.preventDefault()
                                                await router.push("/login")
                                            }}
                                        >
                                            ENTRAR
                                        </Button>
                                    </Col>
                                </Row>
                            </Col>
                        </Row>
                    </>
                )}
            </Main>
        </>
    )
}

export default NewSign
