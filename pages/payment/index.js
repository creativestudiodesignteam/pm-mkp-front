import React, {useEffect, useState} from "react"
import Head from "next/head"
import Main from "../../src/components/Main"
import {Col, Row} from "reactstrap"
import Stepper from "react-stepper-horizontal"
import {cartParseFromServer, fromServer, hasKey,} from "../../src/assets/helpers"

import dynamic from "next/dynamic"

const Overall = dynamic(() => import("../../src/components/payment/overall"), {
    ssr: false,
})

const Payment = ({
                     cart,
                     context,
                     dispatch,
                     creditCard,
                     address: serverAddress,
                     serverToken,
                     ...props
                 }) => {
    const [address, setAddress] = useState([])
    const [cards, setCards] = useState({selected: "", options: []})
    const [installments, setInstallments] = useState([])
    const [installmentsValue, setInstallmentsValue] = useState(0)

    useEffect(() => {
        console.log({installmentsValue})
    }, [installmentsValue])
    useEffect(() => {
        console.log('installments ', {installments})
    }, [installments])
    const window = require("global/window")

    useEffect(() => {
        if (!address.length) {
            setAddress(serverAddress)
        }
        if (!cards.length) {
            setCards({
                ...cards,
                options: creditCard,
                selected: `${creditCard.find((c) => !!c?.selected)?.id || ""}`,
            })
        }
        !!window && window.scrollTo(0, 0)
    }, [])

    return (
        <>
            <Head>
                <title>Portal da Maria | Pagamento</title>
                <link rel="icon" href={"/favicon.ico"}/>
                <meta name="format-detection" content="telephone=no"/>
                <meta
                    httpEquiv="Content-Type"
                    content="text/html; charset=UTF-8"
                />
                <meta
                    name="viewport"
                    content="width=device-width, initial-scale=1, shrink-to-fit=no"
                />
                <meta httpEquiv="X-UA-Compatible" content="IE=edge"/>
                <link
                    href="https://fonts.googleapis.com/css?family=Montserrat%7CRoboto:100,300,400,500,700,900%7CRoboto+Condensed:100,300,400,500,700"
                    rel="stylesheet"
                />
            </Head>
            <Main
                _BreadCrumb={[
                    {
                        node_text: "Carrinho de Compras",
                        href: "/cart",
                    },
                    {
                        node_text: "Pagamentos",
                    },
                ]}
            >
                {!!window.innerWidth > 767.99 && (
                    <Row>
                        <Col md="12" style={{marginBottom: "5rem"}}>
                            <Stepper
                                steps={[
                                    {title: "Carrinho de compras"},
                                    {title: "Identificação"},
                                    {title: "Pagamento"},
                                    {title: "Finalização"},
                                ]}
                                disabledSteps={[3]}
                                activeStep={2}
                                activeColor={"#e3171b"}
                                completeColor={"#e3171b"}
                                activeTitleColor={"#e3171b"}
                                completeTitleColor={"#e3171b"}
                                completeBarColor={"#e3171b"}
                                size={60}
                                circleFontSize={20}
                                titleFontSize={17}
                                defaultTitleOpacity={"5"}
                                activeTitleOpacity={"1"}
                            />
                        </Col>
                    </Row>
                )}
                <Overall
                    serverToken={serverToken}
                    installments={installments}
                    setInstallments={setInstallments}
                    installmentsValue={installmentsValue}
                    setInstallmentsValue={setInstallmentsValue}
                    cart={cart}
                    cards={cards}
                    setCards={setCards}
                    address={address}
                    setAddress={setAddress}
                />
            </Main>
        </>
    )
}

export const getServerSideProps = async ({res, query}) => {
    if (hasKey(query, "token") && query.token.length) {
        const {token} = query
        const cart = await fromServer("/carts", token)
        const creditCard = await fromServer("/creditcard", token)
        const address = await fromServer("/addresses", token)

        if (
            hasKey(cart, "error") ||
            hasKey(creditCard, "error") ||
            hasKey(address, "error")
        ) {
            const {error} = cart
            console.log(
                "fail to load carts",
                error.message,
                creditCard.error.message,
                address.error.message
            )
        } else {
            return {
                props: {
                    cart: cart.map(cartParseFromServer),
                    creditCard,
                    address,
                    serverToken: token
                },
            }
        }
    }
    res.setHeader("location", "/login")
    res.statusCode = 302
    res.end()
    return {
        props: {},
    }
}
export default Payment
