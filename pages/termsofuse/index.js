import React from "react"
import Head from "next/head"
import Main from "../../src/components/Main"
import { Row, Col, Table } from "reactstrap"

const TermsOfUse = () => {
    return (
        <>
            <Head>
                <title>Portal da Maria | Termos de uso</title>
                <link rel="icon" href={"/favicon.ico"} />
                <meta name="format-detection" content="telephone=no" />
                <meta
                    httpEquiv="Content-Type"
                    content="text/html; charset=UTF-8"
                />
                <meta
                    name="viewport"
                    content="width=device-width, initial-scale=1, shrink-to-fit=no"
                />
                <meta httpEquiv="X-UA-Compatible" content="IE=edge" />
                <link
                    href="https://fonts.googleapis.com/css?family=Montserrat%7CRoboto:100,300,400,500,700,900%7CRoboto+Condensed:100,300,400,500,700"
                    rel="stylesheet"
                />
            </Head>
            <Main
                _BreadCrumb={[
                    {
                        node_text: "Termos de uso",
                        href: "/termsofuse",
                        query: {},
                    },
                ]}
            >
                <Row>
                    <Col
                        xs="12"
                        style={{
                            fontWeight: 300,
                            fontSize: "18px",
                            lineHeight: "35px",
                            margin: "0 auto",
                            float: "none",
                            textAlign: "justify",
                            marginTop: "3rem",
                        }}
                    >
                        <h1>Termos de uso e condições gerais</h1>
                        <p>
                            Estes Termos e condições gerais aplicam-se ao uso
                            dos serviços oferecidos pelo Fhibra Comercial Ltda.,
                            empresa devidamente inscrita no CNPJ/MF sob o n°
                            10.729.483/0001-13, situada na Rua Doutor Virgílio
                            do Nascimento, 445, CEP 03.027-020,Brás, São Paulo,
                            doravante nominada PORTAL DA MARIA, por meio da
                            plataforma wwww.portaldamaria.com.
                        </p>
                        <p>
                            Qualquer pessoa, doravante nominada Usuário, que
                            pretenda utilizar os serviços do PORTAL DA MARIA
                            deverá aceitar os Termos e condições gerais e todas
                            as demais políticas e princípios que o regem.
                        </p>
                        <p>
                            A aceitação destes Termos e condições gerais é
                            absolutamente indispensável à utilização da
                            plataforma PORTAL DA MARIA.{" "}
                        </p>
                        <p>
                            O Usuário deverá ler, certificar-se de haver
                            entendimento e aceitar todas as condições
                            estabelecidas nos Termos de Uso e Condições Gerais e
                            nas Políticas de privacidade, assim como nos demais
                            documentos a eles incorporados por referência, antes
                            de seu cadastro como Usuário do PORTAL DA MARIA.
                        </p>
                        <p>
                            O PORTAL DA MARIA não é fornecedor de quaisquer
                            produtos ou serviços anunciados, prestando um
                            serviço consistente na oferta de uma plataforma na
                            internet, que fornece espaços para que PARCEIROS
                            possam anunciar seus produtos e serviços, para os
                            milhares de clientes que visitam nossa página
                            mensalmente.
                        </p>
                        <p>
                            O PARCEIRO ao concordar com os nossos termos,
                            compromete-se a disponibilizar produtos com estoque
                            ou serviços que possam vender e gerar entrega aos
                            seus consumidores de acordo com o item ou serviço
                            ofertado, identificado por registro em fotos, vídeos
                            e textos previamente cadastrado no PORTAL DA MARIA.
                        </p>
                        <p>
                            Ao aceitar o Termo de POLÍTICA DE PRIVACIDADE e
                            CONFIDENCIALIDADE DA INFORMAÇÃO, O PARCEIRO
                            compromete-se a manter em sigilo dados de cadastro
                            dos consumidores aos quais emitiram notas fiscais de
                            vendas, bem como assegurar também em sigilo as
                            informações dos dados dos parceiros do PORTAL DA
                            MARIA, como transportadoras e outros prestadores de
                            serviços que fazem parte do total funcionamento de
                            nossas operações. O PARCEIRO, após se registrar
                            criando um cadastro com login e senha, estes
                            intransferíveis, único e pessoal, será meio para
                            manusear suas operações de cadastro de produtos e
                            vendas. O PORTAL DA MARIA, nem quaisquer de seus
                            empregados ou prepostos, solicitará por qualquer
                            meio, físico ou eletrônico, que seja informada sua
                            senha.
                        </p>
                        <p>
                            O PORTAL DA MARIA pode recusar qualquer solicitação
                            de cadastro, advertir, suspender, temporária ou
                            definitivamente a conta de um usuário desde que
                            observada qualquer irregularidade, não é permitido
                            anunciar produtos expressamente proibidos pela
                            legislação vigente ou pelos Termos de Uso e
                            CondiçõesGerais da plataforma que não possuam a
                            devida autorização específica dos órgãos reguladores
                            competentes, ou que violem direitos de terceiros.
                        </p>
                        <p>
                            O PORTAL DA MARIA não se responsabiliza pela
                            veracidade das avaliações recebidas pelos usuários.
                            Só assumindo o controle do envio e recebimento.{" "}
                        </p>

                        <p
                            style={{
                                fontWeight: "bold",
                            }}
                        >
                            1. Objeto{" "}
                        </p>
                        <p style={{ paddingLeft: "2em" }}>
                            1.1. Este Termo de Uso e Condições Gerais dos
                            serviços oferecidos pelo PORTAL DA MARIA, tem por
                            finalidade orientar O PARCEIRO com informações
                            importantes para o devido uso da Plataforma,
                            preservando a relação contratual e respeitando as
                            especificidades de cada Serviço contratado.
                        </p>
                        <p
                            style={{
                                fontWeight: "bold",
                            }}
                        >
                            2. Cadastro
                        </p>
                        <p style={{ paddingLeft: "2em" }}>
                            2.1. O PORTAL DA MARIA apenas confirmará o cadastro
                            do Usuário que preencher todos os campos
                            obrigatórios e com informações verídicas. Os
                            serviços do PORTAL DA MARIA estão disponíveis para
                            as pessoas físicas e jurídicas que tenham capacidade
                            legal para contratá-los e prestá-los.
                        </p>
                        <p style={{ paddingLeft: "2em" }}>
                            2.2. É proibido o cadastro de Usuários que não
                            tenham capacidade civil (com relação a pessoas
                            físicas), bem como de Usuários que tenham sido
                            suspensos do PORTAL DA MARIA, temporária ou
                            definitivamente, sem prejuízo da aplicação das
                            sanções legais previstas no Código Civil Brasileiro.
                        </p>
                        <p style={{ paddingLeft: "2em" }}>
                            2.3. Ao se cadastrar no PORTAL DA MARIA, o Usuário
                            poderá utilizar todos os serviços disponibilizados
                            na plataforma conforme descrito no site do PORTAL DA
                            MARIA, declarando, para tanto, ter lido,
                            compreendido e aceitado os respectivos Termos de uso
                            e Condições gerais de cada um destes serviços
                            ofertados.
                        </p>
                        <p style={{ paddingLeft: "2em" }}>
                            2.4. O Usuário acessará sua conta através de e-mail
                            (ou login) e senha e compromete-se a não informar a
                            terceiros esses dados, responsabilizando-se
                            integralmente pelo uso que deles seja feito.
                        </p>
                        <p style={{ paddingLeft: "2em" }}>
                            2.5. O PARCEIRO compromete-se a notificar o PORTAL
                            DA MARIA imediatamente, e por meio seguro, a
                            respeito de qualquer uso não autorizado de sua
                            conta, bem como seu acesso não autorizado por
                            terceiros a mesma.
                        </p>
                        <p style={{ paddingLeft: "2em" }}>
                            2.6. O PORTAL DA MARIA, deve ser comunicado
                            imediatamente por meio seguro, se necessário, a
                            respeito de qualquer alteração significativa no
                            quadro societário que venha ocorrer com o PARCEIRO,
                            incluindo, mas não se limitando, à baixa de CNPJ,
                            abertura de falência, recuperação judicial,
                            intervenção, liquidação judicial ou extrajudicial,
                            incorporação, fusão e/o cisão.
                        </p>
                        <p style={{ paddingLeft: "2em" }}>
                            2.7. É obrigação do PARCEIRO informar imediatamente
                            ao PORTAL DA MARIA toda e qualquer alteração nas
                            informações fornecidas de seu cadastro, devendo
                            mantê-las sempre atualizadas.
                        </p>
                        <p style={{ paddingLeft: "2em" }}>
                            2.8. Em nenhuma hipótese será permitida a cessão,
                            venda, aluguel ou outra forma de transferência da
                            conta. Também não se permitirá a manutenção de mais
                            de um cadastro por uma mesma pessoa, ou ainda
                            acriação de novos cadastros por pessoas cujos
                            cadastros originais tenham sido suspensos temporária
                            ou definitivamente por infrações às políticas do
                            PORTAL DA MARIA.
                        </p>
                        <p style={{ paddingLeft: "2em" }}>
                            2.9. O PORTAL DA MARIA não se responsabiliza pela
                            correção dos dados inseridos pelo PARCEIRO. Tendo
                            este a responsabilidade de garantir e responder, em
                            qualquer caso, civil e criminalmente pela
                            veracidade, exatidão e autenticidade, dos dados
                            cadastrados.
                        </p>
                        <p style={{ paddingLeft: "2em" }}>
                            2.10. Caso o PORTAL DA MARIA decida checar a
                            veracidade dos dados cadastrais de um Usuário e se
                            constate haver entre eles dados incorretos ou
                            inverídicos, ou ainda caso o PARCEIRO se furte ou se
                            negue a enviar os documentos requeridos, o PORTAL DA
                            MARIA poderá suspender temporariamente ou
                            definitivamente a conta, sem prejuízo de outras
                            medidas que entender necessárias e oportunas.
                            Havendo a aplicação de quaisquer das sanções acima
                            referidas, automaticamente serão cancelados os
                            anúncios do respectivo Usuário, não lhe assistindo,
                            por essa razão, qualquer indenização ou
                            ressarcimento.
                        </p>
                        <p
                            style={{
                                fontWeight: "bold",
                            }}
                        >
                            3. Modificações dos Termos de Uso e Condições Gerais{" "}
                        </p>
                        <p style={{ paddingLeft: "2em" }}>
                            3.1. O PORTAL DA MARIA poderá alterar, a qualquer
                            tempo, estes Termos de Uso e Condições Gerais,
                            visando seu aprimoramento e melhoria dos serviços
                            prestados.
                        </p>
                        <p style={{ paddingLeft: "2em" }}>
                            3.2. Os novos Termos de Uso e Condições Gerais
                            entrarão em vigor 10 (dez) dias após sua publicação
                            nos Sites. No prazo de 5 (cinco) dias contados a
                            partir da publicação da nova versão, o PARCEIRO
                            deverá comunicar-se por e-mail caso não concorde com
                            os termos alterados. Nesse caso, o vínculo
                            contratual deixará de existir, desde que não haja
                            contas ou dívidas em aberto.
                        </p>
                        <p style={{ paddingLeft: "2em" }}>
                            3.3. Não havendo manifestação no prazo estipulado,
                            entender-se-á que o PARCEIRO aceitou os novos Termos
                            de Uso e Condições Gerais e o contrato continuará
                            vinculando as partes.
                        </p>
                        <p style={{ paddingLeft: "2em" }}>
                            3.4. As alterações não vigorarão em relação às
                            compras já realizadas antes de sua publicação,
                            permanecendo, nestes casos, vigente a redação
                            anterior.
                        </p>
                        <p style={{ paddingLeft: "2em" }}>
                            3.5. É de exclusiva responsabilidade do PARCEIRO
                            manter-se atualizado acessando frequentemente os
                            Termos de Uso e Condições Gerais disponíveis no
                            PORTAL DA MARIA, assegurando de que seus itens podem
                            ser ofertados dentro das condições atualizadas.
                        </p>
                        <p
                            style={{
                                fontWeight: "bold",
                            }}
                        >
                            4. Produtos anunciados
                        </p>
                        <p>
                            O PORTAL DA MARIA poderá alterar, a qualquer tempo,
                            estes Termos e condições gerais, visando seu
                            aprimoramento e melhoria dos serviços prestados.
                        </p>
                        <p style={{ paddingLeft: "2em" }}>
                            4.1. O PARCEIRO poderá anunciar a venda de produtos
                            ou serviços em suas respectivas categorias e
                            subcategorias. Os anúncios devem conter textos,
                            descrições, fotos, vídeos, tempo de entrega e outras
                            informações relevantes do produto ou serviço
                            oferecido, desde que tal prática não viole nenhum
                            dispositivo previsto em lei e nas demais políticas
                            do PORTAL DA MARIA.
                        </p>
                        <p style={{ paddingLeft: "2em" }}>
                            4.2. O produto ou serviço oferecido pelo PARCEIRO
                            deve ser descrito com clareza, contendo todas as
                            características relevantes. Presumir-se-á que,
                            mediante a inclusão do anúncio no PORTAL DA MARIA, o
                            PARCEIRO manifesta a intenção e declara possuir o
                            direito de vender o produto ou oferecer o serviço,
                            além de dispor do produto para entrega imediata.
                        </p>
                        <p style={{ paddingLeft: "2em" }}>
                            4.3. Os anúncios devem conter de forma destacada,
                            caso necessário, a descrição de todos os tributos
                            incidentes sobre a transação em estrita observância
                            à legislação tributária aplicável.
                        </p>
                        <p style={{ paddingLeft: "2em" }}>
                            4.4. Não é permitido fazer publicidade de outros
                            meios de pagamentos que não sejam expressamente
                            disponibilizados nos Sites. Caso o PARCEIRO infrinja
                            o disposto nesta cláusula, o PORTAL DA MARIA poderá
                            editar o anúncio ou solicitar ao PARCEIRO que o
                            faça, ou remover o respectivo anúncio, não sendo,
                            neste caso, estornada qualquer quantia paga ou
                            dívida relativa ao anúncio suprimido.
                        </p>
                        <p style={{ paddingLeft: "2em" }}>
                            4.5. Sendo os produtos comercializados de origem
                            seminovos, reembalados, usados, remanufaturados e/ou
                            remoldados é imprescindível que o PARCEIRO deixe
                            claro essas informações na descrição do produto, e
                            que não haverá detalhes que comprometam sua
                            funcionalidade.
                        </p>
                        <p style={{ paddingLeft: "5em" }}>
                            4.5.1. O PARCEIRO compromete-se a anunciar apenas
                            produtos que estejam em perfeito estado de
                            funcionamento e sem qualquer impedimento que
                            comprometa sua funcionalidade. Mas não se limitando,
                            a problemas com sistemas, desgaste oriundo do tempo
                            e/ou do uso, bloqueio por senha e peças faltantes
                            e/ou danificadas.
                        </p>
                        <p style={{ paddingLeft: "2em" }}>
                            4.6. O produto deve vir acompanhado do apelido e
                            logotipo do PARCEIRO, esse não tendo semelhança com
                            o PORTAL DA MARIA, e/ou insinue que os produtos ou
                            serviços anunciados pertençam ou tenham qualquer
                            vínculo com o PORTAL DA MARIA e para os apelidos
                            considerados ofensivos, estes serão excluídos.
                        </p>
                        <p
                            style={{
                                fontWeight: "bold",
                            }}
                        >
                            5. Processamento de pagamento
                        </p>
                        <p style={{ paddingLeft: "2em" }}>
                            5.1. Todos os pagamentos efetuados serão de
                            incumbência de um Gateway de Pagamento terceirizado,
                            sendo todas as informações de natureza financeira
                            administradas e armazenadas por essas empresas, sem
                            interferência de qualquer natureza por parte do
                            PORTAL DA MARIA. Ao terceirizar esse serviço para
                            empresas reconhecidas e especializadas na área de
                            operações pela internet, pretendemos oferecer e
                            propiciar, além de opções personalizadas de
                            pagamento, maior segurança às transações online.
                        </p>
                        <p style={{ paddingLeft: "2em" }}>
                            5.2. Os prazos para liberação do pagamento serão
                            contados a partir da liberação de crédito do Gateway
                            de Pagamento, sendo ela ainda responsável por todo o
                            armazenamento de dados pessoais e bancários do
                            PARCEIRO.
                        </p>
                        <p
                            style={{
                                fontWeight: "bold",
                            }}
                        >
                            6. Privacidade da Informação
                        </p>
                        <p style={{ paddingLeft: "2em" }}>
                            6.1. Toda informação ou Dado Pessoal do Usuário é
                            armazenado em servidores de grandes provedores.
                            Salvo com relação às informações que são publicadas
                            nos Sites, o PORTAL DA MARIA adotará todas as
                            medidas possíveis para manter a confidencialidade e
                            a segurança das informações sigilosas, porém não se
                            responsabilizará por eventuais prejuízos que sejam
                            decorrentes da divulgação de tais informações por
                            parte de terceiros que utilizem as redes públicas ou
                            a internet, subvertendo os sistemas de segurança
                            para acessar as informações de Usuários.
                        </p>
                        <p style={{ paddingLeft: "2em" }}>
                            6.2. O PARCEIRO expressamente autoriza que suas
                            informações e dados pessoais sejam compartilhados
                            pelo PORTAL DA MARIA com as demais empresas
                            integrantes do grupo econômico, parceiros
                            comerciais, autoridades e pessoas físicas ou
                            jurídicas que aleguem ter sido lesadas.
                        </p>
                        <p style={{ paddingLeft: "2em" }}>
                            6.3. A Política de Privacidade e Confidencialidade
                            da Informação que integra os presentes termos de uso
                            e condições gerais, tratará da coleta e
                            armazenamento dos dados pessoais.
                        </p>
                        <p
                            style={{
                                fontWeight: "bold",
                            }}
                        >
                            7. Obrigações
                        </p>
                        <p
                            style={{
                                fontWeight: "bold",
                            }}
                        >
                            7.1. Das Obrigações do Consumidor
                        </p>
                        <p style={{ paddingLeft: "2em" }}>
                            7.1.1. Os CONSUMIDORES interessados em comprar
                            produtos ou contratar serviços anunciados por
                            PARCEIROS do PORTAL DA MARIA devem manifestar seu
                            interesse no período que o anúncio estiver ativo. Ao
                            manifestar o interesse em algum produto, o
                            CONSUMIDOR obriga-se a compreender às condições de
                            vendas descritas no anúncio.
                        </p>
                        <p style={{ paddingLeft: "2em" }}>
                            7.1.2. Os CONSUMIDORES deverão atentar-se no ato da
                            compra, às informações sobre as qualificações do
                            vendedor, preço, prazo de entrega, descrição do
                            serviço ou produto, eventuais serviços ou produtos
                            adicionais, e forma de pagamento que deverão ser
                            apenas as previstas nos presentes termos.
                        </p>
                        <p style={{ paddingLeft: "2em" }}>
                            7.1.3. Cabe ao CONSUMIDOR a responsabilidade de
                            conferir o produto adquirido e informar qualquer
                            erro, não funcionamento, ou qualquer irregularidade
                            que não esteja de acordo com a descrição fornecida
                            pelo vendedor.
                        </p>
                        <p style={{ paddingLeft: "2em" }}>
                            7.1.4 O PORTAL DA MARIA não se responsabiliza pelas
                            obrigações tributárias que recaiam sobre as
                            atividades dos Usuários da Plataforma. Assim como
                            estabelece a legislação pertinente em vigor, o
                            CONSUMIDOR deverá exigir nota fiscal do vendedor em
                            suas negociações, salvo se o vendedor estiver
                            realizando uma venda eventual e não se enquadre no
                            conceito legal de comerciante/empresário quanto aos
                            bens postos em negociação.
                        </p>
                        <p
                            style={{
                                fontWeight: "bold",
                            }}
                        >
                            7.2. Das obrigações do Parceiro
                        </p>
                        <p style={{ paddingLeft: "2em" }}>
                            7.2.1. O PARCEIRO tem a obrigação de cumprir o prazo
                            descrito no ato da contratação do serviço, sendo que
                            caso não entregue o serviço ou produto no período
                            informado, o CONSUMIDOR poderá cancelar o pedido,
                            tendo resguardado o direito de ressarcimento do
                            valor pago.
                        </p>
                        <p style={{ paddingLeft: "2em" }}>
                            7.2.2. O PARCEIRO deve defender, indenizar e isentar
                            o PORTAL DA MARIA, diretores, administradores,
                            colaboradores, representantes e empregados de
                            quaisquer responsabilidade, obrigações, danos,
                            defeitos, prejuízos, reclamações e despesas, diretas
                            e/ou indiretas resultantes ou decorrentes de: (i)
                            qualquer ação ou omissão do PARCEIRO na prestação
                            dos seus serviços ou comercialização de produtos a
                            seus CONSUMIDORES; (ii) qualquer declaração
                            enganosa, violação de declaração ou garantia ou
                            descumprimento de qualquer avença ou acordo do
                            PARCEIRO; ou (iii) qualquer reivindicação com base
                            em alegada violação pelo PARCEIRO de quaisquer
                            direitos de terceiros.
                        </p>
                        <p style={{ paddingLeft: "2em" }}>
                            7.2.3. Ao PORTAL DA MARIA se reserva o direito de
                            requerer, de acordo com os critérios que estime
                            pertinentes, que determinados produtos ou serviços,
                            bem como PARCEIROS somente anunciem seus bens e
                            serviços na plataforma mediante a utilização dos
                            Serviços de gerenciamento de pagamento da plataforma
                            e/ou outras ferramentas disponibilizadas pelo PORTAL
                            DA MARIA para cobrança do bem vendido e das tarifas
                            pela utilização dos serviços, importando
                            eventualmente no pagamento de tarifas aplicáveis
                            pela utilização destes.
                        </p>
                        <p style={{ paddingLeft: "2em" }}>
                            7.2.4. É de responsabilidade do PARCEIRO o conteúdo
                            por ele gerado, de modo que caso o serviço ofertado,
                            bem como sua descrição, envolvendo vídeos e fotos,
                            trate-se de plágio, viole direitos autorais,
                            propriedade intelectual, o PARCEIRO será única e
                            exclusivamente responsabilizado pelas violações e
                            danos decorrentes, posto que o PORTAL DA MARIA não
                            tem a responsabilidade em verificar se o conteúdo
                            disponibilizado pelo PARCEIRO viola direitos
                            autorais etc.
                        </p>
                        <p
                            style={{
                                fontWeight: "bold",
                            }}
                        >
                            8. Política de devolução
                        </p>
                        <p style={{ paddingLeft: "2em" }}>
                            8.1. Caso o CONSUMIDOR venha desistir do produto
                            adquirido, deverá exercer seu direito dentro do
                            prazo legal e previsto na legislação de Defesa do
                            Consumidor.
                        </p>
                        <p style={{ paddingLeft: "2em" }}>
                            8.2. Sempre que houver trocas e/ou cancelamentos de
                            vendas dos produtos que venham apresentar defeitos
                            e/ou vícios, ou cancelamentos por arrependimento,
                            cabe ao PARCEIRO sempre nos termos do Código de
                            Defesa e Legislação do Consumidor realizar o
                            atendimento bem como a reparação.
                        </p>
                        <p style={{ paddingLeft: "2em" }}>
                            8.3. Caso o PARCEIRO não efetue o custeio da
                            devolução, o PORTAL DA MARIA irá efetuar a retenção
                            de valores a serem recebidos pelo PARCEIRO até que o
                            valor seja suficiente para reembolsar todos os
                            custos despendidos pelo CONSUMIDOR.
                        </p>
                        <p
                            style={{
                                fontWeight: "bold",
                            }}
                        >
                            9. Violação no sistema ou da base de dados
                        </p>
                        <p style={{ paddingLeft: "2em" }}>
                            9.1. Não é permitido a utilização de nenhum
                            dispositivo, software ou outro recurso que venha a
                            interferir nas atividades e operações da PORTAL DA
                            MARIA, bem como nos anúncios, descrições, contas ou
                            seus bancos de dados.
                        </p>
                        <p style={{ paddingLeft: "2em" }}>
                            9.2. Qualquer intromissão, tentativa ou atividade
                            que viole ou contrarie as leis de direito de
                            propriedade intelectual e/ou as proibições
                            estipuladas nestes Termos de Uso e Condições Gerais,
                            tornarão o responsável passível das ações legais
                            pertinentes, bem como das sanções aqui previstas,
                            sendo ainda responsável pelas indenizações por
                            eventuais danos causados.
                        </p>
                        <p style={{ paddingLeft: "2em" }}>
                            9.3. O PORTAL DA MARIA não se responsabiliza por
                            qualquer dano, prejuízo ou perda sofridos pelo
                            Usuário em razão de falhas na internet, no sistema
                            ou no servidor utilizado pelo usuário, decorrentes
                            de condutas de terceiros, caso fortuito ou força
                            maior. O PORTAL DA MARIA também não será responsável
                            por qualquer vírus que possa atacar o equipamento do
                            usuário em decorrência do acesso, utilização ou
                            navegação na internet ou como consequência da
                            transferência de dados, arquivos, imagens, textos,
                            vídeos ou áudio.
                        </p>
                        <p
                            style={{
                                fontWeight: "bold",
                            }}
                        >
                            10. Alcance dos Serviços
                        </p>
                        <p style={{ paddingLeft: "2em" }}>
                            10.1. Estes Termos de Uso e Condições Gerais não
                            geram nenhum contrato de sociedade, mandato,
                            franquia ou relação de trabalho entre o PORTAL DA
                            MARIA e o PARCEIRO.
                        </p>
                        <p style={{ paddingLeft: "2em" }}>
                            10.2. O Usuário manifesta ciência de que o PORTAL DA
                            MARIA não é parte de nenhuma da negociação realizada
                            entre Usuários, nem possui controle algum sobre a
                            existência, qualidade, segurança ou legalidade dos
                            produtos ou serviços anunciados pelos Usuários,
                            sobre a veracidade ou exatidão dos anúncios
                            elaborados pelos usuários e sobre a capacidade dos
                            Usuários para negociar. O PORTAL DA MARIA não pode
                            assegurar o êxito de qualquer negociação realizada
                            entre Usuários, tampouco verificar a identidade ou
                            dos Dados Pessoais dos usuários.
                        </p>
                        <p
                            style={{
                                fontWeight: "bold",
                            }}
                        >
                            11. Pagamentos
                        </p>
                        <p style={{ paddingLeft: "2em" }}>
                            11.1. Será sempre cobrada uma Tarifa de Venda de
                            Produtos que somente será paga ao PORTAL DA MARIA
                            quando a negociação se concretizar. Os valores de
                            comissão do PORTAL DA MARIA serão os seguintes:
                        </p>
                        <div className="allignerCenter">
                            <Table bordered className="w-50">
                                <thead>
                                    <tr>
                                        <th
                                            width="100%"
                                            colSpan="2"
                                            style={{
                                                backgroundColor: "lightgrey",
                                            }}
                                        >
                                            TABELA DE COMISSÃO DO PORTAL DA
                                            MARIA
                                        </th>
                                    </tr>
                                    <tr>
                                        <th
                                            width="50%"
                                            style={{
                                                backgroundColor: "lightgrey",
                                            }}
                                        >
                                            CATEGORIA{" "}
                                        </th>
                                        <th
                                            width="50%"
                                            style={{
                                                backgroundColor: "lightgrey",
                                            }}
                                        >
                                            VAREJO
                                        </th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td
                                            className="text-capitalize"
                                            width="50%"
                                        >
                                            Tecidos
                                        </td>
                                        <td width="50%">15%</td>
                                    </tr>
                                    <tr>
                                        <td
                                            className="text-capitalize"
                                            width="50%"
                                        >
                                            Artesanato pronto
                                        </td>
                                        <td width="50%">15%</td>
                                    </tr>
                                    <tr>
                                        <td
                                            className="text-capitalize"
                                            width="50%"
                                        >
                                            Roupa pronta
                                        </td>
                                        <td width="50%">15%</td>
                                    </tr>
                                    <tr>
                                        <td
                                            className="text-capitalize"
                                            width="50%"
                                        >
                                            Serviços
                                        </td>
                                        <td width="50%">15%</td>
                                    </tr>
                                    <tr>
                                        <td
                                            className="text-capitalize"
                                            width="50%"
                                        >
                                            Calçados
                                        </td>
                                        <td width="50%">15%</td>
                                    </tr>
                                </tbody>
                            </Table>
                        </div>
                        <p style={{ paddingLeft: "2em" }}>
                            11.2. Os valores serão liberados aos PARCEIROS
                            apenas após a confirmação de recebimento do produto
                            e/ou serviços por parte do CONSUMIDOR.
                        </p>
                        <p style={{ paddingLeft: "2em" }}>
                            11.3. O PORTAL DA MARIA se reserva o direito de
                            tomar as medidas judiciais e extrajudiciais
                            pertinentes para receber os valores devidos.
                        </p>
                        <p
                            style={{
                                fontWeight: "bold",
                            }}
                        >
                            12. Propriedade Intelectual
                        </p>
                        <p style={{ paddingLeft: "2em" }}>
                            12.1. O uso comercial da expressão "PORTAL DA MARIA"
                            como marca, nome empresarial ou nome de domínio, bem
                            como os conteúdos das telas relativas aos serviços
                            do PORTAL DA MARIA assim como os bancos de dados,
                            redes, arquivos que permitem ao Usuário acessar e
                            usar a sua Conta são propriedade do PORTAL DA MARIA
                            e estão protegidos pelas leis e tratados
                            internacionais de direito autoral, marcas, patentes,
                            modelos e desenhos industriais. O uso indevido e a
                            reprodução total ou parcial dos referidos conteúdos
                            são proibidos, salvo a autorização prévia e expressa
                            por escrito do PORTAL DA MARIA.
                        </p>
                        <p style={{ paddingLeft: "2em" }}>
                            12.2. O PARCEIRO declara e garante que é titular,
                            possui a devida autorização do(s) titular(es) de
                            direito(s) de propriedade intelectual ou que, de
                            outra forma, pode anunciar no PORTAL DA MARIA,
                            oferecer produtos e/ou serviços anunciados ou
                            declarar-se loja oficial de determinada marca, sendo
                            o único responsável pelo conteúdo das suas
                            publicações.
                        </p>
                        <p style={{ paddingLeft: "2em" }}>
                            12.3. O PARCEIRO assume total responsabilidade por
                            todos os prejuízos, diretos e indiretos, inclusive
                            indenizações, lucros cessantes, honorários
                            advocatícios e demais encargos judiciais e
                            extrajudiciais que o PORTAL DA MARIA seja obrigado a
                            incorrer em virtude de ato ou omissão do PARCEIRO.
                        </p>
                        <p style={{ paddingLeft: "2em" }}>
                            12.4. Se o PORTAL DA MARIA receber alguma reclamação
                            ou questionamento de terceiros (por exemplo, dos
                            titulares de marca), o PORTAL DA MARIA poderá
                            remover o seu anúncio e aplicar as sanções cabíveis.
                        </p>
                        <p
                            style={{
                                fontWeight: "bold",
                            }}
                        >
                            13. Da ausência de vínculo empregatício entre as
                            partes
                        </p>
                        <p style={{ paddingLeft: "2em" }}>
                            13.1. Os PARCEIROS estão cientes que na presente
                            relação não há qualquer vínculo empregatício entre
                            as partes, posto que ausente todos os requisitos,
                            quais sejam pessoalidade, subordinação, onerosidade
                            e habitualidade.
                        </p>
                        <p
                            style={{
                                fontWeight: "bold",
                            }}
                        >
                            14. Da entrega
                        </p>
                        <p style={{ paddingLeft: "2em" }}>
                            14.1. O PORTAL DA MARIA não se responsabilizará pela
                            entrega dos produtos/peças adquiridas, lembrando que
                            o PORTAL DA MARIA é mero intermediador entre
                            usuários fornecedores e compradores.
                        </p>
                        <p style={{ paddingLeft: "2em" }}>
                            14.2. A entrega ficará a cargo de terceiro
                            (parceiro/intermediador) que será indicado logo na
                            finalização da compra através da plataforma do
                            PORTAL DA MARIA.
                        </p>
                        <p style={{ paddingLeft: "2em" }}>
                            14.3. O CONSUMIDOR terá a opção de escolher se a
                            entrega ocorrerá através do terceiro indicado pelo
                            site, devendo se atentar aos valores cobrados pelo
                            frete, ou se a entrega ocorrerá de outra forma.
                        </p>
                        <p style={{ paddingLeft: "2em" }}>
                            14.4. A taxa de entrega sofrerá variações de acordo
                            com o peso, tamanho e o deslocamento a ser
                            realizado.
                        </p>
                        <p
                            style={{
                                fontWeight: "bold",
                            }}
                        >
                            15. Disposições Gerais
                        </p>
                        <p style={{ paddingLeft: "2em" }}>
                            15.1. Todos os itens destes Termos de Uso e
                            Condições Gerais são regidos pelas leis vigentes na
                            República Federativa do Brasil. Para todos os
                            assuntos referentes à interpretação, ao cumprimento
                            ou qualquer outro questionamento relacionado a estes
                            Termos de Uso e Condições Gerais, as partes
                            concordam em se submeter ao Foro Comarca de São
                            Paulo, Estado de São Paulo.
                        </p>
                        <p>Atualizado no dia 30/09/2020 às 15:00 horas.</p>
                    </Col>
                </Row>
            </Main>
        </>
    )
}

export default TermsOfUse
