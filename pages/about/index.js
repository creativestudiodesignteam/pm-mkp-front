import React from "react"
import Head from "next/head"
import Main from "../../src/components/Main"
import { LinkTo } from "../../src/components"
import { Col, Row } from "reactstrap"
import Img from "../../src/components/Img"

const About = () => {
    return (
        <>
            <Head>
                <title>Portal da Maria | Sobre o portal</title>
                <link rel="icon" href={"/favicon.ico"} />
                <meta name="format-detection" content="telephone=no" />
                <meta
                    httpEquiv="Content-Type"
                    content="text/html; charset=UTF-8"
                />
                <meta
                    name="viewport"
                    content="width=device-width, initial-scale=1, shrink-to-fit=no"
                />
                <meta httpEquiv="X-UA-Compatible" content="IE=edge" />
                <link
                    href="https://fonts.googleapis.com/css?family=Montserrat%7CRoboto:100,300,400,500,700,900%7CRoboto+Condensed:100,300,400,500,700"
                    rel="stylesheet"
                />
            </Head>
            <Main
                _BreadCrumb={[
                    {
                        node_text: "Sobre",
                        href: "/about",
                        query: {},
                    },
                ]}
            >
                <Row>
                    <Col xs="12">
                        <Img
                            style={{ width: "100%", cursor: "pointer" }}
                            image_name_with_extension="conheça-nossa-história.png"
                            alt="Product Image . . ."
                        />
                    </Col>
                    <Col
                        xs="12"
                        style={{
                            fontWeight: 400,
                            fontSize: "18px",
                            lineHeight: "35px",
                            margin: "0 auto",
                            float: "none",
                            textAlign: "justify",
                            marginTop: "3rem",
                        }}
                    >
                        <p>
                            O Portal da Maria foi empreendimento a muito tempo
                            sonhado por uma mulher empreendedora e a frente de
                            seu tempo, acreditando ser possível juntar pessoas
                            jurídicas e físicas em um ambiente de utilização
                            simples com uma plataforma segura, garantindo a
                            ambos total tranquilidade nas suas transações
                            comerciais. Nunca se limitando ao habitual, tornou
                            possível com essa ferramenta a base de tecnologias
                            de última geração, com que outras pessoas realizem
                            seus sonhos empreendendo com custos menores e
                            tornando-se donos do seu próprio negócio.
                        </p>
                        <h2 className="h1">
                            Você já pensou em ter um serviço diferenciado de
                            compras? E ainda ter uma plataforma para vender seus
                            produtos com rapidez e com muita segurança?
                        </h2>
                        <p>
                            O Portal da Maria é um marketplace que nasceu com um
                            propósito: transformar experiências de compras e
                            vendas pela internet.
                        </p>
                        <p>
                            O Portal da Maria é uma plataforma completa, onde
                            vendedores de todos os tipos de produtos se
                            cadastram e são conectados, diariamente, com
                            milhares de potenciais clientes de seus produtos. Ou
                            seja, é um meio tecnológico, seguro e eficaz de
                            transacionar compras e vendas online.
                        </p>
                        <p>Mas antes de começar quero te fazer uma pergunta,</p>
                        <h2 className="h1">
                            Você sabe o que é um marketplace? Se não, vamos te
                            explicar:
                        </h2>
                        <p>
                            O Marketplace é um espaço virtual onde acontecem
                            várias transações comerciais entre Vendedores e
                            Compradores, todos com um único propósito, fazer o
                            melhor negócio possível durante 24 horas por dia, 7
                            dias por semana com vendas e compras constantes.
                        </p>
                        <p>
                            - Você pode vender seus produtos a qualquer dia e
                            hora. Ou,
                        </p>
                        <p>
                            - Você pode comprar o que quiser, quando quiser,
                            apenas com alguns cliques!
                        </p>
                        <p>
                            {" "}
                            E é aí que o Portal da Maria aparece unindo
                            compradores e vendedores, oferecendo segurança
                            nessas transações com suporte tecnológico de última
                            geração.
                        </p>
                        <p>
                            <h2 className="text_dark_2 bold h2">
                                <u>Nossos diferenciais.</u>
                            </h2>
                            <ul className="about" style={{ listStyle: "none" }}>
                                <li>Segurança safe2pay;</li>
                                <li>
                                    Seguro para riscos digitais e profissionais
                                    em tempo integral monitorando o
                                    comportamento do site;
                                </li>
                                <li>Menores custos com divulgação;</li>
                                <li>
                                    Visibilidades para seus produtos 100%
                                    garantida;
                                </li>
                                <li>Seus produtos bem posicionados;</li>
                                <li>Segurança na transação de pagamento;</li>
                                <li>Mais visibilidade para o seu negócio;</li>
                                <li>Envio para todo o Brasil.</li>
                            </ul>
                        </p>
                        <h2 style={{ paddingLeft: "4rem" }} className="h1">
                            <u className="bold">Agora que já conhece!</u>
                        </h2>
                        <p>Descubra como suas vendas podem aumentar!</p>
                        <p>E como você vai vender até dormindo!</p>
                        <p>
                            Cadastre-se{" "}
                            <LinkTo _href="/login">
                                <u className="text_blue">Clique aqui.</u>
                            </LinkTo>
                        </p>
                    </Col>
                </Row>
            </Main>
        </>
    )
}

export default About
