import React, { useEffect } from "react"
import Head from "next/head"
import Main from "../../src/components/Main"
import ClipLoader from "react-spinners/ClipLoader"
import { userByToken, __storage } from "../../src/assets/auth"
import { useRouter } from "next/router"
import { awaitable } from "../../src/assets/helpers"
import { dispatchFunctions } from "../../src/store"

const Email = ({ context, query, dispatch, ...props }) => {
    const router = useRouter()

    useEffect(() => {
        const _token =
            context?.token ||
            __storage(
                (storage) => storage.getItem("token"),
                (error) => {
                    console.log(
                        "tryInitializeDataStore _tryInitializeDataStore",
                        error
                    )
                    return ""
                },
                true
            )
        const user = userByToken(_token)
        setTimeout(() => {
            if (!!user) {
                // redireciona para pagina de vendas com token na url
                awaitable(async () => {
                    dispatch(
                        dispatchFunctions.login({
                            user,
                            token: _token,
                        })
                    )
                    await router.push(
                        "/account/[feature]",
                        `/account/request?token=${_token}`
                    )
                })
            } else {
                router.push("/login")
            }
        }, 500)
    }, [context.token])
    return (
        <>
            <Head>
                <title>Portal da Maria | Redirecionamento</title>
                <link rel="icon" href={"/favicon.ico"} />
                <meta name="format-detection" content="telephone=no" />
                <meta
                    httpEquiv="Content-Type"
                    content="text/html; charset=UTF-8"
                />
                <meta
                    name="viewport"
                    content="width=device-width, initial-scale=1, shrink-to-fit=no"
                />
                <meta httpEquiv="X-UA-Compatible" content="IE=edge" />
                <link
                    href="https://fonts.googleapis.com/css?family=Montserrat%7CRoboto:100,300,400,500,700,900%7CRoboto+Condensed:100,300,400,500,700"
                    rel="stylesheet"
                />
            </Head>
            <Main>
                <div className="spinnerLoader">
                    <ClipLoader
                        size={150}
                        color={"var(--red-1)"}
                        loading={true}
                    />
                </div>
            </Main>
        </>
    )
}

export const getServerSideProps = async ({ query, res }) => {
    return { props: { query } }
}
export default Email
