import React from "react"
import Head from "next/head"
import Main from "../../src/components/Main"
import { Row, Col, Table } from "reactstrap"

const PrivacyPolicy = () => {
    return (
        <>
            <Head>
                <title>Portal da Maria | Política de privacidade</title>
                <link rel="icon" href={"/favicon.ico"} />
                <meta name="format-detection" content="telephone=no" />
                <meta
                    httpEquiv="Content-Type"
                    content="text/html; charset=UTF-8"
                />
                <meta
                    name="viewport"
                    content="width=device-width, initial-scale=1, shrink-to-fit=no"
                />
                <meta httpEquiv="X-UA-Compatible" content="IE=edge" />
                <link
                    href="https://fonts.googleapis.com/css?family=Montserrat%7CRoboto:100,300,400,500,700,900%7CRoboto+Condensed:100,300,400,500,700"
                    rel="stylesheet"
                />
            </Head>
            <Main
                _BreadCrumb={[
                    {
                        node_text: "Política de privacidade",
                        href: "/privacypolicy",
                        query: {},
                    },
                ]}
            >
                <Row>
                    <Col
                        xs="12"
                        style={{
                            fontWeight: 300,
                            fontSize: "18px",
                            lineHeight: "35px",
                            margin: "0 auto",
                            float: "none",
                            textAlign: "justify",
                            marginTop: "3rem",
                        }}
                    >
                        <h1>Política de privacidade </h1>
                        <p>
                            O presente documento, doravante denominado “Política
                            de Privacidade”, detalha a forma com que a PORTAL DA
                            MARIA SERVIÇOS LTDA , pessoa jurídica de direito
                            privado inscrita no CNPJ/MF sob o no
                            37.917.490/0001-03 com sede na Rua Dr. Virgílio do
                            Nascimento no 445, Brás, São Paulo/SP, CEP
                            03027-020, coleta, armazena e utiliza os dados
                            pessoais de usuários dos seus produtos e serviços,
                            bem como de seus parceiros comerciais.
                        </p>
                        <p>
                            Ao concordarem com as normas previstas na Política
                            de Privacidade terá acesso a utilização dos produtos
                            e serviços disponibilizados pelo PORTAL DA MARIA.
                        </p>
                        <p style={{ paddingLeft: "5em" }}>1. DISPOSIÇÃO</p>
                        <p style={{ paddingLeft: "6em" }}>
                            A Disposição para utilização dos dados de
                            PARCEIROS/CONSUMIDORES no PORTAL DA MARIA se deve
                            tanto para a comercialização e entrega de produtos,
                            bem como a melhoria e criação de serviços, tais
                            como:
                        </p>
                        <p style={{ paddingLeft: "6em" }}>
                            a) Compra, faturamento e entrega de produtos de
                            nossos parceiros de venda, pagamento, quanto do
                            PORTAL DA MARIA, essas informações se fazem
                            necessárias para efetivação de pagamentos,
                            independentemente do modo, entrega de produtos e
                            faturamento.
                        </p>
                        <p style={{ paddingLeft: "6em" }}>
                            b) Para melhoria do desempenho, disposição de
                            conteúdo e alteração de jornadas, utilizamos os
                            dados dos PARCEIROS/CONSUMIDORES.
                        </p>
                        <p style={{ paddingLeft: "6em" }}>
                            c) Utilizamos as preferências do CONSUMIDOR como,
                            por exemplo, tipo de produto mais visitado, para
                            facilitar suas próximas visitas e tornando seu
                            acesso mais fácil.
                        </p>
                        <p style={{ paddingLeft: "6em" }}>
                            d) Para o cumprimento de obrigações legais podemos
                            usar os dados tanto para cumprir legislações locais
                            quanto para cumprir processos ou outras necessidades
                            jurídicas.
                        </p>
                        <p style={{ paddingLeft: "6em" }}>
                            e) Para proteção ao crédito, antifraude e avaliação
                            de riscos usamos os dados dos PARCEIROS/CONSUMIDORES
                            para agregar valor à proteção dos nossos serviços,
                            tornando o ambiente mais seguro.
                        </p>
                        <p style={{ paddingLeft: "6em" }}>
                            f) O PORTAL DA MARIA também pode pedir consentimento
                            para fins específicos, informando claramente a
                            finalidade.
                        </p>
                        <p style={{ paddingLeft: "5em" }}>
                            2. COLETA DE DADOS DE USUÁRIOS
                        </p>
                        <p style={{ paddingLeft: "6em" }}>
                            O PORTAL DA MARIA exclusivamente coleta dados de
                            usuários para fins legítimos e em completa
                            conformidade com a legislação e padrões
                            estabelecidos por entidades fiscalizadoras de
                            práticas do mercado.
                        </p>
                        <p style={{ paddingLeft: "6em" }}>
                            2.1. Podemos realizar a coleta de dados das
                            seguintes maneiras:
                        </p>
                        <p style={{ paddingLeft: "8em" }}>
                            a) Mediante preenchimento de formulários pelo
                            próprio usuário.
                        </p>
                        <p style={{ paddingLeft: "8em" }}>
                            b) Via software de forma automatizadas e armazenadas
                            no lado do usuário e, também, comunicadas para os
                            nossos FORNECEDORES, podendo ser armazenadas e
                            processadas em ambos os lados da comunicação.
                        </p>
                        <p style={{ paddingLeft: "5em" }}>
                            3. ARMAZENAMENTO DE DADOS
                        </p>
                        <p style={{ paddingLeft: "6em" }}>
                            Todos os Dados de Usuários coletados são armazenados
                            em servidores e bancos de dados do PORTAL DA MARIA
                            ou de parceiros da mesma (nacionais ou
                            internacionais), todos em plena conformidade com as
                            boas práticas de mercado e utilizando criptografia
                            de canal para o tráfego dessas informações em redes
                            públicas (internet).
                        </p>
                        <p style={{ paddingLeft: "6em" }}>
                            3.1. Os dados de usuário serão armazenados até:
                        </p>
                        <p style={{ paddingLeft: "8em" }}>
                            a) Que o usuário solicite sua exclusão.
                        </p>
                        <p style={{ paddingLeft: "8em" }}>
                            b) Que o próprio PORTAL DA MARIA decida em não
                            utilizar mais os dados.
                        </p>
                        <p style={{ paddingLeft: "8em" }}>
                            c) Que a exclusão seja determinada por autoridade
                            governamental (administrativa ou Judiciária).
                        </p>
                        <p style={{ paddingLeft: "5em" }}>
                            4. TRATAMENTO E USO DOS DADOS
                        </p>
                        <p style={{ paddingLeft: "6em" }}>
                            O PORTAL DA MARIA, realiza operação de tratamento
                            dos dados do usuário em plena conformidade com os
                            limites previstos na Lei 13.709 de 2018 e demais
                            regulamentações vigentes.
                        </p>
                        <p style={{ paddingLeft: "6em" }}>
                            4.1. Todos os dados de usuários são utilizados pelo
                            PORTAL DA MARIA e compartilhado com parceiros
                            nacionais ou internacionais. Dentre elas:
                        </p>
                        <p style={{ paddingLeft: "8em" }}>
                            a) Anônimo, quando destituídas dos dados pessoais
                            que permitem a individualização do Usuário (exemplo:
                            para elaboração de estudos e relatórios gerenciais);
                        </p>
                        <p style={{ paddingLeft: "8em" }}>
                            b) Criptografada, quando os dados devem ser
                            transmitidos de forma segura e de acordo com padrões
                            de criptografia (ex.: captura e transmissão de dados
                            de pagamento);
                        </p>
                        <p style={{ paddingLeft: "8em" }}>
                            c) Não anônimo, quando desnecessária a criptografia
                            e os dados pessoais devem ser utilizados e/ou
                            compartilhado com parceiros, nos termos das
                            permissões previstas no item 5 abaixo.
                        </p>
                        <p style={{ paddingLeft: "6em" }}>
                            4.2. O PORTAL DA MARIA em caso de solicitação das
                            autoridades policiais e judiciais, bem como a
                            própria pessoa proprietária dos dados, poderá
                            compartilhar os dados de usuários quando notificado
                            ou a seu próprio critério, quando entender
                            necessário o compartilhamento para cooperar com
                            investigações de fraude (utilização indevida de
                            dados pessoais e financeiros de terceiros) ou
                            qualquer outro tipo de atividade ilegal pelos
                            usuários.
                        </p>
                        <p style={{ paddingLeft: "5em" }}>
                            5. DA PERMISSÃO DO USUÁRIO
                        </p>
                        <p style={{ paddingLeft: "6em" }}>
                            Ao cadastrar como PARCEIRO/CONSUMIDOR, o usuário
                            concorda que seus dados:
                        </p>
                        <p style={{ paddingLeft: "8em" }}>
                            a) Sejam coletados nas formas previstas na cláusula
                            1 deste termo;
                        </p>
                        <p style={{ paddingLeft: "8em" }}>
                            b) Sejam utilizados para personalizar sua navegação
                            nos sites do PORTAL DA MARIA (ex.: otimização dos
                            resultados das buscas; visualização da listagem de
                            produtos; visualização de banners promocionais);
                        </p>
                        <p style={{ paddingLeft: "8em" }}>
                            c) Sejam incluídos em listas de recebimento de
                            comunicações do PORTAL DA MARIA (via e-mail;
                            mensagens de texto via SMS ou Whatsapp e afins),
                            incluindo ofertas e promoções.
                        </p>
                        <p style={{ paddingLeft: "8em" }}>
                            d) Sejam utilizados sem identificação individual
                            para elaboração de estudos estatísticos e relatórios
                            gerenciais;
                        </p>
                        <p style={{ paddingLeft: "8em" }}>
                            e) Sejam armazenados em bancos seguros e utilizados
                            para otimização de novas compras (ex.: parte dos
                            dados de cartões de crédito são salvas para que o
                            Usuário não precise inseri-los novamente a cada
                            compra que realizar no site);
                        </p>
                        <p style={{ paddingLeft: "8em" }}>
                            f) Sejam compartilhados de forma anônima com
                            parceiros (nacionais ou internacionais) do PORTAL DA
                            MARIA para fins de estudos estatísticos, com sua
                            segurança e conformidade de uso de acordo com os
                            contratos estabelecidos entre o PORTAL DA MARIA e
                            seus parceiros;
                        </p>
                        <p style={{ paddingLeft: "8em" }}>
                            g) Sejam compartilhados com parceiros (nacionais ou
                            internacionais) do PORTAL DA MARIA para cumprimento
                            de uma compra (ex.: os dados financeiros são
                            transmitidos ao banco emissor para aprovação do
                            pagamento; os dados de endereço e contato de
                            telefone são compartilhados com transportadoras que
                            realizam a entrega do produto);
                        </p>
                        <p style={{ paddingLeft: "8em" }}>
                            h) Sejam compartilhados com parceiros (nacionais ou
                            internacionais) do PORTAL DA MARIA para fins de
                            consulta a bases de dados com fins de proteção ao
                            crédito tais como: SERASA, SPC, SCR – BACEN;
                        </p>
                        <p style={{ paddingLeft: "8em" }}>
                            i) Sejam compartilhados com parceiros (nacionais ou
                            internacionais) do PORTAL DA MAIRA para fins de
                            oferecimento de produtos e serviços desses parceiros
                            nas plataformas do PORTAL DA MARIA (exemplos:
                            oferecimento de opções alternativas de pagamento).
                        </p>
                        <p style={{ paddingLeft: "5em" }}>6. DIREITOS</p>
                        <p style={{ paddingLeft: "6em" }}>
                            6.1. É direito do usuário:
                        </p>
                        <p style={{ paddingLeft: "8em" }}>
                            a) Excluir, parcial ou totalmente, os seus dados de
                            usuário dos bancos de dados do PORTAL DA MARIA,
                            mediante opção na área do cliente do site
                            www.portaldamaria.com.br, ou solicitação direta à
                            respectiva área comercial, em caso de parceiros
                            comerciais, salvos os dados protegidos por lei de
                            maior soberania ou necessidades jurídicas;
                        </p>
                        <p style={{ paddingLeft: "8em" }}>
                            b) Excluir os Cookies, mediante procedimento
                            realizado por sua conta e risco em seus próprios
                            dispositivos;
                        </p>
                        <p style={{ paddingLeft: "8em" }}>
                            c) Ser removido das listas de recebimento de ofertas
                            e promoções mediante link de remoção contido nas
                            próprias mensagens enviadas;
                        </p>
                        <p style={{ paddingLeft: "8em" }}>
                            d) Atualizar dados, mediante alteração dos mesmos na
                            área do cliente do site www.portaldamaria.com.br;
                        </p>
                        <p style={{ paddingLeft: "8em" }}>
                            e) Não finalizar o cadastro nas plataformas do
                            PORTAL DA MARIA caso não concorde com quaisquer
                            termos previstos na presente Política.
                        </p>
                        <p style={{ paddingLeft: "5em" }}>
                            7. GERENCIAMENTO DE COOKIES
                        </p>
                        <p style={{ paddingLeft: "6em" }}>
                            Cookies são arquivos criados pelos websites que você
                            visita. Eles tornam a experiência online dos
                            usuários mais fácil, economizando informações de
                            navegação. Com os cookies, os sites podem manter os
                            usuários conectados, lembrando-os suas preferências
                            do site e fornecendo conteúdo relevante localmente.
                        </p>
                        <p style={{ paddingLeft: "6em" }}>
                            Os cookies são utilizados geralmente para:
                        </p>
                        <p style={{ paddingLeft: "8em" }}>
                            a) Acompanhar suas preferências para enviar somente
                            anúncios de seu interesse;
                        </p>
                        <p style={{ paddingLeft: "8em" }}>
                            b) Acompanhar os itens armazenados no seu carrinho
                            de compras;
                        </p>
                        <p style={{ paddingLeft: "8em" }}>
                            c) Realização de pesquisas e diagnósticos para
                            melhorar o conteúdo, produtos e serviços;
                        </p>
                        <p style={{ paddingLeft: "8em" }}>
                            d) Impedir atividades fraudulentas;
                        </p>
                        <p style={{ paddingLeft: "8em" }}>
                            e) Melhorar a segurança.
                        </p>
                        <p style={{ paddingLeft: "6em" }}>
                            O usuário pode bloquear ou rejeitar nossos cookies,
                            e como consequência não poderá adicionar itens ao
                            seu carrinho de compras, prosseguir para o checkout
                            ou usar nossos produtos e serviços dentro da área
                            autenticada.
                        </p>
                        <p style={{ paddingLeft: "5em" }}>
                            8. NOSSAS RESPONSABILIDADES
                        </p>
                        <p style={{ paddingLeft: "6em" }}>
                            8.1. O PORTAL DA MARIA se compromete a:
                        </p>
                        <p style={{ paddingLeft: "8em" }}>
                            a) além de cumprir com padrões de segurança exigidos
                            pelas regulamentações, empenhar seus melhores
                            esforços para manter a privacidade dos dados de
                            usuários, com a utilização de tecnologia e
                            protocolos de segurança da informação, observado o
                            estado da técnica disponível conforme padrões de
                            mercado;
                        </p>
                        <p style={{ paddingLeft: "8em" }}>
                            b) excluir e/ou atualizar os Dados de Usuário após
                            solicitação conforme item 5.1 alíneas “a, c e d”
                            acima;
                        </p>
                        <p style={{ paddingLeft: "8em" }}>
                            c) somente firmar parcerias com terceiros que
                            envolvam compartilhamento de dados caso estes
                            terceiros também se comprometam a cumprir com todos
                            os padrões e obrigações exigidos por lei no que
                            concerne à coleta, armazenamento e tratamento de
                            dados;
                        </p>
                        <p style={{ paddingLeft: "8em" }}>
                            d) relatar às autoridades competentes e aos Usuários
                            quaisquer eventos que envolvam exposição indevida
                            dos Dados de Usuários, bem como empenhar seus
                            melhores esforços para evitar prejuízos aos Usuários
                            em razão de tais eventos;
                        </p>
                        <p style={{ paddingLeft: "6em" }}>
                            8.2. O PORTAL DA MARIA não se responsabiliza por
                            perdas ou danos sofridos por Usuários em razão de
                            exposição indevida de seus Dados que tenha ocorrido
                            por qualquer outra razão senão o dolo ou
                            descumprimento comprovado da legislação.
                        </p>
                        <p style={{ paddingLeft: "6em" }}>
                            8.3. A presente Política de Privacidade poderá ser
                            alterada unilateralmente pelo PORTAL DA MARIA a
                            qualquer momento. Os novos termos entraram em vigor
                            10(dez) dias após sua publicação, cabe ao usuário
                            manter-se atualizado as novas versões. Qualquer
                            acesso realizado por usuários após a alteração da
                            Política de Privacidade implicará em aceite tácito
                            das novas condições previstas.
                        </p>
                        <p style={{ paddingLeft: "5em" }}>
                            9. USO POR MENORES DE 18 ANOS
                        </p>
                        <p style={{ paddingLeft: "6em" }}>
                            Os serviços do PORTAL DA MARIA podem oferecer venda
                            de produtos direcionados ao público infantil ou
                            adolescente, todo processo de compra deve ser
                            realizado por um adulto (Maior de 18 anos), sendo um
                            dos pais ou responsável legal.
                        </p>
                        <p style={{ paddingLeft: "5em" }}>10. CONTATO</p>
                        <p style={{ paddingLeft: "6em" }}>
                            Todo contato relativo à privacidade e proteção de
                            dados pode ser efetivado através da comunicação
                            direta por e-mail. Para isso, deve ser utilizado o
                            e-mail atendimento@portaldamaria.com
                        </p>
                        <p style={{ paddingLeft: "6em" }}>
                            Os titulares dos dados, de acordo com a Lei Geral de
                            Proteção de Dados Pessoais, podem exercer os seus
                            direitos por meio de:
                        </p>
                        <p style={{ paddingLeft: "8em" }}>
                            a) Confirmação da existência de tratamento;
                        </p>
                        <p style={{ paddingLeft: "8em" }}>
                            b) Acesso aos dados;
                        </p>
                        <p style={{ paddingLeft: "8em" }}>
                            c) Correção de dados incompletos, inexatos ou
                            desatualizados;
                        </p>
                        <p style={{ paddingLeft: "8em" }}>
                            d) Anonimização, bloqueio ou eliminação de dados
                            desnecessários, excessivos ou tratados em
                            desconformidade com o disposto nesta Lei;
                        </p>
                        <p style={{ paddingLeft: "8em" }}>
                            e) Portabilidade dos dados a outro fornecedor de
                            serviço ou produto, mediante requisição expressa, de
                            acordo com a regulamentação da autoridade nacional,
                            observados os segredos comercial e industrial;
                        </p>
                        <p style={{ paddingLeft: "8em" }}>
                            f) Eliminação dos dados pessoais tratados com o
                            consentimento do titular;
                        </p>
                        <p style={{ paddingLeft: "8em" }}>
                            g) Informação das entidades públicas e privadas com
                            as quais o controlador realizou uso compartilhado de
                            dados;
                        </p>
                        <p style={{ paddingLeft: "8em" }}>
                            h) Informação sobre a possibilidade de não fornecer
                            consentimento e sobre as consequências da negativa;
                        </p>
                        <p style={{ paddingLeft: "8em" }}>
                            i) Revogação do consentimento.
                        </p>
                        <p>Atualizado no dia 30/09/2020 às 15:00 horas.</p>
                    </Col>
                </Row>
            </Main>
        </>
    )
}

export default PrivacyPolicy
