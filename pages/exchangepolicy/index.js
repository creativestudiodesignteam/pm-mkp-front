import React from "react"
import Head from "next/head"
import Main from "../../src/components/Main"
import { Row, Col, Table } from "reactstrap"

const ExchangePolicy = () => {
    return (
        <>
            <Head>
                <title>Portal da Maria | Política de trocas e devoluções</title>
                <link rel="icon" href={"/favicon.ico"} />
                <meta name="format-detection" content="telephone=no" />
                <meta
                    httpEquiv="Content-Type"
                    content="text/html; charset=UTF-8"
                />
                <meta
                    name="viewport"
                    content="width=device-width, initial-scale=1, shrink-to-fit=no"
                />
                <meta httpEquiv="X-UA-Compatible" content="IE=edge" />
                <link
                    href="https://fonts.googleapis.com/css?family=Montserrat%7CRoboto:100,300,400,500,700,900%7CRoboto+Condensed:100,300,400,500,700"
                    rel="stylesheet"
                />
            </Head>
            <Main
                _BreadCrumb={[
                    {
                        node_text: "Política de trocas",
                        href: "/exchangepolicy",
                        query: {},
                    },
                ]}
            >
                <Row>
                    <Col
                        xs="12"
                        style={{
                            fontWeight: 300,
                            fontSize: "18px",
                            lineHeight: "35px",
                            margin: "0 auto",
                            float: "none",
                            textAlign: "justify",
                            marginTop: "3rem",
                        }}
                    >
                        <h1>Termo de troca e devoluções </h1>
                        <p>
                            PORTAL DA MARIA SERVIÇOS LTDA, pessoa jurídica de
                            direito privado inscrita no CNPJ/MF sob o no
                            37.917.490/0001-03 com sede na Rua Dr. Virgílio do
                            Nascimento no 445, Brás, São Paulo/SP, CEP
                            03027-020, empresa varejista que atua através do
                            comércio varejista online, em cumprimento ao Decreto
                            no 7962/2013, apresenta abaixo as condições gerais
                            para troca e devoluções de compras realizadas
                            através do Portal da Maria.
                        </p>
                        <p style={{ paddingLeft: "5em" }}>
                            1) O objetivo deste termo é apresentar para os
                            clientes do Portal da Maria as condições gerais para
                            a troca e devoluções de compras realizadas através
                            do site www.portaldamaria.com.br.
                        </p>
                        <p style={{ paddingLeft: "5em" }}>
                            2) Caso o cliente deseje trocar ou desistir da
                            compra após a entrega, terá o direito de devolver
                            o(s) produto(s) e receber a restituição do valor
                            pago ou trocar pelo item desejado.
                        </p>
                        <p style={{ paddingLeft: "5em" }}>
                            3) O prazo para desistência da compra é de 7 (sete)
                            dias corridos a contar do dia seguinte do
                            recebimento do produto, conforme estipula o artigo
                            49 Código de Defesa do Consumidor.
                        </p>
                        <p style={{ paddingLeft: "5em" }}>
                            4) A troca ou cancelamento somente será realizada
                            caso o produto esteja em sua embalagem original, sem
                            indícios de mau uso, sem violação do lacre original
                            do fabricante, acompanhado de nota fiscal, manual e
                            de todos os seus acessórios.
                        </p>
                        <p style={{ paddingLeft: "5em" }}>
                            5) Para que a troca ou cancelamento seja realizado,
                            o produto passará por análise técnica dentro de até
                            10 (dez) dias úteis, realizada pelo fornecedor,
                            contando a partir do recebimento do produto por ele.
                            Caso o produto atenda às exigências, será possível a
                            realização da troca ou cancelamento, sendo que o
                            novo frete para devolução do produto será por conta
                            integralmente do fornecedor.
                        </p>
                        <p style={{ paddingLeft: "5em" }}>
                            6) AVARIAS NO PRODUTO: Em caso de avarias no
                            produto, será realizada a troca do produto ou
                            cancelamento da compra. No entanto, será necessário
                            que o produto esteja em sua embalagem original, sem
                            indícios de mau uso, sem violação do lacre original
                            do fabricante, acompanhado de nota fiscal, manual e
                            todos os seus acessórios para que seja feita a
                            substituição por outro produto da mesma espécie e
                            modelo.
                        </p>
                        <p style={{ paddingLeft: "10em" }}>
                            6.1) Neste caso, o prazo para entrar em contato com
                            o Portal da Maria é de 30 (trinta) dias corridos
                            para produtos não duráveis e 90 (noventa) dias para
                            produtos duráveis, a contar do dia seguinte ao
                            recebimento do produto. A troca ou cancelamento do
                            pedido somente será realizado após a análise técnica
                            do produto pelo fornecedor. Caso o produto atenda às
                            exigências, será possível a realização da troca ou
                            cancelamento, sendo que o novo frete para devolução
                            do produto será por conta integralmente do
                            fornecedor.
                        </p>
                        <p style={{ paddingLeft: "5em" }}>
                            7) PRODUTO COM DEFEITO: A solicitação de troca
                            deverá ser comunicada ao Portal da Maria em até 30
                            (trinta) dias corridos para produtos não duráveis e
                            90 (noventa) dias para produtos duráveis, a contar
                            do dia seguinte ao recebimento do produto. Dentro
                            desse prazo, o cliente tem o direito de solicitar a
                            troca, desde que o produto esteja na embalagem
                            original, sem indícios de mau uso, sem violação do
                            lacre original do fabricante, acompanhado de nota
                            fiscal, manual e de todos os seus acessórios. O
                            prazo para coleta do produto será informado via
                            e-mail ou telefone.
                        </p>
                        <p style={{ paddingLeft: "10em" }}>
                            7.1) Os produtos serão analisados pelo fornecedor,
                            podendo ser devolvidos sem consulta prévia no caso
                            de descumprimento dos critérios listados acima ou se
                            não for constatado o defeito mencionado. O prazo
                            total para a troca irá variar de acordo com a região
                            onde a coleta e a nova entrega serão realizadas.
                            Caso o produto atenda às exigências, será possível a
                            realização da troca ou cancelamento, sendo que o
                            novo frete para devolução do produto será por conta
                            integralmente do fornecedor.
                        </p>
                        <p style={{ paddingLeft: "10em" }}>
                            7.2) Se o produto adquirido no Portal da Maria
                            apresentar defeito após o prazo informado no item
                            “7” acima, mas dentro do prazo de garantia do
                            fornecedor, o cliente deverá entrar em contato com o
                            fornecedor para comunicar o fato e obter
                            esclarecimentos. Caso o produto atenda às
                            exigências, será possível a realização da troca ou
                            cancelamento, sendo que o novo frete para devolução
                            do produto será por conta integralmente do
                            fornecedor.
                        </p>
                        <p style={{ paddingLeft: "5em" }}>
                            8) PRODUTO EM DESACORDO COM O PEDIDO: Caso receba
                            algum produto em desacordo com o seu pedido, o
                            cliente deverá recusar a entrega, fazer uma pequena
                            anotação no verso da nota fiscal ou comprovante de
                            entrega explicando a recusa e entrar em contato
                            imediatamente com o Portal da Maria. Após a recusa,
                            o item retornará ao fornecedor e assim que
                            finalizado o processo de troca, o fornecedor enviará
                            o produto correto de acordo com o prazo para entrega
                            na região.
                        </p>
                        <p style={{ paddingLeft: "10em" }}>
                            8.1) Caso identifique o erro após o recebimento, o
                            cliente não deverá abrir o lacre original do produto
                            e deverá entrar em contato imediatamente com o
                            Portal da Maria, informando os dados constantes na
                            etiqueta que estiver na caixa do produto, no prazo
                            máximo de 7 (sete) dias corridos, a contar do dia do
                            recebimento do produto. O prazo para coleta do
                            produto será informado via e-mail ou telefone. Após
                            o retorno do item e finalização do processo de
                            troca, o fornecedor enviará o produto correto de
                            acordo com o prazo para entrega na região, sendo que
                            o novo frete para devolução do produto será por
                            conta integralmente do fornecedor.
                        </p>
                        <p style={{ paddingLeft: "10em" }}>
                            8.2) Caso o produto esteja indisponível no estoque
                            do fornecedor, o cliente poderá substituí-lo por
                            outro no mesmo valor ou solicitar o cancelamento da
                            compra. Se o produto escolhido for de valor
                            superior, o cliente deverá efetuar o pagamento da
                            diferença imediatamente, sendo que o novo frete para
                            devolução do produto será por conta integralmente do
                            fornecedor.
                        </p>
                        <p style={{ paddingLeft: "5em" }}>
                            9) Se for solicitado o cancelamento da compra, a
                            restituição do valor pago será conforme opções de
                            pagamento abaixo:
                        </p>
                        <p style={{ paddingLeft: "6em" }}>
                            a) Cartão de crédito: após a análise do produto pelo
                            fornecedor, será solicitado o estorno do débito à
                            administradora do seu cartão. O estorno das parcelas
                            pagas poderá ocorrer em até três faturas
                            subsequentes, pois dependendo da data do vencimento
                            da fatura, o estorno poderá ocorrer no mês seguinte
                            à solicitação de cancelamento. Ressalta-se que esse
                            procedimento é de responsabilidade da administradora
                            do cartão.
                        </p>
                        <p style={{ paddingLeft: "6em" }}>
                            b) Boleto bancário: após a análise do produto pelo
                            fornecedor, a restituição será feita em sua conta
                            corrente em até 15 (quinze) dias úteis.
                        </p>
                        <p style={{ paddingLeft: "6em" }}>
                            c) Débito on-line: após a análise do produto pelo
                            fornecedor, a restituição será feita em sua conta
                            corrente em até 15 (quinze) dias úteis.
                        </p>
                        <p style={{ paddingLeft: "5em" }}>
                            10) Caso o cancelamento do cliente não seja aprovado
                            após a análise, pelo fato de o produto não estar nas
                            condições listadas acima, ele será devolvido ao
                            cliente sem consulta prévia.
                        </p>

                        <p>Atualizado no dia 30/09/2020 às 15:00 horas.</p>
                    </Col>
                </Row>
            </Main>
        </>
    )
}

export default ExchangePolicy
