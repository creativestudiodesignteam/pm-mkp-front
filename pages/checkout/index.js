import React from "react"
import Head from "next/head"
import Main from "../../src/components/Main"
import Overall from "../../src/components/checkout/overall"

const Checkout = ({query, ...props}) => {
    const {total, address, shipping, ...rest} = query
    const _address = JSON.parse(address || '{}')
    return (
        <>
            <Head>
                <title>Portal da Maria | Checkout</title>
                <link rel="icon" href={"/favicon.ico"}/>
                <meta name="format-detection" content="telephone=no"/>
                <meta
                    httpEquiv="Content-Type"
                    content="text/html; charset=UTF-8"
                />
                <meta
                    name="viewport"
                    content="width=device-width, initial-scale=1, shrink-to-fit=no"
                />
                <meta httpEquiv="X-UA-Compatible" content="IE=edge"/>
                <link
                    href="https://fonts.googleapis.com/css?family=Montserrat%7CRoboto:100,300,400,500,700,900%7CRoboto+Condensed:100,300,400,500,700"
                    rel="stylesheet"
                />
            </Head>
            <Main>
                <Overall
                    {...props}
                    {...rest}
                    _address={_address}
                    total={total}
                    shipping={shipping}
                />
            </Main>
        </>
    )
}

export const getServerSideProps = async ({query}) => {
    return {
        props: {query},
    }
}
export default Checkout
