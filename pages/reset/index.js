import React, { useEffect } from "react"
import Head from "next/head"
import Main from "../../src/components/Main"
import { Button } from "reactstrap"
import {
    AuthenticationContainer,
    InputDefault,
    LinkTo,
} from "../../src/components"
import {
    add_invalid,
    add_valid,
    elm,
    fromServer,
    hasKey,
} from "../../src/assets/helpers"
import { withRouter } from "next/router"
import { MySwal } from "../../src/assets/sweetalert"

function Index({ context: { token: serverToken }, router, token }) {
    useEffect(() => {
        if (!!serverToken) {
            router.push("/login")
        }
        if (!token) {
            MySwal.fire(
                "Voce não possue token de acesso valido para esta pagina",
                "",
                "error"
            ).then(() => {
                router.push("/login")
            })
        }
    }, [token])

    const recovery_access = async (e) => {
        e.preventDefault()
        const { value: new_password } = elm("[name=new_password]")
        const { value: new_confirmPassword } = elm("[name=new_confirmPassword]")
        if (
            !!new_password &&
            !!new_confirmPassword &&
            new_password === new_confirmPassword
        ) {
            const response = await fromServer(
                `/forgot_password/reset`,
                false,
                "PUT",
                {
                    new_password,
                    new_confirmPassword,
                    token,
                }
            )
            if (hasKey(response, "error")) {
                await MySwal.fire(response?.error?.message, "", "error")
            } else {
                await MySwal.fire(response?.message, "", "success")
                await router.push("/login")
            }
        }
    }
    return (
        <>
            <Head>
                <title>Portal da Maria | Recuperar acesso</title>
                <link rel="icon" href={"/favicon.ico"} />
                <meta name="format-detection" content="telephone=no" />
                <meta
                    httpEquiv="Content-Type"
                    content="text/html; charset=UTF-8"
                />
                <meta
                    name="viewport"
                    content="width=device-width, initial-scale=1, shrink-to-fit=no"
                />
                <meta httpEquiv="X-UA-Compatible" content="IE=edge" />
                <script src={"//code.jquery.com/jquery-3.2.1.slim.min.js"} />

                <link
                    href="//fonts.googleapis.com/css?family=Montserrat%7CRoboto:100,300,400,500,700,900%7CRoboto+Condensed:100,300,400,500,700"
                    rel="stylesheet"
                />
            </Head>
            <Main
                _className={"mt-5"}
                _BreadCrumb={[
                    {
                        node_text: "login",
                        href: "/login",
                    },
                    {
                        node_text: "esqueci minha senha",
                        href: "/remind",
                    },
                ]}
            >
                <AuthenticationContainer
                    className="justify-content-center mx-auto mt-5 w-50"
                    maxW={"100%"}
                    minW={"max-content"}
                    onSubmit={recovery_access}
                >
                    <h1 className={"h1 font-weight-bold mb-3"}>
                        Recuperação de senha
                    </h1>
                    <div className="d-flex flex-column justify-content-center mb-2">
                        <InputDefault
                            _minLength={6}
                            has_label
                            name="new_password"
                            label={"Nova senha"}
                            type={"password"}
                            small={">= 6 caracteres"}
                            _required
                        />
                        <InputDefault
                            _minLength={6}
                            blur={(e) => {
                                e.preventDefault()
                                const { value: new_password } = elm(
                                    "[name=new_password]"
                                )
                                const { value: new_confirmPassword } = e.target
                                if (new_password !== new_confirmPassword) {
                                    add_invalid(
                                        e.target,
                                        "As senhas não coincidem"
                                    )
                                } else {
                                    add_valid(e.target)
                                    add_valid(elm("[name=new_password]"))
                                }
                            }}
                            has_label
                            name="new_confirmPassword"
                            label={"Confirmar nova senha"}
                            type={"password"}
                            small={">= 6 caracteres"}
                            _required
                        />
                    </div>
                    <div className="d-flex flex-row justify-content-start align-items-center">
                        <Button
                            size={"lg"}
                            className={
                                "rounded-0 border-0  px-5 py-4 btn-custom actively mr-5"
                            }
                            type="submit"
                        >
                            Recuperar Acesso
                        </Button>
                        <LinkTo
                            _href="/login"
                            className={"text-muted  m-0 my-auto"}
                        >
                            <u className={"text-dark"}>
                                <b>Voltar</b>
                            </u>
                        </LinkTo>
                    </div>
                </AuthenticationContainer>
            </Main>
        </>
    )
}

export const getServerSideProps = async ({ query }) => {
    const { token } = query
    return {
        props: { query, token },
    }
}
export default withRouter(Index)
