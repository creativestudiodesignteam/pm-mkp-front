import React, { useCallback, useEffect, useState } from "react"
import Head from "next/head"
import Main from "../../src/components/Main"
import {
    add_invalid,
    arrayDivideBy,
    awaitable,
    conditions,
    dataURItoBlob,
    elm,
    elmAll,
    formatPriceToServer,
    fromServer,
    getMaxValueOnArrayOfObjectBy,
    hasKey,
    OBJ,
    deliveryTypes,
    add_valid,
} from "../../src/assets/helpers"
import { Button, Col, Form, Row } from "reactstrap"
import InputDefault from "../../src/components/InputDefault"
import SelectDefault from "../../src/components/SelectDefault"
import dynamic from "next/dynamic"
import Loader from "../../src/components/loader"
import { useRouter } from "next/router"
import { LinkTo } from "../../src/components"
import { MySwal } from "../../src/assets/sweetalert"
import ClipLoader from "react-spinners/ClipLoader"

const SelectCustom = dynamic(
    () => import("../../src/components/SelectCustom"),
    {
        ssr: false,
        loading: Loader,
    }
)

const GridList = dynamic(
    () => import("../../src/components/product/GridList/index"),
    {
        ssr: false,
        loading: Loader,
    }
)

function Index({ context: { token }, ...props }) {
    const {
        serverToken,
        product: {
            brand,
            categories,
            datasheet,
            description,
            details,
            id,
            model,
            name,
            sku,
            subcategories,
            grids: serverGrids,
        },
        categories: serverCategories,
    } = props
    const [loaderState, setLoaderState] = useState(false)

    const subCategoriesSelectedId = subcategories.map(
        (sc) => sc.subcategories.id
    )
    const [gridPayload, setGridPayload] = useState(
        serverGrids.map((gridServer) => {
            return {
                id: gridServer.id,
                amount: gridServer.amount,
                price: gridServer.price,
                gallery: gridServer.gallery.map(({ id: fk_files }) => ({
                    fk_files,
                })),
                options_grids: gridServer.options_grids.map(
                    ({
                        options: { id: fk_options },
                        options_names: { id: fk_options_names },
                    }) => ({
                        fk_options,
                        fk_options_names,
                    })
                ),
            }
        })
    )
    const router = useRouter()
    const [fields, setFields] = useState({})
    const [currentCategory, setCurrentCategory] = useState({
        id: false,
        name: "",
    })
    const [subCategoriesValues, setSubCategoriesValues] = useState([])
    const [sheetOptions, setSheetOptions] = useState(datasheet)
    const [condition, setCondition] = useState(props?.product?.condition || "")
    const [selectedCondition, setSelectedCondition] = useState(false)

    function _setSelectedCondition(_condition) {
        if (condition) {
            try {
                const { id } = conditions.find(({ name }) => name === condition)

                setSelectedCondition(id)
            } catch (e) {
                console.error(
                    "Nao conseguimos encontrar uma opção de condição do produto com as informações atuais",
                    {
                        _condition,
                        condition,
                    }
                )
            }
        } else {
            setSelectedCondition(0)
        }
    }

    useEffect(() => {
        _setSelectedCondition(condition)
    }, [])
    useEffect(() => {
        _setSelectedCondition(condition)
    }, [condition])

    const updateAttr = useCallback(
        (grid, value, _gridPayload_) => (e) => {
            const newState = _gridPayload_.map((_gridPayload) =>
                _gridPayload.id === grid.id
                    ? {
                          ..._gridPayload,
                          options_grids: _gridPayload.options_grids.map(
                              (og) => {
                                  return og.fk_options === value.id
                                      ? {
                                            fk_options: og.fk_options,
                                            fk_options_names: Number(
                                                e.target.selectedOptions[0]
                                                    .value
                                            ),
                                        }
                                      : og
                              }
                          ),
                      }
                    : _gridPayload
            )
            setGridPayload(newState)
        },
        [gridPayload, props, fields]
    )
    useEffect(() => {
        if (!currentCategory.name.length) {
            setCurrentCategory({ ...categories })
            let allInputValuesWithNames = {}
            for (const input of elmAll("input:not([name*=sheet])")) {
                allInputValuesWithNames = {
                    ...allInputValuesWithNames,
                    [input.name]: input.value,
                }
            }
            setFields({
                ...allInputValuesWithNames,
                type_packing: Number(details?.type_packing),
            })
        }
    })
    useEffect(() => {
        awaitable(async () => {
            const serverSubCategories = await fromServer(
                `/subcategories/${currentCategory.id}`
            )
            if (!hasKey(serverSubCategories, "error")) {
                setSubCategoriesValues(serverSubCategories)
            } else {
                console.error(serverSubCategories?.error?.message)
            }
        })
    }, [currentCategory])

    function defaultListener(e) {
        e.preventDefault()
        setFields({ ...fields, [e.target.name]: e.target.value })
    }

    function subCategoriesSelectedValues() {
        return currentCategory.id === props.product.categories.id
            ? subCategoriesValues?.filter(({ id }) =>
                  subCategoriesSelectedId.includes(id)
              )
            : fields?.sub_categories?.length
            ? fields?.sub_categories
            : []
    }

    async function save(e) {
        e.preventDefault()
        const btn = elm(".btn_save")
        btn.disabled = true
        btn.innerHTML = `Carregando ... <div class="spinner-border text-light" role="status"><span class="sr-only">Loading...</span></div>`
        //validation
        let input = []
        for (const input_required_node of elmAll(
            "input[required=true], select[required=true]"
        )) {
            if (!input_required_node.value) {
                input = [...input, input_required_node]
            }
            !input_required_node.value &&
                add_invalid(input_required_node, "campo obrigatorio")
        }
        if (input.length) {
            setTimeout(() => {
                for (const _input_required_node of input) {
                    _input_required_node.classList.remove("invalid")
                }
            }, 2000)
            return false
        }
        //validation
        //sheet
        setLoaderState(true)
        let sheet = Array.of(
            ...document.querySelectorAll("[data-name*=linha-] input")
        )
        sheet = arrayDivideBy(sheet, 2)
        const datasheet = sheet.map(
            ([{ value: name }, { value: description }]) => ({
                name,
                description,
            })
        )
        //sheet
        let detailsPayload = {}
        if (fields.type_packing === 1) {
            detailsPayload = {
                length: fields.length,
                height: fields.height,
                width: fields.width,
                weight: fields.weight,
                situation: fields.situation,
                type_packing: Number(fields.type_packing),
                delivery_time: elm("[name=delivery_time]").value,
            }
        } else if (fields.type_packing === 2) {
            detailsPayload = {
                length: fields.length,
                diameter: fields.diameter,
                weight: fields.weight,
                situation: fields.situation,
                type_packing: Number(fields.type_packing),
                delivery_time: elm("[name=delivery_time]").value,
            }
        } else if (fields.type_packing === 3) {
            detailsPayload = {
                length: fields.length,
                width: fields.width,
                weight: fields.weight,
                situation: fields.situation,
                type_packing: Number(fields.type_packing),
                delivery_time: elm("[name=delivery_time]").value,
            }
        }

        let payload = {
            products: {
                name: fields.name,
                description: fields.description,
                brand: fields.brand,
                model: fields.model,
                sku: fields.sku,
                fk_categories: currentCategory.id,
                condition,
            },
            subcategories: OBJ.hasKey(fields, "sub_categories")
                ? fields.sub_categories.map(({ id: fk_subcategories }) => ({
                      fk_subcategories,
                  }))
                : subcategories.map(
                      ({ subcategories: { id: fk_subcategories } }) => ({
                          fk_subcategories,
                      })
                  ),

            details: detailsPayload,
            datasheet: datasheet,
        }
        let gridIdAndGallery = null
        if (hasKey(fields, "grids")) {
            gridIdAndGallery = await Promise.all(
                await fields.grids.map(async (grd) => {
                    if (OBJ.hasKey(grd, "newImages")) {
                        return {
                            id: grd.id,
                            newImages: await Promise.all(
                                grd.newImages.map(async (img) => {
                                    const _file = dataURItoBlob(img.dataURL)
                                    const fd = new FormData()
                                    fd.append("file", _file)
                                    return await fromServer(
                                        "/files",
                                        token,
                                        "POST",
                                        fd
                                    )
                                })
                            ),
                        }
                    }
                    return grd
                })
            )
            let images = null
            payload.grids = fields.grids.map((_grid) => {
                const data = gridIdAndGallery.filter(
                    (gag) => gag.id === _grid.id
                )
                if (data?.length && data[0]?.newImages) {
                    images = data[0].newImages?.map(({ id: fk_files }) => ({
                        fk_files,
                    }))
                }
                let [_gridPayload] = gridPayload.filter(
                    (__grid) => __grid.id === _grid.id
                )

                return images !== null
                    ? {
                          id: _grid.id,
                          amount: _grid.amount,
                          price: formatPriceToServer(_grid.price),
                          gallery: [
                              ..._grid.gallery.map((g) => ({
                                  fk_files: g?.files?.id,
                              })),
                              ...images,
                          ],
                          options_grids: _gridPayload.options_grids,
                      }
                    : {
                          id: _grid.id,
                          amount: _grid.amount,
                          price: formatPriceToServer(_grid.price || "0", [
                              ",",
                              ".",
                          ]),
                          gallery: [
                              ..._grid.gallery.map((g) => ({
                                  fk_files: g.files.id,
                              })),
                          ],
                          options_grids: _gridPayload.options_grids,
                      }
            })
        } else {
            payload.grids = gridPayload.grids.map((_grid_) => {
                return {
                    ..._grid_,
                    price: formatPriceToServer(_grid_.price),
                }
            })
        }
        if (OBJ.hasKey(fields, "thumb")) {
            let file = dataURItoBlob(fields.thumb.base64)
            const _formData = new FormData()
            _formData.append("file", file)
            const _thumb = await fromServer("/files", token, "POST", _formData)
            if (!OBJ.hasKey(_thumb, "error")) {
                payload.products.fk_thumb = _thumb.id
            }
        } else {
            payload.products.fk_thumb = props.product.thumb.id
        }
        const response = await fromServer(
            "/products/" + id,
            token || serverToken,
            "PUT",
            payload
        )
        if (!hasKey(response, "error")) {
            btn.disabled = true
            btn.innerHTML = `SALVAR`
            await MySwal.fire("Produto Atualizado", "", "success")
            await router.push("/account/[feature]", "/account/product")
        } else {
            await MySwal.fire(
                "Não foi possivel atualizar o produto",
                response?.error?.message || "",
                "warning"
            )
            setLoaderState(false)
        }
        btn.disabled = false
        btn.innerHTML = `SALVAR`
        setLoaderState(false)
        return false
    }

    return (
        <>
            <Head>
                <title>Portal da Maria | Edição do produto</title>
                <link rel="icon" href={"/favicon.ico"} />
                <meta name="format-detection" content="telephone=no" />
                <meta
                    httpEquiv="Content-Type"
                    content="text/html; charset=UTF-8"
                />
                <meta
                    name="viewport"
                    content="width=device-width, initial-scale=1, shrink-to-fit=no"
                />
                <meta httpEquiv="X-UA-Compatible" content="IE=edge" />
                <link
                    href="https://fonts.googleapis.com/css?family=Montserrat%7CRoboto:100,300,400,500,700,900%7CRoboto+Condensed:100,300,400,500,700"
                    rel="stylesheet"
                />
            </Head>
            <Main
                _BreadCrumb={[
                    {
                        node_text: "login",
                        href: "/login",
                    },
                    {
                        node_text: "remind",
                        href: "/changeproduct",
                        query: { token: token },
                    },
                ]}
            >
                {loaderState && (
                    <div className="spinnerLoader">
                        <ClipLoader
                            size={150}
                            color={"var(--red-1)"}
                            loading={loaderState}
                        />
                    </div>
                )}
                <Form onSubmit={save}>
                    <Row className="mb-4">
                        <h1 className="text_red">Edite seu produto</h1>
                    </Row>
                    <Row className="mb-4">
                        <Col sm={"12"} className="m-0 p-0 d-flex">
                            <div className="rectEdit" />
                            <h1 className="m-0 p-0">
                                Informações gerais do produto
                            </h1>
                        </Col>
                    </Row>
                    <Row className="mb-4">
                        <Col sm={"12"} className="mb-4">
                            <InputDefault
                                has_label
                                label={"Nome do produto"}
                                name={"name"}
                                id={"name"}
                                type={"text"}
                                initial_value={name}
                                _required
                                onChange={defaultListener}
                            />
                        </Col>
                        <Col sm={"12"} className="mb-4">
                            <InputDefault
                                styleInput={{
                                    background: "var(--gray-1)",
                                    fontSize: "14px",
                                }}
                                has_label
                                label={"Descrição do produto"}
                                name={"description"}
                                id={"description"}
                                type={"textarea"}
                                initial_value={description}
                                onChange={defaultListener}
                                _required
                            />
                        </Col>
                        <Col sm={"12"} md={"6"} lg={"6"}>
                            <InputDefault
                                has_label
                                label={"Marca"}
                                name={"brand"}
                                id={"brand"}
                                type={"text"}
                                initial_value={brand}
                                onChange={defaultListener}
                            />
                        </Col>
                        <Col sm={"12"} md={"6"} lg={"6"}>
                            <InputDefault
                                has_label
                                label={"Referência ou código"}
                                name={"model"}
                                id={"model"}
                                type={"text"}
                                initial_value={model}
                                onChange={defaultListener}
                            />
                        </Col>
                        <Col sm={"12"} md={"6"} lg={"6"} className="mb-4">
                            <InputDefault
                                has_label
                                label={"Código de barras"}
                                name={"sku"}
                                id={"sku"}
                                type={"text"}
                                initial_value={sku}
                                onChange={defaultListener}
                            />
                        </Col>
                        <Col sm={"6"}>
                            {selectedCondition !== false && (
                                <SelectDefault
                                    name={"condition"}
                                    id={"condition"}
                                    has_label
                                    label={"Selecione a condição do produto"}
                                    initial_value={selectedCondition}
                                    values={conditions}
                                    className={"w-100"}
                                    _required
                                    listener={(e) => {
                                        setCondition(
                                            e.target?.selectedOptions[0]?.text
                                        )
                                        e.preventDefault()
                                    }}
                                />
                            )}
                        </Col>
                        <Col sm={"6"}>
                            <SelectDefault
                                name={"category"}
                                id={"category"}
                                has_label
                                label={"Selecione sua categoria"}
                                listener={(e) => {
                                    e.preventDefault()
                                    setCurrentCategory({
                                        id: e.target?.selectedOptions[0]?.value,
                                        name:
                                            e.target?.selectedOptions[0]?.text,
                                    })
                                    setFields({ ...fields, sub_categories: [] })
                                }}
                                initial_value={
                                    currentCategory.id || categories.id
                                }
                                values={serverCategories}
                                className={"w-100"}
                                _required
                            />
                        </Col>
                        <Col sm={"6"}>
                            <SelectCustom
                                multiple
                                name={"sub_categories"}
                                id={"sub_categories"}
                                has_label
                                label={"Selecione sua subcategoria"}
                                selectedValues={subCategoriesSelectedValues()}
                                values={subCategoriesValues}
                                onRemove={(e) => {
                                    setFields({ ...fields, sub_categories: e })
                                }}
                                onSelect={(e) => {
                                    setFields({ ...fields, sub_categories: e })
                                }}
                                _required
                            />
                        </Col>
                    </Row>
                    <Row className="mb-4">
                        <Col sm={"12"} className="m-0 p-0 d-flex">
                            <div className="rectEdit" />
                            <h1 className="m-0 p-0">Detalhes</h1>
                        </Col>
                    </Row>
                    <Row className="mb-4">
                        <Col sm="12" md="6">
                            <SelectDefault
                                placeholder={"Tipo de embalagem"}
                                has_label
                                label={"Tipo de embalagem*"}
                                id={"packageoption"}
                                initial_value={details?.type_packing}
                                _required={true}
                                listener={(e) => {
                                    e.preventDefault()
                                    setFields({
                                        ...fields,
                                        type_packing: Number(e.target.value),
                                    })
                                }}
                                name={"packageoption"}
                                values={deliveryTypes.map((t) =>
                                    t.id === Number(details.type_packing)
                                        ? {
                                              ...t,
                                              selected: true,
                                          }
                                        : t
                                )}
                            />
                        </Col>
                        <Col sm="12" md="6">
                            <InputDefault
                                id="delivery_time"
                                name="delivery_time"
                                has_label
                                label="Tempo de entrega do produto"
                                onChange={defaultListener}
                                initial_value={details.delivery_time}
                                _required
                                placeholder={"Exemplo: Pronta entrega, 2 dias"}
                            />
                        </Col>
                    </Row>
                    {fields.type_packing === 1 && (
                        <Row className="mb-4">
                            <Col sm="12" md="6">
                                <InputDefault
                                    id={"length"}
                                    name="length"
                                    has_label
                                    label="Comprimento (em centimetro)"
                                    initial_value={details?.length}
                                    onBlur={(e) => {
                                        if (e.target.value < 16) {
                                            add_invalid(
                                                e.target,
                                                "O comprimento deve ser maior que 15cm"
                                            )
                                        } else if (e.target.value > 100) {
                                            add_invalid(
                                                e.target,
                                                "O comprimento deve ser menor que 101cm"
                                            )
                                        } else {
                                            add_valid(e.target)
                                        }

                                        e.preventDefault()
                                    }}
                                    onChange={defaultListener}
                                    _required
                                />
                            </Col>
                            <Col sm="12" md="6">
                                <InputDefault
                                    id={"height"}
                                    name="height"
                                    has_label
                                    label="Altura (em centimetro)"
                                    initial_value={details?.height}
                                    onBlur={(e) => {
                                        if (e.target.value < 2) {
                                            add_invalid(
                                                e.target,
                                                "A altura deve ser maior que 1cm"
                                            )
                                        } else if (e.target.value > 100) {
                                            add_invalid(
                                                e.target,
                                                "A altura deve ser menor que 101cm"
                                            )
                                        } else {
                                            add_valid(e.target)
                                        }

                                        e.preventDefault()
                                    }}
                                    onChange={defaultListener}
                                    _required
                                />
                            </Col>
                            <Col sm="12" md="6">
                                <InputDefault
                                    id={"width"}
                                    name="width"
                                    has_label
                                    label="Largura (em centimetro)"
                                    initial_value={details?.width}
                                    onBlur={(e) => {
                                        if (e.target.value < 11) {
                                            add_invalid(
                                                e.target,
                                                "A largura deve ser maior que 10cm"
                                            )
                                        } else if (e.target.value > 100) {
                                            add_invalid(
                                                e.target,
                                                "A largura deve ser menor que 101cm"
                                            )
                                        } else {
                                            add_valid(e.target)
                                        }

                                        e.preventDefault()
                                    }}
                                    onChange={defaultListener}
                                    _required
                                />
                            </Col>
                            <Col sm="12" md="6">
                                <InputDefault
                                    id={"weight"}
                                    name="weight"
                                    has_label
                                    label="Peso (em grama)"
                                    onChange={defaultListener}
                                    initial_value={details?.weight * 1000}
                                    _required
                                />
                            </Col>
                        </Row>
                    )}
                    {fields.type_packing === 2 && (
                        <Row className="mb-4">
                            <Col sm="12" md="5">
                                <InputDefault
                                    id={"length"}
                                    name="length"
                                    has_label
                                    label="Comprimento (em centimetro)"
                                    initial_value={details?.length}
                                    onBlur={(e) => {
                                        if (e.target.value < 18) {
                                            add_invalid(
                                                e.target,
                                                "O comprimento deve ser maior que 17cm"
                                            )
                                        } else if (e.target.value > 100) {
                                            add_invalid(
                                                e.target,
                                                "O comprimento deve ser menor que 101cm"
                                            )
                                        } else {
                                            add_valid(e.target)
                                        }

                                        e.preventDefault()
                                    }}
                                    onChange={defaultListener}
                                    _required
                                />
                            </Col>
                            <Col sm="12" md="5">
                                <InputDefault
                                    id={"diameter"}
                                    name="diameter"
                                    has_label
                                    label="Diâmetro (em centimetro)"
                                    initial_value={details?.diameter}
                                    onBlur={(e) => {
                                        if (e.target.value < 4) {
                                            add_invalid(
                                                e.target,
                                                "O diâmetro deve ser maior que 4cm"
                                            )
                                        } else if (e.target.value > 91) {
                                            add_invalid(
                                                e.target,
                                                "O diâmetro deve ser menor que 92cm"
                                            )
                                        } else {
                                            add_valid(e.target)
                                        }

                                        e.preventDefault()
                                    }}
                                    onChange={defaultListener}
                                    _required
                                />
                            </Col>
                            <Col sm="12" md="2">
                                <InputDefault
                                    id={"weight"}
                                    name="weight"
                                    has_label
                                    label="Peso (em grama)"
                                    onChange={defaultListener}
                                    initial_value={details?.weight * 1000}
                                    _required
                                />
                            </Col>
                        </Row>
                    )}
                    {fields.type_packing === 3 && (
                        <Row className="mb-4">
                            <Col sm="12" md="5">
                                <InputDefault
                                    id={"length"}
                                    name="length"
                                    has_label
                                    label="Comprimento (em centimetro)"
                                    initial_value={details?.length}
                                    onBlur={(e) => {
                                        if (e.target.value < 16) {
                                            add_invalid(
                                                e.target,
                                                "O comprimento deve ser maior que 15cm"
                                            )
                                        } else if (e.target.value > 60) {
                                            add_invalid(
                                                e.target,
                                                "O comprimento deve ser menor que 61cm"
                                            )
                                        } else {
                                            add_valid(e.target)
                                        }

                                        e.preventDefault()
                                    }}
                                    onChange={defaultListener}
                                    _required
                                />
                            </Col>
                            <Col sm="12" md="5">
                                <InputDefault
                                    id={"width"}
                                    name="width"
                                    has_label
                                    label="Largura (em centimetro)"
                                    initial_value={details?.width}
                                    onBlur={(e) => {
                                        if (e.target.value < 11) {
                                            add_invalid(
                                                e.target,
                                                "A largura deve ser maior que 10cm"
                                            )
                                        } else if (e.target.value > 60) {
                                            add_invalid(
                                                e.target,
                                                "A largura deve ser menor que 61cm"
                                            )
                                        } else {
                                            add_valid(e.target)
                                        }

                                        e.preventDefault()
                                    }}
                                    onChange={defaultListener}
                                    _required
                                />
                            </Col>
                            <Col sm="12" md="2">
                                <InputDefault
                                    id={"weight"}
                                    name="weight"
                                    has_label
                                    label="Peso (em grama)"
                                    onChange={defaultListener}
                                    initial_value={details?.weight * 1000}
                                    _required
                                />
                            </Col>
                        </Row>
                    )}

                    <Row className="mb-4">
                        <Col sm={"12"} className="m-0 p-0 d-flex">
                            <div className="rectEdit" />
                            <h1 className="m-0 p-0">
                                Características e atributos
                            </h1>
                        </Col>
                    </Row>
                    <GridList
                        updateAttr={updateAttr}
                        gridPayload={gridPayload}
                        setGridPayload={setGridPayload}
                        fields={fields}
                        setFields={setFields}
                        token={serverToken || token}
                        id={id}
                    />
                    <Row className="mb-4">
                        <Col className="m-0 p-0 d-flex mb-4" xs={"12"}>
                            <div className="rectEdit" />
                            <h1 className="m-0 p-0 ">Ficha Tecnica</h1>
                        </Col>
                        <Col className="mb-4 allignerCenter p-0" xs={"12"}>
                            <Button
                                outline
                                className="redButtonOutline"
                                style={{ width: "40%" }}
                                onClick={(e) => {
                                    e.preventDefault()
                                    const nextId = Number(
                                        sheetOptions.reduce(
                                            getMaxValueOnArrayOfObjectBy("id"),
                                            0
                                        ) + 1
                                    )
                                    setSheetOptions([
                                        ...sheetOptions,
                                        {
                                            id: nextId,
                                            name: "",
                                            description: "",
                                        },
                                    ])
                                }}
                            >
                                Adicionar mais detalhes a ficha técnica
                            </Button>
                        </Col>
                    </Row>
                    <Row className={" w-100 m-0 p-0 allignerCenter"}>
                        {sheetOptions.map(
                            ({ id, name, description: s_description }, idx) => {
                                return (
                                    <div
                                        key={id}
                                        className={`h-100 w-100 allignerCenter`}
                                        data-name={`linha-${idx}`}
                                    >
                                        <Col md={"5"}>
                                            <InputDefault
                                                has_label
                                                label={"Titulo"}
                                                id={`sheetOption-name-${id}`}
                                                name={`sheetOption-name-${id}`}
                                                initial_value={name}
                                                onChange={defaultListener}
                                                _required
                                            />
                                        </Col>
                                        <Col md={"5"}>
                                            <InputDefault
                                                has_label
                                                label={"Definição"}
                                                id={`sheetOption-description-${id}`}
                                                name={`sheetOption-description-${id}`}
                                                initial_value={s_description}
                                                onChange={defaultListener}
                                                _required
                                            />
                                        </Col>
                                        <Col
                                            md={"2"}
                                            className="allignerCenter"
                                        >
                                            <Button
                                                outline
                                                className="redButtonOutline"
                                                onClick={(e) => {
                                                    e.preventDefault()
                                                    setSheetOptions(
                                                        sheetOptions.filter(
                                                            (so) => so.id !== id
                                                        )
                                                    )
                                                }}
                                            >
                                                Excluir
                                            </Button>
                                        </Col>
                                    </div>
                                )
                            }
                        )}
                    </Row>
                    <Row className="mt-4 allignerCenter">
                        <Col md={"6"} className="p-0 allignerCenter mb-4">
                            <Button
                                className="buttonSave70 btn_save"
                                type={"submit"}
                            >
                                SALVAR
                            </Button>
                        </Col>
                        <Col
                            md={"6"}
                            className="p-0 allignerCenter adjustLinkTo"
                        >
                            <LinkTo
                                _href={"/account/[feature]"}
                                as={"/account/product"}
                                query={{ token: token || serverToken }}
                            >
                                <Button
                                    className="buttonSave70"
                                    type={"button"}
                                    onClick={(e) => setLoaderState(true)}
                                >
                                    VOLTAR
                                </Button>
                            </LinkTo>
                        </Col>
                    </Row>
                </Form>
            </Main>
        </>
    )
}

export const getServerSideProps = async ({ res, query }) => {
    if (OBJ.hasKey(query, "token")) {
        const { token: serverToken, id } = query
        const product = await fromServer(`/products/${id}`, serverToken)
        const categories = await fromServer("/categories", serverToken)
        if (!OBJ.hasKey(product, "error") && !OBJ.hasKey(categories, "error")) {
            return { props: { serverToken, product, categories } }
        } else {
            console.log(product?.errror?.message, categories?.errror?.message) //categories
        }
    }
    res.setHeader("location", "/login")
    res.statusCode = 302
    res.end()
    return { props: { serverToken: "fail" } }
}

export default Index
