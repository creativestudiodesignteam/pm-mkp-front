import React, { useContext, useEffect, useState } from "react"
import Head from "next/head"
import {
    Img,
    InputDefault,
    LinkTo,
    Main,
    SelectDefault,
} from "../../src/components"
import {
    Button,
    Col,
    FormGroup,
    Input,
    Label,
    Nav,
    NavItem,
    NavLink,
    Row,
    TabContent,
    Table,
    TabPane,
} from "reactstrap"
import classnames from "classnames"
import styles from "./index.module.css"
import ImageGallery from "react-image-gallery"
import Slider from "react-slick"
import {
    add_invalid,
    add_valid,
    brlFormat,
    compareBy,
    defaultListener,
    disable_btn_and_put_innerHTML,
    elm,
    enable_btn_and_put_innerHTML,
    fromServer,
    hasKey,
    optionsParseFromSever,
    postcode,
    tryCatch,
} from "../../src/assets/helpers"
import { useRouter } from "next/router"
import { Context, dispatchFunctions } from "../../src/store"
import { MySwal } from "../../src/assets/sweetalert"
import useForceUpdate from "use-force-update"
import { _storage } from "../../src/assets/auth"

const ProductDetails = ({
    gridId,
    product: productFromServer,
    serverOptionsGrid,
    _serverOptionsGrid,
    related,
}) => {
    const forceUpdate = useForceUpdate()
    const [postcodeDelivery, setPostcodeDelivery] = useState("")
    const [deliveryValue, setDeliveryValue] = useState([])
    const [amountSelected, setAmountSelected] = useState(1)
    const [current, setCurrent] = useState({
        price: "",
        amount: 0,
        status: false,
        id: 0,
    })
    const [context, dispatch] = useContext(Context)
    const [gridOptions, setOptions] = useState({
        serverOptionsGrid,
        current: "",
    })
    const [selectedList, setSelectedList] = useState([])
    const [images, setImages] = useState([
        {
            original: productFromServer?.product?.thumb
                ? productFromServer?.product?.thumb?.url
                : "img/400x400.png",
            thumbnail: productFromServer?.product?.thumb
                ? productFromServer?.product?.thumb?.url
                : "img/400x400.png",
        },
    ])
    const [activeTab, setActiveTab] = useState(1)
    const [foundGrid, setFoundGrid] = useState(null)
    const router = useRouter()
    const settings = {
        infinite: false,
        autoplay: true,
        arrows: true,
        speed: 500,
        slidesToShow: 4,
        slidesToScroll: 1,
    }
    const settingsMobile = {
        infinite: true,
        autoplay: true,
        arrows: true,
        speed: 500,
        slidesToShow: 1,
        slidesToScroll: 1,
    }

    useEffect(() => {
        /*filter serverOptionsGrid and set gridOptions */
        const nextConfigGridOptions = _serverOptionsGrid.filter((_sOG) => {
            let ok = true
            for (const { fk_options, fk_options_names } of selectedList) {
                ok = !_sOG.options.find(
                    (_sOG_o) =>
                        _sOG_o.fk_options === fk_options &&
                        _sOG_o.fk_options_names === fk_options_names
                )
                    ? false
                    : ok
            }
            return ok
        })
        if (nextConfigGridOptions.length === 1) {
            setFoundGrid(nextConfigGridOptions[0])
            try {
                const {
                    Grids: { amount, price, id, status },
                } = productFromServer.product.GridsList.find(
                    (_gd) => _gd.Grids.id === nextConfigGridOptions[0].id
                )
                setCurrent({ amount, price, id, status })
            } catch (e) {
                console.error(e, { nextConfigGridOptions })
            }
            fromServer("/galleries/" + nextConfigGridOptions[0].id).then(
                (data) => {
                    if (data.length > 0) {
                        setImages(
                            data.map((uurl) => ({
                                original: uurl,
                                thumbnail: uurl,
                            }))
                        )
                    } else {
                        setImages([
                            {
                                original:
                                    productFromServer?.product?.thumb !== null
                                        ? productFromServer?.product?.thumb.url
                                        : "img/400x400.png",
                                thumbnail:
                                    productFromServer?.product?.thumb !== null
                                        ? productFromServer?.product?.thumb?.url
                                        : "img/400x400.png",
                            },
                        ])
                    }
                }
            )
        } else {
            setFoundGrid(null)
            setImages([
                {
                    original:
                        productFromServer?.product?.thumb !== null
                            ? productFromServer?.product?.thumb.url
                            : "250x250.png",
                    thumbnail:
                        productFromServer?.product?.thumb !== null
                            ? productFromServer?.product?.thumb?.url
                            : "250x250.png",
                },
            ])
        }
        setOptions({
            ...gridOptions,
            serverOptionsGrid: optionsParseFromSever(nextConfigGridOptions),
        })
    }, [selectedList])
    useEffect(() => {
        setImages([
            {
                original:
                    productFromServer?.product?.thumb !== null
                        ? productFromServer?.product?.thumb.url
                        : "250x250.png",
                thumbnail:
                    productFromServer?.product?.thumb !== null
                        ? productFromServer?.product?.thumb?.url
                        : "250x250.png",
            },
        ])
    }, [productFromServer])

    useEffect(() => {
        setOptions({
            serverOptionsGrid: serverOptionsGrid,
            current: "",
        })
    }, [serverOptionsGrid])
    const toggle = (tab) => {
        if (activeTab !== tab) setActiveTab(tab)
    }

    function mergeOptionsToCurrent(gridOption) {
        if (Number(gridOption.fk_options) === Number(gridOptions.current)) {
            return serverOptionsGrid
                .filter((sog) => sog.fk_options === gridOption?.fk_options)[0]
                .names?.map(
                    ({ fk_options_names: id, name_options_names: name }) => ({
                        id,
                        name,
                    })
                )
        }
        return gridOption.names.map(
            ({ fk_options_names: id, name_options_names: name }) => ({
                id,
                name,
            })
        )
    }

    return (
        <>
            <Head>
                <title>
                    Portal da Maria |{" "}
                    {`${productFromServer?.product?.name}`.slice(0, 10) + "..."}
                </title>
                <link rel="icon" href={"/favicon.ico"} />
                <meta name="format-detection" content="telephone=no" />
                <meta
                    httpEquiv="Content-Type"
                    content="text/html; charset=UTF-8"
                />
                <meta
                    name="viewport"
                    content="width=device-width, initial-scale=1, shrink-to-fit=no"
                />
                <meta httpEquiv="X-UA-Compatible" content="IE=edge" />
                <link
                    href="https://fonts.googleapis.com/css?family=Montserrat%7CRoboto:100,300,400,500,700,900%7CRoboto+Condensed:100,300,400,500,700"
                    rel="stylesheet"
                />
            </Head>
            <Main
                _BreadCrumb={[
                    {
                        node_text: "Todas as categorias",
                        href: "/allcategories",
                    },
                    {
                        node_text: productFromServer.product.categories.name,
                        href: `/category/`,
                        query: {
                            id_cat: productFromServer.product.categories.id,
                        },
                    },
                    {
                        node_text: productFromServer.product.name,
                        href: "/details",
                    },
                ]}
            >
                <div className="top-product-detail relative">
                    <Row style={{ marginBottom: "3rem" }}>
                        <Col
                            md="7"
                            style={{
                                display: "flex",
                                justifyContent: "center",
                            }}
                            className="imageGalleryProduct"
                        >
                            <ImageGallery
                                items={images}
                                showPlayButton={false}
                                showFullscreenButton={false}
                            />
                        </Col>
                        <Col md="5">
                            <Row>
                                <Col
                                    md="12"
                                    className="name-ranking-product relative bottom-padding-15-default bottom-margin-15-default"
                                    style={{
                                        border: "1px solid #dedede",
                                        borderLeft: 0,
                                        borderRight: 0,
                                        borderTop: 0,
                                        boxSizing: "border-box",
                                    }}
                                >
                                    <h1>{productFromServer.product.name}</h1>
                                    <p className="clearfix price-product">
                                        {current.price !== ""
                                            ? brlFormat(current.price, true)
                                            : productFromServer.product
                                                  .GridsList &&
                                              brlFormat(
                                                  productFromServer.product
                                                      .GridsList[0]?.Grids
                                                      ?.price,
                                                  true
                                              )}
                                    </p>
                                    <div
                                        className="clearfix full-width"
                                        style={{ display: "table" }}
                                    >
                                        <p
                                            className="float-left relative"
                                            style={{
                                                margin: "0 3rem 0 0",
                                                padding: "0 3rem 0 0",
                                            }}
                                        >
                                            Cod.Item: #
                                            {productFromServer.product.id}
                                        </p>
                                        <p className="float-left clear-margin">
                                            <span
                                                className={
                                                    (current.price !== ""
                                                      ? current.status &&
                                                        current.amount > 0
                                                      : productFromServer
                                                            .product.statusProd)
                                                        ? "text-green"
                                                        : "text-red"
                                                }
                                            >
                                                {(current.price !== ""
                                                  ? current.status &&
                                                    current.amount > 0
                                                  : productFromServer.product
                                                        .statusProd)
                                                    ? "Em estoque"
                                                    : "Sem estoque"}
                                            </span>
                                        </p>
                                    </div>
                                </Col>
                                <Col
                                    md="12"
                                    className="relative bottom-padding-15-default mb-4"
                                    style={{
                                        border: "1px solid #dedede",
                                        borderLeft: 0,
                                        borderRight: 0,
                                        borderTop: 0,
                                        boxSizing: "border-box",
                                    }}
                                >
                                    {gridOptions.serverOptionsGrid.map(
                                        (gridOption, index) => (
                                            <SelectDefault
                                                key={gridOption.fk_options}
                                                id={gridOption.fk_options}
                                                name={gridOption.name_options}
                                                has_label
                                                label={gridOption.name_options}
                                                className="mb-4"
                                                values={[
                                                    {
                                                        id: "",
                                                        name:
                                                            "Selecione uma das opções abaixo",
                                                        selected: true,
                                                        value: "",
                                                    },
                                                    ...mergeOptionsToCurrent(
                                                        gridOption
                                                    ),
                                                ]}
                                                listener={(e) => {
                                                    e.preventDefault()
                                                    const {
                                                        target: { value },
                                                    } = e
                                                    setOptions({
                                                        ...gridOptions,
                                                        current:
                                                            gridOption.fk_options,
                                                    })
                                                    if (value) {
                                                        if (
                                                            !!selectedList.find(
                                                                (sl) =>
                                                                    sl.fk_options ===
                                                                    gridOption.fk_options
                                                            )
                                                        ) {
                                                            setSelectedList(
                                                                selectedList.map(
                                                                    (sl) =>
                                                                        sl.fk_options ===
                                                                        gridOption.fk_options
                                                                            ? {
                                                                                  fk_options:
                                                                                      gridOption.fk_options,
                                                                                  fk_options_names: Number(
                                                                                      value
                                                                                  ),
                                                                              }
                                                                            : sl
                                                                )
                                                            )
                                                        } else {
                                                            setSelectedList([
                                                                ...selectedList,
                                                                {
                                                                    fk_options:
                                                                        gridOption.fk_options,
                                                                    fk_options_names: Number(
                                                                        value
                                                                    ),
                                                                },
                                                            ])
                                                        }
                                                    } else {
                                                        setSelectedList(
                                                            selectedList.filter(
                                                                (sl) =>
                                                                    sl.fk_options !==
                                                                    gridOption.fk_options
                                                            )
                                                        )
                                                    }
                                                }}
                                            />
                                        )
                                    )}
                                    <FormGroup>
                                        <Label for="exampleNumber">
                                            Quantidade: &nbsp;
                                        </Label>
                                        {current.price !== "" ? (
                                            <>
                                                {" "}
                                                <input
                                                    className="form-control"
                                                    type="number"
                                                    name="number"
                                                    id="exampleNumber"
                                                    value={amountSelected}
                                                    onKeyUp={(e) => {
                                                        if (
                                                            e.target.value < 1
                                                        ) {
                                                            add_invalid(
                                                                e.target,
                                                                "Quantidade deve ser maior que 0"
                                                            )
                                                        } else if (
                                                            e.target.value >
                                                            current.amount
                                                        ) {
                                                            add_invalid(
                                                                e.target,
                                                                "Quantidade maior que o estoque disponível"
                                                            )
                                                        } else {
                                                            add_valid(e.target)
                                                            setAmountSelected(
                                                                Number(
                                                                    e.target
                                                                        .value
                                                                )
                                                            )
                                                        }
                                                        e.preventDefault()
                                                    }}
                                                    onChange={(e) => {
                                                        setAmountSelected(
                                                            Number(
                                                                e.target.value
                                                            )
                                                        )

                                                        e.preventDefault()
                                                    }}
                                                />{" "}
                                            </>
                                        ) : (
                                            <Input
                                                type="number"
                                                name="number"
                                                id="exampleNumber"
                                                min={1}
                                            />
                                        )}
                                    </FormGroup>
                                </Col>
                                <Col
                                    md="12"
                                    className="bottom-padding-15-default d-flex"
                                    style={{
                                        border: "1px solid #dedede",
                                        borderLeft: 0,
                                        borderRight: 0,
                                        borderTop: 0,
                                        boxSizing: "border-box",
                                    }}
                                >
                                    <InputDefault
                                        has_label
                                        label={"Calcule o frete"}
                                        name={"postcode"}
                                        _id={"postcode"}
                                        type={"text"}
                                        initial_value={""}
                                        onKeyUp={postcode}
                                        options={{
                                            delimiters: ["-"],
                                            blocks: [5, 3],
                                        }}
                                        onChange={defaultListener(
                                            setPostcodeDelivery,
                                            postcodeDelivery
                                        )}
                                    />
                                    <FormGroup className="pl-4">
                                        <label
                                            htmlFor="buttonFrete"
                                            className="d-block"
                                        >
                                            &nbsp;
                                        </label>
                                        <Button
                                            outline
                                            className="buttonFrete"
                                            name="buttonFrete"
                                            id="buttonFrete"
                                            onClick={async (e) => {
                                                e.preventDefault()
                                                disable_btn_and_put_innerHTML(
                                                    "#buttonFrete"
                                                )
                                                if (!foundGrid) {
                                                    await MySwal.fire(
                                                        "Selecione caracteristicas para calcular o frete",
                                                        "",
                                                        "info"
                                                    )
                                                    enable_btn_and_put_innerHTML(
                                                        "#buttonFrete",
                                                        "CALCULAR"
                                                    )
                                                } else {
                                                    const deliveryCalc = await fromServer(
                                                        `/shipping_product`,
                                                        "",
                                                        "POST",
                                                        {
                                                            product:
                                                                productFromServer
                                                                    .product.id,
                                                            fk_grids:
                                                                foundGrid?.id,
                                                            postcode:
                                                                postcodeDelivery.postcode,
                                                        }
                                                    )

                                                    if (
                                                        !hasKey(
                                                            deliveryCalc,
                                                            "error"
                                                        )
                                                    ) {
                                                        setDeliveryValue(
                                                            deliveryCalc
                                                        )
                                                        forceUpdate()

                                                        enable_btn_and_put_innerHTML(
                                                            "#buttonFrete",
                                                            "CALCULAR"
                                                        )
                                                    } else {
                                                        await MySwal.fire(
                                                            "Não foi possível calcular o frete",
                                                            deliveryCalc.error
                                                                .message || "",
                                                            "error"
                                                        )
                                                        enable_btn_and_put_innerHTML(
                                                            "#buttonFrete",
                                                            "CALCULAR"
                                                        )
                                                    }
                                                }
                                            }}
                                        >
                                            {" "}
                                            CALCULAR{" "}
                                        </Button>
                                    </FormGroup>
                                </Col>
                                {!!deliveryValue.length && (
                                    <Col
                                        xs="12"
                                        style={{
                                            marginBottom: "2rem",
                                            marginTop: "2.5rem",
                                            padding: 0,
                                        }}
                                    >
                                        <Table className="w-100" borderless>
                                            <thead>
                                                <tr>
                                                    <th width="33%">Tipo</th>
                                                    <th width="33%">
                                                        Tempo de entrega
                                                    </th>
                                                    <th width="33%">Preço</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                {deliveryValue[0] && (
                                                    <tr key="0">
                                                        <td
                                                            className="text-capitalize"
                                                            width="33%"
                                                        >
                                                            {
                                                                deliveryValue[0]
                                                                    .type
                                                            }
                                                        </td>
                                                        <td width="33%">
                                                            {
                                                                deliveryValue[0]
                                                                    .shipping_time
                                                            }{" "}
                                                            dia(s)
                                                        </td>
                                                        <td width="33%">
                                                            {brlFormat(
                                                                deliveryValue[0]
                                                                    .price,
                                                                true
                                                            )}
                                                        </td>
                                                    </tr>
                                                )}
                                                {deliveryValue[1] && (
                                                    <tr key="1">
                                                        <td className="text-capitalize">
                                                            {
                                                                deliveryValue[1]
                                                                    .type
                                                            }
                                                        </td>
                                                        <td width="33%">
                                                            {
                                                                deliveryValue[1]
                                                                    .shipping_time
                                                            }{" "}
                                                            dia(s)
                                                        </td>
                                                        <td width="33%">
                                                            {brlFormat(
                                                                deliveryValue[1]
                                                                    .price,
                                                                true
                                                            )}
                                                        </td>
                                                    </tr>
                                                )}
                                            </tbody>
                                        </Table>
                                    </Col>
                                )}
                                <Col
                                    md="12"
                                    style={{
                                        marginBottom: "2rem",
                                        marginTop: "2.5rem",
                                        padding: 0,
                                    }}
                                >
                                    <Button
                                        size="lg"
                                        id="buttonAddCart"
                                        className={`${styles.buttonAddCart} ${
                                            !foundGrid ||
                                            !selectedList.length ||
                                            !amountSelected ||
                                            amountSelected > current.amount
                                                ? "desabled"
                                                : ""
                                        }`}
                                        disabled={
                                            !foundGrid ||
                                            !selectedList.length ||
                                            !amountSelected ||
                                            amountSelected > current.amount
                                        }
                                        onClick={async () => {
                                            disable_btn_and_put_innerHTML(
                                                "#buttonAddCart"
                                            )

                                            const qtd = elm("#exampleNumber")
                                            if (qtd && !!qtd.value) {
                                                const payload = {
                                                    fk_grids: foundGrid.id,
                                                    amount: Number(qtd?.value),
                                                }
                                                if (context.token) {
                                                    const response = await fromServer(
                                                        "/carts",
                                                        context.token,
                                                        "POST",
                                                        payload
                                                    )
                                                    if (
                                                        response &&
                                                        !hasKey(
                                                            response,
                                                            "error"
                                                        )
                                                    ) {
                                                        await MySwal.fire(
                                                            "Produto adicionado ao carrinho",
                                                            "",
                                                            "success"
                                                        )
                                                        tryCatch(async () => {
                                                            const {
                                                                length,
                                                            } = await fromServer(
                                                                "/carts",
                                                                context.token
                                                            )
                                                            console.log({
                                                                length,
                                                            })
                                                            dispatch(
                                                                dispatchFunctions.cart_length(
                                                                    Number(
                                                                        length
                                                                    )
                                                                )
                                                            )
                                                        })
                                                        forceUpdate()
                                                        enable_btn_and_put_innerHTML(
                                                            "#buttonAddCart",
                                                            "Adicionar ao carrinho"
                                                        )
                                                    } else {
                                                        await MySwal.fire(
                                                            "Não foi possivel adicionar ao carrinho",
                                                            response?.error
                                                                ?.message,
                                                            "error"
                                                        )
                                                        enable_btn_and_put_innerHTML(
                                                            "#buttonAddCart",
                                                            "Adicionar ao carrinho"
                                                        )
                                                    }
                                                } else {
                                                    try {
                                                        const cart = JSON.parse(
                                                            localStorage.getItem(
                                                                "cart"
                                                            ) || "[]"
                                                        )
                                                        const _fundGrid = productFromServer?.product?.GridsList?.find(
                                                            ({
                                                                Grids: { id },
                                                            }) =>
                                                                id ===
                                                                foundGrid.id
                                                        )
                                                        const total = productFromServer?.product?.GridsList?.reduce(
                                                            (acc, gl) =>
                                                                acc +
                                                                Number(
                                                                    gl.Grids
                                                                        .amount
                                                                ),
                                                            0
                                                        )
                                                        const gallery = await fromServer(
                                                            "/galleries/" +
                                                                foundGrid?.id
                                                        )

                                                        if (
                                                            !cart.length ||
                                                            !cart.find(
                                                                (c) =>
                                                                    c.id_grid ===
                                                                    foundGrid.id
                                                            )
                                                        ) {
                                                            localStorage.setItem(
                                                                "cart",
                                                                JSON.stringify([
                                                                    ...cart,
                                                                    {
                                                                        amount: Number(
                                                                            qtd?.value
                                                                        ),
                                                                        amount_stock: total,
                                                                        id:
                                                                            foundGrid.id,
                                                                        id_grid:
                                                                            foundGrid.id,
                                                                        id_options_grid:
                                                                            foundGrid.id,
                                                                        name:
                                                                            productFromServer
                                                                                .product
                                                                                .name,
                                                                        price:
                                                                            _fundGrid
                                                                                .Grids
                                                                                .price /
                                                                            100,
                                                                        url:
                                                                            gallery[0] ||
                                                                            "",
                                                                    },
                                                                ])
                                                            )
                                                        } else {
                                                            localStorage.setItem(
                                                                "cart",
                                                                JSON.stringify(
                                                                    cart.map(
                                                                        (c) =>
                                                                            c.id ===
                                                                            _fundGrid
                                                                                .Grids
                                                                                .id
                                                                                ? {
                                                                                      ...c,
                                                                                      amount:
                                                                                          Number(
                                                                                              c.amount
                                                                                          ) +
                                                                                          Number(
                                                                                              qtd?.value
                                                                                          ),
                                                                                  }
                                                                                : c
                                                                    )
                                                                )
                                                            )
                                                        }
                                                        await MySwal.fire(
                                                            "Produto adicionado",
                                                            "",
                                                            "success"
                                                        )
                                                        tryCatch(() => {
                                                            const storage = _storage()
                                                            if (!!storage) {
                                                                const {
                                                                    length,
                                                                } = JSON.parse(
                                                                    storage.getItem(
                                                                        "cart"
                                                                    )
                                                                )
                                                                dispatch(
                                                                    dispatchFunctions.cart_length(
                                                                        Number(
                                                                            length
                                                                        )
                                                                    )
                                                                )
                                                            }
                                                        })
                                                        enable_btn_and_put_innerHTML(
                                                            "#buttonAddCart",
                                                            "Adicionar ao carrinho"
                                                        )
                                                    } catch (e) {
                                                        await MySwal.fire(
                                                            "Produto não adicionado",
                                                            "",
                                                            "error"
                                                        )
                                                        enable_btn_and_put_innerHTML(
                                                            "#buttonAddCart",
                                                            "Adicionar ao carrinho"
                                                        )
                                                    }
                                                }
                                            } else {
                                                await MySwal.fire(
                                                    "",
                                                    "Quantidade invalida",
                                                    "warning"
                                                )
                                                enable_btn_and_put_innerHTML(
                                                    "#buttonAddCart",
                                                    "Adicionar ao carrinho"
                                                )
                                            }
                                            return false
                                        }}
                                    >
                                        Adicionar ao carrinho
                                    </Button>
                                </Col>
                            </Row>
                        </Col>
                    </Row>
                </div>
                <div className="info-product-detail bottom-margin-default relative">
                    <Row>
                        <Col md="12" className="relative overfollow-hidden">
                            <Nav
                                tabs
                                className="title-tabs clearfix relative"
                                style={{ borderBottom: 0, zIndex: 2 }}
                            >
                                <NavItem className={styles.navItem}>
                                    <NavLink
                                        style={{
                                            marginRight: "0",
                                            lineHeight: "3.5rem",
                                            fontSize: "1.5rem",
                                            color: "#2b2b2b",
                                            cursor: "pointer",
                                            fontWeight: "bold",
                                        }}
                                        className={classnames({
                                            active: activeTab === 1,
                                        })}
                                        onClick={() => {
                                            toggle(1)
                                        }}
                                    >
                                        {"Descrição".toUpperCase()}
                                    </NavLink>
                                </NavItem>
                                {!!productFromServer?.product?.datasheet
                                    .length && (
                                    <NavItem className={styles.navItem}>
                                        <NavLink
                                            style={{
                                                marginRight: "0",
                                                lineHeight: "3.5rem",
                                                fontSize: "1.5rem",
                                                color: "#2b2b2b",
                                                cursor: "pointer",
                                                fontWeight: "bold",
                                            }}
                                            className={classnames({
                                                active: activeTab === 2,
                                            })}
                                            onClick={() => {
                                                toggle(2)
                                            }}
                                        >
                                            {"Ficha Técnica".toUpperCase()}
                                        </NavLink>
                                    </NavItem>
                                )}
                            </Nav>
                            <TabContent
                                activeTab={activeTab}
                                style={{ zIndex: 1 }}
                            >
                                <TabPane
                                    tabId={1}
                                    style={{
                                        marginRight: "0",
                                        fontSize: "1.5rem",
                                        lineHeight: "2.5rem",
                                        color: "#2b2b2b",
                                    }}
                                    className="relative border bottom-padding-default top-padding-default left-padding-default right-padding-default"
                                >
                                    <p>
                                        {
                                            productFromServer?.product
                                                ?.description
                                        }
                                    </p>
                                </TabPane>
                                {!!productFromServer?.product?.datasheet
                                    .length && (
                                    <TabPane
                                        tabId={2}
                                        style={{
                                            marginRight: "0",
                                            fontSize: "1.5rem",
                                            lineHeight: "2.5rem",
                                            color: "#2b2b2b",
                                        }}
                                        className="relative border bottom-padding-default top-padding-default left-padding-default right-padding-default"
                                    >
                                        {productFromServer?.product?.datasheet
                                            ?.length > 0 && (
                                            <table className="table table-striped  table-hover w-100 h-100 mb-0">
                                                <tbody>
                                                    {productFromServer?.product?.datasheet
                                                        ?.sort(
                                                            compareBy("name")
                                                        )
                                                        ?.map((sht) => (
                                                            <tr key={sht.id}>
                                                                <th>
                                                                    {sht.name}
                                                                </th>
                                                                <td>
                                                                    {
                                                                        sht.description
                                                                    }
                                                                </td>
                                                            </tr>
                                                        ))}
                                                </tbody>
                                            </table>
                                        )}
                                    </TabPane>
                                )}
                            </TabContent>
                        </Col>
                    </Row>
                </div>
                <div className="slide-product-bottom relative">
                    <Row>
                        <Col md="12">
                            <p className="bold title-slide-product-bottom">
                                PRODUTOS RELACIONADOS
                            </p>
                            <div className="slideRelated">
                                {related.length > 0 && (
                                    <Slider
                                        {...settings}
                                        className="showAboveTablet"
                                    >
                                        {related?.map((product, idx) => {
                                            return (
                                                <Col
                                                    key={product.id}
                                                    md="12"
                                                    className="product-category relative effect-hover-boxshadow animate-default"
                                                >
                                                    <div className="relative overfollow-hidden">
                                                        <LinkTo
                                                            _href="/details"
                                                            query={{
                                                                id: product.id,
                                                            }}
                                                        >
                                                            <div className="center-vertical-image">
                                                                <Img
                                                                    className="imageRelated"
                                                                    image_name_with_extension={
                                                                        product.thumb
                                                                            ? product
                                                                                  .thumb
                                                                                  ?.url
                                                                            : "250x250.png"
                                                                    }
                                                                    outSide={
                                                                        product.thumb
                                                                            ? product
                                                                                  .thumb
                                                                                  ?.url
                                                                            : ""
                                                                    }
                                                                    alt="Product Image . . ."
                                                                />
                                                            </div>
                                                        </LinkTo>
                                                    </div>
                                                    <h3 className="title-product clearfix full-width title-hover-black">
                                                        <a href="#">
                                                            {product.name}
                                                        </a>
                                                    </h3>
                                                    <p className="clearfix price-product">
                                                        <span className="price-old">
                                                            {product.oldPrice}
                                                        </span>{" "}
                                                        {product.price}
                                                    </p>
                                                </Col>
                                            )
                                        })}
                                    </Slider>
                                )}
                                {related.length > 0 && (
                                    <Slider
                                        {...settingsMobile}
                                        className="showTabletAndLess"
                                    >
                                        {related.map((product, idx) => {
                                            return (
                                                <Col
                                                    key={product.id}
                                                    md="12"
                                                    className="product-category relative effect-hover-boxshadow animate-default"
                                                >
                                                    <div className="relative overfollow-hidden">
                                                        <LinkTo
                                                            _href="/details"
                                                            query={{
                                                                id: product.id,
                                                            }}
                                                        >
                                                            <div className="center-vertical-image">
                                                                <Img
                                                                    className="imageRelated"
                                                                    image_name_with_extension={
                                                                        product.thumb
                                                                            ? product
                                                                                  .thumb
                                                                                  ?.url
                                                                            : "250x250.png"
                                                                    }
                                                                    outSide={
                                                                        product.thumb
                                                                            ? product
                                                                                  .thumb
                                                                                  ?.url
                                                                            : ""
                                                                    }
                                                                    alt="Product Image . . ."
                                                                />
                                                            </div>
                                                        </LinkTo>
                                                    </div>
                                                    <h3 className="title-product clearfix full-width title-hover-black">
                                                        <a href="#">
                                                            {product.name}
                                                        </a>
                                                    </h3>
                                                    <p className="clearfix price-product">
                                                        <span className="price-old">
                                                            {product.oldPrice}
                                                        </span>{" "}
                                                        {product.price}
                                                    </p>
                                                </Col>
                                            )
                                        })}
                                    </Slider>
                                )}
                            </div>
                        </Col>
                    </Row>
                </div>
            </Main>
        </>
    )
}
export const getServerSideProps = async ({ query }) => {
    let gridId = 1
    let product = {}
    let related = {}
    if (!query || hasKey(query, "id")) {
        product = await fromServer(`/products/find/${query.id}`, "", "get")
        if (!product || hasKey(product, "error")) {
            const { error } = product
            console.log("fail to load carts", error)
        } else {
            related = await fromServer(`/products/related/${query.id}/6`)
            const _serverOptionsGrid = await fromServer(
                `/v2/options/products/${product.product.id}`
            )
            if (!hasKey(_serverOptionsGrid, "error")) {
                const serverOptionsGrid = optionsParseFromSever(
                    _serverOptionsGrid
                )
                return {
                    props: {
                        product,
                        gridId,
                        related,
                        serverOptionsGrid,
                        _serverOptionsGrid,
                    },
                }
            }
        }
    }
    return {
        props: { gridId },
    }
}
export default ProductDetails
