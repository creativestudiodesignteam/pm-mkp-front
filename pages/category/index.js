import React, { useState, useEffect } from "react"
import Head from "next/head"
import Main from "../../src/components/Main"
import { Row, Col, Alert } from "reactstrap"
import SideMenu from "../../src/components/category/SideMenu"
import Products from "../../src/components/category/Products"
import { useRouter } from "next/router"
import { awaitable, fromServer } from "../../src/assets/helpers"
import dynamic from "next/dynamic"
import ClipLoader from "react-spinners/ClipLoader"

const SideMenuSub = dynamic(
    () => import("../../src/components/category/SideMenuSub"),
    {
        ssr: false,
        loading: function Loading() {
            return (
                <div className="spinnerLoader">
                    <ClipLoader
                        size={150}
                        color={"var(--red-1)"}
                        loading={true}
                    />
                </div>
            )
        },
    }
)

const Category = () => {
    const router = useRouter()
    const { id_sub, id_cat } = router.query

    const [products, setProducts] = useState([])
    const [activeTab, setActiveTab] = useState(0)
    const [subcategories, setSubCategories] = useState([])
    const [categories, setCategories] = useState([])
    const [page, setPage] = useState({
        categories: [0, 10],
        subcategories: [0, 10],
    })
    const [subcategoryName, setSubCategoryName] = useState("")
    const [categoryName, setCategoryName] = useState("")
    const [showSub, setShowSub] = useState(false)

    const [loaderState, setLoaderState] = useState(false)

    const [alert, setAlert] = useState(false)
    const toggleAlert = () => setAlert(!alert)

    useEffect(() => {
        awaitable(async () => {
            setLoaderState(true)
            const dataCat = await fromServer(`/categories`, "", "get")
            if (dataCat.length > 0) {
                setCategories(dataCat)
                if (id_sub !== undefined && id_cat !== undefined) {
                    await getSubs(id_cat, id_sub)
                    await getProductsSub(id_sub)
                    setLoaderState(false)
                } else if (id_cat !== undefined && id_sub === undefined) {
                    await getSubs(id_cat)
                    await getProductsCat(id_cat)
                    const categoryActual = dataCat.find((cat) => {
                        if (cat.id === Number(id_cat)) {
                            return cat
                        }
                    })
                    setCategoryName(categoryActual.name)
                    setLoaderState(false)
                } else {
                    await getSubs(1)
                    setLoaderState(false)
                }
            } else {
                setAlert(true)
                setLoaderState(false)
            }
            setLoaderState(false)
        })
    }, [])

    const toggle = (tab) => {
        if (activeTab !== tab) setActiveTab(tab)
    }
    const getProductsCat = async (idCat) => {
        setLoaderState(true)
        const data = await fromServer(
            `/products/categories/${idCat}`,
            "",
            "get"
        )
        if (data.length < 1) {
            setAlert(true)
            setLoaderState(false)
        } else {
            setAlert(false)
            setLoaderState(false)
        }

        setShowSub(false)
        setProducts(data)
        toggle(Number(idCat))
    }
    const getProductsSub = async (idSub) => {
        setLoaderState(true)

        const data = await fromServer(`/prodsubcats/${idSub}`, "", "get")
        if (data.length < 1) {
            setAlert(true)
            setLoaderState(false)
        } else {
            setAlert(false)
            setLoaderState(false)
        }
        setShowSub(true)
        setProducts(data)
    }
    const getSubs = async (idCat, idSub) => {
        setLoaderState(true)
        const data = await fromServer(`/subcategories/${idCat}`, "", "get")

        if (data && data.length > 0) {
            setSubCategories(data)
            if (idSub !== undefined) {
                const subactual = data.find((sub) => {
                    if (sub.id === Number(idSub)) {
                        return sub
                    }
                })
                setSubCategoryName(subactual.name)
                toggle(Number(idCat))
                setLoaderState(false)
            }
            setLoaderState(false)
        } else {
            setAlert(true)
            setLoaderState(false)
        }
    }

    const [currentPage, setCurrentPage] = useState(0)
    const pageSize = 9
    const pagesCount = Math.ceil(products.length / pageSize)

    return (
        <>
            <Head>
                <title>Portal da Maria | Categoria</title>
                <link rel="icon" href={"/favicon.ico"} />
                <meta name="format-detection" content="telephone=no" />
                <meta
                    httpEquiv="Content-Type"
                    content="text/html; charset=UTF-8"
                />
                <meta
                    name="viewport"
                    content="width=device-width, initial-scale=1, shrink-to-fit=no"
                />
                <meta httpEquiv="X-UA-Compatible" content="IE=edge" />
                <link
                    href="https://fonts.googleapis.com/css?family=Montserrat%7CRoboto:100,300,400,500,700,900%7CRoboto+Condensed:100,300,400,500,700"
                    rel="stylesheet"
                />
            </Head>
            <Main>
                {loaderState && (
                    <div className="spinnerLoader">
                        <ClipLoader
                            size={150}
                            color={"var(--red-1)"}
                            loading={loaderState}
                        />
                    </div>
                )}
                <Row>
                    <Col
                        lg="3"
                        className="relative right-padding-default clear-padding paddingMobileCategory"
                    >
                        <SideMenu
                            page={page}
                            categories={categories.slice(...page.categories)}
                            setPage={setPage}
                            activeTab={activeTab}
                            toggle={toggle}
                            nameCategory={"Categorias"}
                            getSubs={getSubs}
                            setSubCategories={setSubCategories}
                            subcategories={subcategories}
                            setSubCategoryName={setSubCategoryName}
                            getProductsCat={getProductsCat}
                            setCategoryName={setCategoryName}
                        />
                        <SideMenuSub
                            page={page}
                            setPage={setPage}
                            subcategories={subcategories.slice(
                                ...page.subcategories
                            )}
                            activeTab={activeTab}
                            nameCategory={"Subcategorias"}
                            setSubCategoryName={setSubCategoryName}
                            getProductsSub={getProductsSub}
                            toggle={toggle}
                            setCategoryName={setCategoryName}
                        />
                    </Col>

                    <Col lg="9" className="relative clear-padding">
                        {alert && (
                            <>
                                {!showSub && (
                                    <p className="titleCategory">
                                        {categoryName}
                                    </p>
                                )}
                                {showSub && (
                                    <p className="titleCategory">
                                        {subcategoryName}
                                    </p>
                                )}
                                <Alert
                                    color="primary"
                                    isOpen={alert}
                                    toggle={toggleAlert}
                                    style={{
                                        opacity: 1,
                                        color: "white",
                                        backgroundColor: "var(--red-1)",
                                        padding: "2rem",
                                        fontSize: "16px",
                                        textAlign: "center",
                                        marginTop: "2rem",
                                    }}
                                >
                                    Não foram encontrados produtos
                                </Alert>
                            </>
                        )}
                        {!alert && (
                            <Products
                                categories={categories}
                                activeTab={activeTab}
                                currentPage={currentPage}
                                setCurrentPage={setCurrentPage}
                                pageSize={pageSize}
                                pagesCount={pagesCount}
                                products={products}
                                subcategoryName={subcategoryName}
                                showSub={showSub}
                            />
                        )}
                    </Col>
                </Row>
            </Main>
        </>
    )
}

export default Category
