import React, {useEffect} from "react"
import propTypes from "prop-types"
import Head from "next/head"
import Main from "../../src/components/Main"
import dynamic from "next/dynamic"
import {fromServer, hasKey} from "../../src/assets/helpers"
import {redirectIfIsLogged, userByToken} from "../../src/assets/auth"
import {MySwal} from "../../src/assets/sweetalert"
import {useRouter} from "next/router"
import ClipLoader from "react-spinners/ClipLoader"

const LoginWrap = dynamic(
    () => import("../../src/components/authentication/LoginWrap"),
    {
        ssr: false,
        loading: function Loading() {
            return (
                <div className="spinnerLoader">
                    <ClipLoader
                        size={150}
                        color={"var(--red-1)"}
                        loading={true}
                    />
                </div>
            )
        },
    }
)

function Login({context, dispatch, query, ...props}) {
    const router = useRouter()
    useEffect(() => {
        redirectIfIsLogged(context.token, router, '/account', true)
        if (!!query?.activate_token) {
            fromServer("/activate_user", false, "PUT", {
                token: query.activate_token,
            })
                .then((r) => {
                    if (hasKey(r, "error")) {
                        MySwal.fire(r?.error?.message, "", "error").then(() => {
                            router.push("/login")
                        })
                    } else {
                        MySwal.fire(r?.message, "", "success").then(() => {
                            router.push("/login")
                        })
                    }
                })
                .catch((error) => {
                    MySwal.fire(
                        `<pre>${JSON.stringify(error)}</pre>`,
                        "",
                        "info"
                    )
                    router.push("/login")
                })
        }
    }, [])
    useEffect(() => {
        redirectIfIsLogged(context.token, router, '/account', true)
    }, [context.token])
    return (
        <>
            <Head>
                <title>Portal da Maria</title>
                <link rel="icon" href={"/favicon.ico"}/>
                <meta name="format-detection" content="telephone=no"/>
                <meta
                    httpEquiv="Content-Type"
                    content="text/html; charset=UTF-8"
                />
                <meta
                    name="viewport"
                    content="width=device-width, initial-scale=1, shrink-to-fit=no"
                />
                <meta httpEquiv="X-UA-Compatible" content="IE=edge"/>
                <link
                    href="https://fonts.googleapis.com/css?family=Montserrat%7CRoboto:100,300,400,500,700,900%7CRoboto+Condensed:100,300,400,500,700"
                    rel="stylesheet"
                />
            </Head>
            <Main
                _BreadCrumb={[
                    {
                        node_text: "authentication",
                        href: "/login",
                    },
                ]}
            >
                <LoginWrap context={context} dispatch={dispatch} {...props} />
            </Main>
        </>
    )
}

Login.propTypes = {
    context: propTypes.object.isRequired,
    dispatch: propTypes.func.isRequired,
}
export const getServerSideProps = async ({res, query}) => {
    if (hasKey(query, "token")) {
        if (query.token.length) {
            const user = userByToken(query.token)
            if (!user) {
                console.log("Fail-", query.token, user)
            } else {
                res.setHeader("location", "/account?token=" + query.token)
                res.statusCode = 302
                res.end()
            }
        }
    }
    return {
        props: {query},
    }
}
export default Login
