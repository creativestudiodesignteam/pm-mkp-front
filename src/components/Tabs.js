import React, { useEffect, useState } from "react"
import dynamic from "next/dynamic"
import propTypes from "prop-types"
import { Nav, NavItem, NavLink, TabContent, TabPane } from "reactstrap"
import classnames from "classnames"
import { add_invalid, awaitable, elmAll, fromServer } from "../assets/helpers"
import GridOptions from "./product/gridOption/index"
import SheetOption from "./product/SheetOption"
import ClipLoader from "react-spinners/ClipLoader"

const GeralOption = dynamic(() => import("./product/GeralOption"), {
    ssr: false,
    loading: function Loading() {
        return (
            <div className="spinnerLoader">
                <ClipLoader size={150} color={"var(--red-1)"} loading={true} />
            </div>
        )
    },
})
const DataOption = dynamic(() => import("./product/DataOption"), {
    ssr: false,
    loading: function Loading() {
        return (
            <div className="spinnerLoader">
                <ClipLoader size={150} color={"var(--red-1)"} loading={true} />
            </div>
        )
    },
})

const Tabs = ({
    state,
    setState,
    grid,
    changeGrid,
    token,
    thumb,
    setThumb,
    defaultState = {},
    packing,
    setPacking,
    setCondition,
    condition,
}) => {
    const [activeTab, setActiveTab] = useState("1")
    const toggle = (tab) => {
        if (activeTab !== tab) setActiveTab(tab)
    }

    function listener({ target: { textContent: name, value: id } }) {
        event.preventDefault()
        setState({
            ...state,
            current_category: { id, name },
            sub_categories: [],
        })
    }

    useEffect(
        function() {
            awaitable(async () => {
                for (const node of elmAll(
                    `#multiselectContainerReact>div>span`
                )) {
                    node.remove()
                }
                if (
                    state &&
                    state.current_category &&
                    state.current_category.id
                ) {
                    const sub_categories = await fromServer(
                        `/subcategories/${state.current_category.id}`,
                        token
                    )
                    sub_categories && setState({ ...state, sub_categories })
                }
            })
        },
        [state.current_category]
    )

    return (
        <>
            {
                <Nav
                    id="nav-tabs"
                    tabs
                    style={{ cursor: "pointer" }}
                    className="w-75 mx-auto displayFlex flex-row justify-content-start mb-4 showAboveTablet"
                >
                    <NavItem className="mr-2">
                        <NavLink
                            className={classnames({
                                active: activeTab === "1",
                            })}
                            onClick={() => {
                                toggle("1")
                            }}
                        >
                            Dados do produto
                        </NavLink>
                    </NavItem>
                    <NavItem className="mr-2">
                        <NavLink
                            className={classnames({
                                active: activeTab === "2",
                            })}
                            onClick={() => {
                                toggle("2")
                            }}
                        >
                            Embalagem
                        </NavLink>
                    </NavItem>
                    <NavItem className="mr-2">
                        <NavLink
                            className={classnames({
                                active: activeTab === "3",
                            })}
                            onClick={() => {
                                toggle("3")
                            }}
                        >
                            Grade e fotos
                        </NavLink>
                    </NavItem>
                    <NavItem className="mr-2">
                        <NavLink
                            className={classnames({
                                active: activeTab === "4",
                            })}
                            onClick={() => {
                                toggle("4")
                            }}
                        >
                            Ficha Técnica
                        </NavLink>
                    </NavItem>
                </Nav>
            }
            {
                <Nav
                    id="nav-tabs"
                    tabs
                    className="w-100 mx-auto displayFlex flex-row justify-content-center mb-4 showTabletAndLess"
                >
                    <NavItem className="mr-2">
                        <NavLink
                            className={classnames({
                                active: activeTab === "1",
                            })}
                            onClick={() => {
                                toggle("1")
                            }}
                        >
                            Dados do produto
                        </NavLink>
                    </NavItem>
                    <NavItem className="mr-2">
                        <NavLink
                            className={classnames({
                                active: activeTab === "2",
                            })}
                            onClick={() => {
                                toggle("2")
                            }}
                        >
                            Embalagem
                        </NavLink>
                    </NavItem>
                    <NavItem className="mr-2">
                        <NavLink
                            className={classnames({
                                active: activeTab === "3",
                            })}
                            onClick={() => {
                                toggle("3")
                            }}
                        >
                            Grade e fotos
                        </NavLink>
                    </NavItem>
                    <NavItem className="mr-2">
                        <NavLink
                            className={classnames({
                                active: activeTab === "4",
                            })}
                            onClick={() => {
                                toggle("4")
                            }}
                        >
                            Ficha Técnica
                        </NavLink>
                    </NavItem>
                </Nav>
            }
            {
                <TabContent
                    id="general"
                    className="w-75 mx-auto"
                    activeTab={activeTab}
                >
                    <TabPane tabId="1">
                        <GeralOption
                            condition={condition}
                            setCondition={setCondition}
                            defaultState={defaultState}
                            listener_sub_caregories={(list) =>
                                setState({
                                    ...state,
                                    current_subcategory: list,
                                })
                            }
                            categories={state.categories}
                            sub_categories={state.sub_categories}
                            listener={listener}
                        />
                    </TabPane>
                    <TabPane tabId="2">
                        <DataOption
                            defaultState={defaultState}
                            packing={packing}
                            setPacking={setPacking}
                        />
                    </TabPane>
                    <TabPane tabId="3">
                        <GridOptions
                            defaultState={defaultState}
                            thumb={thumb}
                            setThumb={setThumb}
                            token={token}
                            grid={grid}
                            changeGrid={changeGrid}
                        />
                    </TabPane>
                    <TabPane tabId="4">
                        <SheetOption grid={grid} changeGrid={changeGrid} />
                    </TabPane>
                </TabContent>
            }
        </>
    )
}
Tabs.propTypes = {
    state: propTypes.object.isRequired,
    user: propTypes.object.isRequired,
    token: propTypes.string.isRequired,
    setState: propTypes.func.isRequired,
    dispatch: propTypes.func.isRequired,
    grid: propTypes.object.isRequired,
    changeGrid: propTypes.func.isRequired,
    thumb: propTypes.string.isRequired,
    setThumb: propTypes.func.isRequired,
}
export default Tabs
