import React, {useEffect} from "react"
import propTypes from "prop-types"
import {Col, Row} from "reactstrap"
import InputDefault from "../../InputDefault"
import SelectDefault from "../../SelectDefault"
import SelectCustom from "../../SelectCustom"
import {elm, obrigatory_blur} from "../../../assets/helpers"

const GeralOption = ({
                         sub_categories,
                         categories,
                         listener,
                         listener_sub_caregories,
                         defaultState,
                         setCondition,
                         condition,
                     }) => {
    useEffect(() => {
        //console.log("!!!!!!!!!!!!!!!!!!!", {sub_categories, defaultState})
        if (!!defaultState?.id) {
            elm("[name=name]").value = defaultState?.name || ""
            elm("[name=description]").value = defaultState?.description || ""
            elm("[name=brand]").value = defaultState?.brand || ""
            elm("[name=model]").value = defaultState?.model || ""
            elm("[name=sku]").value = defaultState?.sku || ""
        }
    })
    return (
        <>
            <Row className="mb-5">
                <Col>
                    <h1 className={"h1"}>{"Cadastrando seu produto"}</h1>
                </Col>
            </Row>
            <Row>
                <Col sm="12">
                    <SelectDefault
                        id="category"
                        name="fk_categories"
                        has_label
                        label="Selecione sua categoria *"
                        className="mb-4"
                        initial_value={
                            categories.find(
                                (c) => c.id === defaultState?.categories?.id
                            )?.id
                        }
                        values={categories}
                        listener={listener}
                    />
                    <SelectCustom
                        onRemove={listener_sub_caregories}
                        onSelect={listener_sub_caregories}
                        id="sub-category"
                        name="subcategories"
                        has_label
                        label="Selecione sua subcategoria"
                        values={sub_categories}
                        className="mb-4"
                    />
                    <InputDefault
                        name="name"
                        has_label
                        label="Nome do produto *"
                        blur={e => obrigatory_blur(e, false, false)}
                        _required
                        className="mb-4"
                    />
                    <InputDefault
                        name="description"
                        styleInput={{
                            fontSize: "14px",
                            border: "solid 1px #dedede",
                            backgroundColor: "#f7f7f7",
                        }}
                        placeholder={
                            "Descreva seu produto com as características que ele possui, e diga para o que ele serve"
                        }
                        type="textarea"
                        rows="10"
                        cols="10"
                        has_label
                        label="Descrição do produto *"
                        className="mb-4"
                        _maxLength={"10000"}
                        _required
                        blur={e => obrigatory_blur(e, false, false)}
                    />
                    <InputDefault
                        name="brand"
                        has_label
                        label="Seu produto possui marca? Se sim, qual?"
                        className="mb-4"
                    />
                    <InputDefault
                        has_label
                        name="model"
                        label="Qual é a referência ou código do seu produto?"
                        className="mb-4"
                    />
                    <SelectDefault
                        name={"condition"}
                        id={"condition"}
                        has_label
                        label={"Selecione a condição do produto"}
                        initial_value={
                            [
                                {id: 1, name: "Novo"},
                                {
                                    id: 2,
                                    name: "Usado",
                                },
                            ].filter(({name}) => name === condition)?.id || ""
                        }
                        values={[
                            {id: 1, name: "Novo"},
                            {id: 2, name: "Usado"},
                        ]}
                        className={"w-100"}
                        _required
                        listener={(e) => {
                            setCondition(e.target?.selectedOptions[0]?.text)
                            e.preventDefault()
                        }}
                    />
                    <InputDefault
                        name="sku"
                        has_label
                        label="Código de barras"
                        className="mb-4"
                    />
                </Col>
            </Row>
        </>
    )
}
GeralOption.propTypes = {
    listener_sub_caregories: propTypes.func.isRequired,
    sub_categories: propTypes.array.isRequired,
    categories: propTypes.array.isRequired,
    listener: propTypes.func.isRequired,
}

export default GeralOption
