import React, { useContext, useEffect, useState } from "react"
import Collapse from "../../pure/Collapse"
import Attr from "../gridOption/components/Attr"
import { Col, Label, Row } from "reactstrap"
import { getToken } from "../../../assets/auth"
import SelectCustom from "../../SelectCustom"
import Img from "../../Img"
import Button from "@material-ui/core/Button"
import ImageUploading from "react-images-uploading"
import InputDefault from "../../InputDefault"
import {awaitable, elm, forceFormat, fromServer, OBJ} from "../../../assets/helpers"
import styles from "../gridOption/components/PhotoUpload/index.module.css"
import { Context } from "../../../store"
import useForceUpdate from "use-force-update"
import "array.prototype.move"
import PhotoUploader from "../../photoUploader"
import IntlCurrencyInput from "react-intl-currency-input"

const Grid_ = ({
    grid,
    grids,
    options_names,
    idx,
    setGrids,
    fields,
    setFields,
    gridPayload,
    setGridPayload,
    updateAttr,
}) => {
    const forceUpdate = useForceUpdate()
    const [{ token }] = useContext(Context)
    const [options, setOptions] = useState([])
    const [gallery, setGallery] = useState([])
    const [_cgp] = gridPayload.filter((gp) => gp.id === grid.id)
    const products_options = grid?.options_grids?.map(
        ({ options: { id, name } }) => ({ id, name })
    )
    const products_options_name = _cgp.options_grids.filter((optg) =>
        OBJ.hasKey(optg, "fk_options_names")
    )

    const maxMbFileSize = 5 * 1024 * 1024 // 5Mb
    const currencyConfig = {
        locale: "pt-BR",
        formats: {
            number: {
                BRL: {
                    minimumFractionDigits: 2,
                    maximumFractionDigits: 2,
                },
            },
        },
    }
    function updateOptions(products_options_name) {
        return (selectedList) => {
            const [c_g] = gridPayload.filter((cg) => cg.id === grid.id)
            // console.log("updateOptions", c_g)
            const newState = gridPayload.map((_gridPayload) =>
                _gridPayload.id === grid.id
                    ? {
                          ..._gridPayload,
                          options_grids: selectedList.map(
                              ({ id: fk_options }) => {
                                  const [has] = c_g.options_grids.filter(
                                      (opg) => opg.fk_options === fk_options
                                  )
                                  if (has) {
                                      return {
                                          ...has,
                                      }
                                  } else {
                                      return {
                                          fk_options,
                                      }
                                  }
                              }
                          ),
                      }
                    : _gridPayload
            )
            setGridPayload(newState)
            setOptions(
                selectedList.map(function(value) {
                    return (
                        <Collapse
                            key={value.id}
                            _key={value.id}
                            title={value.name}
                            collapseOpen={true}
                        >
                            <Attr
                                listener={updateAttr(grid, value, newState)}
                                selected_id_list={products_options_name
                                    .filter((op) => op.fk_options === value.id)
                                    .map(({ fk_options_names: id }) => id)}
                                current_grid_id={"current_grid_id"}
                                name={value.name}
                                token={token || getToken()}
                                option_id={value.id}
                                changeGrid={false}
                                grid={false}
                            />
                        </Collapse>
                    )
                })
            )
        }
    }

    function onChange(id) {
        return (imageList) => {
            const newImages = imageList.filter((it) => {
                let newImage = true
                grid.gallery.forEach((photo) => {
                    //console.log("newImageForEach", photo.id, it.id)
                    if (it.id !== undefined && photo.id === it.id) {
                        newImage = false
                    }
                })
                if (newImage) {
                    return it.dataURL
                }
            })
            let _grid = null
            if (!JSON.parse(localStorage.getItem("grids") || "[]").length) {
                _grid = grids.map((grid) =>
                    grid.id === id ? { ...grid, newImages } : grid
                )
                //console.log({_grid})
            } else {
                _grid = JSON.parse(localStorage.getItem("grids")).map((grid) =>
                    grid.id === id ? { ...grid, newImages } : grid
                )
                //console.log({_grid})
            }
            //console.log(_grid)
            setGrids(_grid)
            forceUpdate()
            /*console.log("newImages was changed", {
                newImages,
                grids,
                _grid,
                imageList,
            })*/
        }
    }

    useEffect(() => {
        setFields({ ...fields, grids })
        setGridPayload(
            gridPayload.map((_gridPayload) =>
                _gridPayload.id === grid.id
                    ? {
                          ..._gridPayload,
                          price: elm(`#price-${grid.id}`).value,
                          amount: elm(`#input-amount-${grid.id}`).value,
                      }
                    : _gridPayload
            )
        )
    }, [grids])
    useEffect(() => {
        // console.log("========", { _cgp, products_options_name })
        if (!options.length) {
            updateOptions(products_options_name)(products_options)
        }
        if (!gallery.length) {
            const galleryDefault = grid.gallery.map((photo) => {
                return { dataURL: photo.files.url, id: photo.id }
            })
            //console.log({galleryDefault})
            setGallery(galleryDefault)
            forceUpdate()
        }
        //console.log({grid})
    }, [])
    return (
        <>
            <Row className="m-0 mb-4 ">
                <Col xs="6" className="mb-4">
                    <div>
                        <h4>
                            Imagens da Grid
                            <span className="text-muted"> {idx + 1}</span>
                        </h4>
                    </div>
                </Col>
                {idx > 0 && (
                    <Col xs="6" className="mb-4">
                        <div className="d-flex flex-row justify-content-center align-items-center h-100 w-100">
                            <Button
                                outline="true"
                                className="redButtonOutline "
                                onClick={(e) => {
                                    e.preventDefault()
                                    setGrids(
                                        grids.filter((g) => g.id !== grid.id)
                                    )
                                }}
                            >
                                REMOVER GRID
                            </Button>
                        </div>
                    </Col>
                )}
            </Row>
            <Row>
                {gallery.length > 0 && (
                    <ImageUploading
                        maxNumber={3}
                        multiple
                        maxFileSize={maxMbFileSize}
                        acceptType={["jpg", "png", "jpeg"]}
                        defaultValue={gallery}
                        onChange={onChange(grid.id)}
                    >
                        {({ imageList, onImageUpload }) => (
                            <>
                                {imageList.map((image, idx) => (
                                    <Col
                                        xs="12"
                                        md="4"
                                        className={`mb-4 allignerCenter`}
                                        key={image.key}
                                        style={{ flexDirection: "column" }}
                                    >
                                        <Img
                                            className="buttonAddImage"
                                            image_name_with_extension={
                                                image?.dataURL
                                            }
                                            outSide={image?.dataURL}
                                            alt="Product Image . . ."
                                        />
                                        <span className="inline">&nbsp;</span>
                                        <Row className={styles.aligner}>
                                            <Col
                                                xs="12"
                                                className="allignerCenter mb-4"
                                            >
                                                <Button
                                                    disabled={idx === 0}
                                                    onClick={(e) => {
                                                        e.preventDefault()
                                                        imageList.move(
                                                            idx,
                                                            idx - 1
                                                        )
                                                        const result = grids.map(
                                                            (_grid) =>
                                                                _grid.id ===
                                                                grid.id
                                                                    ? {
                                                                          ..._grid,
                                                                          gallery: _grid.gallery.move(
                                                                              idx,
                                                                              idx -
                                                                                  1
                                                                          ),
                                                                      }
                                                                    : _grid
                                                        )
                                                        //console.log({result})
                                                        localStorage.setItem(
                                                            "grids",
                                                            JSON.stringify(
                                                                result
                                                            )
                                                        )
                                                        onChange(grid.id)
                                                        forceUpdate()
                                                    }}
                                                    className="dark2ButtonOutline"
                                                >
                                                    Mover para esquerda
                                                </Button>
                                            </Col>
                                            <Col
                                                xs="12"
                                                className="allignerCenter mb-4"
                                            >
                                                <Button
                                                    disabled={
                                                        idx ===
                                                        imageList.length - 1
                                                    }
                                                    onClick={(e) => {
                                                        e.preventDefault()
                                                        imageList.move(
                                                            idx,
                                                            idx + 1
                                                        )
                                                        const result = grids.map(
                                                            (_grid) =>
                                                                _grid.id ===
                                                                grid.id
                                                                    ? {
                                                                          ..._grid,
                                                                          gallery: _grid.gallery.move(
                                                                              idx,
                                                                              idx +
                                                                                  1
                                                                          ),
                                                                      }
                                                                    : _grid
                                                        )
                                                        //console.log({result})
                                                        localStorage.setItem(
                                                            "grids",
                                                            JSON.stringify(
                                                                result
                                                            )
                                                        )
                                                        onChange(grid.id)
                                                        forceUpdate()
                                                    }}
                                                    className="dark2ButtonOutline"
                                                >
                                                    Mover para direita
                                                </Button>
                                            </Col>
                                            <Col
                                                xs="12"
                                                className="allignerCenter mb-4"
                                            >
                                                <Button
                                                    onClick={(e) => {
                                                        const result = grids.map(
                                                            (_grid) =>
                                                                _grid.id ===
                                                                grid.id
                                                                    ? {
                                                                          ..._grid,
                                                                          gallery: _grid.gallery.filter(
                                                                              (
                                                                                  g
                                                                              ) => {
                                                                                  if (
                                                                                      image.id &&
                                                                                      g.id !==
                                                                                          image.id
                                                                                  ) {
                                                                                      return g
                                                                                  }
                                                                              }
                                                                          ),
                                                                      }
                                                                    : _grid
                                                        )
                                                        //console.log({result})
                                                        localStorage.setItem(
                                                            "grids",
                                                            JSON.stringify(
                                                                result
                                                            )
                                                        )
                                                        image.onRemove()
                                                        /*console.log(
                                                            "remove: ",
                                                            {
                                                                imageList,
                                                                grids,
                                                            }
                                                        )*/
                                                    }}
                                                    className="redButtonOutline"
                                                >
                                                    Remover
                                                </Button>
                                            </Col>
                                        </Row>
                                    </Col>
                                ))}
                                <Col
                                    xs="12"
                                    md="3"
                                    className={`${
                                        imageList?.length < 3 ? "" : "d-none"
                                    }
                                        buttonAddImage`}
                                >
                                    <Button
                                        outline="true"
                                        onClick={(e) => {
                                            /*console.log({
                                                imageList,
                                                gallery,
                                            })*/
                                            onImageUpload()
                                            e.preventDefault()
                                        }}
                                        className={`${
                                            styles.buttonUpload
                                        } addPhotoAllign`}
                                    >
                                        <img
                                            src={"/img/ar-camera.svg"}
                                            alt=""
                                        />
                                        <span>
                                            <p className="mt-3">
                                                Adicionar fotos
                                            </p>
                                        </span>
                                    </Button>
                                </Col>
                            </>
                        )}
                    </ImageUploading>
                )}{" "}
            </Row>
            <Row classname="m-0 mb-4">
                <Col xs="12" md="6">
                    <SelectCustom
                        className="w-75 mt-4"
                        name="options"
                        id="options"
                        has_label
                        label={"Características e atributos"}
                        selectedValues={products_options}
                        values={options_names}
                        onRemove={updateOptions(products_options_name)}
                        onSelect={updateOptions(products_options_name)}
                        placeholder={"Selecione uma ou mais características"}
                    />
                </Col>
                <Col xs="12" className="mb-4">
                    {options}
                </Col>
            </Row>
            <Row className="mb-4">
                <Col md="6">
                    <InputDefault
                        type="number"
                        name={`amount-${grid.id}`}
                        label="Quantidade"
                        has_label
                        _required
                        initial_value={grid.amount}
                        onChange={(e) => {
                            setGrids(
                                grids.map((g) =>
                                    g.id === grid.id
                                        ? {
                                              ...g,
                                              amount: e.target.value,
                                          }
                                        : g
                                )
                            )
                        }}
                    />
                </Col>
                <Col md="6">
                    <div className="form-group">
                        <Label for={`price-${grid.id}`} className="d-block">
                            Preço
                        </Label>
                        <IntlCurrencyInput
                            className="pl-4 form-control w-100"
                            name={`price-${grid.id}`}
                            id={`price-${grid.id}`}
                            currency="BRL"
                            config={currencyConfig}
                            max={1000000}
                            defaultValue={`${Number(
                                Number(grid.price)
                            )}`.replace(".", ",")}
                            onBlur={(event, value) => {
                                event.preventDefault()
                                console.log(
                                    `${Number(Number(grid.price))}`.replace(
                                        ".",
                                        ","
                                    )
                                )
                                console.log(value)
                                setGrids(
                                    grids.map((g) =>
                                        g.id === grid.id
                                            ? {
                                                  ...g,
                                                  price: value * 100,
                                              }
                                            : g
                                    )
                                )
                            }}
                        />
                    </div>
                </Col>
            </Row>
        </>
    )
}
const GridList = ({
    id,
    token,
    fields,
    setFields,
    gridPayload,
    setGridPayload,
    updateAttr,
}) => {
    const [grids, setGrids] = useState([])
    const [thumb, setThumb] = useState({})
    const [newThumb, setNewThumb] = useState(false)
    const maxMbFileSize = 5 * 1024 * 1024 // 5Mb
    const [loading, setLoading] = useState(true)
    /*useEffect(() => {
        console.log({thumb, newThumb})
    }, [thumb, newThumb])
    useEffect(() => {
        console.log({thumb, newThumb})
    }, [])*/

    function onChangePrincipal(imageList) {
        //console.log("onChangePrincipal", {imageList})
        setNewThumb(imageList[0]?.dataURL)
        setFields({ ...fields, thumb: { base64: imageList[0]?.dataURL } })
    }

    function removePrincipal(e) {
        e.preventDefault()
        setThumb(false)
        setNewThumb(true)
    }

    const [options_names, setOptions_names] = useState([])
    /*useEffect(() => {
        console.log("grid has changed OUT", {grids})
    }, [grids])*/
    useEffect(() => {
        awaitable(async () => {
            if (!options_names.length) {
                const server_option = await fromServer("/options", token)
                if (!OBJ.hasKey(server_option, "error")) {
                    setOptions_names(
                        server_option.map(({ options: { id, name } }) => ({
                            id,
                            name,
                        }))
                    )
                }
            }
            if (loading) {
                const response = await fromServer(
                    `/products/${id}`,
                    token || getToken(),
                    "get"
                )
                // console.log({ response })
                if (!response?.error && response?.grids && !grids.length) {
                    setGrids(response.grids)
                    setThumb(response.thumb)
                    setLoading(false)
                } else {
                    console.log("no grids", response)
                }
            }
        })
    }, [id])
    useEffect(() => {
        localStorage.setItem("grids", "")
    }, [])

    function getDefaultValueToPhotoUploader() {
        const result =
            !!newThumb && newThumb !== true
                ? [{ dataURL: newThumb }]
                : !!thumb
                ? [{ dataURL: thumb.url }]
                : []
        //console.log({result})
        return result
    }

    return (
        <>
            <Row className="m-0 mb-4">
                <Col xs="12">
                    <h4>Imagem principal</h4>
                </Col>
                {!newThumb && (
                    <Col
                        md="4"
                        className="allignerCenter"
                        style={{ flexDirection: "column" }}
                    >
                        {Boolean(thumb?.url) && (
                            <Img
                                className="buttonAddImage"
                                image_name_with_extension={thumb?.url}
                                outSide={thumb?.url}
                                alt="Product Image . . ."
                            />
                        )}

                        <Button
                            className="redButtonOutline"
                            onClick={removePrincipal}
                        >
                            REMOVER
                        </Button>
                    </Col>
                )}
                {newThumb && (
                    <Col
                        md="4"
                        className="allignerCenter"
                        style={{ flexDirection: "column" }}
                    >
                        <PhotoUploader
                            maxNumber={1}
                            maxFileSize={maxMbFileSize}
                            defaultValue={getDefaultValueToPhotoUploader()}
                            acceptType={["jpg", "png", "jpeg"]}
                            listener={onChangePrincipal}
                        />
                    </Col>
                )}
            </Row>
            {grids?.map((grid, idx) => (
                <Grid_
                    updateAttr={updateAttr}
                    gridPayload={gridPayload}
                    setGridPayload={setGridPayload}
                    fields={fields}
                    setFields={setFields}
                    options_names={options_names}
                    setGrids={setGrids}
                    key={idx}
                    grid={grid}
                    idx={idx}
                    grids={grids}
                />
            ))}
        </>
    )
}

export default GridList
