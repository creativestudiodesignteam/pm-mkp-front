import React, { useState } from "react"
import { Button, Col, Row, Table } from "reactstrap"
import styles from "./index.module.css"
import propTypes from "prop-types"
import InputDefault from "../../InputDefault"

const SheetOption = ({ grid, changeGrid, defaultState }) => {
    const [rows, setRows] = useState([{ id: 1, name: "", description: "" }])

    function handleAddRow(event) {
        event.preventDefault()
        setRows([
            ...rows,
            { id: Number((rows.length && rows[rows.length - 1].id + 1) || 1) },
        ])
    }

    function syncGlobal(id) {
        return (event) => {
            event.preventDefault()
            const actual = {
                ...grid.sheetOption.filter((value) => value.id === id)[0],
                [event.target.name]: event.target.value,
                id,
            }
            changeGrid({
                ...grid,
                sheetOption: [
                    ...grid.sheetOption.filter((value) => value.id !== id),
                    actual,
                ],
            })
        }
    }

    return (
        <Row className="mb-5">
            <Col>
                <h1 className={"h1"}>Ficha técnica:</h1>
                <Row className={styles.aligner}>
                    <Col xs="12">
                        <Row className="mb-5">
                            <Col
                                xs={{ size: "12" }}
                                md={{ size: "8", offset: 2 }}
                            >
                                <Table size="sm">
                                    <tbody>
                                        {rows &&
                                            rows.map((row, idx) => (
                                                <tr key={idx}>
                                                    <td
                                                        className={
                                                            styles.tableSheetLeft
                                                        }
                                                    >
                                                        <InputDefault
                                                            type="text"
                                                            initial_value={
                                                                row.name
                                                            }
                                                            className={
                                                                styles.inputLeft
                                                            }
                                                            placeholder="Titulo"
                                                            name={`sheetOption-label-${
                                                                row.id
                                                            }`}
                                                            listenner={syncGlobal(
                                                                row.id
                                                            )}
                                                        />
                                                    </td>
                                                    <td
                                                        className={
                                                            styles.tableSheetRight
                                                        }
                                                    >
                                                        <InputDefault
                                                            initial_value={
                                                                row.description
                                                            }
                                                            type="text"
                                                            className={
                                                                styles.inputRight
                                                            }
                                                            placeholder="Definição"
                                                            name={`sheetOption-data-${
                                                                row.id
                                                            }`}

                                                            listenner={syncGlobal(
                                                                row.id
                                                            )}
                                                        />
                                                    </td>
                                                    <td
                                                        className={
                                                            styles.tableButton
                                                        }
                                                    >
                                                        <Button
                                                            size="lg"
                                                            block
                                                            className={
                                                                styles.removeLine
                                                            }
                                                            onClick={(
                                                                event
                                                            ) => {
                                                                event.preventDefault()
                                                                const newSheetOption = grid.sheetOption.filter(
                                                                    (value) =>
                                                                        value.id !==
                                                                        row.id
                                                                )
                                                                changeGrid({
                                                                    ...grid,
                                                                    sheetOption: newSheetOption,
                                                                })
                                                                setRows(
                                                                    rows.filter(
                                                                        (
                                                                            value
                                                                        ) =>
                                                                            value.id !==
                                                                            row.id
                                                                    )
                                                                )
                                                            }}
                                                        >
                                                            X
                                                        </Button>
                                                    </td>
                                                </tr>
                                            ))}
                                    </tbody>
                                </Table>
                            </Col>
                        </Row>
                    </Col>
                    <Col xs={{ size: "12" }} md={{ size: "3" }}>
                        <Button
                            size="lg"
                            block
                            className={styles.addLine}
                            onClick={handleAddRow}
                        >
                            + Adicionar Linha
                        </Button>
                    </Col>
                </Row>
            </Col>
        </Row>
    )
}

SheetOption.propTypes = {
    grid: propTypes.object.isRequired,
    changeGrid: propTypes.func.isRequired,
}

export default SheetOption
