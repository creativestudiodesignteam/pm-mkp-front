import React, { useEffect } from "react"
import PropTypes from "prop-types"
import { Col, Row } from "reactstrap"
import InputDefault from "../../InputDefault"
import { SelectDefault } from "../../"
import {
    obrigatory_blur,
    deliveryTypes,
    add_invalid,
    add_valid,
} from "../../../assets/helpers"

const DataOption = ({ defaultState, setPacking, packing }) => {
    useEffect(() => {})
    return (
        <>
            <Row className="mb-5">
                <Col>
                    <h1 className={"h1"}>Dados da embalagem do produto</h1>
                </Col>
            </Row>
            <Row className="mb-4">
                <Col sm="12" md="6">
                    <SelectDefault
                        placeholder={"Tipo de embalagem"}
                        has_label
                        label={"Tipo de embalagem*"}
                        id={"packageoption"}
                        _required
                        initial_value={packing}
                        listener={(e) => {
                            e.preventDefault()
                            setPacking(e.target.value)
                        }}
                        name={"packageoption"}
                        values={deliveryTypes}
                    />
                </Col>
                <Col md="6">
                    <InputDefault
                        name="delivery_time"
                        has_label
                        label="Tempo de entrega do produto *"
                        blur={(e) => obrigatory_blur(e, false, false)}
                        _required
                        placeholder={"Exemplo: Pronta entrega, 2 dias"}
                    />
                </Col>
            </Row>
            {Number(packing) === 1 && (
                <Row className="mb-4">
                    <Col sm="12" md="6">
                        <InputDefault
                            initial_value={defaultState?.details?.length}
                            name="length"
                            has_label
                            label="Comprimento (em centimetro)*"
                            onBlur={(e) => {
                                if (e.target.value < 16) {
                                    add_invalid(
                                        e.target,
                                        "O comprimento deve ser maior que 15cm"
                                    )
                                } else if (e.target.value > 100) {
                                    add_invalid(
                                        e.target,
                                        "O comprimento deve ser menor que 101cm"
                                    )
                                } else {
                                    add_valid(e.target)
                                }

                                e.preventDefault()
                            }}
                            _required
                        />
                    </Col>
                    <Col sm="12" md="6">
                        <InputDefault
                            initial_value={defaultState?.details?.height}
                            name="height"
                            has_label
                            label="Altura (em centimetro)*"
                            onBlur={(e) => {
                                if (e.target.value < 2) {
                                    add_invalid(
                                        e.target,
                                        "A altura deve ser maior que 1cm"
                                    )
                                } else if (e.target.value > 100) {
                                    add_invalid(
                                        e.target,
                                        "A altura deve ser menor que 101cm"
                                    )
                                } else {
                                    add_valid(e.target)
                                }

                                e.preventDefault()
                            }}
                            _required
                        />
                    </Col>
                    <Col sm="12" md="6">
                        <InputDefault
                            initial_value={defaultState?.details?.width}
                            name="width"
                            has_label
                            label="Largura (em centimetro)*"
                            onBlur={(e) => {
                                if (e.target.value < 11) {
                                    add_invalid(
                                        e.target,
                                        "A largura deve ser maior que 10cm"
                                    )
                                } else if (e.target.value > 100) {
                                    add_invalid(
                                        e.target,
                                        "A largura deve ser menor que 101cm"
                                    )
                                } else {
                                    add_valid(e.target)
                                }

                                e.preventDefault()
                            }}
                            _required
                        />
                    </Col>
                    <Col sm="12" md="6">
                        <InputDefault
                            initial_value={defaultState?.details?.weight}
                            name="weight"
                            type={"number"}
                            min={1}
                            has_label
                            label="Peso (em grama)*"
                            blur={(e) => obrigatory_blur(e, false, false)}
                            _required
                        />
                    </Col>
                </Row>
            )}
            {Number(packing) === 2 && (
                <Row className="mb-4">
                    <Col sm="12" md="5">
                        <InputDefault
                            initial_value={defaultState?.details?.length}
                            name="length"
                            has_label
                            label="Comprimento (em centimetro)*"
                            onBlur={(e) => {
                                if (e.target.value < 18) {
                                    add_invalid(
                                        e.target,
                                        "O comprimento deve ser maior que 17cm"
                                    )
                                } else if (e.target.value > 100) {
                                    add_invalid(
                                        e.target,
                                        "O comprimento deve ser menor que 101cm"
                                    )
                                } else {
                                    add_valid(e.target)
                                }

                                e.preventDefault()
                            }}
                            _required
                        />
                    </Col>
                    <Col sm="12" md="4">
                        <InputDefault
                            initial_value={defaultState?.details?.diameter}
                            name="diameter"
                            has_label
                            label="Diâmetro (em centimetro)*"
                            onBlur={(e) => {
                                if (e.target.value < 4) {
                                    add_invalid(
                                        e.target,
                                        "O diâmetro deve ser maior que 4cm"
                                    )
                                } else if (e.target.value > 91) {
                                    add_invalid(
                                        e.target,
                                        "O diâmetro deve ser menor que 92cm"
                                    )
                                } else {
                                    add_valid(e.target)
                                }

                                e.preventDefault()
                            }}
                            _required
                        />
                    </Col>
                    <Col sm="12" md="3">
                        <InputDefault
                            initial_value={defaultState?.details?.weight}
                            name="weight"
                            type={"number"}
                            min={1}
                            has_label
                            label="Peso (em grama)*"
                            blur={(e) => obrigatory_blur(e, false, false)}
                            _required
                        />
                    </Col>
                </Row>
            )}
            {Number(packing) === 3 && (
                <Row className="mb-4">
                    <Col sm="12" md="5">
                        <InputDefault
                            initial_value={defaultState?.details?.length}
                            name="length"
                            has_label
                            label="Comprimento (em centimetro)*"
                            onBlur={(e) => {
                                if (e.target.value < 16) {
                                    add_invalid(
                                        e.target,
                                        "O comprimento deve ser maior que 15cm"
                                    )
                                } else if (e.target.value > 60) {
                                    add_invalid(
                                        e.target,
                                        "O comprimento deve ser menor que 61cm"
                                    )
                                } else {
                                    add_valid(e.target)
                                }

                                e.preventDefault()
                            }}
                            _required
                        />
                    </Col>
                    <Col sm="12" md="4">
                        <InputDefault
                            initial_value={defaultState?.details?.width}
                            name="width"
                            has_label
                            label="Largura (em centimetro)*"
                            onBlur={(e) => {
                                if (e.target.value < 11) {
                                    add_invalid(
                                        e.target,
                                        "A largura deve ser maior que 10cm"
                                    )
                                } else if (e.target.value > 60) {
                                    add_invalid(
                                        e.target,
                                        "A largura deve ser menor que 61cm"
                                    )
                                } else {
                                    add_valid(e.target)
                                }

                                e.preventDefault()
                            }}
                            _required
                        />
                    </Col>
                    <Col sm="12" md="3">
                        <InputDefault
                            initial_value={defaultState?.details?.weight}
                            name="weight"
                            type={"number"}
                            min={1}
                            has_label
                            label="Peso (em grama)*"
                            blur={(e) => obrigatory_blur(e, false, false)}
                            _required
                        />
                    </Col>
                </Row>
            )}
        </>
    )
}

DataOption.propTypes = {
    unit_product: PropTypes.array,
}

export default DataOption
