import React, { useCallback, useEffect, useState } from "react"
import ImageUploading from "react-images-uploading"
import styles from "./index.module.css"
import { Button, Col, Row } from "reactstrap"
import propTypes from "prop-types"
import { autoRemoveNode } from "../../../../../assets/helpers"
import "array.prototype.move"
import useForceUpdate from "use-force-update"

function Gallery_({ imageList, onImageUpload, errors }) {
    const forceUpdate = useForceUpdate()

    return (
        <Row className="mb-4 m-0 allignerCenter">
            <Col xs="12">
                {!!errors.acceptType && (
                    <>
                        <p
                            className="d-block alert bg_red relative text-white-50  pointer p-4 mx-auto my-2 alert-dismissable acceptType"
                            onClick={(event) =>
                                autoRemoveNode(event, ".acceptType")
                            }
                        >
                            A imagem deve ser do formato JPG, JPEG ou PNG{" "}
                            <spam
                                className=" absolute text-white"
                                style={{ right: 8 }}
                            >
                                X
                            </spam>
                        </p>
                    </>
                )}
                {!!errors.maxFileSize && (
                    <>
                        <p
                            className="alert bg_red relative text-white-50  pointer p-4 mx-auto my-2 alert-dismissable maxFileSize"
                            onClick={(event) =>
                                autoRemoveNode(event, ".maxFileSize")
                            }
                        >
                            A imagem ultrapassa limite de 5mb
                            <spam
                                className=" absolute text-white"
                                style={{ right: 8 }}
                            >
                                X
                            </spam>
                        </p>
                    </>
                )}
                {!!errors.maxNumber && (
                    <>
                        <p
                            className="alert bg_red relative text-white-50  pointer p-4 mx-auto my-2 alert-dismissable maxNumber"
                            onClick={(event) =>
                                autoRemoveNode(event, ".maxNumber")
                            }
                        >
                            Upload máximo de 3 foto(s)
                            <spam
                                className=" absolute text-white"
                                style={{ right: 8 }}
                            >
                                X
                            </spam>
                        </p>
                    </>
                )}
            </Col>
            <Col xs="12" md="3" className={`${styles.colUpload} mb-4`}>
                <Button
                    outline
                    onClick={() => {
                        onImageUpload()
                        forceUpdate()
                    }}
                    className={styles.buttonUpload}
                >
                    <img src={"/img/ar-camera.svg"} alt="" />
                    <span>
                        <p className="mt-3">Adicionar fotos</p>
                    </span>
                </Button>
            </Col>
            <Col xs="12" md="9">
                <Row className={`${styles.photoGallery} mb-4`}>
                    <div className="showAboveTablet">
                        {imageList.map((image, idx) => (
                            <Col
                                xs="12"
                                md="4"
                                className={`${styles.photoUpload} mb-4`}
                                key={image.key}
                            >
                                <img alt={""} src={image.dataURL} />
                                <span className="inline">&nbsp;</span>
                                <Row className={styles.aligner}>
                                    <Col xs="12" className="mb-4">
                                        <Button
                                            disabled={idx === 0}
                                            onClick={(e) => {
                                                e.preventDefault()
                                                imageList.move(idx, idx - 1)
                                                forceUpdate()
                                            }}
                                            outline="true"
                                            className={
                                                "w-100 dark2ButtonOutline"
                                            }
                                        >
                                            Mover para esquerda
                                        </Button>
                                    </Col>
                                    <Col xs="12" className="mb-4">
                                        <Button
                                            disabled={
                                                idx === imageList.length - 1
                                            }
                                            onClick={(e) => {
                                                e.preventDefault()
                                                imageList.move(idx, idx + 1)
                                                forceUpdate()
                                            }}
                                            outline="true"
                                            className={
                                                "w-100 dark2ButtonOutline"
                                            }
                                        >
                                            Mover para direita
                                        </Button>
                                    </Col>
                                    <Col xs="12" className="mb-4">
                                        <Button
                                            onClick={(e) => {
                                                e.preventDefault
                                                image.onRemove()
                                                forceUpdate()
                                            }}
                                            outline="true"
                                            className={"w-100 redButtonOutline"}
                                        >
                                            Remover
                                        </Button>
                                    </Col>
                                </Row>
                            </Col>
                        ))}
                    </div>
                    <div className="showTabletAndLess">
                        {imageList.map((image, idx) => (
                            <Col
                                xs="12"
                                md="4"
                                className={`${styles.photoUpload} mb-4`}
                                key={image.key}
                            >
                                <img src={image.dataURL} alt={image.dataURL} />
                                <span className="inline">&nbsp;</span>
                                <Row className={styles.aligner}>
                                    <Col xs="12">
                                        <Button
                                            disabled={idx === 0}
                                            onClick={(e) => {
                                                e.preventDefault()
                                                imageList.move(idx, idx - 1)
                                                forceUpdate()
                                            }}
                                            outline="true"
                                            className={
                                                "w-100 dark2ButtonOutline"
                                            }
                                        >
                                            Mover para esquerda
                                        </Button>

                                        <Button
                                            disabled={
                                                idx === imageList.length - 1
                                            }
                                            onClick={(e) => {
                                                e.preventDefault()
                                                imageList.move(idx, idx + 1)
                                                forceUpdate()
                                            }}
                                            outline="true"
                                            className={
                                                "w-100 dark2ButtonOutline"
                                            }
                                        >
                                            Mover para direita
                                        </Button>
                                    </Col>
                                    <Col xs="12">
                                        <Button
                                            onClick={(e) => {
                                                e.preventDefault
                                                image.onRemove()
                                                forceUpdate()
                                            }}
                                            outline="true"
                                            className={"w-100 redButtonOutline"}
                                        >
                                            Remover
                                        </Button>
                                    </Col>
                                </Row>
                            </Col>
                        ))}
                    </div>
                </Row>
            </Col>
        </Row>
    )
}

const PhotoUpload = ({ id, defaultImages, grid, changeGrid }) => {
    const maxNumber = 3
    const maxMbFileSize = 5 * 1024 * 1024 // 5Mb

    const [_defaultImage, setDefaultImages] = useState(defaultImages)
    const [alertFormat, setAlertFormat] = useState(false)
    const [alertSize, setAlertSize] = useState(false)
    const [alertNumber, setAlertNumber] = useState(false)

    useEffect(() => {
        const [row] = grid.rows.filter((r) => r.id === id)
        const newImages = row.newImages.map((i) => ({ dataURL: i }))
        console.log("grid.rows", id, { row, newImages })
        setDefaultImages(newImages)
    }, [grid.rows, id])

    useEffect(() => {}, [])

    const onChange = useCallback(
        (imageList) => {
            const newImages = imageList.map((it) => it.dataURL)
            changeGrid({
                ...grid,
                rows: grid.rows.map((row) => {
                    if (row.id === id) {
                        return {
                            ...row,
                            newImages,
                        }
                    }
                    return row
                }),
            })
            console.log("newImages was changed", grid)
        },
        [grid]
    )

    return (
        <>
            <ImageUploading
                onChange={onChange}
                maxNumber={maxNumber}
                multiple
                defaultValue={_defaultImage}
                maxFileSize={maxMbFileSize}
                acceptType={["jpg", "png", "jpeg"]}
            >
                {({ ...props }) => <Gallery_ {...props} />}
            </ImageUploading>
            <small className="text-muted">
                as imagens devem ser jpg, jpeg ou png,
                <br />
                com tamanho máximo de 5 Mb
                <br />e número máximo de imagens são 3
            </small>
        </>
    )
}
PhotoUpload.propTypes = {
    defaultImages: propTypes.array.isRequired,
    grid: propTypes.object.isRequired,
    changeGrid: propTypes.func.isRequired,
    id: propTypes.any.isRequired,
}

export default PhotoUpload
