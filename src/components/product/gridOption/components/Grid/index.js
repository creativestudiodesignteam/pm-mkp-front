import React, { useEffect, useState } from "react"
import PropTypes from "prop-types"
import { Button, Col, Row } from "reactstrap"
import { Collapse, SelectCustom } from "../../../.."
import PhotoUpload from "../PhotoUpload/"
import QuantityPrice from "../QuantityPrice"
import Attr from "../Attr"

const Grid = ({
    defaultImages,
    current_grid_id,
    grid,
    token,
    changeGrid,
    index,
}) => {
    const [selectedList, setSelectedList] = useState([])
    const node_name = `shape-${current_grid_id}`
    const window = require("global/window")
    useEffect(() => {
        const newGrid = {
            ...grid,
            rows: grid.rows.map((row) =>
                row.id === current_grid_id
                    ? {
                          ...row,
                          attrs: row.attrs.filter((attr) =>
                              selectedList
                                  .map(({ id }) => id)
                                  .includes(attr.fk_options)
                          ),
                      }
                    : row
            ),
        }
        //console.log("updateOptions - useEffect", { selectedList })
        changeGrid(newGrid)
    }, [selectedList])

    function updateOptions(list) {
        const id_list = selectedList.map(({ id }) => id)
        // console.log('updateOptions listener', {list, selectedList, id_list})
        setSelectedList([
            ...list.filter(({ id }) => id_list.includes(id)),
            ...list.filter(({ id }) => !id_list.includes(id)),
        ])
    }

    function removeGridRow(e) {
        e.preventDefault()
        const rows = grid.rows.filter((row) => row.id !== current_grid_id)
        changeGrid({ ...grid, rows })
    }

    return (
        <>
            <Row className="w-100 m-0">
                <Col
                    xs={`${index === 0 ? "12" : "8"}`}
                    md={`${index === 0 ? "12" : "9"}`}
                >
                    <SelectCustom
                        className="w-100 mt-4"
                        name={`${node_name}-name`}
                        id={`${node_name}-id`}
                        has_label
                        label={"Grade e fotos"}
                        values={grid.optionsNames}
                        onRemove={updateOptions}
                        onSelect={updateOptions}
                        placeholder={"Selecione uma ou mais características"}
                    />
                </Col>
                {index !== 0 && (
                    <Col xs="4" md="3" className="allignerCenter">
                        <Button
                            className="buttonSave buttonGrid"
                            onClick={removeGridRow}
                        >
                            Remover Grid
                        </Button>
                    </Col>
                )}
            </Row>

            <Row className="mb-4 W-100 m-0">
                <Col xs="12" style={{ paddingRight: "3.7rem" }}>
                    {selectedList?.map(function(value, index) {
                        return (
                            <Collapse
                                key={value.id}
                                _key={value.id}
                                title={value.name}
                                collapseOpen={true}
                            >
                                <Attr
                                    current_grid_id={current_grid_id}
                                    name={value.name}
                                    grid={grid}
                                    changeGrid={changeGrid}
                                    token={token}
                                    option_id={value.id}
                                />
                            </Collapse>
                        )
                    })}
                </Col>
            </Row>
            <Row className="w-100 m-0">
                <Col>
                    <Row className="m-0 mb-4 allignerCenter">
                        <Col xs="12">
                            <PhotoUpload
                                defaultImages={defaultImages}
                                id={current_grid_id}
                                grid={grid}
                                changeGrid={changeGrid}
                            />
                        </Col>
                    </Row>
                    <Row className="m-0 mb-4 allignerCenter">
                        <Col xs="12">
                            <QuantityPrice index={current_grid_id} />
                        </Col>
                    </Row>
                </Col>
            </Row>
        </>
    )
}

Grid.propTypes = {
    defaultImages: PropTypes.array.isRequired,
    current_grid_id: PropTypes.any.isRequired,
    token: PropTypes.string.isRequired,
    grid: PropTypes.object.isRequired,
    changeGrid: PropTypes.func.isRequired,
}

export default Grid
