import React, { useMemo } from "react"
import propTypes from "prop-types"
import { Col, Label, Row } from "reactstrap"
import { InputDefault } from "../../../.."
import {
    add_invalid,
    elm,
    forceFormat,
    obrigatory_blur,
} from "../../../../../assets/helpers"
import IntlCurrencyInput from "react-intl-currency-input"

const QuantityPrice = ({ index }) => {
    const currencyConfig = {
        locale: "pt-BR",
        formats: {
            number: {
                BRL: {
                    minimumFractionDigits: 2,
                    maximumFractionDigits: 2,
                },
            },
        },
    }

    const handleChange = (event, value, maskedValue) => {
        event.preventDefault()
        console.log(value) // value without mask (ex: 1234.56)
        console.log(maskedValue) // masked value (ex: R$1234,56)
    }
    return useMemo(
        () => (
            <Row className="mb-4">
                <Col xs={6}>
                    <InputDefault
                        type="number"
                        name={`amount-${index}`}
                        label="Quantidade"
                        has_label
                        blur={(e) => obrigatory_blur(e, false, false)}
                        _required
                    />
                </Col>
                <Col xs={6}>
                    <div className="form-group">
                        <Label for={`price-${index}`} className="d-block">
                            Preço
                        </Label>
                        <IntlCurrencyInput
                            className="pl-4 form-control w-100"
                            name={`price-${index}`}
                            id="price"
                            currency="BRL"
                            config={currencyConfig}
                            max={1000000}
                            onChange={handleChange}
                        />
                    </div>
                </Col>
            </Row>
        ),
        [index]
    )
}
QuantityPrice.propTypes = {
    index: propTypes.any.isRequired,
}
export default QuantityPrice
