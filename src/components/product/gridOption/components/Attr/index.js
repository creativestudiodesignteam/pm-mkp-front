import React, {useCallback, useEffect, useState} from "react"
import {awaitable, fromServer} from "../../../../../assets/helpers"
import {SelectDefault} from "../../../.."

function emptyFunction() {
}

function Attr({current_grid_id, name, option_id, token, changeGrid, grid = false, selected_id_list = [], listener}) {
    const [state, setState] = useState([])
    useEffect(() => {
        awaitable(async () => {
            console.log(token)
            if (!state.length) {
                const options_names = await fromServer(
                    `/options_name/${option_id}`,
                    token
                )
                console.log({options_names, option_id})
                setState(
                    options_names.map((o) =>
                        selected_id_list.includes(o.id)
                            ? {...o, selected: true}
                            : o
                    )
                )
            }
        })
    }, [option_id])
    useEffect(() => {
        console.log('selected', state.find(r => r?.selected)?.id || 0)
    }, [state])
    const node_name = `${name}-${option_id}`
    if (grid === false) {
        return (
            !!state.length &&
            <div className="d-flex flex-row justify-content-start mb-4">
                <SelectDefault
                    placeholder={'Selecione o valor do atributo'}
                    className={"w-100"}
                    id={node_name}
                    _required
                    name={node_name}
                    initial_value={state.find(r => r?.selected)?.id || 0}
                    values={[...state.map(s => s?.selected ? {id: s.id, name: s.name} : s), {id: 0, name: ""}]}
                    listener={listener}
                />
            </div>
        )
    } else {
        const updateAttrOnGridRow = useCallback(
            ({target}) => {
                const newGridState = {
                    ...grid,
                    rows: grid.rows.map((row) => {
                        if (row.id === current_grid_id) {
                            const fk_options_ids = row.attrs.map(a => a.fk_options)
                            console.log('attr value has changed', {row, fk_options_ids, option_id}, [...row.attrs, {
                                fk_options: Number(option_id),
                                fk_options_names: Number(target.value),
                            }])
                            const attrs = row.attrs.length > 0 ?
                                row.attrs.map(a => a.fk_options).includes(Number(option_id)) ?
                                    row.attrs.map(atrs => atrs.fk_options === Number(option_id) ? {
                                            fk_options: Number(option_id),
                                            fk_options_names: Number(target.value),
                                        } : atrs
                                    ) : [...row.attrs, {
                                        fk_options: Number(option_id),
                                        fk_options_names: Number(target.value),
                                    }] :
                                [{
                                    fk_options: Number(option_id),
                                    fk_options_names: Number(target.value),
                                }]
                            console.log({attrs, option_id, row})
                            return {
                                ...row,
                                attrs,
                            }
                        }
                        return row
                    }),
                }
                console.log({newGridState, changeGrid})
                changeGrid(newGridState)
            },
            [grid, current_grid_id, option_id, state, changeGrid]
        )
        return (
            !!state.length &&
            <div className="d-flex flex-row justify-content-start mb-4">
                <SelectDefault
                    placeholder={'Selecione o valor do atributo'}
                    className={"w-100"}
                    id={node_name}
                    _required
                    name={node_name}
                    initial_value={state.find(r => r?.selected)?.id || 0}
                    values={[...state.map(s => ({id: s.id, name: s.name})), {id: 0, name: ''}]}
                    listener={updateAttrOnGridRow}
                />
            </div>
        )
    }
}

export default Attr
