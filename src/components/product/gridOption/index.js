import React, { useCallback, useEffect, useState } from "react"
import propTypes from "prop-types"
import { Button, Col, Row, Alert } from "reactstrap"
import Grid from "./components/Grid/"
import ImageUploading from "react-images-uploading"
import styles from "./components/PhotoUpload/index.module.css"
import Collapse from "../../pure/Collapse"
import {
    awaitable,
    convertBlobToBase64,
    fromServer,
    getMaxValueOnArrayOfObjectBy,
} from "../../../assets/helpers"
import PhotoUploader from "../../photoUploader/index"

const GridOption = ({
    token,
    grid,
    changeGrid,
    thumb,
    setThumb,
    defaultState,
}) => {
    const [defaultValue, setDefaultValue] = useState([{ dataURL: "" }])
    const [alertFormat, setAlertFormat] = useState(true)
    const [alertSize, setAlertSize] = useState(true)
    const [alertNumber, setAlertNumber] = useState(true)

    useEffect(() => {
        awaitable(async () => {
            if (!defaultValue[0].dataURL.length && defaultState.thumb) {
                try {
                    const blobImage = await fromServer(defaultState.thumb)
                    const base64Image = await convertBlobToBase64(blobImage)
                    setDefaultValue([{ dataURL: base64Image }])
                } catch (e) {
                    console.log(e)
                }
            } else {
                //console.log({ defaultValue, defaultState })
            }
        })
    }, [])
    /*useEffect(
        function() {
            console.log("GridOption", { token, grid, changeGrid })
        },
        [token, grid, changeGrid]
    )*/
    const add = useCallback(
        (e) => {
            e.preventDefault()
            changeGrid({
                ...grid,
                rows: [
                    ...grid.rows,
                    {
                        id: !grid?.rows?.length
                            ? 1
                            : Number(
                                  //Math.max(...grid.rows.map((r) => r.id)) + 1
                                  grid.rows.reduce(
                                      getMaxValueOnArrayOfObjectBy("id"),
                                      0
                                  ) + 1
                              ),
                        attrs: [],
                        newImages: [],
                    },
                ],
            })
        },
        [grid]
    )

    return (
        <>
            <Row className="mb-4">
                <Col>
                    <h1 className={"h1"}>
                        Adicione fotos e grades com atributos do seu produto:
                    </h1>
                </Col>
            </Row>
            <Row>
                <Col xs="12" sm={"12"} className={"mb-4"}>
                    <h2 className={"h2"}>
                        Selecione a imagem principal do produto (será exibida na
                        página de pesquisas)
                    </h2>
                </Col>
                <Col xs="12" className="allignerCenter flex-column w-100">
                    <PhotoUploader
                        listener={(e) => {
                            //console.log("ImageUploading", { e })
                            setThumb(e)
                        }}
                        defaultValue={
                            defaultValue[0].dataURL.length ? defaultValue : []
                        }
                        maxNumber={1}
                        maxFileSize={5 * 1024 * 1024}
                        acceptType={["jpg", "png", "jpeg"]}
                    />
                </Col>
            </Row>
            {grid?.rows?.map(({ id, newImages }, index) => (
                <Row key={id} className={"mb-4 w-100 gridSize m-0"}>
                    <Collapse
                        collapseOpen={true}
                        _key={id}
                        title={`Quais são as características e atributos deste produto?`}
                        className="collapseGrid"
                    >
                        <Grid
                            className="w-100"
                            index={index}
                            defaultImages={newImages.map((i) => ({
                                dataURL: i,
                            }))}
                            current_grid_id={id}
                            grid={grid}
                            token={token}
                            changeGrid={changeGrid}
                        />
                    </Collapse>
                </Row>
            ))}
            <Row>
                <div
                    className={`w-100 mx-auto m-0 mb-4 border bg-white text_light_gray text-center p-5 fix`}
                    style={{ cursor: "pointer" }}
                    onClick={add}
                >
                    <div className={"font-weight-bold text_dark_2 ml-1 fix"}>
                        <u>
                            Clique para selecionar mais características e
                            atributos
                        </u>
                    </div>
                </div>
            </Row>
        </>
    )
}
GridOption.propTypes = {
    token: propTypes.string.isRequired,
    grid: propTypes.object.isRequired,
    changeGrid: propTypes.func.isRequired,
}

export default GridOption
