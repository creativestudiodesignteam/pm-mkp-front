import React, { useMemo, useContext } from "react"
import { Button, Col, Row } from "reactstrap"
import Img from "../../Img"
import styles from "./index.module.css"
import { useRouter } from "next/router"
import Main from "../../Main"

const Feedback = ({ setIsFeedBack, setLoaderState }) => {
    const router = useRouter()
    return (
        <>
            <Main>
                <Row className={styles.aligner}>
                    <Col xs="12" className={styles.cardPrincipal}>
                        <Row className="mb-4 w-100 mt-5">
                            <Col md="12" className={styles.aligner}>
                                <Img
                                    className={styles.imgMary}
                                    image_name_with_extension={"Mary-2.png"}
                                    alt={"Seja Bem vindo"}
                                />
                            </Col>
                        </Row>
                        <Row>
                            <Col md="12" className={styles.aligner}>
                                <h1
                                    className={`${
                                        styles.textOne
                                    } showAboveTablet`}
                                >
                                    Seu produto foi cadastrado!
                                </h1>
                                <h2
                                    className={`${
                                        styles.textOne
                                    } showTabletAndLess`}
                                >
                                    Seu produto foi cadastrado!
                                </h2>
                            </Col>
                        </Row>
                        <Row className="mb-4">
                            <Col
                                md={{ size: 10, offset: 1 }}
                                className={styles.aligner}
                            >
                                <h4
                                    className={`${
                                        styles.textTwo
                                    } showAboveTablet`}
                                >
                                    Acessando seu perfil você pode inserir seus
                                    produtos e configurar suas
                                </h4>
                                <h6
                                    className={`${
                                        styles.textTwo
                                    } showTabletAndLess`}
                                >
                                    Acessando seu perfil você pode inserir seus
                                    produtos e configurar suas
                                </h6>
                            </Col>
                            <Col
                                md={{ size: 10, offset: 1 }}
                                className={styles.aligner}
                            >
                                <h4
                                    className={`${
                                        styles.textTwo
                                    } showAboveTablet`}
                                >
                                    formas de parcelamento e valores dos
                                    produtos
                                </h4>
                                <h6
                                    className={`${
                                        styles.textTwo
                                    } showTabletAndLess`}
                                >
                                    formas de parcelamento e valores dos
                                    produtos
                                </h6>
                            </Col>
                        </Row>
                        <Row className="mb-5">
                            <Col md="12" className={styles.aligner}>
                                <Button
                                    className={styles.buttonLeft}
                                    size="lg"
                                    onClick={(e) => {
                                        e.preventDefault()
                                        setLoaderState(true)
                                        router.push(
                                            "/account/[feature]",
                                            "/account/product"
                                        )
                                    }}
                                >
                                    CERTO! ENTENDI
                                </Button>
                            </Col>
                        </Row>
                    </Col>
                </Row>
            </Main>
        </>
    )
}
export default Feedback
