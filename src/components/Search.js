import React, {useRef} from 'react';
import {PureInput} from './'
import {focusOnMouseOver} from '../assets/helpers'

const Component = ({
                       OnClick = e => false,
                       OnBlur = e => false,
                       OnChange = e => false,
                       OnKeyUp = e => false,
                       classNames = [],
                       placeholder = "Pesquisar ... ",
                       id,
                       name = false
                   }) => {
    let inputRef = useRef(null)
    return (
        <form
            id={id}
            className={`form-inline ${classNames[0] || ''}`}
        >
            <PureInput
                name={name}
                id={id}
                className={`form-inline ${classNames[1] || ''}`}
                ref={inputRef}
                onMouseOver={focusOnMouseOver({nodeRef: inputRef.current})}
                required={true}
                type={'search'}
                placeholder={placeholder}
                onBlur={OnBlur}
                onChange={OnChange}
                onKeyUp={OnKeyUp}
            />
            <datalist
                className={`form-inline ${classNames[3] || ''}`}
                id="anrede"
            >

                <option
                    className={`form-inline ${classNames[4] || ''}`}
                    value="Eletronicos"
                />
                <option
                    className={`form-inline ${classNames[4] || ''}`}
                    value="Vestuario"
                />
            </datalist>
            <button
                onClick={OnClick}
                className={`btn ${classNames[2] || ''}`}
                type="submit"
            >
                Buscar
            </button>
        </form>
    );
}

export default Component;
