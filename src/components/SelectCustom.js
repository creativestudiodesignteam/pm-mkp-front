import React, {useEffect} from "react"
import {Multiselect} from "multiselect-react-dropdown"
import {FormGroup, Label} from "reactstrap"
import propTypes from "prop-types"

const SelectCustom = ({
                          emptyRecordMsg = "",
                          onSelect,
                          onRemove,
                          placeholder = "",
                          _style_label_wrap = {},
                          id,
                          has_label = false,
                          label,
                          name,
                          _style_label = {},
                          values = [],
                          selectedValues = [],
                          _required = false,
                          ...rest
                      }) => {
    const __style_label = {
        ..._style_label,
        ...(has_label ? {display: "block"} : {display: "none"}),
    }
    const labelId = `${id}-${name}-SelectCustom`
    /*useEffect(function () {
        console.log(`SelectCustom ${id}`, values)
    }, [])*/

    return (
        <FormGroup {...rest}>
            <Label
                _style_label_wrap={_style_label_wrap}
                id={labelId}
                for={name}
                style={__style_label}
                className="mr-2"
            >
                {label}
            </Label>
            <Multiselect
                selectedValues={selectedValues}
                style={{width: "100%"}}
                emptyRecordMsg={emptyRecordMsg}
                onSelect={onSelect}
                onRemove={onRemove}
                closeIcon="close"
                placeholder={placeholder}
                displayValue="name"
                options={values}
            />
        </FormGroup>
    )
}

SelectCustom.propTypes = {
    emptyRecordMsg: propTypes.string,
    onSelect: propTypes.func.isRequired,
    onRemove: propTypes.func.isRequired,
    placeholder: propTypes.string,
    _style_label_wrap: propTypes.object,
    id: propTypes.any.isRequired,
    has_label: propTypes.bool,
    label: propTypes.string,
    name: propTypes.string.isRequired,
    _style_label: propTypes.object,
    values: propTypes.array.isRequired,
}
export default SelectCustom
