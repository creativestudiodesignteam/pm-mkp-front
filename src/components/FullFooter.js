import React, { useEffect, useState } from "react"
import Img from "./Img"
import { LinkTo } from "./"
import { useRouter } from "next/router"
import {
    FaEnvelope,
    FaPhone,
    FaWhatsapp,
    FaFacebookSquare,
    FaInstagram,
} from "react-icons/fa"
import ClipLoader from "react-spinners/ClipLoader"

const FullFooter = ({ context, dispatch }) => {
    const { token } = context
    const router = useRouter()
    const [loaderState, setLoaderState] = useState(false)

    const checkRoute = (next) => {
        if (router.pathname !== next) {
            setLoaderState(true)
        }
    }

    useEffect(() => {
        setLoaderState(false)
    }, [router.pathname])

    return (
        <>
            {loaderState && (
                <div className="spinnerLoader">
                    <ClipLoader
                        size={150}
                        color={"var(--red-1)"}
                        loading={loaderState}
                    />
                </div>
            )}
            <footer className="relative full-width footerApp">
                <div className="w-100 footer-red">
                    <div className="clearfix container-web relative">
                        <div className=" container clear-padding">
                            <div className="row">
                                <div className="clearfix col-md-3 col-sm-6 col-xs-12 text-footer-white">
                                    <p>MINHA CONTA</p>
                                    <ul className="list-footer">
                                        {!!token && (
                                            <>
                                                <li>
                                                    <LinkTo
                                                        _href="/account"
                                                        query={{ token }}
                                                        onClick={(e) =>
                                                            checkRoute(
                                                                "/account"
                                                            )
                                                        }
                                                    >
                                                        Minha conta
                                                    </LinkTo>
                                                </li>
                                                <li>
                                                    <LinkTo
                                                        _href="/cart"
                                                        query={{ token }}
                                                        onClick={(e) =>
                                                            checkRoute("/cart")
                                                        }
                                                    >
                                                        Meu carrinho
                                                    </LinkTo>
                                                </li>
                                            </>
                                        )}
                                        {!token && (
                                            <>
                                                <li>
                                                    <LinkTo
                                                        _href="/account"
                                                        onClick={(e) =>
                                                            checkRoute(
                                                                "/account"
                                                            )
                                                        }
                                                    >
                                                        Minha conta
                                                    </LinkTo>
                                                </li>
                                                <li>
                                                    <LinkTo
                                                        _href="/cart"
                                                        onClick={(e) =>
                                                            checkRoute("/ cart")
                                                        }
                                                    >
                                                        Meu carrinho
                                                    </LinkTo>
                                                </li>
                                            </>
                                        )}
                                    </ul>
                                </div>
                                <div className="clearfix col-md-3 col-sm-6 col-xs-12 text-footer-white">
                                    <p>INFORMAÇÃO</p>
                                    <ul className="list-footer">
                                        <li>
                                            <LinkTo
                                                _href={"/termsofuse"}
                                                onClick={(e) =>
                                                    checkRoute("/termsofuse")
                                                }
                                            >
                                                Termos de uso e condições gerais
                                            </LinkTo>
                                        </li>
                                        <li>
                                            <LinkTo
                                                _href={"/privacypolicy"}
                                                onClick={(e) =>
                                                    checkRoute("/privacypolicy")
                                                }
                                            >
                                                Política de privacidade
                                            </LinkTo>
                                        </li>
                                        <li>
                                            <LinkTo
                                                _href={"/exchangepolicy"}
                                                onClick={(e) =>
                                                    checkRoute(
                                                        "/exchangepolicy"
                                                    )
                                                }
                                            >
                                                Termos de trocas e devoluções
                                            </LinkTo>
                                        </li>
                                    </ul>
                                </div>
                                <div className="clearfix col-md-3 col-sm-6 col-xs-12 text-footer-white">
                                    <p>LINKS ÚTEIS</p>
                                    <ul className="icon-footer">
                                        <li>
                                            <LinkTo
                                                _href={"/about"}
                                                onClick={(e) =>
                                                    checkRoute("/about")
                                                }
                                            >
                                                Sobre o Portal
                                            </LinkTo>
                                        </li>
                                        <li style={{ cursor: "pointer" }}>
                                            <div>
                                                <a
                                                    href={`//www.facebook.com/PortaldaMaria`}
                                                    target="_blank"
                                                >
                                                    <FaFacebookSquare
                                                        color="white"
                                                        className="mr-2 icons-footer"
                                                    />{" "}
                                                    <span className="text-icon-footer">
                                                        Facebook
                                                    </span>
                                                </a>
                                            </div>
                                        </li>
                                        <li style={{ cursor: "pointer" }}>
                                            <div>
                                                <a
                                                    href={`//www.instagram.com/portaldamaria_/ `}
                                                    target="_blank"
                                                >
                                                    <FaInstagram
                                                        color="white"
                                                        className="mr-2 icons-footer"
                                                    />{" "}
                                                    <span className="text-icon-footer">
                                                        Instagram
                                                    </span>
                                                </a>
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                                <div className="clearfix col-md-3 col-sm-6 col-xs-12 text-footer-white">
                                    <p>CONTATE-NOS</p>
                                    <ul className="icon-footer">
                                        <li>
                                            <div>
                                                <FaEnvelope
                                                    color="white"
                                                    className="mr-2 "
                                                />{" "}
                                                <span className="text-icon-footer">
                                                    atendimento@portaldamaria.com
                                                </span>
                                            </div>
                                        </li>
                                        <li>
                                            <div>
                                                <FaPhone
                                                    color="white"
                                                    className="mr-2"
                                                />{" "}
                                                <span className="text-icon-footer">
                                                    4004-8888
                                                </span>
                                            </div>
                                        </li>
                                        {/* <li style={{ cursor: "pointer" }}>
                                            <div>
                                                <a
                                                    href={`//api.whatsapp.com/send?phone=+551140048888&text=Fale%20com%20a%20Maria`}
                                                    target="_blank"
                                                >
                                                    <FaWhatsapp
                                                        color="white"
                                                        className="mr-2 icons-footer"
                                                    />{" "}
                                                </a>
                                                <span className="text-icon-footer">
                                                    Fale com a Maria
                                                </span>
                                            </div>
                                        </li> */}
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div className=" bottom-footer full-width">
                    <div className="clearfix container-web">
                        <div className=" container">
                            <div className="row">
                                <div className="clearfix col-7 clear-padding copyright">
                                    <p>
                                        Copyright © 2020 by Creative Devs &
                                        Design. Todos os direitos reservados.
                                    </p>
                                </div>
                                <div className="clearfix footer-icon-bottom col-5 float-right clear-padding logo-safe2pay">
                                    <div className="icon_logo_footer float-right">
                                        <Img
                                            image_name_with_extension="safe2pay.png"
                                            style={{ height: "6rem" }}
                                            alt=""
                                        />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </footer>
        </>
    )
}
export default FullFooter
