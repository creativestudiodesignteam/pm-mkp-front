import React from 'react';


const Index = ({classNames = ['', ''], over = false, overClassNames = ['', ''], children, ...props}) => {
    const [cl_first, cl_second] = over ? overClassNames : classNames
    return (
        <div className={over ? cl_first : `clearfix container-web ${cl_first}`} {...props}>
            <div className={over ? cl_second : `container ${cl_second}`}>
                {children}
            </div>
        </div>
    )
};

export default Index;