import React, { useState } from "react"
import { Img, TitleCategory, LinkTo } from "../"
import { Col, Row } from "reactstrap"
import propTypes from "prop-types"
import Slider from "react-slick"
import ProductCard from "./ProductCard"
import ClipLoader from "react-spinners/ClipLoader"

const Fashion = ({ products }) => {
    const [first, second] = [products.slice(0, 3), products.slice(3)]
    const settings = {
        infinite: true,
        autoplay: true,
        speed: 500,
        slidesToShow: 1,
        slidesToScroll: 1,
        arrows: false,
    }
    const [loaderState, setLoaderState] = useState(false)
    const goTo = () => {
        setLoaderState(true)
    }
    return (
        <>
            {loaderState && (
                <div className="spinnerLoader">
                    <ClipLoader
                        size={150}
                        color={"var(--red-1)"}
                        loading={loaderState}
                    />
                </div>
            )}
            <Col md="6" className="paddingRightDesktop">
                <Row>
                    <Col md="12" className="paddingZero">
                        <div className="clearfix title-box full-width border">
                            <TitleCategory
                                node_text={"Moda Feminina"}
                                alt={"Icon Fashion"}
                                img={"icon_fashion.png"}
                                classNames={[
                                    "title-gold-bg relative",
                                    "",
                                    "text-white",
                                ]}
                            />
                            <div className="clearfix menu-title-box">
                                <p
                                    className="view-all-product-category title-hover-red"
                                    onClick={(e) => goTo()}
                                >
                                    <LinkTo
                                        className="animate-default"
                                        _href="/category"
                                        query={{
                                            id_cat: 26,
                                        }}
                                    >
                                        Ver Mais
                                    </LinkTo>
                                </p>
                            </div>
                        </div>
                        <LinkTo
                            _href="/category"
                            query={{
                                id_cat: 26,
                            }}
                        >
                            <div className=" banner-percent-product zoom-image-hover overfollow-hidden relative">
                                <Img
                                    image_name_with_extension="bannershome/Moda.png"
                                    className="max-width"
                                    alt="Image . . ."
                                    onClick={(e) => goTo()}
                                />
                            </div>
                        </LinkTo>
                    </Col>
                </Row>

                <Row className="marginMobile showPhoneAndLess">
                    <div className="box-electric-content relative animate-default active-box-category hidden-content-box border-collapsed-box mobileDisplay">
                        <div className="slideProducts">
                            <Slider {...settings}>
                                {products?.map(
                                    ({
                                        id,
                                        thumb,
                                        name,
                                        infos: { minPrice, maxPrice },
                                    }) => (
                                        <Col
                                            xs="12"
                                            key={id}
                                            className="paddingZero"
                                        >
                                            <div className="relative product-no-ranking h-100">
                                                <ProductCard
                                                    id={id}
                                                    thumb={thumb}
                                                    name={name}
                                                    infos={{
                                                        minPrice,
                                                        maxPrice,
                                                    }}
                                                    little={true}
                                                />
                                            </div>
                                        </Col>
                                    )
                                )}
                            </Slider>
                        </div>
                    </div>
                </Row>

                <Row className="showAbovePhone">
                    {first?.map(
                        ({
                            id,
                            thumb,
                            name,
                            infos: { minPrice, maxPrice },
                        }) => (
                            <Col md={4} key={id} className="paddingZero">
                                <div className="relative product-no-ranking h-100">
                                    <ProductCard
                                        id={id}
                                        thumb={thumb}
                                        name={name}
                                        infos={{ minPrice, maxPrice }}
                                        little={true}
                                    />
                                </div>
                            </Col>
                        )
                    )}
                </Row>

                <Row
                    className="showAbovePhone"
                    style={{ marginBottom: "10rem" }}
                >
                    {second?.map(
                        ({
                            id,
                            thumb,
                            name,
                            infos: { minPrice, maxPrice },
                        }) => (
                            <Col md={4} key={id} className="paddingZero">
                                <div className="relative product-no-ranking h-100">
                                    <ProductCard
                                        id={id}
                                        thumb={thumb}
                                        name={name}
                                        infos={{ minPrice, maxPrice }}
                                        little={true}
                                    />
                                </div>
                            </Col>
                        )
                    )}
                </Row>
            </Col>
        </>
    )
}
Fashion.propTypes = {
    products: propTypes.array.isRequired,
}
export default Fashion
