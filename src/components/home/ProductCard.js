import React, { useState } from "react"
import { Card, CardBody, CardTitle, CardSubtitle } from "reactstrap"
import { Img, LinkTo } from "../../components"
import { brlFormat, toFixed } from "../../assets/helpers"
import ClipLoader from "react-spinners/ClipLoader"

const ProductCard = ({
    id,
    thumb,
    name,
    infos: { minPrice, maxPrice },
    little,
    href = "/details",
}) => {
    const [loaderState, setLoaderState] = useState(false)
    const goTo = () => {
        setLoaderState(true)
    }

    return (
        <>
            {loaderState && (
                <div className="spinnerLoader">
                    <ClipLoader
                        size={150}
                        color={"var(--red-1)"}
                        loading={loaderState}
                    />
                </div>
            )}
            <Card className="w-100 h-100">
                <LinkTo
                    _href={href}
                    query={{
                        id: id,
                    }}
                >
                    <div className="effect-hover-zoom overfollow-hidden">
                        <Img
                            onClick={(e) => goTo()}
                            className={
                                little ? "imgSizeHomeLittle" : "imgSizeHome"
                            }
                            image_name_with_extension={
                                thumb && thumb.url
                                    ? thumb.url
                                    : "product_home_8-min.png"
                            }
                            outSide={`${thumb && thumb.url ? "thumb.url" : ""}`}
                            alt={name}
                        />
                    </div>
                </LinkTo>
                <CardBody className="text-center">
                    <CardTitle className="text-center" onClick={(e) => goTo()}>
                        <p className="title-product titleHome">
                            <LinkTo
                                _href={href}
                                query={{
                                    id: id,
                                }}
                            >
                                {name.length > 20
                                    ? `${name.slice(0, 20) + "..."}`
                                    : name}
                            </LinkTo>
                        </p>
                    </CardTitle>
                    <CardSubtitle className="price-product">
                        <span
                            style={{
                                color: "var(--gray-text)",
                                fontSize: "11px",
                            }}
                        >
                            {"a partir de"}
                        </span>
                        <br />
                        {brlFormat(minPrice / 100)} <br />
                    </CardSubtitle>
                </CardBody>
            </Card>
        </>
    )
}

export default ProductCard
