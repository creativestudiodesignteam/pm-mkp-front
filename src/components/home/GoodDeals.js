import React, { useState } from "react"
import { Img, Link, SemiContainer, LinkTo } from ".."
import { Row, Col } from "reactstrap"
import ClipLoader from "react-spinners/ClipLoader"

const GoodDeals = () => {
    const [loaderState, setLoaderState] = useState(false)
    const goTo = () => {
        setLoaderState(true)
    }
    return (
        <>
            {loaderState && (
                <div className="spinnerLoader">
                    <ClipLoader
                        size={150}
                        color={"var(--red-1)"}
                        loading={loaderState}
                    />
                </div>
            )}
            <Row>
                <Col md="12" style={{ padding: "0!important" }}>
                    <SemiContainer
                        over={true}
                        overClassNames={[
                            "clearfix title-box full-width",
                            "clearfix name-title-box good-deals-v2 relative",
                        ]}
                    >
                        <Img
                            image_name_with_extension="icon_percent_color.png"
                            alt="Good Deal Today"
                            className="absolute clear-left"
                        />
                        <p className="text-default-color">
                            Conheça nossos produtos
                        </p>
                    </SemiContainer>
                </Col>
            </Row>
            <Row style={{ marginBottom: "10rem" }}>
                <Col md="6">
                    <Row>
                        <Col md="12" className="paddingZero">
                            <div className="clearfix border-collapsed-element-good relative width100percent">
                                <LinkTo
                                    _href="/category"
                                    query={{
                                        id_cat: 19,
                                    }}
                                >
                                    <div
                                        className="effect-hover-zoom zoom-image-hover overfollow-hidden"
                                        style={{ cursor: "pointer" }}
                                    >
                                        <Img
                                            className="width100percent"
                                            image_name_with_extension="bannershome/Banner-game.png"
                                            alt="Banner Home V3"
                                            onClick={(e) => goTo()}
                                        />
                                    </div>
                                </LinkTo>
                            </div>
                        </Col>
                        <Col md="6" className="paddingZero paddingTablet">
                            <div className="clearfix border-collapsed-element-good relative width100percent">
                                <LinkTo
                                    _href="/category"
                                    query={{
                                        id_cat: 30,
                                    }}
                                >
                                    <div
                                        className="effect-hover-zoom zoom-image-hover overfollow-hidden"
                                        style={{ cursor: "pointer" }}
                                    >
                                        <Img
                                            className="width100percent"
                                            image_name_with_extension="bannershome/Banner-papelaria.png"
                                            alt="Banner Home V3"
                                            onClick={(e) => goTo()}
                                        />
                                    </div>
                                </LinkTo>
                            </div>
                        </Col>
                        <Col
                            md="6"
                            className="paddingZero noneMobile paddingTablet"
                        >
                            <div className="clearfix border-collapsed-element-good relative width100percent">
                                <LinkTo
                                    _href="/category"
                                    query={{
                                        id_cat: 33,
                                    }}
                                >
                                    <div
                                        className="effect-hover-zoom zoom-image-hover overfollow-hidden"
                                        style={{ cursor: "pointer" }}
                                    >
                                        <Img
                                            image_name_with_extension="bannershome/Banner-jóias.png"
                                            alt="Banner Home V3"
                                            onClick={(e) => goTo()}
                                        />
                                    </div>
                                </LinkTo>
                            </div>
                        </Col>
                    </Row>
                </Col>
                <Col md="6">
                    <Row>
                        <Col md="6" className="paddingZero noneMobile">
                            <div className="clearfix border-collapsed-element-vertical relative width100percent">
                                <LinkTo
                                    _href="/category"
                                    query={{
                                        id_cat: 31,
                                    }}
                                >
                                    <div
                                        className="effect-hover-zoom zoom-image-hover overfollow-hidden"
                                        style={{ cursor: "pointer" }}
                                    >
                                        <Img
                                            className="width100percent heightGoodDeals"
                                            image_name_with_extension="bannershome/Banner-pet.png"
                                            alt="Banner Home V3"
                                            onClick={(e) => goTo()}
                                        />
                                    </div>
                                </LinkTo>
                            </div>
                        </Col>
                        <Col md="6" className="paddingZero">
                            <div className="clearfix border-collapsed-element-good relative width100percent">
                                <LinkTo
                                    _href="/category"
                                    query={{
                                        id_cat: 29,
                                    }}
                                >
                                    <div
                                        className="effect-hover-zoom zoom-image-hover overfollow-hidden"
                                        style={{ cursor: "pointer" }}
                                    >
                                        <Img
                                            className="width100percent"
                                            image_name_with_extension="bannershome/Banner-casa.png"
                                            alt="Banner Home V3"
                                            onClick={(e) => goTo()}
                                        />
                                    </div>
                                </LinkTo>
                            </div>
                            <div className="clearfix border-collapsed-element-good relative width100percent">
                                <LinkTo
                                    _href="/category"
                                    query={{
                                        id_cat: 16,
                                    }}
                                >
                                    <div
                                        className="effect-hover-zoom zoom-image-hover overfollow-hidden"
                                        style={{ cursor: "pointer" }}
                                    >
                                        <Img
                                            className="width100percent"
                                            image_name_with_extension="bannershome/Banner-esporte.png"
                                            alt="Banner Home V3"
                                            onClick={(e) => goTo()}
                                        />
                                    </div>
                                </LinkTo>
                            </div>
                        </Col>
                    </Row>
                </Col>
            </Row>
        </>
    )
}

export default GoodDeals
