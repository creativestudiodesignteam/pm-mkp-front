import React, { useState } from "react"
import { Row, Col } from "reactstrap"
import { Img, Link, LinkTo } from ".."
import ImageGallery from "react-image-gallery"
import ClipLoader from "react-spinners/ClipLoader"

const Banner = () => {
    const images = [
        {
            original: "/img/bannershome/Banner-principal.png",
        },
        {
            original: "/img/bannershome/Banner principal 2.png",
        },
    ]
    function idByCategoryName(category) {
        switch (category) {
            case "ELETRÔNICOS": {
                return
            }
        }
    }
    const [loaderState, setLoaderState] = useState(false)
    const goTo = () => {
        setLoaderState(true)
    }

    return (
        <>
            {loaderState && (
                <div className="spinnerLoader">
                    <ClipLoader
                        size={150}
                        color={"var(--red-1)"}
                        loading={loaderState}
                    />
                </div>
            )}
            <Row style={{ marginBottom: "3rem" }}>
                <Col xs="9" className="p-0">
                    <div className="border-collapsed-element-good relative allignerCenter">
                        <div
                            className="bannerHomeSlide effect-hover-zoom zoom-image-hover center-vertical-image overfollow-hidden"
                            style={{
                                cursor: "pointer",
                                zIndex: 2,
                            }}
                        >
                            <ImageGallery
                                items={images}
                                showPlayButton={false}
                                showFullscreenButton={false}
                                showThumbnails={false}
                                showBullets={true}
                                showNav={false}
                                className=""
                            />
                        </div>
                        <Link _href={"/"} />
                    </div>
                </Col>
                <Col xs="3" className="p-0">
                    <div className="border-collapsed-element-good relative">
                        <LinkTo
                            _href="/category"
                            query={{
                                id_cat: 19,
                            }}
                        >
                            <div
                                className="effect-hover-zoom zoom-image-hover overfollow-hidden"
                                style={{ cursor: "pointer" }}
                            >
                                <Img
                                    image_name_with_extension="bannershome/Banner-lateral---cima.png"
                                    alt="Banner Home V3"
                                    onClick={(e) => goTo()}
                                />
                            </div>
                        </LinkTo>
                    </div>
                    <div className="border-collapsed-element-good relative">
                        <LinkTo
                            _href="/category"
                            query={{
                                id_cat: 6,
                                id_sub: 262,
                            }}
                        >
                            <div
                                className="effect-hover-zoom zoom-image-hover overfollow-hidden"
                                style={{ cursor: "pointer" }}
                            >
                                <Img
                                    image_name_with_extension="bannershome/Banner-lateral---baixo.png"
                                    alt="Banner Home V3"
                                    onClick={(e) => goTo()}
                                />
                            </div>
                        </LinkTo>
                    </div>
                </Col>
            </Row>
        </>
    )
}

export default Banner
