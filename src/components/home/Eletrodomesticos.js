import React, { useState } from "react"
import { Img, TitleCategory, LinkTo } from ".."
import { Col, Row } from "reactstrap"
import propTypes from "prop-types"
import Slider from "react-slick"
import ProductCard from "./ProductCard"
import ClipLoader from "react-spinners/ClipLoader"

const Electric = ({ products }) => {
    const settings = {
        infinite: true,
        autoplay: true,
        speed: 500,
        slidesToShow: 1,
        slidesToScroll: 1,
        arrows: false,
    }
    const [loaderState, setLoaderState] = useState(false)
    const goTo = () => {
        setLoaderState(true)
    }
    return (
        <>
            {loaderState && (
                <div className="spinnerLoader">
                    <ClipLoader
                        size={150}
                        color={"var(--red-1)"}
                        loading={loaderState}
                    />
                </div>
            )}
            <Row>
                <Col md="12" className="paddingZero">
                    <div className="clearfix title-box title-box-v3 full-width">
                        <TitleCategory
                            node_text={"Eletrodomésticos"}
                            alt={"Icon Food"}
                            img={"icon_electric_color.png"}
                            classNames={["title-turquoise-bd relative", "", ""]}
                        />
                    </div>
                </Col>
                <Col md="12" className="paddingZero">
                    <LinkTo
                        _href="/category"
                        query={{
                            id_cat: 12,
                        }}
                    >
                        <div
                            className="effect-hover-zoom zoom-image-hover overfollow-hidden"
                            style={{ cursor: "pointer" }}
                        >
                            <Img
                                image_name_with_extension="bannershome/Eletrodomésticos.png"
                                alt="Banner Home V3"
                                onClick={(e) => goTo()}
                            />
                        </div>
                    </LinkTo>
                </Col>
            </Row>
            <Row style={{ marginBottom: "10rem" }}>
                <div className="box-electric-content relative animate-default active-box-category hidden-content-box border-collapsed-box mobileDisplay">
                    <div className="slideProducts showPhoneAndLess">
                        {products.length > 0 && (
                            <Slider {...settings}>
                                {products?.map(
                                    ({
                                        id,
                                        thumb,
                                        name,
                                        infos: { minPrice, maxPrice },
                                    }) => (
                                        <Col
                                            xs="12"
                                            key={id}
                                            className="paddingZero"
                                        >
                                            <div className="relative product-no-ranking">
                                                <ProductCard
                                                    id={id}
                                                    thumb={thumb}
                                                    name={name}
                                                    infos={{
                                                        minPrice,
                                                        maxPrice,
                                                    }}
                                                    little={false}
                                                />
                                            </div>
                                        </Col>
                                    )
                                )}
                            </Slider>
                        )}
                    </div>

                    {products.length > 0 &&
                        products?.map(
                            ({
                                id,
                                thumb,
                                name,
                                infos: { minPrice, maxPrice },
                            }) => (
                                <Col
                                    md={3}
                                    key={id}
                                    className="paddingZero showAbovePhone"
                                >
                                    <div className="relative product-no-ranking">
                                        <ProductCard
                                            id={id}
                                            thumb={thumb}
                                            name={name}
                                            infos={{ minPrice, maxPrice }}
                                            little={false}
                                        />
                                    </div>
                                </Col>
                            )
                        )}
                </div>
            </Row>
        </>
    )
}
Electric.propTypes = {
    products: propTypes.array.isRequired,
}
export default Electric
