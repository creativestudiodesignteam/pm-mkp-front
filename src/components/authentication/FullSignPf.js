import React, { useContext, useEffect, useState } from "react"
import { Button, Row, Col } from "reactstrap"
import { InputDefault } from ".."
import LinkTo from "../LinkTo"
import { Context, dispatchFunctions } from "../../store"
import {
  birthday,
  cpf_blur,
  cpf_rg,
  elm,
  elmAll,
  email_blur,
  hasEmptyNode,
  onKeyUpTelephone,
  fromServer,
} from "../../assets/helpers"
import { MySwal } from "../../assets/sweetalert"
import { withRouter } from "next/router"
import cep from "cep-promise"

const { init } = dispatchFunctions

const Component = ({ router, alt, ...props }) => {
  const [context, dispatch] = useContext(Context)
  const [_gender, set_gender] = useState("")

  useEffect(function() {
    for (const checkbox of elmAll("input[name=man],input[name=woman]")) {
      checkbox.addEventListener("change", function(e) {
        e.preventDefault()
        const man = this.name === "man"
        set_gender(man ? "Masculino" : "Feminino")
      })
    }
  }, [])

  const signPf = async (event) => {
    event.preventDefault()
    const { target } = event
    target.disabled = true
    const [
      { value: name },
      { value: cpf },
      { value: telephone },
      { value: email },
      { value: password },
      { value: birthday },
      { value: nameaddress },
      { value: postcode },
      { value: state },
      { value: city },
      { value: neighborhood },
      { value: street },
      { value: number },
      { value: complement },
      { value: references },
    ] = [
      elm("[name=name]"),
      elm("[name=cpf]"),
      elm("[name=telephone]"),
      elm("[name=email]"),
      elm("[name=password]"),
      elm("[name=birthday]"),
      elm("[name=nameaddress]"),
      elm("[name=postcode]"),
      elm("[name=state]"),
      elm("[name=city]"),
      elm("[name=neighborhood]"),
      elm("[name=street]"),
      elm("[name=number]"),
      elm("[name=complement]"),
      elm("[name=references]"),
    ]
    if (
      !name ||
      !cpf ||
      !telephone ||
      !email ||
      !password ||
      !birthday ||
      !_gender ||
      !nameaddress ||
      !postcode ||
      !state ||
      !city ||
      !neighborhood ||
      !street ||
      !number
    ) {
      await MySwal.fire("Por favor, preencha todos os campos", ``, "error")
      return false
    }
    try {
      const payload = {
        name,
        cpf,
        telephone,
        email,
        password,
        confirmPassword: password,
        type: false,
        data: {
          birthday,
          gender: _gender,
          addresses: {
            name: nameaddress,
            postcode,
            state,
            city,
            neighborhood,
            street,
            number,
            complement,
            references,
          },
        },
      }
      //console.log({ payload })
      const data = await fromServer("/users", "", "post", payload)
      if (data && !data.error) {
        router.push("/login")
      } else {
        console.log({ data })
        await MySwal.fire(data.error.message, "", "info")
        target.disabled = false
      }
    } catch (e) {
      await MySwal.fire("Não foi possivel cadastrar este usuario", ``, "error")
      target.disabled = false
    }
    target.disabled = false
    return true
  }

  const cepBlur = async ({ target: { value } }) => {
    const {
      state: stateBlur,
      city: cityBlur,
      street: streetBlur,
      neighborhood: neighborhoodBlur,
    } = await cep(value)

    elm("[name=street]").value = streetBlur
    elm("[name=neighborhood]").value = neighborhoodBlur
    elm("[name=city]").value = cityBlur
    elm("[name=state]").value = stateBlur
  }

  return (
    <>
      <Row id="sign_pf" className="allinerCenter flex-column mb-2">
        <Row>
          <Col md="12">
            <InputDefault
              name={"email"}
              type={"email"}
              has_label={true}
              label={"E-mail*"}
              onBlur={email_blur}
            />
          </Col>
        </Row>

        <Row className="justify-content-between">
          <Col md="6">
            <InputDefault
              name={"password"}
              type={"password"}
              has_label={true}
              label={"Senha*"}
            />
          </Col>
          <Col md="6">
            <InputDefault
              _minLength="11"
              _maxLength="14"
              onKeyUp={cpf_rg}
              onBlur={cpf_blur}
              className={"w-100 mr-2"}
              name={"cpf"}
              type={"text"}
              has_label={true}
              label={"CPF*"}
              options={{
                delimiters: [".", ".", "-"],
                blocks: [3, 3, 3, 2],
              }}
            />
          </Col>
          <Col md="6">
            <InputDefault
              name={"name"}
              type={"text"}
              has_label={true}
              label={"Nome Completo*"}
            />
          </Col>
          <Col md="6">
            <InputDefault
              onKeyUp={birthday}
              options={{
                delimiters: ["/", "/"],
                blocks: [2, 2, 4],
              }}
              id_={`birthday`}
              _maxLength={"10"}
              name={"birthday"}
              type={"text"}
              has_label={true}
              label={"Data de Nascimento*"}
            />
          </Col>
          <Col md="6">
            <InputDefault
              onKeyUp={onKeyUpTelephone}
              options={{
                delimiters: [" ", "-"],
                blocks: [2, 5, 4],
              }}
              name={"telephone"}
              type={"tel"}
              has_label={true}
              label={"Telefone*"}
            />
          </Col>
          <Col md="6" style={{ paddingTop: "rem" }}>
            <p style={{ fontWeight: 700, paddingBottom: "0.5rem" }}>Sexo*</p>
            <div className="d-flex flex-row justify-content-start">
              <label
                className={"align-text-bottom text-black-50  m-0 my-auto mr-5"}
                htmlFor="lembrar"
              >
                <input
                  checked={_gender === "Masculino"}
                  id={"sexm"}
                  type="checkbox"
                  name={"man"}
                  className={"checkbox-round  m-0 mr-1"}
                />
                Masculino
              </label>
              <label
                className={"align-text-bottom text-black-50  m-0 my-auto"}
                htmlFor="lembrar"
              >
                <input
                  checked={_gender === "Feminino"}
                  id={"sexw"}
                  type="checkbox"
                  name={"woman"}
                  className={"checkbox-round  m-0 mr-1"}
                />
                Feminino
              </label>
            </div>
          </Col>
          <Col md="6">
            <InputDefault
              name={"nameaddress"}
              type={"text"}
              has_label={true}
              label={"Nome para o endereço*"}
            />
          </Col>
          <Col md="6">
            <InputDefault
              name={"postcode"}
              type={"number"}
              has_label={true}
              label={"CEP*"}
              onBlur={cepBlur}
            />
          </Col>
          <Col md="12">
            <InputDefault
              name={"street"}
              type={"text"}
              has_label={true}
              label={"Rua*"}
            />
          </Col>
          <Col md="6">
            <InputDefault
              name={"number"}
              type={"number"}
              has_label={true}
              label={"Número*"}
            />
          </Col>
          <Col md="6">
            <InputDefault
              name={"neighborhood"}
              type={"text"}
              has_label={true}
              label={"Bairro*"}
            />
          </Col>
          <Col md="6">
            <InputDefault
              name={"complement"}
              type={"text"}
              has_label={true}
              label={"Complemento"}
            />
          </Col>
          <Col md="6">
            <InputDefault
              name={"references"}
              type={"text"}
              has_label={true}
              label={"Referências"}
            />
          </Col>
          <Col md="6">
            <InputDefault
              name={"state"}
              type={"text"}
              has_label={true}
              label={"Estado*"}
            />
          </Col>
          <Col md="6">
            <InputDefault
              name={"city"}
              type={"text"}
              has_label={true}
              label={"Cidade*"}
            />
          </Col>
        </Row>
      </Row>
      <Row>
        <Col md="12" className="mt-5 mb-4 allignerCenter">
          <Button
            onClick={signPf}
            size={"lg"}
            className={"buttonSave btn-custom actively"}
          >
            CRIAR CADASTRO
          </Button>
        </Col>
        <Col md="12">
          <div className={"border bg-white text_light_gray text-center p-5"}>
            <p className={`m-0`}>
              Já tem cadastro?
              <LinkTo
                _href="/login"
                className={"font-weight-bold text_dark_2 ml-1"}
              >
                <u>{"Clique aqui para acessar"}</u>
              </LinkTo>
            </p>
          </div>
        </Col>
      </Row>
    </>
  )
}

export default withRouter(Component)
