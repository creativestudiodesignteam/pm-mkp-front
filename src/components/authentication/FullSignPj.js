import React, {useEffect, useState} from "react"
import {
    _password_blur,
    birthday,
    cnpj,
    cnpj_blur,
    code_banks,
    cpf_blur,
    cpf_rg,
    elm,
    elmAll,
    email_blur,
    fromServer,
    hasEmptyNode,
    OBJ,
    onKeyUpTelephone,
    onlyNumber,
    postcode,
    site_blur,
    verifyEmail,
} from "../../assets/helpers"
import {Img, InputDefault, SelectCustom, SelectDefault} from "../index"
import {Button, Col, Container, FormGroup, Input, Label, Row,} from "reactstrap"
import FullSignPf from "./FullSignPf"
import {MySwal} from "../../assets/sweetalert"
import cep from "cep-promise"

function handleKeyDown(event) {
    let charCode = String.fromCharCode(event.which).toLowerCase()

    if (event.ctrlKey && charCode === "v") {
        event.preventDefault()
    }

    // For MAC we can use metaKey to detect cmd key
    if (event.metaKey && charCode === "v") {
        event.preventDefault()
        console.log("Cmd + V pressed")
    }
}

function handlePaste(event) {
    event.preventDefault()
}

const btn_text_resolver = (st) =>
    st.current === 0 || st.current <= 2 ? "CONTINUAR" : "FINALIZAR"
const Second_inputs = ({
                           changeState,
                           _state,
                           id,
                           btn_back,
                           finishCallback,
                       }) => {
    useEffect(reloadBackInputs({id, state: _state}), [id])
    const step_title = titleResolver(_state.current)
    const btn_text = btn_text_resolver(_state)
    const cepBlur = async ({target: {value}}) => {
        const {
            state: stateBlur,
            city: cityBlur,
            street: streetBlur,
            neighborhood: neighborhoodBlur,
        } = await cep(value)

        elm("[name=street]").value = streetBlur || ""
        elm("[name=neighborhood]").value = neighborhoodBlur || ""
        elm("[name=city]").value = cityBlur || ""
        elm("[name=state]").value = stateBlur || ""
    }
    return (
        <>
            <h1 className={"h2 text-center font-weight-bold m-0 mb-5"}>
                {step_title}
            </h1>
            <Row id={id} className="allignerCenter mb-3">
                <Col xs="6">
                    <InputDefault
                        name={"_name"}
                        type={"text"}
                        className={"verify"}
                        has_label={true}
                        label={"Nome do endereço*"}
                    />
                </Col>
                <Col xs="6">
                    <InputDefault
                        options={{
                            delimiters: ["-"],
                            blocks: [5, 3],
                        }}
                        onKeyUp={postcode}
                        name={"postcode"}
                        className={"verify"}
                        type={"text"}
                        has_label={true}
                        label={"CEP*"}
                        onBlur={cepBlur}
                    />
                </Col>
                <Col xs="12">
                    <InputDefault
                        name={"street"}
                        type={"text"}
                        className={"verify"}
                        has_label={true}
                        label={"Endereço*"}
                    />
                </Col>
                <Col xs="6">
                    <InputDefault
                        className={"w-75 mr-2"}
                        name={"neighborhood"}
                        className={"verify"}
                        type={"text"}
                        has_label={true}
                        label={"Bairro*"}
                    />
                </Col>
                <Col xs="6">
                    <InputDefault
                        className={"w-25 mr-2"}
                        name={"number"}
                        className={"verify"}
                        type={"number"}
                        has_label={true}
                        label={"Número*"}
                    />
                </Col>
                <Col xs="6">
                    <InputDefault
                        className={"w-100 mr-2"}
                        name={"city"}
                        type={"text"}
                        className={"verify"}
                        has_label={true}
                        label={"Cidade*"}
                    />
                </Col>
                <Col xs="6">
                    <InputDefault
                        className={"w-100 mr-2"}
                        name={"state"}
                        type={"text"}
                        className={"verify"}
                        has_label={true}
                        label={"Estado*"}
                    />
                </Col>
                <Col xs="6">
                    <InputDefault
                        _required={false}
                        name={"complement"}
                        type={"text"}
                        has_label={true}
                        label={"Complemento"}
                    />
                </Col>
                <Col xs="6">
                    <InputDefault
                        _required={false}
                        name={"references"}
                        type={"text"}
                        has_label={true}
                        label={"Referencia"}
                    />
                </Col>
            </Row>
            <div className="d-flex flex-row justify-content-start">
                <Button
                    onClick={nextStep({
                        _state,
                        _changeState: changeState,
                        _finishCallback: finishCallback,
                        id,
                    })}
                    size={"lg"}
                    className={
                        "rounded-0 border-0  px-5 py-4 btn-custom actively"
                    }
                >
                    {btn_text}
                </Button>
                {btn_back && (
                    <button
                        className={"btn_as_link ml-5"}
                        onClick={prevStep({
                            _state: _state,
                            _changeState: changeState,
                        })}
                    >
                        <u>voltar</u>
                    </button>
                )}
            </div>
        </>
    )
}
const Bank_inputs = ({
                         changeState,
                         _state,
                         id,
                         btn_back,
                         finishCallback,
                         selects,
                         setSelects,
                     }) => {
    useEffect(() => reloadBackInputs({id, state: _state}), [id])
    const step_title = titleResolver(_state.current)
    const btn_text = btn_text_resolver(_state)
    return (
        <>
            <Row>
                <Col sm={"12"}>
                    <h1 className={"h2 text-center font-weight-bold m-0 mb-5"}>
                        {step_title}
                    </h1>
                    <div
                        id={id}
                        className="d-flex flex-column justify-content-center mb-3"
                    >
                        <Container>
                            <Row>
                                <Col
                                    sm={"12"}
                                    lg={"6"}
                                    style={{
                                        paddingRight: "3rem",
                                        paddingTop: "1rem",
                                    }}
                                >
                                    <SelectDefault
                                        _required
                                        className="verify"
                                        name={"bank_code"}
                                        id={"bank_code"}
                                        has_label
                                        label={"Código do banco*"}
                                        values={code_banks
                                            .map(
                                                ({
                                                     label: name,
                                                     value: id,
                                                 }) => ({
                                                    id,
                                                    name,
                                                })
                                            )}
                                        listener={(r) =>
                                            setSelects({
                                                ...selects,
                                                bank_code:
                                                r.target.selectedOptions[0]
                                                    .text,
                                            })
                                        }
                                    />
                                </Col>
                                <Col
                                    md={6}
                                    style={{
                                        paddingRight: "3rem",
                                        paddingTop: "1rem",
                                    }}
                                >
                                    <SelectDefault
                                        _required
                                        className="verify"
                                        name={"account_type"}
                                        id={"account_type"}
                                        has_label
                                        label={"Tipo da conta*"}
                                        values={[
                                            {id: "CC", name: "CC"},
                                            {id: "CI", name: "CI"},
                                        ]}
                                        listener={(r) =>
                                            setSelects({
                                                ...selects,
                                                account_type:
                                                r.target.selectedOptions[0]
                                                    .text,
                                            })
                                        }
                                    />
                                </Col>
                                <Col
                                    md={6}
                                    style={{
                                        paddingRight: "3rem",
                                        paddingTop: "1rem",
                                    }}
                                >
                                    <InputDefault
                                        className="verify"
                                        id={"bank_agency"}
                                        name={"bank_agency"}
                                        type={"text"}
                                        has_label
                                        onKeyUp={onlyNumber}
                                        label={"Agência*"}
                                        _minLength={`${"0001".length}`}
                                        _maxLength={`${"0001".length}`}
                                        _required
                                    />
                                </Col>
                                <Col
                                    md={6}
                                    style={{
                                        paddingRight: "3rem",
                                        paddingTop: "1rem",
                                    }}
                                >
                                    <InputDefault
                                        className="verify"
                                        id={"bank_agency_digit"}
                                        name={"bank_agency_digit"}
                                        type={"text"}
                                        has_label
                                        label={
                                            "Dígito da agencia*" +
                                            "(se não tiver coloque 0)"
                                        }
                                        onKeyUp={onlyNumber}
                                        _minLength={"1"}
                                        _maxLength={"1"}
                                        _required
                                    />
                                </Col>
                                <Col
                                    md={6}
                                    style={{
                                        paddingRight: "3rem",
                                        paddingTop: "1rem",
                                    }}
                                >
                                    <InputDefault
                                        className="verify"
                                        id={"bank_account"}
                                        name={"bank_account"}
                                        type={"text"}
                                        has_label
                                        label={"Conta*"}
                                        onKeyUp={onlyNumber}
                                        _minLength={`${"5628628".length}`}
                                        _maxLength={`${"5628628".length}`}
                                        _required
                                    />
                                </Col>
                                <Col
                                    md={6}
                                    style={{
                                        paddingRight: "3rem",
                                        paddingTop: "1rem",
                                    }}
                                >
                                    <InputDefault
                                        className="verify"
                                        id={"bank_account_digit"}
                                        name={"bank_account_digit"}
                                        type={"text"}
                                        has_label={true}
                                        label={"Dígito da conta*"}
                                        _minLength={`${"9".length}`}
                                        _maxLength={`${"9".length}`}
                                        _required
                                    />
                                </Col>
                            </Row>
                        </Container>
                    </div>
                    <div className="d-flex flex-row justify-content-start">
                        <Button
                            onClick={nextStep({
                                _state,
                                _changeState: changeState,
                                _finishCallback: finishCallback,
                                selects: selects,
                                id,
                            })}
                            size={"lg"}
                            className={
                                "rounded-0 border-0  px-5 py-4 btn-custom actively"
                            }
                        >
                            {btn_text}
                        </Button>
                        {btn_back && (
                            <button
                                className={"btn_as_link ml-5"}
                                onClick={prevStep({
                                    _state: _state,
                                    _changeState: changeState,
                                })}
                            >
                                <u>Voltar</u>
                            </button>
                        )}
                    </div>
                </Col>
            </Row>
        </>
    )
}
const third_inputs = [
    {
        name: "name_completo",
        type: "text",
        className: "verify",
        has_label: true,
        label: "Nome Completo *",
    },
    {
        name: "email",
        className: "verify",
        label: "E-mail comercial *",
        has_label: true,
        type: "email",
        onBlur: email_blur,
    },
    {
        name: "cpf",
        className: "verify",
        label: "CPF *",
        has_label: true,
        type: "text",
        onBlur: cpf_blur,
        onKeyUp: cpf_rg,
        onKeyDown: handleKeyDown,
        onPaste: handlePaste,
        _minLength: 14,
        _maxLength: 14,
        _required: true,
        options: {
            delimiters: [".", ".", "-"],
            blocks: [3, 3, 3, 2],
        },
    },
    {
        name: "telefone_pessoal",
        className: "verify",
        type: "tel",
        has_label: true,
        label: "Telefone *",
        onKeyUp: onKeyUpTelephone,
        onKeyDown: handleKeyDown,
        onPaste: handlePaste,
        options: {
            delimiters: [" ", "-"],
            blocks: [2, 5, 4],
        },
    },
    {
        className: "verify",
        name: "cargo",
        type: "text",
        has_label: true,
        label: "Cargo *",
    },
    {
        className: "verify",
        name: "password",
        type: "password",
        has_label: true,
        _minLength: "6",
        label: "Senha*",
    },
    {
        className: "verify",
        name: "_password",
        type: "password",
        has_label: true,
        _minLength: "6",
        label: "Confirmar Senha*",
        onBlur: _password_blur,
    },
].map(({...rest}, i) => <InputDefault key={i} {...rest} />)
const FinishStep = ({state, finishCallback, ...rest}) => {
    const step_title = titleResolver(state.current)
    const btn_text = btn_text_resolver(state)
    return (
        <>
            <h1 className={"h2 text-center font-weight-bold m-0 mb-5"}>
                {step_title}
            </h1>
            <div className="d-flex flex-row justify-content-center">
                <Img
                    id="ok"
                    image_name_with_extension={"check@2x.png"}
                    alt={"Tudo certo"}
                />
            </div>
            <div className="d-flex flex-row justify-content-center">
                <Button
                    onClick={finishCallback}
                    size={"lg"}
                    className={
                        "rounded-0 border-0  px-5 py-4 btn-custom actively"
                    }
                >
                    {btn_text}
                </Button>
            </div>
        </>
    )
}
export const initial_state = {
    title: "",
    fields: {},
    steps: [],
    current: 0,
}
export const titleResolver = (stp) => {
    switch (Number(stp)) {
        case 0:
            return "Informações da sua loja"
        case 1:
            return "Localização da loja"
        case 2:
            return "Dados Bancários"
        case 3:
            return "Dados do responsável"
        case 4:
            return "Sua loja foi registrada com sucesso"
        default:
            throw new Error(`titleResolver Unknown action: ${stp}`)
    }
}
const prevStep = ({_state, _changeState}) => (e) => {
    e.preventDefault()
    if (_state.current) {
        _changeState({..._state, current: Number(Number(_state.current) - 1)})
    }
}
const nextStep = ({
                      _state,
                      _changeState,
                      _finishCallback,
                      id,
                      ramosPayload,
                      selects,
                      verifyEmail,
                  }) =>
    _finishCallback
        ? _finishCallback
        : async (e) => {
            e.preventDefault()
            console.log({id})
            if (
                hasEmptyNode(
                    `.verify input, .verify select, input.verify, .verify, .verify.form-group`
                )
            ) {
                console.error("Missing fill fields", hasEmptyNode(`.verify`))
                return false
            }

            if (id === "first") {
                if (ramosPayload?.length < 1) {
                    await MySwal.fire(
                        "Nenhum ramo foi selecionado",
                        "",
                        "error"
                    )
                    return false
                }
                if (!selects?.modalidade) {
                    await MySwal.fire(
                        "Nenhuma modalidade foi selecionada",
                        "",
                        "error"
                    )
                    return false
                }
                //   let { value: emailtoVerify } = elm("[name=login]")

                //   if (!!emailtoVerify) {
                //       const result = await verifyEmail(emailtoVerify)
                //       console.log({ result })
                //       if (hasKey(result, "message")) {
                //           await MySwal.fire(result.message, "", "info")
                //           return false
                //       }
                //   }
            }

            if (id === "bank" && !selects?.bank_code) {
                await MySwal.fire("Nenhum banco foi selecionado", "", "error")
                return false
            }

            if (id === "bank" && !selects?.account_type) {
                await MySwal.fire(
                    "Nenhum tipo de conta foi selecionado",
                    "",
                    "error"
                )
                return false
            }

            let obj = {}
            for (const node of elmAll(`#${id} input,#${id} select`)) {
                obj = {...obj, [`${node.name}`]: node.value}
                node.value = ""
            }
            id === "bank" && console.log({obj})
            const _title = titleResolver(_state.current)
            _changeState({
                ..._state,
                title: _title,
                fields: {..._state.fields, ...obj},
                current: Number(Number(_state.current) + 1),
            })
        }
const reloadBackInputs = ({id, state}) => () => {
    console.log(elmAll(`#${id} input`))
    for (const inp of elmAll(`#${id} input`)) {
        inp.value = ""
    }
    for (const [_name, _value] of Object.entries(state.fields)) {
        const node = elm(`input[name='${_name}']:not([type='checkbox'])`)
        console.log("reloadBackInputs", node)
        if (node) node.value = _value
    }
}
const Step = ({
                  id,
                  changeState,
                  state,
                  finishCallback = false,
                  input_fields = [],
                  btn_back = false,
                  selects,
                  setSelects,
                  ramosPayload,
                  setRamosPayload,
                  verifyEmail,
              }) => {
    useEffect(reloadBackInputs({id, state}), [id])
    const step_title = titleResolver(state.current)
    const btn_text = btn_text_resolver(state)
    const onSelectRamos = (ramosSelected) => {
        let selected = ramosSelected.map((ramo) => ramo.name)
        console.log({selected})
        setRamosPayload(selected)
    }
    const ramos = [
        {id: 0, name: "vendas online"},
        {id: 1, name: "vendas offline"},
        {id: 2, name: "vendas porta a porta"},
    ]
    const first_inputs = [
        {
            name: "cnpj",
            type: "text",
            has_label: true,
            className: "verify",
            label: "CNPJ*",
            _required: true,
            options: {
                delimiters: [".", ".", "/", "-"],
                blocks: [2, 3, 3, 4, 2],
            },
            onKeyUp: cnpj,
            onBlur: cnpj_blur,
            onKeyDown: handleKeyDown,
            onPaste: handlePaste,
        },
        {
            name: "login",
            type: "email",
            className: "verify",
            _required: true,
            has_label: true,
            label: "E-mail Comercial",
            onBlur: email_blur,
        },
        {
            name: "reason_social",
            type: "text",
            className: "verify",
            has_label: true,
            label: "Razão Social*",
        },
        {
            name: "fantasy_name",
            type: "text",
            className: "verify",
            has_label: true,
            label: "Nome Fantasia*",
        },
        {
            name: "telephone",
            type: "tel",
            className: "verify",
            has_label: true,
            label: "Telefone Comercial*",
            onKeyUp: onKeyUpTelephone,
            onKeyDown: handleKeyDown,
            onPaste: handlePaste,
            options: {
                delimiters: [" ", "-"],
                blocks: [2, 5, 4],
            },
        },
        {
            name: "telephone_r",
            type: "tel",
            has_label: true,
            label: "Telefone Whatsapp",
            onKeyUp: onKeyUpTelephone,
            onKeyDown: handleKeyDown,
            onPaste: handlePaste,
            options: {
                delimiters: [" ", "-"],
                blocks: [2, 5, 4],
            },
        },
        {
            name: "county_register",
            type: "text",
            has_label: true,
            label: "Inscrição Municipal",
        },
        {
            name: "state_register",
            type: "text",
            has_label: true,
            label: "Inscrição Estadual",
        },
        {
            name: "fundacao",
            type: "text",
            has_label: true,
            label: "Data de fundação",
            onKeyUp: birthday,
            onKeyDown: handleKeyDown,
            onPaste: handlePaste,
            options: {
                delimiters: ["/", "/"],
                blocks: [2, 2, 4],
            },
        },
        {
            name: "qty_f",
            type: "number",
            has_label: true,
            label: "Quantidade de funcionários",
        },
        {
            name: "site",
            type: "text",
            has_label: true,
            label: "Site",
            onBlur: site_blur,
        },
        {
            mult: true,
            select: true,
            className: "verify",
            label: "Ramo de atividades*",
            name: "ramo",
            values: [
                {id: 1, name: "Vendas online"},
                {id: 2, name: "Vendas offline"},
                {id: 3, name: "Vendas porta a porta"},
            ],
        },
        {
            select: true,
            className: "verify",
            label: "Modalidade*",
            name: "modalidade",
            values: [{id: 1, name: "Atacado"}, {id: 2, name: "Varejo"}],
        },
    ].map(({...rest}, i) => {
        if (rest?.mult) {
            return (
                <FormGroup>
                    <Label for={rest.name}>{rest.label}</Label>
                    <SelectCustom
                        id={rest.name}
                        name={rest.name}
                        values={ramos}
                        onSelect={onSelectRamos}
                        onRemove={onSelectRamos}
                    />
                </FormGroup>
            )
        }
        if (!rest?.select) {
            const {select, ...props} = rest
            return <InputDefault key={i} {...props} />
        }
        return (
            <FormGroup className="verify">
                <Label id={rest.name} for={rest.name}>
                    {rest.label}
                </Label>
                <Input
                    className="SelectDefault selectpicker w-100 verify"
                    required={true}
                    type="select"
                    id={rest.id}
                    name={rest.name}
                    onChange={(e) => {
                        setSelects({
                            ...selects,
                            [rest.name]:
                            e?.target?.selectedOptions[0]?.text || "",
                        })
                        console.log(e?.target?.selectedOptions[0]?.text)
                    }}
                    style={{
                        width: "100%",
                        height: "4rem",
                        border: "solid 1px #dedede",
                        backgroundColor: "#f7f7f7",
                    }}
                >
                    <option disabled selected value>
                        {"Selecione a modalidade"}
                    </option>
                    {rest?.values?.map(({id, name}) => (
                        <option key={id} value={id}>
                            {name}
                        </option>
                    ))}
                </Input>
            </FormGroup>
        )
    })
    return (
        <>
            <h1 className={"h2 text-center font-weight-bold m-0 mb-5"}>
                {step_title}
            </h1>
            <div
                id={id}
                className="d-flex flex-column justify-content-center mb-3"
            >
                {id === "first" ? first_inputs : input_fields}
            </div>
            <div className="d-flex flex-row justify-content-start">
                <Button
                    onClick={nextStep({
                        _state: state,
                        _changeState: changeState,
                        _finishCallback: finishCallback,
                        ramosPayload: ramosPayload,
                        selects: selects,
                        verifyEmail: verifyEmail,
                        id,
                    })}
                    size={"lg"}
                    className={
                        "rounded-0 border-0  px-5 py-4 btn-custom actively"
                    }
                >
                    {btn_text}
                </Button>
                {btn_back && (
                    <button
                        className={"btn_as_link ml-5"}
                        onClick={prevStep({
                            _state: state,
                            _changeState: changeState,
                        })}
                    >
                        <u>Voltar</u>
                    </button>
                )}
            </div>
        </>
    )
}
const StepResolver = ({
                          id,
                          btn_back,
                          finishCallback,
                          _state,
                          changeState,
                          input_fields,
                          selects = "",
                          setSelects = "",
                          setRamosPayload,
                          ramosPayload,
                          verifyEmail,
                      }) => {
    const {current} = _state
    return (
        <>
            <Row>
                <Col xs="12">
                    {isMobileOnly && (
                        <div className={"step-wrap mb-7"}>
                            {current === 0 && (
                                <div className={`elipse elipse-active`}>
                                    1 <span>Dados da loja</span>
                                </div>
                            )}

                            {current === 1 && (
                                <div
                                    className={`elipse ${
                                        current >= 1 ? "elipse-active" : ""
                                    }`}
                                >
                                    2<span>Localização</span>
                                </div>
                            )}

                            {current === 2 && (
                                <div
                                    className={`elipse ${
                                        current >= 2 ? "elipse-active" : ""
                                    }`}
                                >
                                    3<span>Dados Bancarios</span>
                                </div>
                            )}

                            {current === 3 && (
                                <div
                                    className={`elipse ${
                                        current >= 3 ? "elipse-active" : ""
                                    }`}
                                >
                                    4<span>Dados Pessoais</span>
                                </div>
                            )}

                            {current === 4 && (
                                <div
                                    className={`elipse ${
                                        current >= 4 ? "elipse-active" : ""
                                    }`}
                                >
                                    5<span>Finalização</span>
                                </div>
                            )}
                        </div>
                    )}
                    {(isBrowser || isTablet) && (
                        <div className={"step-wrap mb-7"}>
                            <div className={`elipse elipse-active`}>
                                1 <span>Dados da loja</span>
                            </div>

                            <div
                                className={`hr ${
                                    current >= 1 ? " bg_red" : ""
                                }`}
                            />
                            <div
                                className={`elipse ${
                                    current >= 1 ? "elipse-active" : ""
                                }`}
                            >
                                2<span>Localização</span>
                            </div>
                            <div
                                className={`hr ${
                                    current >= 2 ? " bg_red" : ""
                                }`}
                            />
                            <div
                                className={`elipse ${
                                    current >= 2 ? "elipse-active" : ""
                                }`}
                            >
                                3<span>Dados Bancarios</span>
                            </div>
                            <div
                                className={`hr ${
                                    current >= 3 ? " bg_red" : ""
                                }`}
                            />
                            <div
                                className={`elipse ${
                                    current >= 3 ? "elipse-active" : ""
                                }`}
                            >
                                4<span>Dados Pessoais</span>
                            </div>
                            <div
                                className={`hr ${
                                    current >= 4 ? " bg_red" : ""
                                }`}
                            />
                            <div
                                className={`elipse ${
                                    current >= 4 ? "elipse-active" : ""
                                }`}
                            >
                                5<span>Finalização</span>
                            </div>
                        </div>
                    )}
                </Col>
            </Row>
            <Row>
                <Col sm={"6"} className={"mx-auto"}>
                    {(Number(current) === 1 && (
                        <Second_inputs
                            finishCallback={finishCallback}
                            btn_back={btn_back}
                            id={id}
                            _state={_state}
                            changeState={changeState}
                        />
                    )) ||
                    (Number(current) === 2 && (
                        <Bank_inputs
                            selects={selects}
                            setSelects={setSelects}
                            finishCallback={finishCallback}
                            btn_back={btn_back}
                            id={id}
                            _state={_state}
                            changeState={changeState}
                        />
                    )) ||
                    ((Number(current) <= 2 || Number(current) === 3) && (
                        <Step
                            selects={selects}
                            setSelects={setSelects}
                            changeState={changeState}
                            state={_state}
                            input_fields={input_fields}
                            id={id}
                            btn_back={btn_back}
                            finishCallback={finishCallback}
                            setRamosPayload={setRamosPayload}
                            ramosPayload={ramosPayload}
                            verifyEmail={verifyEmail}
                        />
                    )) || (
                        <FinishStep
                            id={id}
                            state={_state}
                            finishCallback={finishCallback}
                            ramosPayload={ramosPayload}
                        />
                    )}
                </Col>
            </Row>
        </>
    )
}
const _finishCallback = (
    _router,
    tp,
    state_,
    setState_,
    selects,
    ramosPayload
) => async (e) => {
    e.preventDefault()
    const {target} = e
    target.disabled = true
    const body = {
        ...state_.fields,
        password: elm("[name=password]").value,
        confirmPassword: elm("[name=_password]").value,
    }
    if (body.password !== body.confirmPassword) {
        await MySwal.fire("Senhas não coincidem", "", "error")
    }
    const {
        login,
        password,
        confirmPassword,
        cnpj,
        fantasy_name,
        reason_social,
        state_register,
        telephone,
        telephone_r,
        fundacao,
        qty_f,
        site,
        _name,
        postcode,
        state,
        city,
        neighborhood,
        street,
        number,
        county_register,
        bank_code,
        account_type,
        bank_agency,
        bank_agency_digit,
        bank_account,
        bank_account_digit,
    } = body
    let newPayload = null
    if (body.qty_f === "") {
        newPayload = {
            password: password,
            confirmPassword: confirmPassword,
            email: login,
            type: true,
            data: {
                cnpj: cnpj,
                county_register: county_register,
                fantasy_name: fantasy_name,
                reason_social: reason_social,
                state_register: state_register,
                telephone_commercial: telephone,
                telephone_whatsapp: telephone_r,
                data_fundacao: fundacao,
                qty_funcionarios: 0,
                site: site,
                modalidade: selects.modalidade,
                addresses: {
                    name: _name,
                    postcode: postcode,
                    state: state,
                    city: city,
                    neighborhood: neighborhood,
                    street: street,
                    number: number,
                },
                BankData: {
                    Bank: {
                        Code: bank_code,
                    },
                    AccountType: {
                        Code: account_type,
                    },
                    BankAgency: bank_agency,
                    BankAgencyDigit: bank_agency_digit,
                    BankAccount: bank_account,
                    BankAccountDigit: bank_account_digit,
                },
                responsible: {
                    name: elm("[name=name_completo]").value,
                    cpf: elm("[name=cpf]").value,
                    email: elm("[name=email]").value,
                    telephone: elm("[name=telefone_pessoal]").value,
                    cargo: elm("[name=cargo]").value,
                },
                ramos: ramosPayload,
            },
        }
    } else {
        newPayload = {
            password: password,
            confirmPassword: confirmPassword,
            email: login,
            type: true,
            data: {
                cnpj: cnpj,
                county_register: county_register,
                fantasy_name: fantasy_name,
                reason_social: reason_social,
                state_register: state_register,
                telephone_commercial: telephone,
                telephone_whatsapp: telephone_r,
                data_fundacao: fundacao,
                qty_funcionarios: qty_f,
                site: site,
                modalidade: selects.modalidade,
                addresses: {
                    name: _name,
                    postcode: postcode,
                    state: state,
                    city: city,
                    neighborhood: neighborhood,
                    street: street,
                    number: number,
                },
                BankData: {
                    Bank: {
                        Code: bank_code,
                    },
                    AccountType: {
                        Code: account_type,
                    },
                    BankAgency: bank_agency,
                    BankAgencyDigit: bank_agency_digit,
                    BankAccount: bank_account,
                    BankAccountDigit: bank_account_digit,
                },
                responsible: {
                    name: elm("[name=name_completo]").value,
                    cpf: elm("[name=cpf]").value,
                    email: elm("[name=email]").value,
                    telephone: elm("[name=telefone_pessoal]").value,
                    cargo: elm("[name=cargo]").value,
                },
                ramos: ramosPayload,
            },
        }
    }

    try {
        const data = await fromServer("/users", "", "POST", newPayload)
        if (!OBJ.hasKey(data, "error")) {
            await MySwal.fire("Usuario cadastrado", "", "success")
            _router.push("/login")
        } else {
            await MySwal.fire(
                "Não foi possivel cadastrar este fornecedor",
                data.error?.message,
                "error"
            )
        }
    } catch (e) {
        console.log(e)
        await MySwal.fire(
            "Não foi possivel cadastrar este fornecedor",
            e.message,
            "error"
        )
    }
    target.disabled = false
    return false
}
export default function stepSwitcher(
    state_,
    setState_,
    tp,
    _router,
    selects,
    setSelects
) {
    const [ramosPayload, setRamosPayload] = useState([])
    if (tp === "pf") {
        return <FullSignPf router={_router}/>
    }
    switch (Number(state_.current)) {
        case 0:
            return (
                <StepResolver
                    selects={selects}
                    setSelects={setSelects}
                    changeState={setState_}
                    _state={state_}
                    input_fields={""}
                    id={"first"}
                    btn_back={false}
                    finishCallback={false}
                    setRamosPayload={setRamosPayload}
                    ramosPayload={ramosPayload}
                    verifyEmail={verifyEmail}
                />
            )
        case 1:
            return (
                <StepResolver
                    changeState={setState_}
                    _state={state_}
                    input_fields={third_inputs}
                    id={"second"}
                    btn_back={true}
                    finishCallback={false}
                />
            )
        case 2:
            return (
                <StepResolver
                    selects={selects}
                    setSelects={setSelects}
                    changeState={setState_}
                    _state={state_}
                    id={"bank"}
                    btn_back={true}
                    finishCallback={false}
                />
            )
        case 3:
            return (
                <StepResolver
                    changeState={setState_}
                    _state={state_}
                    input_fields={third_inputs}
                    id={"third"}
                    btn_back={true}
                    finishCallback={_finishCallback(
                        _router,
                        tp,
                        state_,
                        setState_,
                        selects,
                        ramosPayload
                    )}
                />
            )
        case 4:
            return (
                <StepResolver
                    changeState={setState_}
                    _state={state_}
                    input_fields={""}
                    id={"fourth"}
                    btn_back={false}
                    finishCallback={() => _router.push("/login")}
                />
            )
        default:
            throw new Error(`stepSwitcher Unknown action: ${state_.current}`)
    }
}
