import React, { useContext, useEffect, useState } from "react"
import propTypes from "prop-types"
import { Button, Col, Row } from "reactstrap"
import { Context, dispatchFunctions } from "../../store"
import { elm, email_blur, fromServer, tryCatch } from "../../assets/helpers"
import { AuthenticationContainer, InputDefault } from "../index"
import LinkTo from "./../LinkTo"
import { withRouter } from "next/router"
import { MySwal, Toast } from "../../assets/sweetalert"
import {
    _storage,
    isAuthenticated,
    login,
    remember,
    userByToken,
} from "../../assets/auth"
import ClipLoader from "react-spinners/ClipLoader"

const { init } = dispatchFunctions
const initial_state = {
    pf: {
        _is: false,
        cpf: "",
        password: "",
        name: "",
        message: "para cadastrar",
        placeholder: "informe seu E-mail",
    },
    pj: {
        _is: true,
        cnpj: "",
        password: "",
        name: "",
        message: "para cadastrar sua empresa",
        placeholder: "informe seu CNPJ",
    },
}

function LoginWrap({ router }) {
    const [loaderState, setLoaderState] = useState(false)
    const [{ token }, setContext] = useContext(Context)
    const [state, setState] = useState(initial_state)
    useEffect(() => {
        const user = userByToken(token)
        if (!!user) {
            router.push("/account?token=" + token)
            setLoaderState(true)
        }
    }, [])
    const { pf, pj } = state
    const changeClientType = (btn_name) => (e) => {
        e.preventDefault()
        if (btn_name === "pf") {
            elm("#pf").classList.add("actively")
            elm("#pj").classList.remove("actively")
            setState({
                ...state,
                pj: {
                    ...pj,
                    _is: false,
                },
                pf: {
                    ...pf,
                    _is: true,
                },
            })
        } else if (btn_name === "pj") {
            elm("#pj").classList.add("actively")
            elm("#pf").classList.remove("actively")
            setState({
                ...state,
                pj: {
                    ...pj,
                    _is: true,
                },
                pf: {
                    ...pf,
                    _is: false,
                },
            })
        } else {
            //console.log("XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX")
        }
    }
    const submitLogin = async (event) => {
        event.preventDefault()
        setLoaderState(true)
        const btn = elm("form.authentication_container [type=submit]")
        btn.disabled = true
        const { value: email } = elm("#form input[name=login]")
        const { value: password } = elm("#form input[name=password]")
        const type = pj._is
        const _remember = elm("#remember")?.checked || false
        if (email && password) {
            const _user = { email, password, type }
            //console.log("USER", {_user})
            const response = await fromServer("/auth", false, "post", _user)
            //console.log("response", {response})
            if (!!response && response?.token) {
                const user = isAuthenticated(response.token)
                /*console.log("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!", {
                    user,
                    response,
                })*/
                if (user !== false) {
                    login(response.token)
                    remember(_remember, _user)
                    await Toast.fire(`Bem Vindo ${user.name}`, "", "success")
                    try {
                        const storage = _storage()
                        if (!!storage) {
                            const rawCartNotLogged = JSON.parse(
                                `${storage.getItem("cart")}` || "[]"
                            )
                            storage.setItem("cart", "[]")
                            console.log(
                                await Promise.all(
                                    await rawCartNotLogged.map(async (rc) => {
                                        return await fromServer(
                                            "/carts",
                                            response.token,
                                            "POST",
                                            {
                                                fk_grids: rc.id,
                                                amount: rc.amount,
                                            }
                                        )
                                    })
                                )
                            )
                            tryCatch(async () => {
                                const { length } = await fromServer(
                                    "/carts",
                                    response.token
                                )
                                console.log({ length })
                                setContext(
                                    dispatchFunctions.raw({
                                        user,
                                        token: response.token,
                                        cart_length: Number(length),
                                    })
                                )
                            })
                        } else {
                            setContext(
                                dispatchFunctions.login({
                                    user,
                                    token: response.token,
                                })
                            )
                        }
                    } catch (e) {
                        console.error(e)
                    }
                    btn.disabled = false
                    await router.push("/account?token=" + response.token)
                } else {
                    //console.log({user})
                    btn.disabled = false
                    setLoaderState(false)
                }
            } else {
                console.error(response)
                if (
                    response?.error?.message ===
                    "Por favor, verifique seu email para ativar sua conta."
                ) {
                    await MySwal.fire(response.error.message, "", "info")
                    setLoaderState(false)
                } else {
                    setLoaderState(false)
                    await MySwal.fire(response.error.message, "", "error")
                }
                btn.disabled = false
                setLoaderState(false)
            }
            setLoaderState(false)

            return false
        } else {
            await MySwal.fire(
                "Por favor, preencha todos os campos",
                "",
                "error"
            )
        }
        btn.disabled = false
        setLoaderState(false)

        return false
    }
    const { message } = pf._is ? pf : pj
    const __is = pf._is ? "pf" : "pj"
    return (
        <>
            {loaderState && (
                <div className="spinnerLoader">
                    <ClipLoader
                        size={150}
                        color={"var(--red-1)"}
                        loading={loaderState}
                    />
                </div>
            )}
            <AuthenticationContainer maxW={"420px"} onSubmit={submitLogin}>
                <h1 className={"h1 text-center font-weight-bold mb-6"}>
                    Login
                </h1>
                <div className="d-flex flex-row justify-content-center my-5">
                    <Button
                        id={"pj"}
                        onClick={changeClientType("pj")}
                        size={"lg"}
                        className={`rounded-0 border-0  px-5 py-4 mx-2 btn-custom ${
                            pj._is ? "actively" : ""
                        }`}
                    >
                        FORNECEDOR
                    </Button>
                    <Button
                        id={"pf"}
                        onClick={changeClientType("pf")}
                        size={"lg"}
                        className={`rounded-0 border-0  px-5 py-4 mx-2 btn-custom ${
                            pf._is ? "actively" : ""
                        }`}
                    >
                        CLIENTE
                    </Button>
                </div>
                <div className={"border bg-red text_white text-center p-5 fix"}>
                    <p className={`m-0`}>
                        Sou novo por aqui!
                        <LinkTo _href={pj._is ? "/newsign" : "/customersign"}>
                            <span className="text_yellow">Clique aqui </span>
                        </LinkTo>
                        <span className="text_white">{message}</span>
                    </p>
                </div>

                <p className={"text_light_gray my-4"}>
                    Se você ja se cadastrou insira seus dados abaixo. Se é novo
                    por aqui preencha os campos acima!
                </p>
                <div
                    id={"form"}
                    className="d-flex flex-column justify-content-center mb-4"
                >
                    <InputDefault
                        type="email"
                        name="login"
                        id="login"
                        placeholder={"Informe seu e-mail*"}
                        onBlur={({ target }) => {
                            email_blur({ target })
                            const storage = _storage()
                            if (!!storage) {
                                const user = JSON.parse(
                                    `${storage.getItem("user")}` || {}
                                )
                                const passNode = elm(
                                    "#form input[name=password]"
                                )
                                if (user && user.email) {
                                    if (
                                        target.value === user.email &&
                                        passNode
                                    ) {
                                        passNode.value = user.password
                                    }
                                }
                            }
                        }}
                        _required={true}
                    />
                    <InputDefault
                        type="password"
                        name="password"
                        id="password"
                        placeholder="Sua senha*"
                        _required={true}
                    />
                </div>
                <Row className="allignerCenter mb-4">
                    <Col xs="12" md="6" className="allignerCenter mb-2">
                        <Button
                            type={"submit"}
                            size={"lg"}
                            className={
                                "rounded-0 border-0  px-5 py-4 btn-custom actively"
                            }
                        >
                            Entrar
                        </Button>
                    </Col>

                    <Col
                        xs="12"
                        md="6"
                        lg={"4"}
                        className="allignerCenter mb-2 p-0"
                    >
                        <input
                            name={"remember"}
                            autoComplete="off"
                            id="remember"
                            type="checkbox"
                            className={"m-0 mr-4 mb-1"}
                        />
                        <label
                            htmlFor={"remember"}
                            className={"m-0 text_light_gray"}
                        >
                            Lembrar acesso?
                        </label>
                    </Col>
                    <Col xs="12" md="6" className="allignerCenter mb-2">
                        <LinkTo _href={"/remind"} className="m-0">
                            <b className="m-0 text_dark">
                                <u>Esqueci a senha</u>
                            </b>
                        </LinkTo>
                    </Col>
                </Row>
                <Row>
                    <Col xs="12">
                        <p className={`m-0`}>
                            Ao continuar com o acesso, você concorda com nossa{" "}
                            <LinkTo _href={"/privacypolicy"}>
                                <span
                                    className="text_red"
                                    onClick={() => setLoaderState(true)}
                                >
                                    Política de privacidade
                                </span>
                            </LinkTo>
                        </p>
                    </Col>
                </Row>
            </AuthenticationContainer>
        </>
    )
}

LoginWrap.propTypes = {
    router: propTypes.any.isRequired,
    context: propTypes.object.isRequired,
}
export default withRouter(LoginWrap)
