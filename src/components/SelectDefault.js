import React from "react"
import propTypes from "prop-types"
import { FormGroup, Label } from "reactstrap"
import { compareBy } from "../assets/helpers"

const SelectDefault = ({
    listener,
    _required = true,
    id,
    has_label = false,
    label,
    name,
    _style_label = {},
    values = [],
    placeholder = "",
    multiple = false,
    initial_value = "",
    value = false,
    ...rest
}) => {
    const __style_label = {
        ..._style_label,
        ...(has_label ? { display: "block" } : { display: "none" }),
    }
    const labelId = `${id}-${name}-SelectDefault`
    const has_selected = values.reduce(
        (acc, c, index, arr) => (Boolean(arr[index]?.selected) ? true : acc),
        false
    )
    return (
        (!value && (
            <FormGroup {...rest}>
                <Label id={labelId} for={name} style={__style_label}>
                    {label}
                </Label>
                <select
                    multiple={multiple}
                    className="SelectDefault selectpicker w-100"
                    required={_required}
                    onChange={listener}
                    id={id}
                    name={name}
                    defaultValue={initial_value}
                    style={{
                        width: "100%",
                        height: "4rem",
                        border: "solid 1px #dedede",
                        backgroundColor: "#f7f7f7",
                    }}
                >
                    {!has_selected && (
                        <option disabled selected>
                            {placeholder}
                        </option>
                    )}

                    {values
                        .sort(compareBy("name "))
                        ?.map(({ id, name, ...props }, index) => (
                            <option key={index} value={id} {...props}>
                                {name}
                            </option>
                        ))}
                </select>
            </FormGroup>
        )) || (
            <FormGroup {...rest}>
                <Label id={labelId} for={name} style={__style_label}>
                    {label}
                </Label>
                <select
                    multiple={multiple}
                    className="SelectDefault selectpicker w-100"
                    required={_required}
                    onChange={listener}
                    id={id}
                    name={name}
                    defaultValue={initial_value}
                    value={value}
                    style={{
                        width: "100%",
                        height: "4rem",
                        border: "solid 1px #dedede",
                        backgroundColor: "#f7f7f7",
                    }}
                >
                    {!has_selected && (
                        <option disabled selected>
                            {placeholder}
                        </option>
                    )}

                    {values
                        .sort(compareBy("name "))
                        ?.map(({ id, name, ...props }, index) => (
                            <option key={index} value={id}>
                                {name}
                            </option>
                        ))}
                </select>
            </FormGroup>
        )
    )
}

SelectDefault.propTypes = {
    placeholder: propTypes.string,
    listener: propTypes.func.isRequired,
    _required: propTypes.bool,
    id: propTypes.any.isRequired,
    has_label: propTypes.bool,
    label: propTypes.string,
    name: propTypes.string.isRequired,
    _style_label: propTypes.object,
    values: propTypes.array.isRequired,
}
export default SelectDefault
