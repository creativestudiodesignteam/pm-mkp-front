import React from 'react';
import {Img, Link} from "./index";
import SemiContainer from "./SemiContainer";
import {getAttrOrDefault, verifyAllAttrs} from '../assets/helpers'

const Index = ({data = [], classNames = ['', '', ''], over = false, overClassNames = ['', '', ''], children, ...rest}) => {
    const [cl_first, cl_second, cl_third] = over ? overClassNames : classNames
    const _data = (data && data[0] && verifyAllAttrs(data, ['href', 'img', 'alt']) ? data : [
        {
            href: '/',
            img: 'slide_v3.png',
            alt: 'Banner Home V3'

        }, {
            href: '/',
            img: 'banner_v3_1-min.png',
            alt: 'Banner Home V3'

        }
    ]).map(
        ({href, img, alt, ...props}, i) => (
            <div
                key={i}
                className={`${over ? getAttrOrDefault(props, '_class', cl_third) : `item ${cl_third}`}`}>
                <Link _href={href}>
                    <Img image_name_with_extension={img} alt={alt}/>
                    {getAttrOrDefault(props, 'node', null)}
                </Link>
            </div>
        )
    );
    return (
        <SemiContainer over={true} overClassNames={
            over ? [cl_first, cl_second, cl_third] : [
                `clearfix slide-box-home slide-v3 relative top-margin-15-default left-margin-15-default ${cl_first}`,
                `clearfix slide-home owl-carousel owl-theme ${cl_second}`
            ]} {...rest}>
            {_data}
            {children}
        </SemiContainer>
    )

};

export default Index;