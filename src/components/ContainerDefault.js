import React from 'react';
import SemiContainer from "./SemiContainer";


const Index = ({classNames = ['', '', ''], over = false, overClassNames = ['', '', ''], children, ...props}) => {
    const [cl_first, cl_second, cl_third] = over ? overClassNames : classNames
    if (over) {
        return (
            <SemiContainer over={true} overClassNames={[cl_first, cl_second]} {...props}>
                <div className={cl_third}>
                    {children}
                </div>
            </SemiContainer>
        )
    }
    return (
        <SemiContainer classNames={[cl_first, cl_second]} {...props}>
            <div className={`row ${cl_third}`}>
                {children}
            </div>
        </SemiContainer>
    );

};
export default Index;