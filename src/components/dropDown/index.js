import React, {useMemo} from 'react';
import {DropdownItem, DropdownMenu, DropdownToggle, UncontrolledDropdown} from 'reactstrap';
import PropTypes from 'prop-types';
import {LinkTo} from "../index";

const Index = ({title, items}) => {
    return (
        useMemo(() => (
            <UncontrolledDropdown>
                <DropdownToggle color={'transparent'}  caret className='pl-0 pt-0 text-left text_dark'>
                    {title}
                </DropdownToggle>
                <DropdownMenu>
                    {
                        items?.map(({text, ...props}, index) => (
                            props?.href ?
                                <DropdownItem key={index}>
                                    <LinkTo _href={props?.href} {...props}>
                                        {text}
                                    </LinkTo>
                                </DropdownItem> :
                                <DropdownItem key={index} {...props}>
                                    {text}
                                </DropdownItem>
                        ))
                    }
                </DropdownMenu>
            </UncontrolledDropdown>
        ), [title, items])
    );
}

Index.propTypes = {
    title: PropTypes.string.isRequired,
    items: PropTypes.array.isRequired
};

export default Index;

