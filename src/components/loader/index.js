import React from 'react';
import styled from 'styled-components'

export const Index = styled.div`
    &{
      display: inline-block;
      width: 100%!important;
      height: 100%!important;
    }
    &:after {
      content: " "!important;
      display: block!important;
      width: 64px!important;
      height: 64px!important;
      margin: 8px!important;
      border-radius: 50%!important;
      border: 6px solid #fff!important;
      border-color: #fff transparent #fff transparent!important;
      animation: lds-dual-ring 1.2s linear infinite!important;
    }
`;
export default Index;