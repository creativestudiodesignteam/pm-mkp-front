import React from 'react';
import { makeStyles, useTheme } from '@material-ui/core/styles';
import Input from '@material-ui/core/Input';
import MenuItem from '@material-ui/core/MenuItem';
import Select from '@material-ui/core/Select';
import Chip from '@material-ui/core/Chip';
import { FormGroup, Label } from 'reactstrap'
const useStyles = makeStyles((theme) => ({
    formControl: {
        margin: theme.spacing(1),
        minWidth: 120,
        maxWidth: 300,
    },
    chips: {
        display: 'flex',
        flexWrap: 'wrap',
    },
    chip: {
        margin: 2,
    },
    noLabel: {
        marginTop: theme.spacing(3),
    },
}));

const ITEM_HEIGHT = 48;
const ITEM_PADDING_TOP = 8;
const MenuProps = {
    PaperProps: {
        style: {
            maxHeight: ITEM_HEIGHT * 4.5 + ITEM_PADDING_TOP,
            width: 250,
        },
    },
};
function getStyles(name, personName, theme) {
    return {
        fontWeight:
            personName.indexOf(name) === -1
                ? theme.typography.fontWeightRegular
                : theme.typography.fontWeightMedium,
    };
}
const _values = [{
    id: 0,
    name: ' ... '
}];
export default function MultipleSelect({ id, has_label = false, _style_label, name, label, values = _values, ...rest }) {
    const __style_label = { ..._style_label, ...has_label ? { display: 'block' } : { display: 'none' } }
    const classes = useStyles();
    const theme = useTheme();
    const [personName, setPersonName] = React.useState([]);

    const handleChange = (event) => {
        setPersonName(event.target.value);
    };

    const handleChangeMultiple = (event) => {
        try {
            const { options } = event.target;
            const value = [];
            for (let i = 0, l = options.length; i < l; i += 1) {
                //console.log('handleChangeMultiple', options[i].selected, options[i].value)
                value.push(options[i].value);
                if (options[i].selected) {
                    value.push(options[i].value.name);
                }
            }
            setPersonName(value);
        } catch (error) {
            console.log('handleChangeMultiple', error)
        }

    };
    const labelId = `${name}-${id}-multChipLabel`
    return (
        <FormGroup {...rest}>
            <Label id={labelId} style={__style_label} for={name}>{label}</Label>
            <Select
                style={{
                    width: '100%',
                    height: '4rem',
                    border: 'solid 1px #dedede',
                    backgroundColor: '#f7f7f7'
                }}
                labelId={labelId}
                id={id}
                name={name}
                multiple
                value={personName}
                onChange={handleChange}
                input={<Input id="select-multiple-chip" />}
                renderValue={(selected) => (
                    <div className={classes.chips}>
                        {selected.map(({ id, name }) => (
                            <Chip key={id} label={name} className={classes.chip} />
                        ))}
                    </div>
                )}
                MenuProps={MenuProps}
            >
                {values && values.map(
                    ({ id, name }) => (
                        <MenuItem
                            key={id}
                            value={{ id, name }}
                            style={getStyles(name, personName, theme)}
                        >
                            {name}
                        </MenuItem>
                    )
                )}
            </Select>
        </FormGroup>
    );
}
