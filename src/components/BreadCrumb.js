import React from "react"
import {verifyAllAttrs} from "../assets/helpers"
import LinkTo from "./LinkTo"

const Breadcrumb = ({
                        data = [],
                        classNames = ["", "", ""],
                        _style = {},
                        ...rest
                    }) => {
    const [cl_first] = classNames
    const _data = (data && verifyAllAttrs(data[0], ["node_text", "href", "query"])
            ? data
            : []
    ).map(({node_text, href, query, ...props}, i) => (
        <li key={i} className="breadcrumb-item">
            {
                (
                    props?.as?.length > 0 && <LinkTo as={props?.as} _href={href} query={query}>
                        {node_text}
                    </LinkTo>
                ) ||
                <LinkTo _href={href} query={query}>
                    {node_text}
                </LinkTo>
            }

        </li>
    ))
    return (
        <nav
            style={{marginLeft: "-20px!Important"}}
            aria-label="breadcrumb"
            className={` ${cl_first} my-4`}
            {...rest}
        >
            <ol style={_style} className={`breadcrumb breadcrumb_custom`}>
                <li className="breadcrumb-item">
                    <LinkTo _href={"/"}>home</LinkTo>
                </li>
                {_data}
            </ol>
        </nav>
    )
}

export default Breadcrumb
