import React from 'react'
import {Img} from "./index";


function Index({img, alt, classNames = ['', '', ''], over = false, overClassNames = ['', '', ''], children, node_text = '', ...rest}) {
    const [cl_first, cl_second, cl_third] = over ? overClassNames : classNames
    return (
        <div className={over ? cl_first : `clearfix name-title-box title-category ${cl_first}`} {...rest}>
            <Img image_name_with_extension={img} alt={alt} className={over ? cl_second : `absolute ${cl_second}`}/>
            <p className={over ? cl_third : `text-default-color ${cl_third}`}>{node_text}</p>
        </div>
    );
}

export default Index;