import React, {useMemo} from 'react'

export default function Component({children, _style = {}, ...rest}) {
    const defaultStyle = {
        color: 'blue',
        ..._style
    }
    return useMemo(() =>
        <li style={defaultStyle} {...rest}>
            {children}
        </li>, [children, _style]
    )
};
