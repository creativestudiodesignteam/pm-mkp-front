import React, {useContext, useEffect, useState} from "react"
import {Button, Col, Row} from "reactstrap"
import {Img} from "../../.."
import styles from "./Items.module.css"
import {brlFormat, disable_btn_and_put_innerHTML, elm, fromServer, hasKey, tryCatch,} from "../../../../assets/helpers"
import {useRouter} from "next/router"
import {_storage} from "../../../../assets/auth";
import {Context, dispatchFunctions} from "../../../../store";

function ShowAboveTablet({item, token, less, more, remove}) {
    return (
        <>
            <Row className="mb-4">
                <Col lg="12" className={styles.alignerEnd}>
                    <Button
                        id={`remove-${item.id}`}
                        onClick={remove(item.id)}
                        className={styles.delete}
                    >
                        x
                    </Button>
                </Col>
                <Col lg="12" className="allignerAround">
                    <Col lg="3">
                        <Img
                            image_name_with_extension={
                                item.url ||
                                "placeholder/default.png"
                            }
                            alt="Image of Product"
                            outSide={!!item.url}
                            className={styles.imageSize}
                        />
                    </Col>
                    <Col lg="3">
                        <Col lg="12">
                            <Button
                                size="lg"
                                className={styles.count}
                            >
                                {item.name.length > 20 &&
                                item.name.slice(0, 17)}
                                {item.name.length > 20 && (
                                    <small className={"text-muted"}>
                                        {" "}
                                        ...{" "}
                                    </small>
                                )}
                                {item.name.length <= 20 &&
                                item.name}
                                {item.name.length < 20 && (
                                    <small className={"text-white"}>
                                        {Array(20)
                                            .fill(".")
                                            .join("")}
                                    </small>
                                )}
                            </Button>
                        </Col>
                        {/* <Col lg="12">
                                        <Button size="lg" className={styles.days}>
                                            Chegará em X dias úteis
                                        </Button>
                                    </Col> */}
                    </Col>
                    <Col lg="3" style={{textAlign: "end"}}>
                        <div>
                            <Button
                                className={styles.countButtonLeft}
                                outline
                                color="primary"
                                onClick={less(item.id)}
                                id={`less-${item.id}`}
                                disabled={item.amount === 1}
                            >
                                -
                            </Button>
                            <Button
                                className={styles.count}
                                outline
                                color="primary"
                            >
                                {item.amount}
                            </Button>
                            <Button
                                className={styles.countButtonRight}
                                outline
                                color="primary"
                                onClick={more(item.id)}
                                id={`more-${item.id}`}
                                disabled={
                                    item.amount >= item.amount_stock
                                }
                            >
                                +
                            </Button>
                        </div>
                    </Col>
                    <Col lg="3">

                        <Row>
                            <Col lg={12} style={{textAlign: 'end'}}>
                                <Button outline className={styles.price}>
                                    {brlFormat(item.price)}
                                </Button>
                            </Col>
                            {
                                /*
                                    <Col lg={12} style={{textAlign: 'end'}}>
                                        <Button outline className={styles.freight}
                                                style={{color: 'black !important'}}>
                                            <strong>Frete </strong>
                                            &nbsp;
                                            {!token && <>R$ {toFixed(item.price)}</>}
                                            {token && <>R$ {item.price}</>}
                                        </Button>
                                    </Col>
                            */
                            }
                        </Row>
                    </Col>
                </Col>
            </Row>
            <hr/>
        </>
    )
}

function ShowTabletAndLess({item, token, less, more, remove}) {
    return (
        <Row>
            <Col xs="12" className={styles.alignerEnd}>
                <Button
                    id={`remove-${item.id}`}
                    onClick={remove(item.id)}
                    className={styles.delete}
                >
                    Remover do carrinho
                </Button>
            </Col>
            <Col xs="12">
                <Row>
                    <Col xs="12">
                        <Img
                            image_name_with_extension={
                                item.url ||
                                "placeholder/default.png"
                            }
                            alt="Image of Product"
                            outSide={!!item.url}
                            className={styles.imageSize}
                        />
                    </Col>
                    <Col xs="12" className={styles.colName}>
                        <Button size="lg" className={styles.count}>
                            {item.name.length > 20 &&
                            item.name.slice(0, 17)}
                            {item.name.length > 20 && (
                                <small className={"text-muted"}>
                                    {" "}
                                    ...{" "}
                                </small>
                            )}
                            {item.name.length <= 20 && item.name}
                            {item.name.length < 20 && (
                                <small className={"text-white"}>
                                    {Array(20)
                                        .fill(".")
                                        .join("")}
                                </small>
                            )}
                        </Button>
                    </Col>
                    <Col xs="12" className={styles.colName}>
                        <Button outline className={styles.price}>
                            {brlFormat(item.price)}
                        </Button>
                    </Col>
                    {/*<Col xs="12" className={styles.colFreight}>
                                    <Button outline className={styles.freight}>
                                        Frete R$ {item.price}
                                    </Button>
                                </Col>*/}
                    <Col xs="12" className={styles.colName}>
                        <div>
                            <Button
                                className={styles.countButtonLeft}
                                outline
                                color="primary"
                                onClick={less(item.id)}
                                id={`less-${item.id}`}
                                disabled={item.amount === 1}
                            >
                                -
                            </Button>
                            <Button
                                className={styles.count}
                                outline
                                color="primary"
                            >
                                {item.amount}
                            </Button>
                            <Button
                                className={styles.countButtonRight}
                                outline
                                color="primary"
                                onClick={more(item.id)}
                                id={`more-${item.id}`}
                                disabled={
                                    item.amount >= item.amount_stock
                                }
                            >
                                +
                            </Button>
                        </div>
                    </Col>
                </Row>
                <hr/>
            </Col>
        </Row>
    )
}

const Items = ({state, setState, token}) => {
    const [context, dispatch] = useContext(Context)
    const router = useRouter()
    const [items, setItems] = useState(state)
    const spacesNames =
        "\xa0\xa0\xa0\xa0\xa0\xa0\xa0\xa0\xa0\xa0\xa0\xa0\xa0\xa0\xa0\xa0\xa0\xa0\xa0\xa0"

    useEffect(() => {
        setItems(state)
    }, [state])

    function setStates(cart) {
        setState(cart)
        setItems(cart)
    }

    function removeOnState(id) {
        const cart = items.filter((item) => item.id !== id)
        setStates(cart)
    }

    function remove(id) {
        return async (event) => {
            event.preventDefault()
            if (!token) {
                const storage = _storage()
                if (!!storage) {
                    const _cart = JSON.parse(`${storage.getItem("cart")}`)
                    storage.setItem(
                        "cart",
                        JSON.stringify(_cart.filter((item) => item.id !== id))
                    )
                    removeOnState(id)
                    tryCatch(() => {
                        const {length} = JSON.parse(`${storage.getItem("cart")}`)
                        dispatch(dispatchFunctions.cart_length(length))
                    })
                } else {
                    console.log('STORAGE IS NOT DEFINED', {storage})
                }

            } else {
                const data = await fromServer(`/carts/${id}`, token, "DELETE")
                if (!hasKey(data, "error")) {
                    removeOnState(id)
                    tryCatch(async () => {
                        const {length} = await fromServer('/carts', context.token)
                        console.log({length})
                        dispatch(dispatchFunctions.cart_length(Number(length)))
                    })
                } else {
                    console.log("error", {data})
                }
            }
        }
    }

    function less(id) {
        return async (event) => {
            event.preventDefault()
            const btn = elm(`#less-${id}`)
            btn.disabled = true
            const [current] = items.filter((i) => i.id === id)
            const amount = Number(Number(current.amount) - 1)
            const cart = items.map((i) => {
                if (i.id === id) {
                    return {
                        ...i,
                        amount,
                    }
                }
                return i
            })
            if (!token) {
                localStorage.setItem("cart", JSON.stringify(cart))
            } else {
                const data = await fromServer(
                    `/carts/${id}`,
                    token,
                    "PUT",
                    {amount},
                    false,
                    true
                )
            }
            setStates(cart)
            btn.disabled = false
        }
    }

    function more(id) {
        return async (event) => {
            event.preventDefault()
            let amount = 0
            const btn = elm(`#more-${id}`)
            btn.disabled = true
            const cart = items.map((item) => {
                if (item.id === id && item.amount + 1 <= item.amount_stock) {
                    amount = item.amount + 1
                    return {
                        ...item,
                        amount: item.amount + 1,
                    }
                }
                return item
            })
            if (amount) {
                if (token === "") {
                    localStorage.setItem("cart", JSON.stringify(cart))
                } else {
                    console.log(
                        "without token",
                        await fromServer(`/carts/${id}`, token, "PUT", {
                            amount,
                        })
                    )
                }
                btn.disabled = false
                setStates(cart)
            }
            btn.disabled = false
        }
    }

    return (
        <>
            <hr/>
            <div className="showAboveTablet">
                {items?.map((item, index) => (
                    <ShowAboveTablet
                        key={`${index}-showAboveTablet`}
                        token={token}
                        item={item}
                        less={less}
                        more={more}
                        remove={remove}
                    />
                ))}
                <Row className="mt-4">
                    <Col lg="5">
                        <Button
                            size="lg"
                            className={styles.buttonContinue}
                            id="continueBuying"
                            onClick={async (e) => {
                                e.preventDefault()
                                disable_btn_and_put_innerHTML("#continueBuying")
                                await router.push(`/`)
                                window.scrollTo({top: 0, behavior: "smooth"})
                            }}
                        >
                            Continue comprando
                        </Button>
                    </Col>
                </Row>
            </div>
            <div className="showTabletAndLess">
                {items?.map((item, index) => (
                    <ShowTabletAndLess
                        key={`${index}-showTabletAndLess`}
                        token={token}
                        item={item}
                        less={less}
                        more={more}
                        remove={remove}
                    />
                ))}
            </div>
        </>
    )
}
export default Items
