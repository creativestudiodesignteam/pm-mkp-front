import React, {useContext} from "react"
import {Button, Col, Jumbotron, Row} from "reactstrap"
import styles from "./Total.module.css"
import {useRouter} from "next/router"
import Swal from "sweetalert2"
import {
    brlFormat,
    disable_btn_and_put_innerHTML,
    elm,
    enable_btn_and_put_innerHTML,
    fromServer,
    hasKey,
} from "../../../../assets/helpers"
import {Context, dispatchFunctions} from "../../../../store"
import {_storage, getToken, LogIn, userByToken} from "../../../../assets/auth"

const {cart_init} = dispatchFunctions
const Total = ({state, subtotal, shippingValue, token}) => {
    const router = useRouter()
    const [context, dispatch] = useContext(Context)

    async function loginPopup(event) {
        event.preventDefault()
        disable_btn_and_put_innerHTML("#cart_buy")
        if (context.token) {
            disable_btn_and_put_innerHTML("#cart_buy")
            await router.push(`/payment?token=${context.token}`)
        } else {
            const {value: user} = await Swal.fire({
                width: "max(240px, 20%)",
                heightAuto: true,
                padding: "3em",
                customClass: {
                    confirmButton: "btn btn-block actively",
                },
                keydownListenerCapture: true,
                allowEnterKey: true,
                title: "Faça login para continuar",
                html: `
                <input type="email" id="email-swal2-input" class="swal2-input" placeholder="E-mail">
                <input type="password" id="password-swal2-input" class="swal2-input" placeholder="Senha">
                <label for="type-swal2-input">Pessoa juridica?</label>
                <input type="checkbox" id="type-swal2-input" name="type-swal2-input" class="swal2-input" />
            `,
                focusConfirm: false,
                preConfirm: async (e) => {
                    const [
                        {value: email},
                        {value: password},
                        {checked: type},
                    ] = [
                        elm("#email-swal2-input"),
                        elm("#password-swal2-input"),
                        elm("#type-swal2-input"),
                    ]
                    const data = await fromServer(
                        "/auth",
                        token,
                        "POST",
                        {email, password, type},
                        false,
                        true
                    )
                    /*console.log("preConfirm", {
                        data,
                        email,
                        password,
                        type,
                        e,
                    })*/
                    if (hasKey(data, "error")) {
                        Swal.showValidationMessage(
                            `Request failed: ${data.error.message}`
                        )
                    }

                    return data
                },
            })
            //console.log("is logged?", {user})
            if (user) {
                try {
                    console.log(
                        await Promise.all(
                            state.map(async ({amount, id_grid: fk_grids}) => {
                                return await fromServer("/carts", user.token, "POST", {
                                    fk_grids,
                                    amount,
                                })
                            })
                        )
                    )
                } catch (e) {
                    console.error(e)
                }
                const cart = await fromServer("/carts", user.token, "get")
                const storage = _storage();
                !!storage && storage.setItem('cart', JSON.stringify([]))
                const {length: cart_length} = cart
                LogIn(
                    user.token,
                    userByToken(user.token),
                    dispatch,
                    !hasKey(cart, "error") ? {cart, cart_length} : {}
                )
                await router.push(`/cart?token=${context.token||getToken()}`)
            }
        }
        enable_btn_and_put_innerHTML("#cart_buy", "COMPRAR AGORA")
        return true
    }

    return (
        <>
            <hr style={{borderTopColor: "transparent"}}/>

            <Row>
                <Col xs="12">
                    <Jumbotron className={styles.jumbo}>
                        {/*<Row>
                            <Col md="12" className={styles.alignerAround}>
                                <Button size="lg" className={styles.count}>
                                    Subtotal
                                </Button>
                                <Button outline className={styles.price}>
                                    {!token && (
                                        <>
                                            R${" "}
                                            {toFixed(
                                                Number(
                                                    Number(subtotal) +
                                                    Number(shippingValue)
                                                )
                                            )}
                                        </>
                                    )}
                                    {token && (
                                        <>
                                            R${" "}
                                            {Number(
                                                Number(subtotal) +
                                                Number(shippingValue)
                                            )}
                                        </>
                                    )}
                                </Button>
                            </Col>
                        </Row>*/}
                        {
                            /*

                             <Divider />
                            <Row>
                                <Col md="12" className={styles.alignerAround}>
                                    <Button size="lg" className={styles.count}>
                                        Frete
                                    </Button>
                                    <Button outline className={styles.freightMobile}>
                                        {!token && (
                                            <>

                                                R${" "}
                                                {toFixed(
                                                    Number(
                                                        Number(subtotal) +
                                                        Number(shippingValue)
                                                    )
                                                )}
                                            </>
                                        )}
                                        {token && (
                                            <>
                                                R${" "}
                                                {Number(
                                                    Number(subtotal) +
                                                    Number(shippingValue)
                                                )}
                                            </>
                                        )}
                                    </Button>
                                </Col>
                            </Row>
                        */
                        }
                        {/* <Divider /> */}
                        <Row>
                            <Col md="12" className={styles.alignerAround}>
                                <Button size="lg" className={styles.count}>
                                    <strong>Total :</strong>
                                </Button>
                                <Button outline className={styles.price}>
                                    {brlFormat(
                                        Number(
                                            Number(subtotal) +
                                            Number(shippingValue))
                                    )
                                    }
                                </Button>
                            </Col>
                        </Row>
                    </Jumbotron>
                </Col>
                <Col xs="12">
                    <Button
                        className={styles.buttonBuy}
                        onClick={loginPopup}
                        id={"cart_buy"}
                        disabled={Boolean(context.token && !state.length)}
                    >
                        COMPRAR AGORA
                    </Button>
                </Col>
                <Col xs="12" className="allignerCenter showTabletAndLess">
                    <Button size="lg" className={styles.count}>
                        ou
                    </Button>
                </Col>
                <Col xs="12" className="showTabletAndLess">
                    <Button
                        size="lg"
                        className={styles.buttonContinue}
                        id="continueBuying"
                        onClick={async (e) => {
                            e.preventDefault()
                            disable_btn_and_put_innerHTML("#continueBuying")
                            await router.push(`/`)
                            window.scrollTo({top: 0, behavior: "smooth"})
                        }}
                    >
                        Continue comprando
                    </Button>
                </Col>
            </Row>
        </>
    )
}

export default Total
