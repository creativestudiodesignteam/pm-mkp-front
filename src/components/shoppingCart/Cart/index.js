import React, { useEffect } from "react"
import Items from "./Items"
import { Col, Row } from "reactstrap"
import Total from "./Total"

const Cart = ({ state, setState, token }) => {
    const shippingValue = 0
    const subtotal =
        state?.reduce(
            (acc, current, idx) =>
                Number(acc + Number(current.price) * Number(current.amount)),
            0
        ) || 0

    return (
        <>
            <Row className="mb-4" style={{ paddingTop: "4rem" }}>
                <Col xs="12" lg="8">
                    <h1>Carrinho de Compras</h1>
                </Col>
                <Col xs="12" lg="4" className="showAboveTablet">
                    <h1>Total do carrinho</h1>
                </Col>
                <Col xs="12" lg="8">
                    <Items state={state} setState={setState} token={token} />
                </Col>
                <Col xs="12" lg="4" className="showTabletAndLess">
                    <h1>Total do carrinho</h1>
                </Col>
                <Col xs="12" lg="4">
                    <Total
                        state={state}
                        token={token}
                        shippingValue={shippingValue}
                        subtotal={subtotal}
                    />
                </Col>
            </Row>
        </>
    )
}

export default Cart
