import React from 'react'
import ProductCartReview from "./ProductCartReview";
import Helper from './../assets/helpers'

const {verifyAllAttrs} = Helper

const Index = ({data = [{}]}) => {
    const _data = (data && verifyAllAttrs(data[0], ['node_text', 'img', 'alt', 'href', 'price', 'spread']) ? data : [
        {
            node_text: 'MH02-Black09',
            img: 'product_image_6-min.png',
            alt: 'Product MH02-Black09',
            href: '/',
            price: '350.00',
            spread: '(x1)'
        }, {
            node_text: 'Voyage Yoga Bag',
            img: 'product_image_7-min.png',
            alt: 'Product MH02-Black09',
            href: '/',
            price: '350.00',
            spread: '(x1)'
        }
    ])
    const total = (_data.reduce((acc, {price}) => Number(Number(acc) + Number(price)), 0)).toFixed(2)
    const product = _data.map(
        ({...props}, i) => (
            <ProductCartReview key={i} {...props}/>
        )
    )
    return (
        <div className="cart-detail-header border">
            <div className="relative">
                {product}
            </div>
            <div className="relative border no-border-l no-border-r total-cart-header">
                <p className="bold clear-margin">Subtotal:</p>
                <p className=" clear-margin bold">${total}</p>
            </div>
            <div className="relative btn-cart-header">
                <a href="#" className="uppercase bold animate-default">
                    Ver carrinho
                </a>
                <a href="#" className="uppercase bold button-hover-red animate-default">
                    Comprar
                </a>
            </div>
        </div>
    )
}
export default Index;