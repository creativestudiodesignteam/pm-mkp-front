import React, { useEffect } from "react"
import { makeStyles } from "@material-ui/core/styles"
import Pagination from "@material-ui/lab/Pagination"

const useStyles = makeStyles((theme) => ({
    root: {
        "& > *": {
            marginTop: theme.spacing(2),
        },
    },
}))
export default function PaginationRanges({
    count = 0,
    defaultPage = 1,
    siblingCount = 0,
    Listener = (e) => console.log(e),
    ...props
}) {
    useEffect(() => {
        //console.log({ count, defaultPage, siblingCount, Listener, ...props })
        /*if (elm('.pagination_custom_ui ul > li>div') && elm('.pagination_custom_ui ul > li>div').parentElement) {
            elm('.pagination_custom_ui ul > li>div').parentElement.style.border = 'none'
        }*/
    }, [])
    const classes = useStyles()
    const handleChange = (event, value) => {
        Listener(parseInt(`${value}`))
    }
    return (
        <div className={classes.root}>
            <Pagination
                {...props}
                className="pagination_custom_ui"
                count={count}
                defaultPage={defaultPage}
                siblingCount={siblingCount}
                onChange={handleChange}
            />
        </div>
    )
}
