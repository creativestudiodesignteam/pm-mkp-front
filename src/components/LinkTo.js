import React from "react"
import Link from "next/link"

export default function Component({children, _href, as = "", query = {}}) {
    const has_as = as.length > 0
    const has_query = Boolean(Array(Object.values(query)).length > 0)
    const nextLinkProps = {
        as: {
            pathname: as,
            query,
        },
        href: {
            pathname: _href,
            query,
        },
    }
    if (has_as && !has_query) {
        return <Link href={{pathname: _href, as: as}}>
            <a className="linkToA"> {children}</a>
        </Link>
    } else if (has_query && !has_as) {
        return <Link href={{pathname: _href, query}}>
            <a className="linkToA"> {children}</a>
        </Link>
    }
    if (has_query && has_as) {
        return (
            <Link {...nextLinkProps}>
                <a className="linkToA"> {children}</a>
            </Link>
        )
    }
    return (
        <Link href={{pathname: _href}}>
            <a className="linkToA"> {children}</a>
        </Link>
    )
}
