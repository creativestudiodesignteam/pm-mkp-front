import dynamic from "next/dynamic"
import React from "react"
import Link from "./LinkTo"
import LinkTo from "./LinkTo"
import Li from "./Li"
import Img from "./Img"
import ContainerDefault from "./ContainerDefault"
import SemiContainer from "./SemiContainer"
import SlideBoxHome from "./SlideBoxHome"
import BoxBanner from "./BoxBanner"
import CategoryImage from "./CategoryImage"
import ContentProductBox from "./ContentProductBox"
import AuthenticationContainer from "./AuthenticationContainer"
import LoginWrap from "./authentication/LoginWrap"
import BreadCrumb from "./BreadCrumb"
import FullFooter from "./FullFooter"
import FullHeader from "./FullHeader"
import InputDefault from "./InputDefault"
import Main from "./Main"
import TitleCategory from "./TitleCategory"
import fullSignPj, { titleResolver } from "./authentication/FullSignPj"
import PureInput from "./PuerInput"
import Pagination from "./Pagination"
import Collapse from "./pure/Collapse"
import SelectDefault from "./SelectDefault"
import ClipLoader from "react-spinners/ClipLoader"

const SelectCustom = dynamic(() => import("./SelectCustom"), {
    ssr: false,
    loading: function Loading() {
        return (
            <div className="spinnerLoader">
                <ClipLoader size={150} color={"var(--red-1)"} loading={true} />
            </div>
        )
    },
})

export {
    SelectCustom,
    SelectDefault,
    Collapse,
    Pagination,
    PureInput,
    LinkTo,
    Link,
    Li,
    Img,
    ContainerDefault,
    SemiContainer,
    SlideBoxHome,
    BoxBanner,
    CategoryImage,
    ContentProductBox,
    AuthenticationContainer,
    LoginWrap,
    BreadCrumb,
    FullFooter,
    FullHeader,
    InputDefault,
    Main,
    TitleCategory,
    fullSignPj,
    titleResolver,
}
