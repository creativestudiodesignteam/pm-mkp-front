import React, { useState, useEffect, useRef } from "react"

function Article() {
    const editorRef = useRef()
    const [editorLoader, setEditorLoaded] = useState(false)
    const { CKEditor, ClassicEditor } = editorRef.current || {}
    const [data, setData] = useState("")

    useEffect(() => {
        editorRef.current = {
            CKEditor: require("@ckeditor/ckeditor5-react"),
            ClassicEditor: require("@ckeditor/ckeditor5-build-classic"),
        }
        setEditorLoaded(true)
    }, [])

    function onEditorChange(event, editor) {
        setData(editor.getData())
    }
    return editorLoader ? (
        <div className="w-100">
            <p style={{ display: "block" }}>{data}</p>
            <CKEditor
                className="w-100"
                editor={ClassicEditor}
                data={data}
                onChange={onEditorChange}
            />
        </div>
    ) : (
        <div>Chargement...</div>
    )
}

export default Article
