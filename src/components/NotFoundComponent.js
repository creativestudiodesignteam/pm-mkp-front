import React from "react"
import Main from "./Main"
import { Row } from "reactstrap"
export default function NotFoundCOmponent() {
    return (
        <Main
            _BreadCrumb={[
                {
                    node_text: "404",
                    href: "/",
                },
            ]}
        >
            <Row className="relative">
                <div className=" relative content-404">
                    <div className="title-404">
                        <p className="clear-margin">4</p>
                        <img src="img/icon_404-min.png" alt="Image 404" />
                        <p className="clear-margin">4</p>
                    </div>
                    <p>Ops! Essa página não existe</p>
                </div>
            </Row>
        </Main>
    )
}
