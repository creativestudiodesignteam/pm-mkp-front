import React from "react";
import { verifyAllAttrs } from "../assets/helpers";
import { Img, Link } from "./index";

const BoxBanner = ({
  data = [],
  classNames = ["", "", ""],
  over = false,
  overClassNames = ["", "", ""],
  children,
  ...props
}) => {
  const [cl_first, cl_second, cl_third] = over ? overClassNames : classNames;
  const second_class = over
    ? cl_second
    : `effect-layla banner-v3-home center-vertical-image zoom-image-hover relative ${cl_second}`;
  const _data = (data &&
  data[0] &&
  verifyAllAttrs(data[0], ["href", "img", "alt", "_class"])
    ? data
    : [
        {
          href: "/",
          img: "banner_slide_v3.png",
          alt: "Image Slide Banner",
          _class: "bottom-margin-15-default",
        },
        {
          href: "/",
          img: "banner_slide_v3_1-min.png",
          alt: "Image Slide Banner",
          _class: "",
        },
      ]
  ).map(({ alt, img, href, _class }, i) => (
    <div key={i} className={`${second_class} ${_class}`}>
      <Img image_name_with_extension={img} alt={alt} className={cl_third} />
      <Link _href={href} />
    </div>
  ));
  return (
    <div
      className={
        over
          ? cl_first
          : `clearfix box-banner-small-v3 top-margin-15-default left-margin-15-default box-banner-small ${cl_first}`
      }
    >
      {_data}
    </div>
  );
};
export default BoxBanner;
