import React from "react"
import styled from "styled-components"

export const BoardedWrapBox_styled = styled.div`
    &{
        
        height: ${(props) =>
            (props.hasOwnProperty("height") && props.height) || "auto"};
        width: ${(props) =>
            (props.hasOwnProperty("width") && props.width) || "auto"};
        border: solid 1px #dedede;
        background-color: #ffffff;
        ${(props) =>
            props.hasOwnProperty("margin") && props.margin
                ? `margin: ${props.margin};`
                : ""}
        display: ${(props) =>
            props.hasOwnProperty("flex") && props.flex ? "flex" : "block"};
        ${(props) =>
            props.hasOwnProperty("flex") && props.flex
                ? `flex-direction: ${
                      props.hasOwnProperty("row") && props.row
                          ? "row"
                          : "column"
                  };`
                : ""};
        &>div{
            display:flex;
            flex-direction:row;
            align-items: end;
            
            & > img{
                height:calc(10rem*0.29122);
                margin-right:0.5rem;
                object-fit:contain;
            }
        }
        & > h4 {
            margin:0;
        }
        & > h4 + div {
           
         & >div >p{
            margin-bottom: 0.5rem;
         }
        }    
        & > h2{
            font-size: 2.5rem;
            font-family: Roboto;
            font-weight: bold;
            font-stretch: normal;
            font-style: normal;
            line-height: 1.31;
            letter-spacing: normal;
            text-align: center;
            color: #2b2b2b;
        }
       
        & > h5{
            font-family: Roboto;
            font-size: 2.25rem;
            font-weight: bold;
            font-stretch: normal;
            font-style: normal;
            line-height: 1.32;
            letter-spacing: normal;
            text-align: left;
            color: #2b2b2b;
            margin:0!important;
            & > span{
                color: #e3171b !important;   
            }
        }
        & > h2 + small {
            margin-top: -.5rem;
            font-weight: 500;
        }
        & > h3 {
            font-family: Roboto;
            font-size: 1.75rem;
            font-weight: bold;
            font-stretch: normal;
            font-style: normal;
            line-height: 1.3;
            letter-spacing: normal;
            text-align: left;
            color: #2b2b2b;
            margin: 0;
            &>span{
                color: #e3171b;
            }
        }
        & > h4 {
            font-size: 1rem;
            font-weight: 700;
            font-stretch: normal;
            font-style: normal;
            line-height: 1.38;
            letter-spacing: normal;
            text-align: left;
            color: #2b2b2b;
            
        }
        
        & > small{
            font-family: Roboto;
            font-size:${(props) =>
                (props.hasOwnProperty("smallSize") && props.smallSize) ||
                `1.75rem`};          
            font-weight:${(props) =>
                (props.hasOwnProperty("smallWeight") && props.smallWeight) ||
                ` bold`};          
            font-stretch: normal;          
            font-style: normal;          
            line-height: 1.3;          
            letter-spacing: normal;          
            text-align: center;          
            color: #757575;
            font-weight: normal;
        }
        padding: ${(props) =>
            (props.hasOwnProperty("padding") && props.padding) || "10px"};
        align-items: ${(props) =>
            (props.hasOwnProperty("align") && props.align) || "center"};
        justify-content: ${(props) =>
            (props.hasOwnProperty("justify") && props.justify) || "center"};
        &>img:first-child{
            height: 13rem;
            object-fit: contain;
        }
    }
`
