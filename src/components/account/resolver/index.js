import React, {useContext, useMemo, useState} from "react"
import propTypes from "prop-types"
import RequestDashboard from "../request/"
import {valid_checkers, valid_path} from "../store"
import {useRouter} from "next/router"
import {logOut} from "../../../assets/auth"
import {Context} from "../../../store"
import Produtcts from "../product/"
import MyAccount from "../myaccount/index"
import SavedCreditCards from "../savedcreditcards"
import Addresses from "../addresses"
import AlterAddress from "../alteraddress"
import NewAddress from "../newaddress"
import Sales from "../sales/"
import NewCreditCard from "../newcreditcard"
import Options from "../options"
import NewOption from "../newoption"
import AlterOption from "../alteroption"
import PfAccount from "../myaccount/PfAccount"

function Page({ page, products, user, setProducts, token }) {
    const [, dispatch] = useContext(Context)
    const [loaderState, setLoaderState] = useState(false)

    /*useEffect(() => {
        console.log("Page", { user })
    })*/
    const Router = useRouter()

    function getOut() {
        logOut(dispatch, Router)
    }

    function Component() {
        switch (page) {
            case valid_checkers[valid_path.request]:
                return <RequestDashboard  />
            case valid_checkers[valid_path.sales]:
                return (
                    (user.type && <Sales token={token} />) ||
                    "em desenvolvimento"
                )
            case valid_checkers[valid_path.address]:
                return <Addresses />
            case valid_checkers[valid_path.alteraddress]:
                return <AlterAddress />
            case valid_checkers[valid_path.newcreditcard]:
                return <NewCreditCard />
            case valid_checkers[valid_path.newaddress]:
                return <NewAddress />
            case valid_checkers[valid_path.card]:
                return <SavedCreditCards />
            case valid_checkers[valid_path.user]:
                return <p>{valid_path.user}</p>
            case valid_checkers[valid_path.myaccount]:
                return (user.type && <MyAccount />) || <PfAccount />
            case valid_checkers[valid_path.options]:
                return (user.type && <Options />) || "em desenvolvimento"
            case valid_checkers[valid_path.newoption]:
                return (user.type && <NewOption />) || "em desenvolvimento"
            case valid_checkers[valid_path.alteroption]:
                return (user.type && <AlterOption />) || "em desenvolvimento"
            case valid_checkers[valid_path.product]:
                return (
                    (user.type && (
                        <Produtcts
                            token={token}
                            products={products}
                            setProducts={setProducts}
                        />
                    )) || <p>{valid_path.product}</p>
                )
            case valid_checkers[valid_path.out]: {
                getOut()
                return ""
            }
            default:
                console.log("default")
                break
        }
    }

    return useMemo(() => Component(), [
        page,
        products,
        user,
        setProducts,
        token,
    ])
}

Page.propTypes = {
    page: propTypes.string.isRequired,
    user: propTypes.object.isRequired,
    products: propTypes.array.isRequired,
    setProducts: propTypes.func.isRequired,
}
export default Page
