import React, { useContext, useEffect, useState } from "react"
import { Img, LinkTo } from "../../"
import { Alert, Button, Col, ListGroup, ListGroupItem, Row } from "reactstrap"
import {awaitable, elm, fromServer, hasKey} from "../../../assets/helpers"
import { Context } from "../../../store"
import { MySwal, Toast } from "../../../assets/sweetalert"
import styles from "../grid/index.module.css"
import ClipLoader from "react-spinners/ClipLoader"
import Swal from "sweetalert2";

const Options = () => {
    const [{ token }, dispatch] = useContext(Context)
    const [options, setOptions] = useState([])
    const [alert, setAlert] = useState(false)
    const [idsToRemove, setIdsToRemove] = useState([])
    const [loaderState, setLoaderState] = useState(false)

    useEffect(() => {
        setLoaderState(true)
        awaitable(async () => {
            const dataOptions = await fromServer(`/options`, token, "get")
            /*console.log({ dataOptions })*/
            if (dataOptions.length > 0) {
                setOptions(dataOptions)
                setLoaderState(false)
            } else {
                setAlert(true)
                setLoaderState(false)

                console.log("fail to load options", { dataOptions })
            }
        })
    }, [])

    function selectOption(id) {
        return async (e) => {
            e.preventDefault()
            //console.log("selectOPtion")
            const resolve = await fromServer(`/options/${id}`, token, "delete")
            if (resolve?.error) {
                console.log("error", { resolve })
                await MySwal.fire(
                    "Não foi possivel remover esta característica",
                    "",
                    "error"
                )
            } else {
                const newoptions = options.filter((it) => it.options.id !== id)
                setOptions(newoptions)
                //console.log("success", { resolve })
                await MySwal.fire(
                    "Características removida com sucesso",
                    "",
                    "success"
                )
            }
        }
    }

    const toggle = () => setAlert(!alert)
    /*useEffect(() => {
        console.log({ idsToRemove })
    }, [idsToRemove])*/

    async function remove(e) {
        e.preventDefault()
        setLoaderState(true)
        const response = await Promise.all(
            idsToRemove.map(
                async (id) =>
                    await fromServer(`/options/${id}`, token, "delete")
            )
        )
        //console.log({ response })
        response?.map((res) => {
            if (res) {
                if (hasKey(res, "error")) {
                    awaitable(async () => {
                        await MySwal.fire(
                            res?.error?.message || "Atributo removido",
                            "",
                            "error"
                        )
                        setLoaderState(false)
                    })
                } else {
                    awaitable(async () => {
                        await MySwal.fire(
                            res?.msg || "Falha ao remover atributo",
                            "",
                            "success"
                        )
                    })
                    setLoaderState(false)
                }
            }
            setLoaderState(false)
            return res
        })
        setIdsToRemove([])
        const dataOptions = await fromServer(`/options`, token, "get")
        //console.log({ dataOptions })
        if (!!!dataOptions?.error) {
            setOptions(dataOptions)
            setLoaderState(false)
        } else {
            setAlert(true)
            setLoaderState(false)

            console.log("fail to load options", { dataOptions })
        }
    }
    const newOption = (e) => {
        setLoaderState(true)
    }
    return (
        <>
            {loaderState && (
                <div className="spinnerLoader">
                    <ClipLoader
                        size={150}
                        color={"var(--red-1)"}
                        loading={loaderState}
                    />
                </div>
            )}
            <Row>
                <Col xs="12" className="mb-4">
                    <div className="d-flex flex-row align-items-center justify-content-lg-start w-100">
                        <h1 className={"mr-auto"}>
                            Características e atributos
                        </h1>
                        <LinkTo
                            _href="/account/[feature]"
                            as={"/account/newoption"}
                        >
                            <div
                                className={"create-icon mr-3"}
                                onClick={newOption}
                            >
                                <Img
                                    image_name_with_extension="add_icon.png"
                                    alt="insert a new product"
                                />
                            </div>
                        </LinkTo>
                        {Boolean(idsToRemove.length) && (
                            <div className={"delete-icon"} onClick={remove}>
                                <Img
                                    image_name_with_extension="add_icon.png"
                                    alt="insert a new product"
                                />
                            </div>
                        )}
                    </div>
                </Col>
            </Row>
            <Row>
                {alert && (
                    <Col xs="12">
                        <Alert
                            color="primary"
                            isOpen={alert}
                            toggle={toggle}
                            style={{
                                opacity: 1,
                                color: "white",
                                backgroundColor: "var(--red-1)",
                                padding: "2rem",
                                fontSize: "16px",
                                textAlign: "center",
                            }}
                        >
                            Você ainda não possui características de produtos
                            cadastradas!
                        </Alert>
                    </Col>
                )}
                <Col xs="12">
                    <ListGroup className="shadow-none w-100 mx-auto">
                        {options?.map((option) => (
                            <Row
                                key={option.options.id}
                                className="px-4 justify-content-center"
                            >
                                <div className="m-0 p-0 col-auto">
                                    <ListGroupItem
                                        className={"h-100 w-100 m-0"}
                                    >
                                        <span
                                            className={`${
                                                styles.empty
                                            } relative ${
                                                idsToRemove.includes(
                                                    option.options.id
                                                )
                                                    ? "bg-dark"
                                                    : ""
                                            }`}
                                            onClick={(e) => {
                                                e.preventDefault()
                                                if (
                                                    !idsToRemove.includes(
                                                        option.options.id
                                                    )
                                                ) {
                                                    setIdsToRemove([
                                                        ...idsToRemove,
                                                        option.options.id,
                                                    ])
                                                } else {
                                                    setIdsToRemove(
                                                        idsToRemove.filter(
                                                            (id) =>
                                                                id !==
                                                                option.options
                                                                    .id
                                                        )
                                                    )
                                                }
                                            }}
                                        />
                                    </ListGroupItem>
                                </div>
                                <Col xs="8" className="m-0 p-0">
                                    <ListGroupItem
                                        className={"h-100 w-100 m-0"}
                                    >
                                        {option.options.name}
                                    </ListGroupItem>
                                </Col>
                                <Col xs="2" className="m-0 p-0">
                                    <LinkTo
                                        _href="/account/[feature]"
                                        as={"/account/alteroption"}
                                        query={{
                                            id: option.options.id,
                                            name: option.options.name,
                                            typeSaved: option.options.type,
                                        }}
                                    >
                                        <Button
                                            className="h-100"
                                            outline
                                            style={{
                                                width: "100%",
                                                borderRadius: 0,
                                                border:
                                                    "1px solid rgba(0,0,0,.125)",
                                            }}
                                            onClick={newOption}
                                        >
                                            Editar
                                        </Button>
                                    </LinkTo>
                                </Col>

                            </Row>
                        ))}
                    </ListGroup>
                </Col>
            </Row>
        </>
    )
}
export default Options
