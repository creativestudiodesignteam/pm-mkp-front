import React, { useState } from "react"
import { Button, Col, Form, InputGroup, InputGroupAddon, Row } from "reactstrap"
import { Img, LinkTo } from "../../"
import styles from "./index.module.css"
import propTypes from "prop-types"
import {
    elm,
    fromServer,
    OBJ,
    productsParseFromServer,
} from "../../../assets/helpers"
import { MySwal, Toast } from "../../../assets/sweetalert"
import { useRouter } from "next/router"
import InputDefault from "../../InputDefault"
import { FaSearch } from "react-icons/fa"
import ClipLoader from "react-spinners/ClipLoader"

const ProductsGrid = ({ token, products, setProducts, allProducts }) => {
    const router = useRouter()
    const [loaderState, setLoaderState] = useState(false)

    function updateSelected(id) {
        return allProducts.map((product) =>
            product.id === id
                ? { ...product, selected: !product.selected }
                : product
        )
    }

    function _idsToRemove() {
        return allProducts.filter((it) => it.selected === true).map((i) => i.id)
    }

    async function remove() {
        setLoaderState(true)
        const btn = elm(".delete-icon")
        btn.style.cursor = "not-allowed"
        const idsToRemove = _idsToRemove()
        if (idsToRemove.length) {
            const response = await Promise.all(
                idsToRemove.map(
                    async (id) =>
                        await fromServer(`/products/${id}`, token, "DELETE")
                )
            )
            const hasError =
                response.filter((res) => OBJ.hasKey(res, "error")).length > 0
            if (!hasError) {
                const payload = allProducts.filter(
                    (elm) => !idsToRemove.includes(elm.id)
                )
                //console.log({payload, idsToRemove})
                setProducts(payload)
                setLoaderState(false)
            } else {
                console.log("fail to delete product", {
                    hasError,
                    response,
                    idsToRemove,
                })
                await MySwal.fire(
                    "Não foi possivel remover este produto",
                    "",
                    "error"
                )
                setLoaderState(false)
            }
        } else {
            //console.log({idsToRemove, allProducts})
            setLoaderState(false)
        }
        btn.style.cursor = "pointer"
        setLoaderState(false)
    }

    function selectProductCard(id) {
        return (e) => {
            e.preventDefault()
            setProducts(updateSelected(id))
        }
    }

    const edit = () => {
        setLoaderState(true)
    }
    return (
        <>
            {loaderState && (
                <div className="spinnerLoader">
                    <ClipLoader
                        size={150}
                        color={"var(--red-1)"}
                        loading={loaderState}
                    />
                </div>
            )}
            <Row className={"mx-0 mt-0 pt-0 mb-6"}>
                <h1 className="page_title h1 mt-0 mr-auto">Meus Produtos</h1>
                <div
                    onClick={async (event) => {
                        event.preventDefault()
                        setLoaderState(true)
                        await router.push("/product")
                    }}
                    className={"create-icon mr-3"}
                >
                    <Img
                        image_name_with_extension="add_icon.png"
                        alt="insert a new product"
                    />
                </div>
                {Boolean(_idsToRemove().length) && (
                    <div className={"delete-icon"} onClick={remove}>
                        <Img
                            image_name_with_extension="add_icon.png"
                            alt="insert a new product"
                        />
                    </div>
                )}
            </Row>
            <Form
                className={"w-100"}
                onSubmit={async (e) => {
                    e.preventDefault()
                    setLoaderState(true)

                    if (elm("[name=search]").value) {
                        const data = await fromServer(
                            `/search/provider?content=${
                                elm("[name=search]").value
                            }`,
                            token
                        )
                        await Toast.fire(
                            `${data.length}: resultados encontrados`,
                            "",
                            "success"
                        )
                        if (!data.length) {
                            console.log(
                                "nao encontrado",
                                { data },
                                elm("[name=search]").value
                            )
                            const _data = await fromServer("/products", token)
                            if (!OBJ.hasKey(_data, "error")) {
                                setProducts(_data.map(productsParseFromServer))
                                setLoaderState(false)
                            } else {
                                console.log("fail to load produts", { _data })
                                setLoaderState(false)
                            }
                        } else {
                            setProducts(data.map(productsParseFromServer))
                            setLoaderState(false)
                        }
                        setLoaderState(false)
                        //console.log({data})
                    } else {
                        await MySwal.fire(
                            `Informe o termo para busca`,
                            "",
                            "question"
                        )
                        setLoaderState(false)
                    }
                    setLoaderState(false)

                    return false
                }}
            >
                <Row>
                    <Col lg="7" className={`${styles.paddingSearch}`}>
                        <InputDefault
                            type="text"
                            placeholder={"Buscar"}
                            id_={"search"}
                            name={"search"}
                        />
                    </Col>
                    <Col lg="1" className={`${styles.paddingButtonSearch}`}>
                        <InputGroupAddon addonType="append">
                            <button
                                type={"submit"}
                                style={{ height: "4rem" }}
                                className="btn btn-lg btn-block bg_red text-white"
                            >
                                <FaSearch />
                            </button>
                        </InputGroupAddon>
                    </Col>
                    <Col lg="4">
                        <Button
                            outline
                            onClick={async (event) => {
                                event.preventDefault()
                                setLoaderState(true)
                                const _data = await fromServer(
                                    "/products",
                                    token
                                )
                                setProducts(_data.map(productsParseFromServer))
                                setLoaderState(false)
                            }}
                            style={{ height: "4rem" }}
                            className={`redButtonOutline100 ${
                                styles.showAllButton
                            }`}
                        >
                            Mostrar Todos Produtos
                        </Button>
                    </Col>
                </Row>
            </Form>

            <Row className={"m-0 pt-0  mb-4"}>
                <div className={styles.product_grid}>
                    {products?.map(
                        ({
                            id,
                            price,
                            amount,
                            cod,
                            exibition_name,
                            url,
                            selected,
                        }) => (
                            <div
                                key={id}
                                className="product_wrap shadown-sm border  rounded p-5 relative"
                            >
                                <span
                                    className={`${styles.empty} ${
                                        selected ? "bg-dark" : ""
                                    }`}
                                    onClick={selectProductCard(id)}
                                    style={{
                                        position: "absolute",
                                        left: "1.5rem",
                                        top: "1.5rem",
                                    }}
                                />
                                <small className={styles.selected}>
                                    Selecionar
                                </small>
                                <Img
                                    image_name_with_extension={
                                        url !== "-1"
                                            ? url
                                            : "../img/250x250.png"
                                    }
                                    outSide={url && url !== ""}
                                    className="product_front img-fluid pointer my-5"
                                    style={{ height: "25rem" }}
                                />
                                <div className="product_wrap_info d-lex flex-column align-item-center justify-content-between">
                                    <h2 className="mb-2 text-center">
                                        {`${exibition_name.slice(0, 40)}`}
                                        <small className="text-muted">
                                            ...
                                        </small>
                                    </h2>
                                    <small className="mb-2">
                                        Código intem:{" "}
                                        {`${cod.slice(0, 40)} ...`}
                                    </small>
                                    <p className="mb-2">
                                        Quantidade: {`${amount}`}
                                    </p>
                                    <h5
                                        style={{ minWidth: "max-content" }}
                                        className="mb-2"
                                    >
                                        {`R$${price}`.replace(".", ",")}
                                    </h5>
                                    <LinkTo
                                        className="mb-4 px-5 z-index-10000"
                                        _href="/changeproduct"
                                        query={{ id, token }}
                                    >
                                        <Button
                                            className="btn  py-3 m-0 w-100 bg_red"
                                            onClick={edit}
                                        >
                                            Editar produto
                                        </Button>
                                    </LinkTo>
                                </div>
                            </div>
                        )
                    )}
                </div>
            </Row>
        </>
    )
}
ProductsGrid.propTypes = {
    products: propTypes.array.isRequired,
}
export default ProductsGrid
