import React, {useContext, useEffect, useState} from "react"
import {Pagination} from "../../"
import {Alert, Col, Row} from "reactstrap"
import {awaitable, fromServer, hasKey, pag,} from "../../../assets/helpers"
import ProductSold from "./product/"
import {Context} from "../../../store"
import {useRouter} from "next/router"
import ClipLoader from "react-spinners/ClipLoader"

function Component() {
    const [{token},] = useContext(Context)
    const [page, setPage] = useState(0)
    const [requests, setRequests] = useState([])
    const router = useRouter()
    const [alert, setAlert] = useState(false)
    const toggle = () => setAlert(!alert)
    const [loaderState, setLoaderState] = useState(false)
    useEffect(() => {
        setLoaderState(true)
        awaitable(async () => {
            const data = await fromServer("/sales/my_requests", token)
            if (!hasKey(data, "error") && data.length > 0) {
                setRequests(data)
                setLoaderState(false)
            } else {
                setAlert(true)
                setLoaderState(false)
            }
        })
    }, [])

    // useEffect(() => {
    //     if (
    //         elm(".pagination_custom_ui ul > li>div") &&
    //         elm(".pagination_custom_ui ul > li>div").parentElement
    //     ) {
    //         elm(
    //             ".pagination_custom_ui ul > li>div"
    //         ).parentElement.style.border = "none"
    //     }
    // }, [page])
    useEffect(() => {
        //console.log("Pedidos", { requests, page })
        if (requests.length < 1) {
            toggle()
        }
    }, [requests])

    return (
        <>
            {loaderState && (
                <div className="spinnerLoader">
                    <ClipLoader
                        size={150}
                        color={"var(--red-1)"}
                        loading={loaderState}
                    />
                </div>
            )}
            <Row className={"m-0 pt-0 mb-3"}>
                <h1 className="page_title h1 m-0 mr-100">Pedidos</h1>
            </Row>
            <Row className={"m-0 pt-0  mb-4"}>
                <div className="d-flex flex-wrap w-100">
                    {requests.length < 1 && (
                        <Col md="12" className="w-100">
                            <Alert
                                color="primary"
                                isOpen={alert}
                                toggle={toggle}
                                style={{
                                    opacity: 1,
                                    color: "white",
                                    backgroundColor: "var(--red-1)",
                                    padding: "2rem",
                                    fontSize: "16px",
                                    textAlign: "center",
                                    width: "100%",
                                }}
                            >
                                Você ainda não realizou pedidos!
                            </Alert>
                        </Col>
                    )}
                    {requests.length > 0 &&
                    pag(requests, page).map((request) => (
                        <ProductSold
                            key={request.id}
                            product={request.products}
                            request={{
                                canceled_at: request.canceled_at,
                                code: request.code,
                                data: request.data,
                                hash: request.hash,
                                id: request.id,
                                nfe: request.nfe,
                                price: request.price,
                                situations: request.situations,
                                status: request.status,
                                total: request.total,
                            }}
                            token={token}
                        />
                    ))}
                </div>
            </Row>

            {Math.ceil(
                Number(Number(Number(requests.length) / 6))
            ) > 1 && <Row className={"m-0  mb-2 pt-0 w-100 "}>
                <nav className="mx-auto" aria-label="Page navigation example">
                    <Pagination
                        count={parseInt(
                            `${Math.ceil(
                                Number(Number(Number(requests.length) / 6))
                            )}`
                        )}
                        Listener={(page_) => setPage(Number(page_ - 1))}
                    />
                </nav>
            </Row>}
        </>
    )
}

export default Component
