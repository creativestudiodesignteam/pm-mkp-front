import React, { useEffect, useState } from "react"
import { BoardedWrapBox_styled } from "../styled-components/"
import { Img, LinkTo } from "../../../"
import styles from "./Product.module.css"
import { Row, Col } from "reactstrap"
import { brlFormat, toFixed } from "../../../../assets/helpers"
import ClipLoader from "react-spinners/ClipLoader"

const index = ({ product, request, token }) => {
    const [loaderState, setLoaderState] = useState(false)
    return (
        <>
            {loaderState && (
                <div className="spinnerLoader">
                    <ClipLoader
                        size={150}
                        color={"var(--red-1)"}
                        loading={loaderState}
                    />
                </div>
            )}
            <div
                className="mb-2 mb-md-4 displayFlex flex-row w-100 showAboveTablet"
                onClick={(e) => setLoaderState(true)}
            >
                <LinkTo
                    _href="/request"
                    query={{
                        id: request.id,
                        token: token,
                    }}
                >
                    <BoardedWrapBox_styled
                        className={"mr-2 mr-md-4"}
                        flex
                        padding="2rem 4rem"
                        width="34rem"
                    >
                        <Img
                            image_name_with_extension={
                                product.thumb !== null
                                    ? product.thumb
                                    : "130x130.png"
                            }
                            outSide={
                                product.thumb !== null ? product.thumb : ""
                            }
                        />

                        <h2 className="mb-0">{request.situations}</h2>
                        <small className="mb-3">
                            {product.name.slice(0, 17)}
                        </small>
                        {/* <Img
                        className={"checked"}
                        alt={"ok"}
                        image_name_with_extension={"check.png"}
                    /> */}
                    </BoardedWrapBox_styled>

                    <BoardedWrapBox_styled
                        flex
                        width="100%"
                        align="self-start"
                        padding="0 4rem"
                        smallSize="1.5rem"
                        smallWeight="normal"
                    >
                        <h2>Pedido número - {request.hash}</h2>
                        <small>Data de compra {request.data}</small>
                        <small className="mb-2">
                            Código do item: {product.id}
                        </small>
                        <div className="d-flex flex-row w-100 mb-2">
                            <h5 className={styles.textValue}>
                                Total:{" "}
                                <span>{brlFormat(request.total, true)}</span>
                            </h5>
                        </div>
                        {/* <Row className="w-100">
                        <Col md="12">
                            <h5 className={styles.textProgress}>
                                Progresso do pedido
                            </h5>
                        </Col>
                        <Col md="12">
                            <Progress color="#e3171b" value="100" />
                        </Col>
                    </Row> */}
                        {/* <Row className="w-100">
                        <Col md="3">
                            <h5>Pedido Recebido </h5>
                            <h5>03/11/2019 21:43</h5>
                        </Col>
                        <Col md="3">
                            <h5>Pedido Recebido </h5>
                            <h5>03/11/2019 21:43</h5>
                        </Col>
                        <Col md="3">
                            <h5>Pedido Recebido </h5>
                            <h5>03/11/2019 21:43</h5>
                        </Col>
                        <Col md="3">
                            <h5>Pedido Recebido </h5>
                            <h5>03/11/2019 21:43</h5>
                        </Col>
                    </Row> */}
                    </BoardedWrapBox_styled>
                </LinkTo>
            </div>
            <div
                className="showTabletAndLess w-100"
                onClick={(e) => setLoaderState(true)}
            >
                <LinkTo
                    _href="/request"
                    query={{
                        id: request.id,
                        token: token,
                    }}
                >
                    <Row>
                        <Col xs="12" className="allignerCenter">
                            <BoardedWrapBox_styled
                                // className={"mr-2 mr-md-4"}
                                flex
                                padding="2rem 4rem"
                                width="90vw"
                            >
                                <Img
                                    style={{ height: "20rem" }}
                                    image_name_with_extension={
                                        product.thumb !== null
                                            ? product.thumb
                                            : "130x130.png"
                                    }
                                    outSide={
                                        product.thumb !== null
                                            ? product.thumb
                                            : "130x130.png"
                                    }
                                />

                                <h2 className="mb-0">{request.situations}</h2>
                                <small className="mb-3">{product.name}</small>
                                {/* <Img
                                className={"checked"}
                                alt={"ok"}
                                image_name_with_extension={"check.png"}
                            /> */}
                            </BoardedWrapBox_styled>
                        </Col>
                    </Row>
                    <Row className="mb-4">
                        <Col xs="12" className="allignerCenter">
                            <BoardedWrapBox_styled
                                style={{ paddingTop: "3rem" }}
                                flex
                                width="90vw"
                                align="self-center"
                                smallSize="1.5rem"
                                smallWeight="normal"
                            >
                                <h2>Pedido número - {request.hash}</h2>
                                <small>Data de compra {request.data}</small>
                                <small className="mb-4">
                                    Código do item: {product.id}
                                </small>
                                <div className="d-flex flex-row w-100 mb-4 allignerCenter">
                                    <h5 className={styles.textValue}>
                                        Total:{" "}
                                        <span>
                                            {brlFormat(request.total, true)}
                                        </span>
                                    </h5>
                                </div>

                                {/* <h5 className={styles.textProgress}>
                                Progresso do pedido
                            </h5>

                            <Progress
                                style={{ width: "75%" }}
                                color="#e3171b"
                                value="100"
                            />
                            <h5 style={{ fontSize: "1.5rem" }}>
                                Pedido Recebido - 03/11/2019 21:43
                            </h5>
                            <h5 style={{ fontSize: "1.5rem" }}>
                                Pedido Recebido - 03/11/2019 21:43
                            </h5>
                            <h5 style={{ fontSize: "1.5rem" }}>
                                Pedido Recebido - 03/11/2019 21:43
                            </h5>
                            <h5 style={{ fontSize: "1.5rem" }}>
                                Pedido Recebido - 03/11/2019 21:43
                            </h5> */}
                            </BoardedWrapBox_styled>
                        </Col>
                    </Row>
                </LinkTo>
            </div>
        </>
    )
}

export default index
