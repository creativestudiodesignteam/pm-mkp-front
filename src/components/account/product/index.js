import React, {useState} from "react"
import PropTypes from "prop-types"
import {Row} from "reactstrap"
import {LinkTo, Pagination} from "../.."
import Grid from "../grid/"
import {pag} from "../../../assets/helpers"
import ClipLoader from "react-spinners/ClipLoader"

const index = ({token, products, setProducts}) => {
    const [page, setPage] = useState(0)
    const [loaderState, setLoaderState] = useState(false)
    const newProduct = () => {
        setLoaderState(true)
    }
    return (
        <>
            {loaderState && (
                <div className="spinnerLoader">
                    <ClipLoader
                        size={150}
                        color={"var(--red-1)"}
                        loading={loaderState}
                    />
                </div>
            )}
            <Grid
                token={token}
                setProducts={setProducts}
                allProducts={products}
                products={pag(products, page)}
            />
            <LinkTo
                _href="/product"
                className={"font-weight-bold text_dark_2 ml-1 fix"}
            >
                <div
                    className={
                        "w-100 mx-auto m-0 border bg-white text_light_gray text-center p-5 fix"
                    }
                    onClick={newProduct}
                >
                    <div className={"font-weight-bold text_dark_2 ml-1 fix"}>
                        <u>Clique aqui para adicionar um novo produto</u>
                    </div>
                </div>
            </LinkTo>
            {parseInt(
                `${Math.ceil(
                    Number(Number(Number(products.length) / 6))
                )}`
            ) > 1 && <Row className={"m-0  mb-2 pt-0 w-100 "}>
                <nav className="mx-auto" aria-label="Page navigation example">
                    <Pagination
                        count={parseInt(
                            `${Math.ceil(
                                Number(Number(Number(products.length) / 6))
                            )}`
                        )}
                        Listener={(page_) => setPage(Number(page_ - 1))}
                    />
                </nav>
            </Row>}
        </>
    )
}

index.propTypes = {
    products: PropTypes.array.isRequired,
}

export default index
