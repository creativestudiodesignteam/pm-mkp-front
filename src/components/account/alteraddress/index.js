import React, {useContext, useEffect, useState} from "react"
import {Button, Col, Form, Row} from "reactstrap"
import {useRouter} from "next/router"
import {Context} from "../../../store"
import {InputDefault, LinkTo} from "../.."
import {
    add_invalid,
    add_valid,
    cepBlur,
    defaultListener,
    elm,
    fromServer,
    OBJ,
    postcode,
    state_address,
} from "../../../assets/helpers"
import {MySwal} from "../../../assets/sweetalert"
import SelectDefault from "../../SelectDefault"
import ClipLoader from "react-spinners/ClipLoader"

const Alteraddress = () => {
    const [{token}, dispatch] = useContext(Context)
    const router = useRouter()
    const [loaderState, setLoaderState] = useState(false)
    const {
        name,
        street,
        number,
        neighborhood,
        city,
        state: state_register,
        postcode: cep,
        id,
    } = router.query
    const [state, setState] = useState({
        name: name || "",
        postcode: cep || "",
        state: state_register || "",
        city: city || "",
        neighborhood: neighborhood || "",
        street: street || "",
        number: number || "",
        complement: router?.query?.complement || "",
        references: router?.query?.references || "",
    })
    useEffect(() => {
        setLoaderState(false)
    }, [])

    const save = async (e) => {
        e.preventDefault()
        setLoaderState(true)
        const btn = elm(".buttonSave")
        btn.disabled = true
        btn.innerHTML = `Carregando ... <div class="spinner-border text-light" role="status"><span class="sr-only">Loading...</span></div>`
        const data = await fromServer(`/addresses/${id}`, token, "put", state)
        if (!OBJ.hasKey(data, "error")) {
            btn.disabled = true
            btn.innerHTML = `SALVAR`
            await MySwal.fire("Endereço atualizado com sucesso", ``, "success")
            await router.push("/account/[feature]", "/account/address")
        } else {
            btn.disabled = true
            btn.innerHTML = `SALVAR`
            await MySwal.fire(
                "Não foi possível atualizar este endereço, tente novamente",
                ``,
                "error"
            )
            setLoaderState(false)

            console.log("fail to load addresses", {data})
        }
        setLoaderState(false)

        return false
    }

    const goBack = (e) => {
        setLoaderState(true)
    }

    return (
        <>
            {loaderState && (
                <div className="spinnerLoader">
                    <ClipLoader
                        size={150}
                        color={"var(--red-1)"}
                        loading={loaderState}
                    />
                </div>
            )}
            <Row>
                <Col md="12" style={{marginBottom: "2rem"}}>
                    <h1>Alteração de Endereço</h1>
                </Col>
                <Col md="12">
                    <Form onSubmit={save}>
                        <Row form className="mb-4">
                            <Col md={6}>
                                <InputDefault
                                    name={"name"}
                                    type={"text"}
                                    has_label={true}
                                    label={"De onde é este endereço? *"}
                                    uncontrolled
                                    _value={state.name}
                                    _handle={defaultListener(setState, state)}
                                    _required
                                />
                            </Col>
                            <Col md={6}>
                                <InputDefault
                                    has_label
                                    label={"CEP*"}
                                    name={"postcode"}
                                    _id={"postcode"}
                                    type={"text"}
                                    initial_value={state.postcode}
                                    _required
                                    onBlur={async (e) => {
                                        const result = await cepBlur(e, {
                                            state_address: "state",
                                        })
                                        if (result === false) {
                                            /*console.log("result false", {
                                                result,
                                            })*/
                                            add_invalid(
                                                elm("[name=postcode]"),
                                                "CEP invalido !",
                                                ""
                                            )
                                            setState({
                                                ...state,
                                                street: "",
                                                city: "",
                                                state: "",
                                                neighborhood: "",
                                                postcode: "",
                                            })
                                            elm("[name=postcode]").value = ""
                                        } else {
                                            add_valid(elm("[name=postcode]"))
                                            setState({
                                                ...state,
                                                street: result?.street || "",
                                                city: result?.city || "",
                                                state: result?.state || "",
                                                neighborhood:
                                                    result?.neighborhood || "",
                                            })
                                            elm("[name=state]").value =
                                                result?.state || ""
                                        }
                                        //console.log("out", result)
                                    }}
                                    onKeyUp={postcode}
                                    options={{
                                        delimiters: ["-"],
                                        blocks: [5, 3],
                                    }}
                                    onChange={defaultListener(setState, state)}
                                />
                            </Col>
                            <Col md={6}>
                                <InputDefault
                                    name={"street"}
                                    type={"text"}
                                    has_label={true}
                                    label={"Rua*"}
                                    uncontrolled
                                    _value={state.street}
                                    _handle={defaultListener(setState, state)}
                                    _required
                                />
                            </Col>
                            <Col md={6}>
                                <InputDefault
                                    name={"number"}
                                    type={"number"}
                                    has_label={true}
                                    label={"Número*"}
                                    uncontrolled
                                    _value={state.number}
                                    _handle={defaultListener(setState, state)}
                                    _required
                                />
                            </Col>
                            <Col md={6}>
                                <InputDefault
                                    name={"complement"}
                                    type={"text"}
                                    has_label={true}
                                    label={"Complemento"}
                                    uncontrolled
                                    _value={state.complement}
                                    _handle={defaultListener(setState, state)}
                                />
                            </Col>
                            <Col md={6}>
                                <InputDefault
                                    name={"references"}
                                    type={"text"}
                                    has_label={true}
                                    label={"Informações de referência"}
                                    uncontrolled
                                    _value={state.references}
                                    _handle={defaultListener(setState, state)}
                                />
                            </Col>
                            <Col md={6}>
                                <InputDefault
                                    name={"neighborhood"}
                                    type={"text"}
                                    has_label={true}
                                    label={"Bairro*"}
                                    uncontrolled
                                    _value={state.neighborhood}
                                    _handle={defaultListener(setState, state)}
                                    _required
                                />
                            </Col>
                            <Col md={6}>
                                <InputDefault
                                    name={"city"}
                                    type={"text"}
                                    has_label={true}
                                    label={"Cidade*"}
                                    uncontrolled
                                    _value={state.city}
                                    _handle={defaultListener(setState, state)}
                                    _required
                                />
                            </Col>
                            <Col md={6}>
                                <SelectDefault
                                    id={"state"}
                                    name={"state"}
                                    initial_value={state.state}
                                    has_label
                                    label={"Estado*"}
                                    listener={(r) => {
                                        setState({
                                            ...state,
                                            state: r.selectedOptions[0].value,
                                        })
                                    }}
                                    values={state_address}
                                />
                            </Col>
                        </Row>
                        <Row>
                            <Col md="4" className="mt-4">
                                <Button
                                    size="lg"
                                    className="buttonSave"
                                    type={"submit"}
                                >
                                    SALVAR
                                </Button>
                            </Col>
                            <Col md="4" className="mt-4">
                                <LinkTo
                                    _href={"/account/[feature]"}
                                    as={"/account/address"}
                                    query={token}
                                >
                                    <button
                                        size="lg"
                                        className="btn btn-block btn-outline-dark h-100 rounded-0"
                                        onClick={goBack}
                                    >
                                        VOLTAR
                                    </button>
                                </LinkTo>
                            </Col>
                        </Row>
                    </Form>
                </Col>
            </Row>
        </>
    )
}

export default Alteraddress
