import React, { useCallback, useContext, useEffect, useState } from "react"
import { Button, Col, Form, Row } from "reactstrap"
import { InputDefault, LinkTo } from "../../"
import {
    add_invalid,
    add_valid,
    awaitable,
    elm,
    elmAll,
    fromServer,
    getMaxValueOnArrayOfObjectBy,
} from "../../../assets/helpers"
import { Context } from "../../../store"
import { MySwal } from "../../../assets/sweetalert"
import { useRouter } from "next/router"
import ClipLoader from "react-spinners/ClipLoader"

const NewOption = () => {
    const [{ token }, dispatch] = useContext(Context)
    const [values, setValues] = useState([{ id: 0, value: "" }])
    const [options, setOptions] = useState([])
    const router = useRouter()
    const [loaderState, setLoaderState] = useState(false)

    useEffect(() => {
        setLoaderState(true)
        if (options.length < 1) {
            awaitable(async () => {
                const dataOptions = await fromServer(`/options`, token, "get")
                console.log({ dataOptions })
                if (dataOptions.length > 0) {
                    setOptions(dataOptions)
                    setLoaderState(false)
                } else {
                    setLoaderState(false)

                    console.log("no options", { dataOptions })
                }
                setLoaderState(false)
            })
        }
        setLoaderState(false)
    }, [options])

    const blurName = useCallback(
        (e) => {
            e.preventDefault()
            setLoaderState(true)

            console.log(e.target.value)
            const name = options.filter((option) => {
                if (option.options.name === e.target.value) {
                    setLoaderState(false)
                    return option.options.name
                }
            })
            if (name[0]?.options?.name) {
                console.log(name[0].options.name)
                add_invalid(e.target, "Nome já utilizado")
                setLoaderState(false)
            } else if (e.target.value.length < 1) {
                add_invalid(e.target, "Nome não pode estar vazio")
                setLoaderState(false)
            } else {
                add_valid(e.target)
                setLoaderState(false)
            }
            setLoaderState(false)
        },
        [options]
    )

    function remove(id) {
        return (e) => {
            e.preventDefault()
            setLoaderState(true)
            const newValues = values.filter((item) => item.id !== id)
            console.log({ newValues })
            setValues(newValues)
            setLoaderState(false)
        }
    }

    const add = useCallback(
        (e) => {
            e.preventDefault()
            setLoaderState(true)
            setValues([
                ...values,
                {
                    id: !values?.length
                        ? 1
                        : Number(
                              values.reduce(
                                  getMaxValueOnArrayOfObjectBy("id"),
                                  0
                              ) + 1
                          ),
                    value: "",
                },
            ])
            setLoaderState(false)
        },
        [values]
    )
    const save = async () => {
        setLoaderState(true)
        const payload = {
            name: elm("[name=name]")?.value,
        }
        console.log({ payload })
        const response = await fromServer(`/options/`, token, "POST", payload)

        if (response?.error) {
            console.log("error: ", { response })
            await MySwal.fire(
                "Não foi possivel adicionar esta característica, tente novamente ",
                "",
                "error"
            )
            setLoaderState(false)
        } else {
            console.log("success", { response })
            const payloadValues = values.map((value) => {
                let newvalue = {
                    name: elm(`[name=value${value.id}]`)?.value,
                    fk_options: response.id,
                }
                return newvalue
            })
            console.log({ payloadValues })

            const responseOP = await fromServer(
                `/options_name`,
                token,
                "POST",
                payloadValues
            )

            if (responseOP?.error) {
                console.log("error: ", { responseOP })
                await MySwal.fire(
                    "Não foi possivel adicionar esta característica, tente novamente",
                    "",
                    "error"
                )
                setLoaderState(false)

                for (const option_name_input of elmAll("[name^=value]")) {
                    !option_name_input.value &&
                        add_invalid(option_name_input, "Preencha o campo")
                    !!option_name_input.value && add_valid(option_name_input)
                }
                setTimeout(() => {
                    for (const option_name_input of elmAll("[name^=value]")) {
                        option_name_input.classList.remove("invalid")
                        option_name_input.classList.remove("valid")
                    }
                }, 2000)
                setLoaderState(false)
            } else {
                console.log("success", { responseOP })
                await MySwal.fire(
                    "Característica adicionada com sucesso",
                    "",
                    "success"
                )
                await router.push("/account/[feature]", "/account/options")
            }
            setLoaderState(false)
        }
        setLoaderState(false)
    }

    return (
        <>
            {loaderState && (
                <div className="spinnerLoader">
                    <ClipLoader
                        size={150}
                        color={"var(--red-1)"}
                        loading={loaderState}
                    />
                </div>
            )}
            <Row>
                <Col xs="12" className="mb-4">
                    <h1>Nova Característica</h1>
                </Col>
                <Col xs="12">
                    <Form>
                        <Row form className="mb-4">
                            <Col md="12">
                                <InputDefault
                                    name={"name"}
                                    type={"text"}
                                    has_label={true}
                                    label={"Nome da Característica*"}
                                    onBlur={blurName}
                                    _required
                                    placeholder={"Exemplo: Cor"}
                                />
                            </Col>
                        </Row>
                        {values?.map(({ value, id }) => {
                            return (
                                <Row key={id}>
                                    <Col xs="6" md={10}>
                                        <InputDefault
                                            initial_value={value}
                                            name={`value${id}`}
                                            id={`value${id}`}
                                            type={"text"}
                                            has_label={true}
                                            label={"Atributo*"}
                                            placeholder={"Exemplo: Azul"}
                                        />
                                    </Col>
                                    <Col
                                        xs="4"
                                        md="2"
                                        className="allignerCenter"
                                    >
                                        {
                                            <Button
                                                className="buttonSave showTabletAndLess"
                                                style={{
                                                    marginTop: "0.75rem",
                                                    height: "3rem!important",
                                                }}
                                                onClick={remove(id)}
                                            >
                                                Remover
                                            </Button>
                                        }
                                        {
                                            <Button
                                                className="buttonSave showAboveTablet"
                                                style={{ marginTop: "0.75rem" }}
                                                onClick={remove(id)}
                                            >
                                                Remover
                                            </Button>
                                        }
                                    </Col>
                                </Row>
                            )
                        })}
                    </Form>
                </Col>
                <Col xs="12" style={{ marginTop: "1rem" }}>
                    <Row>
                        <Col md="12">
                            <div
                                className={`w-100 mx-auto m-0 border bg-white text_light_gray text-center p-5 fix`}
                                style={{ cursor: "pointer" }}
                                onClick={add}
                            >
                                <div
                                    className={
                                        "font-weight-bold text_dark_2 ml-1 fix"
                                    }
                                >
                                    <u>
                                        Clique aqui para adicionar um novo
                                        atributo
                                    </u>
                                </div>
                            </div>
                        </Col>
                    </Row>
                </Col>
                <Col
                    xs="4"
                    className="allignerCenter"
                    style={{ marginTop: "3rem" }}
                >
                    <Button
                        className="buttonSave"
                        style={{ width: "100%" }}
                        onClick={save}
                    >
                        Salvar
                    </Button>
                </Col>
                <Col xs="4" style={{ marginTop: "3rem" }}>
                    <LinkTo
                        _href={"/account/[feature]"}
                        as={"/account/options"}
                        query={token}
                    >
                        <button
                            size="lg"
                            className="btn btn-block btn-outline-dark h-100 rounded-0"
                        >
                            VOLTAR
                        </button>
                    </LinkTo>
                </Col>
            </Row>
        </>
    )
}

export default NewOption
