import React, { useContext, useState } from "react"
import { Button, Col, Form, Row } from "reactstrap"
import Cards from "react-credit-cards"
import { useRouter } from "next/router"
import { Context } from "../../../store"
import { InputDefault, LinkTo } from "../.."
import {
    birthday,
    elm,
    fromServer,
    OBJ,
    onlyNumber,
} from "../../../assets/helpers"
import { MySwal } from "../../../assets/sweetalert"
import ClipLoader from "react-spinners/ClipLoader"

const NewCreditCard = () => {
    const [{ token }] = useContext(Context)
    const router = useRouter()
    const [card, setCard] = useState({
        cvc: " ",
        expiry: " ",
        focus: " ",
        name: " ",
        number: " ",
    })
    const [loaderState, setLoaderState] = useState(false)

    const handleInputFocus = ({ target: { name } }) => {
        setCard({ ...card, focus: name })
    }
    const handleInputChange = ({ target: { name, value } }) => {
        setCard({ ...card, [name]: value })
    }
    const saveCreditCard = async (e) => {
        e.preventDefault()
        setLoaderState(true)
        const btn = elm(".btn_save")
        btn.disabled = true
        btn.innerHTML = `Carregando ... <div class="spinner-border text-light" role="status"><span class="sr-only">Loading...</span></div>`
        if (token) {
            const data = await fromServer("/creditcard", token, "post", {
                card_holder: card.name,
                card_number: card.number,
                expiration_date: card.expiry,
                security_code: card.cvc,
                type: 2,
            })

            console.log("data", { data })

            if (!OBJ.hasKey(data, "error") && data) {
                console.log("sucesso", { data })
                await MySwal.fire(
                    "Cartão adicionado com sucesso",
                    ``,
                    "success"
                )
                await router.push("/account/[feature]", "/account/card")
                setLoaderState(false)
            } else {
                console.log("falha", { data })
                await MySwal.fire(
                    "Cartão inválido, revise os dados",
                    ``,
                    "error"
                )
                setLoaderState(false)
            }
        }
        btn.disabled = false
        btn.innerHTML = `SALVAR`
        setLoaderState(false)
        return false
    }

    const goBack = (e) => {
        setLoaderState(true)
    }
    return (
        <>
            {loaderState && (
                <div className="spinnerLoader">
                    <ClipLoader
                        size={150}
                        color={"var(--red-1)"}
                        loading={loaderState}
                    />
                </div>
            )}
            <Row>
                <Col md="12" style={{ marginBottom: "2rem" }}>
                    <h1>Adicionar novo cartão de crédito</h1>
                </Col>
                {
                    <Col md="6" className="mb-4 showTabletAndLess">
                        <Cards
                            cvc={card.cvc}
                            expiry={card.expiry}
                            focused={card.focus}
                            name={card.name}
                            number={card.number}
                            acceptedCards={[
                                "visa",
                                "mastercard",
                                "elo",
                                "hipercard",
                                "amex",
                                "dinersclub",
                                "discover",
                            ]}
                            placeholders={{
                                name: "nome no cartão",
                                valid: "valido até",
                            }}
                            locale={{ valid: "valido até" }}
                        />
                    </Col>
                }
                <Col lg="6">
                    <Form onSubmit={saveCreditCard} autocomplete="off">
                        <Row className="mb-4 allignerCenter">
                            <Col
                                md={6}
                                style={{
                                    paddingRight: "3rem",
                                    paddingTop: "1rem",
                                }}
                            >
                                <InputDefault
                                    id={"number"}
                                    name={"number"}
                                    type={"text"}
                                    has_label={true}
                                    label={"Número do cartão *"}
                                    onKeyUp={onlyNumber}
                                    onChange={handleInputChange}
                                    onFocus={handleInputFocus}
                                    _minLength={"14"}
                                    _maxLength={"16"}
                                    _required
                                />
                            </Col>
                            <Col
                                xs={6}
                                style={{
                                    paddingRight: "3rem",
                                    paddingTop: "1rem",
                                }}
                            >
                                <InputDefault
                                    id={"expiry"}
                                    name={"expiry"}
                                    type={"text"}
                                    has_label={true}
                                    label={"Validade*"}
                                    placeholder={"MM/AAAA"}
                                    onChange={handleInputChange}
                                    onFocus={handleInputFocus}
                                    onKeyUp={birthday}
                                    options={{
                                        delimiters: ["/"],
                                        blocks: [2, 6],
                                    }}
                                    _minLength={"7"}
                                    _maxLength={"7"}
                                    _required
                                />
                            </Col>
                            <Col
                                xs={6}
                                style={{
                                    paddingRight: "3rem",
                                    paddingTop: "1rem",
                                }}
                            >
                                <InputDefault
                                    id={"cvc"}
                                    name={"cvc"}
                                    type={"text"}
                                    has_label={true}
                                    label={"CVV*"}
                                    onChange={handleInputChange}
                                    onFocus={handleInputFocus}
                                    onKeyUp={onlyNumber}
                                    placeholder={"XXX"}
                                    _minLength={"3"}
                                    _maxLength={"4"}
                                    _required
                                />
                            </Col>
                            <Col
                                md={6}
                                style={{
                                    paddingRight: "3rem",
                                    paddingTop: "1rem",
                                }}
                            >
                                <InputDefault
                                    id={"name"}
                                    name={"name"}
                                    type={"text"}
                                    has_label={true}
                                    label={"Nome do titular do cartão*"}
                                    onChange={handleInputChange}
                                    onFocus={handleInputFocus}
                                    _required
                                />
                            </Col>

                            <Col md="6" style={{ marginTop: "3rem" }}>
                                <Button
                                    size="lg"
                                    className="buttonSave btn_save"
                                    style={{ width: "100%" }}
                                    type="submit"
                                >
                                    SALVAR
                                </Button>
                            </Col>
                            <Col md="6" style={{ marginTop: "3rem" }}>
                                <LinkTo
                                    _href={"/account/[feature]"}
                                    as={"/account/card"}
                                    query={token}
                                >
                                    <button
                                        size="lg"
                                        className="btn btn-block btn-outline-dark rounded-0 w-100"
                                        style={{ height: "5rem" }}
                                        onClick={goBack}
                                    >
                                        VOLTAR
                                    </button>
                                </LinkTo>
                            </Col>
                        </Row>
                    </Form>
                </Col>
                {
                    <Col md="6" className="showAboveTablet">
                        <Cards
                            cvc={card.cvc}
                            expiry={card.expiry}
                            focused={card.focus}
                            name={card.name}
                            number={card.number}
                            acceptedCards={[
                                "visa",
                                "mastercard",
                                "elo",
                                "hipercard",
                                "amex",
                                "dinersclub",
                                "discover",
                            ]}
                            placeholders={{
                                name: "nome no cartão",
                                valid: "valido até",
                            }}
                            locale={{ valid: "valido até" }}
                        />
                    </Col>
                }
            </Row>
        </>
    )
}

export default NewCreditCard
