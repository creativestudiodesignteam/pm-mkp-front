import React, { useState, useEffect, useContext } from "react"
import { Row, Col, Card, Button, Alert, CardText } from "reactstrap"
import styles from "./SavedCreditCards.module.css"
import Cards from "react-credit-cards"
import { useRouter } from "next/router"
import { isAuthenticated, getToken, logOut, LogIn } from "../../../assets/auth"
import { Context } from "../../../store"
import { awaitable, fromServer, OBJ } from "../../../assets/helpers"
import { MySwal } from "../../../assets/sweetalert"
import ClipLoader from "react-spinners/ClipLoader"

const SavedCreditCards = () => {
    const [{ token }, dispatch] = useContext(Context)
    const router = useRouter()
    const [alert, setAlert] = useState(false)
    const toggle = () => setAlert(!alert)
    const [cards, setCards] = useState([])
    const [loaderState, setLoaderState] = useState(false)

    useEffect(() => {
        setLoaderState(true)
        console.log({ token, dispatch })

        if (token) {
            // Tem Token
        } else {
            // Não tem Token
            const _user = isAuthenticated(token)
            if (!_user) {
                logOut(dispatch, router)
                setLoaderState(false)
            } else if (token === "") {
                LogIn(getToken(), _user, dispatch)
                setLoaderState(false)
            }
        }

        awaitable(async () => {
            if (cards.length < 1 && token) {
                const data = await fromServer("/creditcard", token, "get")

                if (!OBJ.hasKey(data, "error") && data.length > 0) {
                    const adjustedData = data.map((card) => {
                        card.number = card.first_digits.padEnd(16, "X")
                        card.focus = " "
                        card.expiry = " "
                        card.cvc = " "
                        card.selected = card.selected || false
                        return card
                    })
                    console.log({ adjustedData })
                    setCards(...cards, adjustedData)
                    setLoaderState(false)
                } else {
                    setAlert(true)
                    setLoaderState(false)
                }
            }
            setLoaderState(false)
        })
    }, [])

    const remove = (cardId) => async (e) => {
        e.preventDefault()
        setLoaderState(true)

        const data = await fromServer(`/creditcard/${cardId}`, token, "delete")
        if (!OBJ.hasKey(data, "error") && data) {
            let newCards = cards.filter((elem) => {
                if (elem.id != cardId) {
                    return elem
                }
            })
            setCards(newCards)
            await MySwal.fire("Cartão removido com sucesso", ``, "success")
            setLoaderState(false)
        } else {
            await MySwal.fire(
                "Não foi possível remover o cartão, tente novamente",
                ``,
                "error"
            )
            setLoaderState(false)
        }
    }
    const add = (e) => {
        e.preventDefault()
        setLoaderState(true)
        router.push("/account/[feature]", "/account/newcreditcard")
    }

    const saveFavorite = (idFavorite) => async (e) => {
        e.preventDefault()
        setLoaderState(true)

        console.log({ idFavorite })
        const data = await fromServer("/creditcard/select", getToken(), "PUT", {
            id: idFavorite,
        })

        const has_error = OBJ.hasKey(data, "error")
        has_error && console.log("fail to update selected card", data.error)
        if (!has_error) {
            setCards(
                cards.map((card) => {
                    if (card.selected) {
                        return {
                            ...card,
                            selected: false,
                        }
                    }
                    if (card.id === idFavorite) {
                        return {
                            ...card,
                            selected: true,
                        }
                    }
                    return card
                })
            )
            setLoaderState(false)
        }
        setLoaderState(false)
    }
    return (
        <>
            {loaderState && (
                <div className="spinnerLoader">
                    <ClipLoader
                        size={150}
                        color={"var(--red-1)"}
                        loading={loaderState}
                    />
                </div>
            )}
            <Row>
                <Col md="12" style={{ marginBottom: "2rem" }}>
                    <h1>Cartões Salvos</h1>
                </Col>
                {cards.length === 0 && (
                    <Col md="12">
                        <Alert
                            color="primary"
                            isOpen={alert}
                            toggle={toggle}
                            style={{
                                opacity: 1,
                                color: "white",
                                backgroundColor: "var(--red-1)",
                                padding: "2rem",
                                fontSize: "16px",
                                textAlign: "center",
                            }}
                        >
                            Você ainda não possui cartões cadastrados!
                        </Alert>
                    </Col>
                )}
                {cards.length > 0 && (
                    <Col md="12">
                        <Card
                            body
                            className={styles.cardBody}
                            style={{ border: "none" }}
                        >
                            <Row>
                                {cards &&
                                    cards.map((card, idx) => {
                                        return (
                                            <Col
                                                md={`${
                                                    cards.length === 1
                                                        ? "12"
                                                        : "6"
                                                }`}
                                                style={{ marginBottom: "3rem" }}
                                                key={idx}
                                            >
                                                <Cards
                                                    style={{
                                                        marginBottom: "2rem",
                                                    }}
                                                    cvc={card.cvc}
                                                    expiry={card.expiry}
                                                    focused={card.focus}
                                                    name={card.holder_name}
                                                    number={card.number}
                                                    acceptedCards={[
                                                        "visa",
                                                        "mastercard",
                                                        "elo",
                                                        "hipercard",
                                                        "amex",
                                                        "dinersclub",
                                                        "discover",
                                                    ]}
                                                    placeholders={{
                                                        name: "nome no cartão",
                                                        valid: "valido até",
                                                    }}
                                                    locale={{
                                                        valid: "valido até",
                                                    }}
                                                />
                                                <Row>
                                                    <Col
                                                        md="12"
                                                        style={{
                                                            marginTop: "1rem",
                                                            width: "100%",
                                                        }}
                                                    >
                                                        <Row>
                                                            <Col
                                                                md="12"
                                                                style={{
                                                                    display:
                                                                        "inline-grid",
                                                                }}
                                                            >
                                                                {card.selected && (
                                                                    <CardText
                                                                        id={`saveCard-${idx}1`}
                                                                        className={
                                                                            styles.cardTextPrincipalAddress
                                                                        }
                                                                    >
                                                                        <Row>
                                                                            <Col
                                                                                xs="12"
                                                                                className="allignerCenter"
                                                                            >
                                                                                <div
                                                                                    className={
                                                                                        styles.circle
                                                                                    }
                                                                                    style={{
                                                                                        backgroundColor:
                                                                                            "green",
                                                                                    }}
                                                                                />
                                                                                Cartão
                                                                                principal
                                                                            </Col>
                                                                        </Row>
                                                                    </CardText>
                                                                )}
                                                                {!card.selected && (
                                                                    <CardText
                                                                        onClick={saveFavorite(
                                                                            card.id
                                                                        )}
                                                                        id={`saveCard-${idx}`}
                                                                        className={
                                                                            styles.cardTextListAddress
                                                                        }
                                                                    >
                                                                        <Row>
                                                                            <Col
                                                                                xs="12"
                                                                                className="allignerCenter"
                                                                            >
                                                                                <div
                                                                                    className={
                                                                                        styles.circle
                                                                                    }
                                                                                    style={{
                                                                                        border:
                                                                                            "2px solid red",
                                                                                    }}
                                                                                />
                                                                                Cartão
                                                                                principal
                                                                            </Col>
                                                                        </Row>
                                                                    </CardText>
                                                                )}
                                                            </Col>
                                                            <Col
                                                                md="12"
                                                                style={{
                                                                    marginTop:
                                                                        "1rem",
                                                                    display:
                                                                        "inline-grid",
                                                                }}
                                                            >
                                                                <Button
                                                                    size="lg"
                                                                    className={
                                                                        styles.buttonDelete
                                                                    }
                                                                    onClick={remove(
                                                                        card.id
                                                                    )}
                                                                >
                                                                    EXCLUIR
                                                                </Button>
                                                            </Col>
                                                        </Row>
                                                    </Col>
                                                </Row>
                                            </Col>
                                        )
                                    })}
                            </Row>
                        </Card>
                    </Col>
                )}
                <Col md="12" style={{ marginTop: "2rem" }}>
                    <Row>
                        <Col md="12">
                            <div
                                className={`w-100 mx-auto m-0 border bg-white text_light_gray text-center p-5 fix`}
                                style={{ cursor: "pointer" }}
                            >
                                <div
                                    onClick={add}
                                    className={
                                        "font-weight-bold text_dark_2 ml-1 fix"
                                    }
                                >
                                    <u>Clique aqui para adicionar um cartão</u>
                                </div>
                            </div>
                        </Col>
                    </Row>
                </Col>
            </Row>
        </>
    )
}

export default SavedCreditCards
