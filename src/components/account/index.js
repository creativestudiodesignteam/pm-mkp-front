import React, { Suspense, useState } from "react"
import propTypes from "prop-types"
import Main from "../Main"
import Aside from "../Aside"
import { Row, Button, Col } from "reactstrap"
import Loader from "./../loader/"

const Resolver = React.lazy(() => import("./resolver/"))

const AccountResolver = ({
    token,
    products,
    setProducts,
    node_text,
    user,
    links,
    feature,
}) => {
    const [asideToggle, setAsideToggle] = useState(false)
    const toggle = () => setAsideToggle(!asideToggle)

    return (
        <Main
            _BreadCrumb={[
                {
                    node_text: "Login",
                    href: "/login",
                },
                {
                    node_text: node_text,
                    href: `/account/${feature}`,
                    query: {},
                },
            ]}
        >
            <Row>
                {
                    <div className="showTabletAndLess w-100">
                        <Col xs="12" className={asideToggle ? "" : "mb-4"}>
                            <Button
                                className="primary w-100"
                                style={{
                                    backgroundColor: "#e3171b",
                                    fontSize: "2rem",
                                }}
                                onClick={toggle}
                            >
                                {" "}
                                Menu{" "}
                            </Button>
                        </Col>
                        {asideToggle && (
                            <Col xs="12" className="mb-4 w-100">
                                <Aside
                                    token={token}
                                    user={user}
                                    links={links}
                                    feature={feature}
                                    setAsideToggle={setAsideToggle}
                                    asideToggle={asideToggle}
                                />
                            </Col>
                        )}
                    </div>
                }
            </Row>
            <Row className="m-0 flex-nowrap">
                {
                    <div className="showAboveTablet">
                        <Aside
                            token={token}
                            user={user}
                            links={links}
                            feature={feature}
                            setAsideToggle={setAsideToggle}
                            asideToggle={false}
                        />
                    </div>
                }
                <div id="dashboard" className="flex flex-column w-auto">
                    <Suspense fallback={<Loader />}>
                        <Resolver
                            token={token}
                            user={user}
                            page={feature}
                            products={products}
                            setProducts={setProducts}
                        />
                    </Suspense>
                </div>
            </Row>
        </Main>
    )
}
AccountResolver.propTypes = {
    node_text: propTypes.string.isRequired,
    products: propTypes.array.isRequired,
    user: propTypes.object.isRequired,
    links: propTypes.array.isRequired,
    feature: propTypes.string,
}

export default AccountResolver
