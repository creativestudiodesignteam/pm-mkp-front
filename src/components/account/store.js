import { OBJ } from "../../assets/helpers"

export const valid_path = {
    myaccount: "Minha conta",
    sales: "Histórico de vendas",
    request: "Meus Pedidos",
    card: "Cartões salvos",
    address: "Endereços salvos",
    alteraddress: "Alteração de endereço",
    newaddress: "Adicionar endereço",
    newcreditcard: "Adicionar Cartão de Crédito",
    options: "Características e atributos",
    newoption: "Nova característica",
    alteroption: "Alterar característica",
    // user: "Cadastro de usuário",
    product: "Produtos",
    out: "Sair",
}
export const links = Object.entries(valid_path)
export const valid_checkers = links.reduce(
    (acc, [k, v]) => ({ ...acc, [v]: k }),
    {}
)
const STORE = (paths = false) => ({
    valid_path: paths || valid_path,
    links: Object.entries(paths || valid_path),
    valid_checkers: Object.entries(paths || valid_path).reduce(
        (acc, [k, v]) => ({ ...acc, [v]: k }),
        {}
    ),
})
export default STORE
/**
 * @param {object} ObjectWithKeyAndValue
 *@return {string|boolean}
 * */
export function queryParamsAsStringToUseAfterRouterName(ObjectWithKeyAndValue) {
    if (OBJ.isObject(ObjectWithKeyAndValue)) {
        const arr = Object.entries(ObjectWithKeyAndValue)
        return arr.reduce(
            (acc, [key, value], index) =>
                acc +
                (index === arr.length - 1
                    ? `${key}=${value}`
                    : `${key}=${value}&`),
            "?"
        )
    }
    console.log({ ObjectWithKeyAndValue })
    return false
}
