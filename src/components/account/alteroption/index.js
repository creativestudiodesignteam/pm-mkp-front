import React, { useCallback, useContext, useEffect, useState } from "react"
import { Button, Col, Form, Row } from "reactstrap"
import { InputDefault, LinkTo } from "../../"
import {
    awaitable,
    elm,
    fromServer,
    hasKey,
    tryCatch,
} from "../../../assets/helpers"
import { Context } from "../../../store"
import { MySwal } from "../../../assets/sweetalert"
import { useRouter } from "next/router"
import ClipLoader from "react-spinners/ClipLoader"

const AlterOption = () => {
    const [{ token }, dispatch] = useContext(Context)
    const [values, setValues] = useState([])
    const router = useRouter()
    const [loaderState, setLoaderState] = useState(false)

    const { id, name, typesaved } = router.query

    useEffect(() => {
        setLoaderState(true)
        console.log({ id })

        awaitable(async () => {
            const dataOptions = await fromServer(
                `/options_name/${id}`,
                token,
                "get"
            )
            console.log({ dataOptions })
            if (dataOptions.length > 0 && !hasKey(dataOptions, "error")) {
                setValues(dataOptions)
                setLoaderState(false)
            } else {
                console.log("fail to load options", { dataOptions })
                setLoaderState(false)
            }
            setLoaderState(false)
        })
    }, [])

    function remove(id) {
        return async (e) => {
            e.preventDefault()
            setLoaderState(true)
            const current = values.find(
                ({ id: _id }) => Number(id) === Number(_id)
            )
            if (!!current?.created) {
                console.log("!!current", { id, current, values })
                setValues(
                    values.filter(({ id: _id }) => Number(id) !== Number(_id))
                )
                setLoaderState(false)
            } else {
                console.log("!current", { id, current, values })
                const resolve = await fromServer(
                    `/options_name/${id}`,
                    token,
                    "delete"
                )
                if (resolve?.error) {
                    console.log("error", { resolve })
                    await MySwal.fire(
                        "Não foi possivel remover este atributo",
                        "",
                        "error"
                    )
                    setLoaderState(false)
                } else {
                    const newvalues = values.filter((it) => it.id !== id)
                    setValues(newvalues)
                    console.log("success", { resolve })
                    await MySwal.fire(
                        "Atributo removido com sucesso",
                        "",
                        "success"
                    )
                    setLoaderState(false)
                }
                setLoaderState(false)
            }
        }
    }

    const add = useCallback(
        (e) => {
            e.preventDefault()
            setValues([
                ...values,
                {
                    id: !values?.length
                        ? 1
                        : Math.max(...values.map(({ id }) => Number(id))) + 1,
                    value: "",
                    created: true,
                },
            ])
        },
        [values]
    )
    const save = async () => {
        setLoaderState(true)
        let payload = null
        payload = { name: elm("[name=name]")?.value }
        console.log({ payload })
        const response = await fromServer(
            `/options/${id}`,
            token,
            "PUT",
            payload
        )

        if (response?.error) {
            console.log("error: ", { response })
            await MySwal.fire(
                "Não foi possivel editar esta característica, tente novamente",
                "",
                "error"
            )
            setLoaderState(false)
        } else {
            console.log("success", { response })
            tryCatch(async () => {
                const valuesToUpdate = values.filter((value) => !value?.created)
                const valuesToCreate = values
                    .filter((value) => !!value?.created)
                    ?.map(({ id: _id }) => ({
                        name: elm(`[name=value${_id}]`)?.value,
                        fk_options: Number(id),
                    }))
                console.log({ valuesToUpdate, valuesToCreate })
                for (let i = 0; i < valuesToUpdate.length; i++) {
                    const optNode = elm(`[name=value${valuesToUpdate[i].id}]`)
                    const responseOP = await fromServer(
                        `/options_name/${valuesToUpdate[i].id}`,
                        token,
                        "PUT",
                        {
                            name: optNode?.value,
                            fk_options: Number(id),
                        }
                    )

                    if (responseOP?.error) {
                        console.log("error: ", { responseOP })
                        await MySwal.fire(
                            `Não foi possivel editar os valores ${
                                optNode?.value
                            }, apenas o nome da característica, tente novamente`,
                            "",
                            "error"
                        )
                        setLoaderState(false)
                        return false
                    } else {
                        console.log("success", { responseOP })
                    }
                }
                const creationResponse = await fromServer(
                    `/options_name`,
                    token,
                    "POST",
                    valuesToCreate
                )
                if (!creationResponse && hasKey(creationResponse, "error")) {
                    await MySwal.fire(
                        creationResponse?.error?.message ||
                            "Não foi possivel adicionar as caracteristicas novas",
                        "",
                        "success"
                    )
                }
                await MySwal.fire(
                    "Característica editada com sucesso",
                    "",
                    "success"
                )
                await router.push("/account/[feature]", "/account/options")
            })
        }
        setLoaderState(false)
    }
    const goBack = (e) => {
        setLoaderState(true)
    }
    return (
        <>
            {loaderState && (
                <div className="spinnerLoader">
                    <ClipLoader
                        size={150}
                        color={"var(--red-1)"}
                        loading={loaderState}
                    />
                </div>
            )}
            <Row>
                <Col xs="12" className="mb-4">
                    <h1>Editar Característica</h1>
                </Col>
                <Col xs="12">
                    <Form>
                        <Row form className="mb-4">
                            <Col xs="6">
                                <InputDefault
                                    initial_value={name}
                                    name={"name"}
                                    type={"text"}
                                    has_label={true}
                                    label={"Nome da característica*"}
                                    placeholder={"Exemplo: Cor"}
                                />
                            </Col>
                        </Row>
                        {values?.map(({ name, id }) => {
                            return (
                                <Row key={id}>
                                    <Col xs={10}>
                                        <InputDefault
                                            initial_value={name}
                                            name={`value${id}`}
                                            id={`value${id}`}
                                            type={"text"}
                                            has_label={true}
                                            label={"Atributo*"}
                                            placeholder={"Exemplo: Azul"}
                                        />
                                    </Col>
                                    <Col xs="2" className="allignerCenter">
                                        <Button
                                            className="buttonSave"
                                            style={{ marginTop: "0.75rem" }}
                                            onClick={remove(id)}
                                        >
                                            Remover
                                        </Button>
                                    </Col>
                                </Row>
                            )
                        })}
                    </Form>
                </Col>
                <Col xs="12" style={{ marginTop: "1rem" }}>
                    <Row>
                        <Col md="12">
                            <div
                                className={`w-100 mx-auto m-0 border bg-white text_light_gray text-center p-5 fix`}
                                style={{ cursor: "pointer" }}
                                onClick={add}
                            >
                                <div
                                    className={
                                        "font-weight-bold text_dark_2 ml-1 fix"
                                    }
                                >
                                    <u>
                                        Clique aqui para adicionar um novo
                                        atributo
                                    </u>
                                </div>
                            </div>
                        </Col>
                    </Row>
                </Col>
                <Col
                    xs="4"
                    className="allignerCenter"
                    style={{ marginTop: "3rem" }}
                >
                    <Button
                        className="buttonSave"
                        style={{ width: "100%" }}
                        onClick={save}
                    >
                        SALVAR
                    </Button>
                </Col>
                <Col xs="4" style={{ marginTop: "3rem" }}>
                    <LinkTo
                        _href={"/account/[feature]"}
                        as={"/account/options"}
                        query={token}
                    >
                        <button
                            size="lg"
                            className="btn btn-block btn-outline-dark h-100 rounded-0"
                            onClick={goBack}
                        >
                            VOLTAR
                        </button>
                    </LinkTo>
                </Col>
            </Row>
        </>
    )
}

export default AlterOption
