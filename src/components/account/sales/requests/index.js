import React, {useContext} from "react"
import {BoardedWrapBox_styled} from "../styled-components/"
import {Img} from "../../../"
import {BsThreeDotsVertical} from "react-icons/bs"
import {
    brlFormat,
    disable_btn_and_put_innerHTML,
    elm,
    enable_btn_and_put_innerHTML,
    fromServer,
    hasKey,
} from "../../../../assets/helpers"
import {Col, Row} from "reactstrap"
import {Context} from "../../../../store"
import Swal from "sweetalert2"
import withReactContent from "sweetalert2-react-content"
import LinkTo from "../../../LinkTo";
import {useRouter} from 'next/router'

const Sales = ({
                   name: exibition_name,
                   cod,
                   code,
                   price,
                   id,
                   data,
                   situation,
                   url,
                   nfe = false,
                   tracking = false,
               }) => {
    const [{token}] = useContext(Context)
    const router = useRouter()
    const opt = {
        showCloseButton: true,
        width: "40rem",
        height: "50vh",
        padding: "5rem",
        customClass: {
            popup: "popup-class",
            confirmButton: "btn btn-block bg_green btn_save_code",
        },
        keydownListenerCapture: true,
        allowEnterKey: true,
        title: "Inserir código de rastreio:",
        html: `
           <form class="container">
                <input style="border: 1px dashed #AAAAAA;" type="text" id="code-swal2-input" class="swal2-input" placeholder="Informe o código de rastreio aqui"/>
           </form>
`,
        focusConfirm: true,
        confirmButtonText: "SALVAR",
        preConfirm: async (e) => {
            disable_btn_and_put_innerHTML(".btn_save_code")
            const [{value: code}] = [elm("#code-swal2-input")]
            console.log("preConfirm", {e}, {code})
            if (!code || code === "") {
                enable_btn_and_put_innerHTML(".btn_save_code")
                Swal.showValidationMessage(`Valor invalido: ${code}`)
            }
            const data = await fromServer(
                `/sales/tracking/${id}`,
                token,
                "PATCH",
                {
                    tracking: code,
                }
            )
            if (!data || hasKey(data, "error")) {
                enable_btn_and_put_innerHTML(".btn_save_code")
                Swal.showValidationMessage(`${data.error.message}`)
            }
            enable_btn_and_put_innerHTML(".btn_save_code")
            return data
        },
    }
    const Modal = withReactContent(Swal).mixin(opt)
    return (
        <Row className="w-100 p-4 mb-2">
            <Col xs={12} md={12} lg={4} className={"m-0 p-0 mr-lg-auto"}>
                <BoardedWrapBox_styled
                    className={"mr-lg-4 w-100 h-100"}
                    flex
                    padding="2rem 4rem"
                    width="34rem"
                    style={{minWidth: "auto"}}
                >
                    <Img
                        image_name_with_extension={
                            url !== null ? url : "130x130.png"
                        }
                        outSide={url !== null ? url : ""}
                        className="product_front img-fluid"
                    />
                    <h2 className="mb-0">{situation}</h2>
                    <small className="mb-3">{exibition_name}</small>
                    {/* <Img
                        className={"checked"}
                        alt={"ok"}
                        image_name_with_extension={"check.png"}
                    /> */}
                </BoardedWrapBox_styled>
            </Col>
            <Col xs={12} md={12} lg={8} className={"m-0 p-0 h-100"}>
                <BoardedWrapBox_styled
                    className={"ml-lg-4 w-100 heightBoxSales"}
                    flex
                    relative
                    width="100%"
                    align="self-start"
                    padding="0 4rem"
                    smallSize="1.5rem"
                    smallWeight="normal"
                    style={{minWidth: "auto"}}
                >
                    <h2>Pedido número - {`${cod}`}</h2>
                    <small>Data de compra {data}</small>
                    <small style={!tracking ? {marginBottom: "3rem"} : {}}>
                        Código do item: {`${code}`}
                    </small>
                    {!!tracking && (
                        <small style={{marginBottom: "3rem"}}>
                            Código de rastreio:
                            <a
                                target="_blank"
                                href={`https://websro.com.br/detalhes.php?P_COD_UNI=${tracking}`}
                            >
                                <u>{tracking}</u>
                            </a>
                        </small>
                    )}
                    <div className="d-flex flex-row w-100">
                        <BoardedWrapBox_styled
                            height="100%"
                            width="50%"
                            flex
                            padding="3rem"
                        >
                            <h5>
                                Total: <span>{brlFormat(price, true)}</span>
                            </h5>
                        </BoardedWrapBox_styled>
                    </div>
                    <div
                        className="absolute d-flex flex-column align-items-end"
                        style={{top: "1rem", right: "1rem"}}
                    >
                        <BsThreeDotsVertical
                            className="pointer"
                            size="2rem"
                            onClick={(event) => {
                                event.preventDefault()
                                elm(`#nf-${id}`) &&
                                elm(`#nf-${id}`)?.classList?.toggle(
                                    "d-none"
                                )
                                elm(`#finder-${id}`) &&
                                elm(`#finder-${id}`)?.classList?.toggle(
                                    "d-none"
                                )
                            }}
                        />

                        {(!!nfe && (
                            <a
                                style={{width: '10rem'}}
                                target="_blank"
                                href={nfe}
                                id={`nf-${id}`}
                                className=" bg-white d-none p-3 border text_dark text-center mr-4 mt-2"
                                download={true}
                            >
                                Visualizar NF-e
                            </a>
                        )) || (
                            <LinkTo query={{id, token}} _href={"/nf"} style={{width: '10rem'}}>
                                <p
                                    id={`nf-${id}`}
                                    className=" bg-white d-none p-3 border text_dark text-center mr-4 mt-2"
                                >
                                    Informar NF-e
                                </p>
                            </LinkTo>
                        )}
                        {!tracking && (
                            <button
                                style={{width: '10.8rem'}}
                                id={`finder-${id}`}
                                className="bg-white d-none p-3 border text_dark text-center mr-4"
                                onClick={async (e) => {
                                    e.preventDefault()
                                    const {value} = await Modal.fire(opt)
                                    console.log({value})
                                    !!value && router.push('/account/[feature]', `/account/sales?token=${token}`)
                                }}
                            >
                                Informar Codigo de Rastreio
                            </button>
                        )}
                    </div>
                </BoardedWrapBox_styled>
            </Col>
        </Row>
    )
}

export default Sales
