import React from "react"
import propTypes from "prop-types"
import styled from "styled-components"
import { OBJ } from "../../../../assets/helpers/"

const { hasKey: objHasKey } = OBJ
export const BoardedWrapBox_styled = styled.div`
    &{
        ${(props) => (!!props?.relative ? "position:relative;" : "")}
        height: ${(props) =>
            (objHasKey(props, "height") && props.height) || "auto"};
        width: ${(props) =>
            (objHasKey(props, "width") && props.width) || "auto"};
        min-width: max-content;
        border: solid 1px #dedede;
        background-color: #ffffff;
        ${(props) =>
            objHasKey(props, "margin") && props.margin
                ? `margin: ${props.margin};`
                : ""}
        display: ${(props) =>
            objHasKey(props, "flex") && props.flex ? "flex" : "block"};
        ${(props) =>
            objHasKey(props, "flex") && props.flex
                ? `flex-direction: ${
                      objHasKey(props, "row") && props.row ? "row" : "column"
                  };`
                : ""};
        &>div{
            display:flex;
            flex-direction:row;
            min-width:max-content;  
            align-items: end;
            
            & > img{
                height:calc(10rem*0.29122);
                margin-right:0.5rem;
                object-fit:contain;
            }
        }
        & > h4 {
            margin:0;
        }
        & > h4 + div {
           
         & >div >p{
            margin-bottom: 0.5rem;
         }
        }    
        & > h2{
            font-size: 2.5rem;
            font-family: Roboto;
            font-weight: bold;
            font-stretch: normal;
            font-style: normal;
            line-height: 1.31;
            letter-spacing: normal;
            text-align: center;
            color: #2b2b2b;
        }
        
        & > h5{
            font-family: Roboto;
            font-size: 2.25rem;
            font-weight: bold;
            font-stretch: normal;
            font-style: normal;
            line-height: 1.32;
            letter-spacing: normal;
            text-align: left;
            color: #2b2b2b;
            margin:0!important;
            & > span{
                color: #e3171b !important;   
            }
        }
        & > h2 + small {
            margin-top: -.5rem;
            font-weight: 500;
        }
        & > h3 {
            font-family: Roboto;
            font-size: 1.75rem;
            font-weight: bold;
            font-stretch: normal;
            font-style: normal;
            line-height: 1.3;
            letter-spacing: normal;
            text-align: left;
            color: #2b2b2b;
            margin: 0;
            &>span{
                color: #e3171b;
            }
        }
        & > h4 {
            font-size: 1rem;
            font-weight: 700;
            font-stretch: normal;
            font-style: normal;
            line-height: 1.38;
            letter-spacing: normal;
            text-align: left;
            color: #2b2b2b;
            
        }
        
        & > small{
            font-family: Roboto;
            font-size:${(props) =>
                (objHasKey(props, "smallSize") && props.smallSize) ||
                `1.75rem`};          
            font-weight:${(props) =>
                (objHasKey(props, "smallWeight") && props.smallWeight) ||
                ` bold`};          
            font-stretch: normal;          
            font-style: normal;          
            line-height: 1.3;          
            letter-spacing: normal;          
            text-align: center;          
            color: #757575;
            font-weight: normal;
        }
        padding: ${(props) =>
            (objHasKey(props, "padding") && props.padding) || "10px"};
        align-items: ${(props) =>
            (objHasKey(props, "align") && props.align) || "center"};
        justify-content: ${(props) =>
            (objHasKey(props, "justify") && props.justify) || "center"};
        &>img:first-child{
            height: 13rem;
            object-fit: contain;
        }
    }
`
export const DotCard = styled.li`
    & {
        width: 0.5rem;
        height: 0.5rem;
        background-color: #757575;
        border-radius: 100%;
        margin-right: 0.01rem;
    }
`
export const DotList = styled.ul`
    & {
        display: flex;
        flex-direction: row;
        list-style-type: none;
        aligm-items: center;
        margin: 0;
        li:nth-child(3n) {
            margin-right: 0.3rem;
        }
    }
`
export const CardInfo = styled.p`
    & {
        font-family: Roboto;
        font-size: 13px;
        font-weight: normal;
        font-stretch: normal;
        font-style: normal;
        line-height: ${(props) =>
            (objHasKey(props, "lineHeight") && props.lineHeight) || "1.38"};
        letter-spacing: normal;
        text-align: left;
        color: #757575;
        margin: 0;
    }
`

function PointsComponent({ numbers }) {
    return (
        <div className="d-flex align-items-center">
            <DotList>
                <DotCard />
                <DotCard />
                <DotCard />
            </DotList>
            <DotList>
                <DotCard />
                <DotCard />
                <DotCard />
            </DotList>
            <DotList>
                <DotCard />
                <DotCard />
                <DotCard />
            </DotList>
            <CardInfo>{numbers}</CardInfo>
        </div>
    )
}

PointsComponent.propTypes = {
    numbers: propTypes.any.isRequired,
}
export const Points = PointsComponent
