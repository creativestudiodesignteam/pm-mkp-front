import React, {useContext, useEffect, useState} from "react"
import {Pagination} from "../../"
import {Alert, Col, Row} from "reactstrap"
import {awaitable, fromServer, hasKey, pag} from "../../../assets/helpers"
import ProductSold from "./requests/"
import {useRouter} from "next/router"
import {Context} from "../../../store"
import ClipLoader from "react-spinners/ClipLoader"

function Component() {
    const [{token}, dispatch] = useContext(Context)
    const [page, setPage] = useState(0)
    const [products, setProducts] = useState([])
    const router = useRouter()
    const [alert, setAlert] = useState(false)
    const toggle = () => setAlert(!alert)
    const [loaderState, setLoaderState] = useState(false)

    useEffect(() => {
        //console.log("Vendas", {products, page})
        if (products.length < 1) {
            toggle()
        }
    }, [products])

    function parseSalesFromServer(it) {
        return {
            nfe: it?.nfe,
            url: it?.products?.thumb,
            situation: it.situations,
            data: it.data,
            id: it.id,
            price: it.price,
            cod: it.hash,
            exibition_name: it.products.name,
            code: it.code,
            tracking: it?.tracking
        }
    }

    async function getSalesFromServer() {
        const data = await fromServer("/sales/my_sales", token)
        console.log({data})
        if (!!data && !hasKey(data, "error") && data.length > 0) {
            //console.log("ok", data)
            setProducts(data.map(parseSalesFromServer))
        } else {
            setAlert(true)
            console.log("fail", data)
        }
    }

    useEffect(() => {
        awaitable(async () => {
            setLoaderState(true)
            !products.length && (await getSalesFromServer())
            setLoaderState(false)
        })
    }, [])

    return (
        <>
            {loaderState && (
                <div className="spinnerLoader">
                    <ClipLoader
                        size={150}
                        color={"var(--red-1)"}
                        loading={loaderState}
                    />
                </div>
            )}
            <Row className={"m-0 pt-0 mb-3"}>
                <h1 className="page_title h1 m-0 mr-auto">
                    Historico de Vendas
                </h1>
            </Row>

            <Row className={"m-0 pt-0  mb-4"}>
                <div className="d-flex flex-wrap w-100">
                    {products.length < 1 && (
                        <Col md="12" className="w-100">
                            <Alert
                                color="primary"
                                isOpen={alert}
                                toggle={toggle}
                                style={{
                                    opacity: 1,
                                    color: "white",
                                    backgroundColor: "var(--red-1)",
                                    padding: "2rem",
                                    fontSize: "16px",
                                    textAlign: "center",
                                    width: "100%",
                                }}
                            >
                                Você ainda não realizou vendas!
                            </Alert>
                        </Col>
                    )}
                    {products.length > 0 &&
                    pag(
                        products,
                        page
                    ).map(
                        ({
                             id,
                             price,
                             cod,
                             code,
                             exibition_name,
                             data,
                             situation,
                             url,
                             nfe,
                             tracking = false
                         }) => (
                            <ProductSold
                                tracking={tracking}
                                code={code}
                                nfe={nfe}
                                url={url}
                                key={id}
                                situation={situation}
                                id={id}
                                data={data}
                                price={price}
                                cod={cod}
                                name={exibition_name}
                            />
                        )
                    )}
                </div>
            </Row>

            {parseInt(
                `${Math.ceil(
                    Number(Number(Number(products.length) / 6))
                )}`
            ) > 1 && <Row className={"m-0  mb-2 pt-0 w-100 "}>
                <nav className="mx-auto" aria-label="Page navigation example">
                    <Pagination
                        count={parseInt(
                            `${Math.ceil(
                                Number(Number(Number(products.length) / 6))
                            )}`
                        )}
                        Listener={(page_) => setPage(Number(page_ - 1))}
                    />
                </nav>
            </Row>}
        </>
    )
}

export default Component
