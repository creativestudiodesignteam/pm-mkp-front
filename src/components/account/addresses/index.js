import React, { useContext, useEffect, useState } from "react"
import {
    Alert,
    Button,
    Card,
    CardText,
    Col,
    Row,
    Label,
    Input,
    FormGroup,
} from "reactstrap"
import { Img, LinkTo } from "../../"
import styles from "./Addresses.module.css"
import { useRouter } from "next/router"
import { getToken, isAuthenticated, LogIn, logOut } from "../../../assets/auth"
import { Context } from "../../../store"
import { awaitable, fromServer, OBJ } from "../../../assets/helpers"
import { MySwal } from "../../../assets/sweetalert"
import ClipLoader from "react-spinners/ClipLoader"

const Addresses = () => {
    const [{ token, user }, dispatch] = useContext(Context)
    const router = useRouter()
    const [loaderState, setLoaderState] = useState(false)

    useEffect(() => {
        //console.log({ user })
        awaitable(async () => {
            setLoaderState(true)
            if (!addresses.length && token) {
                const data = await fromServer("/addresses", token, "get")
                if (
                    !OBJ.hasKey(data, "error") &&
                    data.length &&
                    OBJ.hasKey(data[0], "id") &&
                    OBJ.hasKey(data[0], "name")
                ) {
                    setdeliveryAddresses(data)
                    setLoaderState(false)
                } else {
                    setAlert(true)
                    setLoaderState(false)
                    console.log("fail to load addresses", { data })
                }
                setLoaderState(false)
            }
            setLoaderState(false)
        })
    }, [])

    const [addresses, setdeliveryAddresses] = useState([])

    const deleteAddress = (address) => (e) => {
        e.preventDefault()
        setLoaderState(true)
        awaitable(async () => {
            const data = await fromServer(
                `/addresses/${address.id}`,
                token,
                "delete"
            )
            if (!OBJ.hasKey(data, "error") && data && OBJ.hasKey(data, "msg")) {
                let newAddresses = addresses.filter((elem) => {
                    if (Number(elem.id) !== Number(address.id)) {
                        return elem
                    }
                })
                setdeliveryAddresses(newAddresses)
                await MySwal.fire(
                    "Endereço removido com sucesso",
                    ``,
                    "success"
                )
                setLoaderState(false)
            } else {
                await MySwal.fire(data.error, ``, "error")
                setLoaderState(false)
            }
            setLoaderState(false)
        })
    }
    const [alert, setAlert] = useState(false)
    const toggle = () => setAlert(!alert)

    const add = async (e) => {
        e.preventDefault()
        setLoaderState(true)
        await router.push("/account/[feature]", "/account/newaddress")
    }
    const alter = (e) => {
        setLoaderState(true)
    }
    const saveFavorite = (idFavorite) => async (e) => {
        e.preventDefault()
        setLoaderState(true)
        const data = await fromServer("/addresses/select", getToken(), "PUT", {
            fk_new_addresses: idFavorite,
        })

        const has_error = OBJ.hasKey(data, "error")
        has_error && console.log("fail to update selected address", data.error)
        if (!has_error) {
            setdeliveryAddresses(
                addresses.map((addr) => {
                    if (addr.select_at) {
                        return {
                            ...addr,
                            select_at: false,
                        }
                    }
                    if (addr.id === idFavorite) {
                        return data
                    }
                    return addr
                })
            )
            setLoaderState(false)
        }
        setLoaderState(false)
    }

    const saveAddressStore = (idFavorite) => async (e) => {
        e.preventDefault()
        setLoaderState(true)
        const data = await fromServer(
            `/addresses/store/${idFavorite}`,
            getToken(),
            "PATCH"
        )

        const has_error = OBJ.hasKey(data, "error")
        has_error && console.log("fail to update selected address", data.error)
        if (!has_error) {
            setdeliveryAddresses(
                addresses.map((addr) => {
                    if (addr.selected_store) {
                        return {
                            ...addr,
                            selected_store: false,
                        }
                    }
                    if (addr.id === idFavorite) {
                        return data
                    }
                    return addr
                })
            )
            setLoaderState(false)
        }
        setLoaderState(false)
    }

    return (
        <>
            {loaderState && (
                <div className="spinnerLoader">
                    <ClipLoader
                        size={150}
                        color={"var(--red-1)"}
                        loading={loaderState}
                    />
                </div>
            )}
            <Row>
                <Col md="12" style={{ marginBottom: "1rem" }}>
                    <h1>Endereços cadastrados</h1>
                </Col>
                {addresses.length < 1 && (
                    <Col md="12">
                        <Alert
                            color="primary"
                            isOpen={alert}
                            toggle={toggle}
                            style={{
                                opacity: 1,
                                color: "white",
                                backgroundColor: "var(--red-1)",
                                padding: "2rem",
                                fontSize: "16px",
                                textAlign: "center",
                            }}
                        >
                            Você ainda não possui endereços cadastrados!
                        </Alert>
                    </Col>
                )}

                {addresses.length > 0 &&
                    addresses.map((address, idx) => {
                        return (
                            <Col
                                md="12"
                                key={idx}
                                style={{ marginBottom: "2rem" }}
                            >
                                <Card body className={styles.cardBody}>
                                    <Row>
                                        <Col
                                            md="12"
                                            className="showTabletAndLess"
                                        >
                                            <div className="mb-4">
                                                <LinkTo
                                                    _href="/account/[feature]"
                                                    as={"/account/alteraddress"}
                                                    query={{
                                                        name: address.name,
                                                        street: address.street,
                                                        number: address.number,
                                                        neighborhood:
                                                            address.neighborhood,
                                                        city: address.city,
                                                        state: address.state,
                                                        postcode:
                                                            address.postcode,
                                                        id: address.id,
                                                    }}
                                                >
                                                    <Button
                                                        outline
                                                        size="lg"
                                                        className={
                                                            styles.buttonSave
                                                        }
                                                        onClick={alter}
                                                    >
                                                        ALTERAR ENDEREÇO
                                                    </Button>
                                                </LinkTo>

                                                <Button
                                                    size="lg"
                                                    className={
                                                        styles.buttonDelete
                                                    }
                                                    onClick={deleteAddress(
                                                        address
                                                    )}
                                                >
                                                    EXCLUIR
                                                </Button>
                                            </div>
                                            <div className="mb-4">
                                                <h1>Endereço de Entrega</h1>
                                            </div>
                                            <div className="mb-4">
                                                <CardText
                                                    className={styles.cardName}
                                                >
                                                    {address.name}
                                                </CardText>
                                                <CardText
                                                    className={styles.cardText}
                                                >
                                                    {address.street},{" "}
                                                    {address.number},{" "}
                                                    {address.complement}
                                                </CardText>
                                                <CardText
                                                    className={styles.cardText}
                                                >
                                                    {address.neighborhood}
                                                </CardText>
                                                <CardText
                                                    className={styles.cardText}
                                                >
                                                    {address.city} -{" "}
                                                    {address.state}
                                                </CardText>
                                                <CardText
                                                    className={styles.cardText}
                                                >
                                                    CEP: {address.postcode}
                                                </CardText>

                                                {address.select_at && (
                                                    <CardText
                                                        id={`address-${idx}1`}
                                                        className={
                                                            styles.cardTextPrincipalAddress
                                                        }
                                                    >
                                                        <Row
                                                            style={{
                                                                paddingLeft:
                                                                    "15px",
                                                                paddingTop:
                                                                    "10px",
                                                            }}
                                                        >
                                                            <div
                                                                className={
                                                                    styles.circle
                                                                }
                                                                style={{
                                                                    backgroundColor:
                                                                        "green",
                                                                }}
                                                            />
                                                            Endereço principal
                                                            de Entrega
                                                        </Row>
                                                    </CardText>
                                                )}
                                                {!address.select_at && (
                                                    <CardText
                                                        onClick={saveFavorite(
                                                            address.id
                                                        )}
                                                        id={`address-${idx}`}
                                                        className={
                                                            styles.cardTextListAddress
                                                        }
                                                    >
                                                        <Row
                                                            style={{
                                                                paddingLeft:
                                                                    "15px",
                                                                paddingTop:
                                                                    "10px",
                                                            }}
                                                        >
                                                            <div
                                                                className={
                                                                    styles.circle
                                                                }
                                                                style={{
                                                                    border:
                                                                        "2px solid red",
                                                                }}
                                                            />
                                                            Endereço principal
                                                            de Entrega
                                                        </Row>
                                                    </CardText>
                                                )}
                                                {address.selected_store &&
                                                    user?.type && (
                                                        <CardText
                                                            id={`address-${idx}1`}
                                                            className={
                                                                styles.cardTextPrincipalAddress
                                                            }
                                                        >
                                                            <Row
                                                                style={{
                                                                    paddingLeft:
                                                                        "15px",
                                                                    paddingTop:
                                                                        "10px",
                                                                }}
                                                            >
                                                                <div
                                                                    className={
                                                                        styles.circle
                                                                    }
                                                                    style={{
                                                                        backgroundColor:
                                                                            "green",
                                                                    }}
                                                                />
                                                                Endereço
                                                                principal da
                                                                Loja
                                                            </Row>
                                                        </CardText>
                                                    )}
                                                {!address.selected_store &&
                                                    user?.type && (
                                                        <CardText
                                                            onClick={saveAddressStore(
                                                                address.id
                                                            )}
                                                            id={`address-${idx}`}
                                                            className={
                                                                styles.cardTextListAddress
                                                            }
                                                        >
                                                            <Row
                                                                style={{
                                                                    paddingLeft:
                                                                        "15px",
                                                                    paddingTop:
                                                                        "10px",
                                                                }}
                                                            >
                                                                <div
                                                                    className={
                                                                        styles.circle
                                                                    }
                                                                    style={{
                                                                        border:
                                                                            "2px solid red",
                                                                    }}
                                                                />
                                                                Endereço
                                                                principal da
                                                                Loja
                                                            </Row>
                                                        </CardText>
                                                    )}
                                            </div>
                                        </Col>

                                        <Col md="6" className="showAboveTablet">
                                            <div className="mb-4">
                                                <LinkTo
                                                    _href="/account/[feature]"
                                                    as={"/account/alteraddress"}
                                                    query={{
                                                        name: address.name,
                                                        street: address.street,
                                                        number: address.number,
                                                        neighborhood:
                                                            address.neighborhood,
                                                        city: address.city,
                                                        state: address.state,
                                                        postcode:
                                                            address.postcode,
                                                        id: address.id,
                                                    }}
                                                >
                                                    <Button
                                                        outline
                                                        size="lg"
                                                        className={
                                                            styles.buttonSave
                                                        }
                                                        onClick={alter}
                                                    >
                                                        ALTERAR ENDEREÇO
                                                    </Button>
                                                </LinkTo>

                                                <Button
                                                    size="lg"
                                                    className={
                                                        styles.buttonDelete
                                                    }
                                                    onClick={deleteAddress(
                                                        address
                                                    )}
                                                >
                                                    EXCLUIR
                                                </Button>
                                            </div>
                                            <div className="mb-4">
                                                <h1>Endereço de Entrega</h1>
                                            </div>
                                            <div className="mb-4">
                                                <CardText
                                                    className={styles.cardName}
                                                >
                                                    {address.name}
                                                </CardText>
                                                <CardText
                                                    className={styles.cardText}
                                                >
                                                    {address.street},{" "}
                                                    {address.number},{" "}
                                                    {address.complement}
                                                </CardText>
                                                <CardText
                                                    className={styles.cardText}
                                                >
                                                    {address.neighborhood}
                                                </CardText>
                                                <CardText
                                                    className={styles.cardText}
                                                >
                                                    {address.city} -{" "}
                                                    {address.state}
                                                </CardText>
                                                <CardText
                                                    className={styles.cardText}
                                                >
                                                    CEP: {address.postcode}
                                                </CardText>

                                                {address.select_at && (
                                                    <CardText
                                                        id={`address-${idx}1`}
                                                        className={
                                                            styles.cardTextPrincipalAddress
                                                        }
                                                    >
                                                        <Row
                                                            style={{
                                                                paddingLeft:
                                                                    "15px",
                                                                paddingTop:
                                                                    "10px",
                                                            }}
                                                        >
                                                            <div
                                                                className={
                                                                    styles.circle
                                                                }
                                                                style={{
                                                                    backgroundColor:
                                                                        "green",
                                                                }}
                                                            />
                                                            Endereço principal
                                                            de Entrega
                                                        </Row>
                                                    </CardText>
                                                )}
                                                {!address.select_at && (
                                                    <CardText
                                                        onClick={saveFavorite(
                                                            address.id
                                                        )}
                                                        id={`address-${idx}`}
                                                        className={
                                                            styles.cardTextListAddress
                                                        }
                                                    >
                                                        <Row
                                                            style={{
                                                                paddingLeft:
                                                                    "15px",
                                                                paddingTop:
                                                                    "10px",
                                                            }}
                                                        >
                                                            <div
                                                                className={
                                                                    styles.circle
                                                                }
                                                                style={{
                                                                    border:
                                                                        "2px solid red",
                                                                }}
                                                            />
                                                            Endereço principal
                                                            de Entrega
                                                        </Row>
                                                    </CardText>
                                                )}
                                                {address.selected_store &&
                                                    user?.type && (
                                                        <CardText
                                                            id={`address-${idx}1`}
                                                            className={
                                                                styles.cardTextPrincipalAddress
                                                            }
                                                        >
                                                            <Row
                                                                style={{
                                                                    paddingLeft:
                                                                        "15px",
                                                                    paddingTop:
                                                                        "10px",
                                                                }}
                                                            >
                                                                <div
                                                                    className={
                                                                        styles.circle
                                                                    }
                                                                    style={{
                                                                        backgroundColor:
                                                                            "green",
                                                                    }}
                                                                />
                                                                Endereço
                                                                principal da
                                                                Loja
                                                            </Row>
                                                        </CardText>
                                                    )}
                                                {!address.selected_store &&
                                                    user?.type && (
                                                        <CardText
                                                            onClick={saveAddressStore(
                                                                address.id
                                                            )}
                                                            id={`address-${idx}`}
                                                            className={
                                                                styles.cardTextListAddress
                                                            }
                                                        >
                                                            <Row
                                                                style={{
                                                                    paddingLeft:
                                                                        "15px",
                                                                    paddingTop:
                                                                        "10px",
                                                                }}
                                                            >
                                                                <div
                                                                    className={
                                                                        styles.circle
                                                                    }
                                                                    style={{
                                                                        border:
                                                                            "2px solid red",
                                                                    }}
                                                                />
                                                                Endereço
                                                                principal da
                                                                Loja
                                                            </Row>
                                                        </CardText>
                                                    )}
                                            </div>
                                        </Col>
                                        <Col md="6" className="showAboveTablet">
                                            <div>
                                                <Img
                                                    image_name_with_extension="address@3x.png"
                                                    alt="insert a new product"
                                                />
                                            </div>
                                        </Col>
                                    </Row>
                                </Card>
                            </Col>
                        )
                    })}
                <Col md="12" style={{ marginTop: "1rem" }}>
                    <Row>
                        <Col md="12">
                            <div
                                className={`w-100 mx-auto m-0 border bg-white text_light_gray text-center p-5 fix`}
                                style={{ cursor: "pointer" }}
                            >
                                <div
                                    onClick={add}
                                    className={
                                        "font-weight-bold text_dark_2 ml-1 fix"
                                    }
                                >
                                    <u>
                                        Clique aqui para adicionar um endereço
                                    </u>
                                </div>
                            </div>
                        </Col>
                    </Row>
                </Col>
            </Row>
        </>
    )
}

export default Addresses
