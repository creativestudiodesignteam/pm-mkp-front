import React, { useContext, useEffect, useState } from "react"
import { Button, Col, Form, Row } from "reactstrap"
import { InputDefault, SelectDefault } from "../.."
import { useRouter } from "next/router"
import { getToken, isAuthenticated, LogIn, logOut } from "../../../assets/auth"
import { Context } from "../../../store"
import {
    add_invalid,
    add_valid,
    cepBlur,
    defaultListener,
    elm,
    fromServer,
    OBJ,
    obrigatory_blur,
    postcode,
    state_address,
} from "../../../assets/helpers"
import { MySwal } from "../../../assets/sweetalert"
import ClipLoader from "react-spinners/ClipLoader"

const NewAddress = () => {
    const [{ token, user }, dispatch] = useContext(Context)
    const [state, setState] = useState({
        fk_users: user.id,
        name: "",
        postcode: "",
        state: "",
        city: "",
        neighborhood: "",
        street: "",
        number: "",
        complenent: "",
        references: "",
    })
    const router = useRouter()
    const [loaderState, setLoaderState] = useState(false)

    useEffect(() => {
        //console.log({ token, dispatch })
        setLoaderState(true)
        if (token) {
            // Tem Token
            console.log("Tem Token")
            setLoaderState(false)
        } else {
            // Não tem Token
            console.log("Não Tem Token")
            const _user = isAuthenticated(token)
            if (!_user) {
                logOut(dispatch, router)
                setLoaderState(false)
            } else {
                LogIn(token || getToken(), _user, dispatch)
                setLoaderState(false)
            }
        }
        setLoaderState(false)
    }, [])

    const save = async (event) => {
        event.preventDefault()
        setLoaderState(true)
        const btn = elm(".buttonSave")
        btn.disabled = true
        btn.innerHTML = `Carregando ... <div class="spinner-border text-light" role="status"><span class="sr-only">Loading...</span></div>`
        const {
            street,
            references,
            state: address_state,
            city,
            number,
            postcode,
            name,
            neighborhood,
        } = state
        const payload = {
            fk_users: user.id,
            street,
            complement: elm("[name=complement").value,
            references,
            state: address_state,
            city,
            number,
            postcode,
            name,
            neighborhood,
        }
        console.log({ payload })
        const data = await fromServer("/addresses", token, "post", payload)
        console.log("data", { data })
        if (!OBJ.hasKey(data, "error")) {
            btn.disabled = true
            btn.innerHTML = "SALVAR"
            await MySwal.fire("Endereço cadastrado com sucesso", ``, "success")
            await router.push("/account/[feature]", "/account/address")
        } else {
            btn.disabled = true
            btn.innerHTML = "SALVAR"
            await MySwal.fire(
                "Não foi possível cadastrar este endereço, tente novamente",
                ``,
                "error"
            )
            setLoaderState(false)
        }
        setLoaderState(false)

        return false
    }

    return (
        <>
            {loaderState && (
                <div className="spinnerLoader">
                    <ClipLoader
                        size={150}
                        color={"var(--red-1)"}
                        loading={loaderState}
                    />
                </div>
            )}
            <Row id="create_address">
                <Col md="12" style={{ marginBottom: "2rem" }}>
                    <h1>Novo Endereço</h1>
                </Col>
                <Col md="12">
                    <Form onSubmit={save} autoComplete="off">
                        <Row form className="mb-4">
                            <Col md={6}>
                                <InputDefault
                                    uncontrolled
                                    _value={state.name}
                                    name={"name"}
                                    type={"text"}
                                    has_label
                                    label={"De onde é este endereço? *"}
                                    _required
                                    blur={(e) =>
                                        obrigatory_blur(e, false, false)
                                    }
                                    _handle={defaultListener(setState, state)}
                                />
                            </Col>
                            <Col md={6}>
                                <InputDefault
                                    has_label
                                    className="verify"
                                    label={"CEP*"}
                                    name={"postcode"}
                                    _id={"postcode"}
                                    type={"text"}
                                    initial_value={state.postcode}
                                    _required
                                    onBlur={async (e) => {
                                        const result = await cepBlur(e, {
                                            state_address: "state",
                                        })
                                        if (result === false) {
                                            console.log("result false", {
                                                result,
                                            })
                                            add_invalid(
                                                elm("[name=postcode]"),
                                                "CEP invalido !",
                                                ""
                                            )
                                            setState({
                                                ...state,
                                                street: "",
                                                city: "",
                                                state: "",
                                                neighborhood: "",
                                                postcode: "",
                                            })
                                            elm("[name=postcode]").value = ""
                                        } else {
                                            add_valid(elm("[name=postcode]"))
                                            setState({
                                                ...state,
                                                street: result?.street || "",
                                                city: result?.city || "",
                                                state: result?.state || "",
                                                neighborhood:
                                                    result?.neighborhood || "",
                                            })
                                            elm("[name=state]").value =
                                                result?.state || ""
                                        }
                                        console.log("out", result)
                                    }}
                                    onKeyUp={postcode}
                                    options={{
                                        delimiters: ["-"],
                                        blocks: [5, 3],
                                    }}
                                    onChange={defaultListener(setState, state)}
                                />
                            </Col>
                            <Col md={6}>
                                <InputDefault
                                    name={"street"}
                                    type={"text"}
                                    has_label
                                    label={"Rua*"}
                                    _required
                                    blur={(e) =>
                                        obrigatory_blur(e, false, false)
                                    }
                                    uncontrolled
                                    _handle={defaultListener(setState, state)}
                                    _value={state.street}
                                />
                            </Col>
                            <Col md={6}>
                                <InputDefault
                                    name={"number"}
                                    type={"number"}
                                    has_label
                                    label={"Número*"}
                                    _required
                                    blur={(e) =>
                                        obrigatory_blur(e, false, false)
                                    }
                                    uncontrolled
                                    _handle={defaultListener(setState, state)}
                                    _value={state.number}
                                />
                            </Col>
                            <Col md={6}>
                                <InputDefault
                                    name={"complement"}
                                    _id={"complement"}
                                    type={"text"}
                                    has_label
                                    initial_value={state.complement}
                                    label={"Complemento"}
                                />
                            </Col>
                            <Col md={6}>
                                <InputDefault
                                    name={"references"}
                                    _id={"references"}
                                    type={"text"}
                                    has_label
                                    uncontrolled
                                    _handle={defaultListener(setState, state)}
                                    _value={state.references}
                                    label={"Informações de referência"}
                                />
                            </Col>
                            <Col md={6}>
                                <InputDefault
                                    name={"neighborhood"}
                                    type={"text"}
                                    uncontrolled
                                    _value={state.neighborhood}
                                    _handle={defaultListener(setState, state)}
                                    has_label
                                    label={"Bairro*"}
                                    _required
                                    blur={(e) =>
                                        obrigatory_blur(e, false, false)
                                    }
                                />
                            </Col>
                            <Col md={6}>
                                <InputDefault
                                    name={"city"}
                                    type={"text"}
                                    has_label
                                    label={"Cidade*"}
                                    _required
                                    blur={(e) =>
                                        obrigatory_blur(e, false, false)
                                    }
                                    uncontrolled
                                    _value={state.city}
                                    _handle={defaultListener(setState, state)}
                                />
                            </Col>
                            <Col md={6}>
                                {
                                    <SelectDefault
                                        has_label
                                        label={"Estado*"}
                                        id={"state"}
                                        name={"state"}
                                        placeholder={"Selecione o seu estado"}
                                        _required
                                        blur={(e) =>
                                            obrigatory_blur(e, false, false)
                                        }
                                        initial_value={state}
                                        values={state_address}
                                        listener={(e) => {
                                            setState({
                                                ...state,
                                                state: String(
                                                    e.target.selectedOptions[0]
                                                        .value
                                                ),
                                            })
                                        }}
                                    />
                                }
                            </Col>
                        </Row>
                        <Row>
                            <Col md="4" className="mt-4">
                                <Button
                                    size="lg"
                                    className="buttonSave"
                                    type={"submit"}
                                >
                                    SALVAR
                                </Button>
                            </Col>
                        </Row>
                    </Form>
                </Col>
            </Row>
        </>
    )
}

export default NewAddress
