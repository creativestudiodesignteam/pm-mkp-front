/*global FormData*/
import React, { useContext, useEffect, useState } from "react"
import { Button, Col, FormGroup, Label, Row } from "reactstrap"
import styles from "./MyAccount.module.css"
import { InputDefault, SelectCustom, SelectDefault } from "../.."
import { useRouter } from "next/router"
import {
    awaitable,
    birthday,
    cpf_blur,
    cpf_rg,
    dataURItoBlob,
    dateChecker,
    elm,
    email_blur,
    fromServer,
    hasKey,
    modalidades,
    OBJ,
    onKeyUpTelephone,
    onlyNumber,
    ramos,
    site_blur,
} from "../../../assets/helpers"
import { Context, dispatchFunctions } from "../../../store"
import { getToken, isAuthenticated, LogIn, logOut } from "../../../assets/auth"
import { MySwal } from "../../../assets/sweetalert"
import PhotoUploader from "./../../photoUploader"
import ClipLoader from "react-spinners/ClipLoader"

const MyAccount = () => {
    const Router = useRouter()
    const [modalidade, setModalidade] = useState({})
    const [user, setUser] = useState({})
    const [{ token, navbar_props }, dispatch] = useContext(Context)
    const [state, setState] = useState({})
    const [fields, setFields] = useState([])
    const [provider_ramos_server, setProvider_ramos_server] = useState([])
    const [ramosPayload, setRamosPayload] = useState([])
    const [thumb, setThumb] = useState("")
    const [loaderState, setLoaderState] = useState(false)

    const onSelectRamos = (ramosSelected) => {
        let selected = ramosSelected.map((ramo) => ramo.name)
        console.log({ selected })
        setRamosPayload(selected)
    }
    const save = async (e) => {
        e.preventDefault()
        setLoaderState(true)
        const btn = elm("button.btn-save")
        btn.disabled = true
        btn.innerHTML = `Carregando ... <div class="spinner-border text-light" role="status"><span class="sr-only">Loading...</span></div>`
        console.log({ state })
        let payload = {}
        if (!!thumb.length) {
            let formData = new FormData()
            let avatar = false
            let file = await dataURItoBlob(thumb)
            formData.append("file", file)
            avatar = await fromServer("/files", false, "POST", formData)
            console.log("AFTER", { avatar, formData })
            if (hasKey(avatar, "error")) {
                console.log({
                    error: avatar.error,
                    formData,
                    file: file,
                    thumb,
                })
            }
            !!avatar?.error?.message &&
                (await MySwal.fire(avatar.error.message, "", "error"))
            if (!!avatar.id) {
                payload = { ...payload, fk_avatar: avatar.id }
            }
        } else if (user?.provider?.avatar?.id) {
            console.log(user.provider.avatar.id)
            payload = {
                ...payload,
                fk_avatar: user.provider.avatar.id,
            }
        }
        payload = {
            ...payload,
            type: true,
            data: {
                county_register: elm("[name=county_register]").value,
                fantasy_name: elm("[name=fantasy_name]").value,
                reason_social: elm("[name=reason_social]").value,
                state_register: elm("[name=state_register]").value,
                telephone_commercial: elm("[name=telephone_commercial]").value,
                telephone_whatsapp: elm("[name=telephone_whatsapp]").value,
                data_fundacao: elm("[name=data_fundacao]").value,
                qty_funcionarios: elm("[name=qty_funcionarios]").value,
                site: elm("[name=site]").value,
                modalidade: modalidade.name,
                responsible: {
                    name: elm("[name=name]").value,
                    cpf: elm("[name=cpf]").value,
                    email: elm("[name=email_responsible]").value,
                    telephone: elm("[name=telephone_responsible]").value,
                    cargo: elm("[name=cargo]").value,
                },
                ramos: ramosPayload,
            },
        }
        if (state?.password?.length) {
            payload = {
                ...payload,
                password: state.password,
                confirmPassword: state.password,
                oldPassword: elm("[name=password_atual]").value,
            }
        }
        console.log({ payload })

        const saveData = await fromServer(`/users`, token, "put", payload)
        console.log("saveData", { saveData })
        if (!OBJ.hasKey(saveData, "error") && saveData && saveData.success) {
            console.log("sucesso")
            const data = await fromServer("/users", token)
            if (!hasKey(data, "error") && !!data?.provider?.avatar?.url) {
                console.log("users")
                dispatch(dispatchFunctions.navbar_props(data))
            }
            await MySwal.fire("Dados atualizados com sucesso", ``, "success")
            setLoaderState(false)
        } else {
            await MySwal.fire(
                "Não foi possível atualizar seus dados, tente novamente",
                ``,
                "error"
            )
            console.log("fail to load addresses")
            setLoaderState(false)
        }
        btn.innerHTML = " SALVAR"
        btn.disabled = false
        setLoaderState(false)
    }

    function defaultListener(e) {
        e.preventDefault()
        //console.log(e.target.name, e.target.value)
        setState({ ...state, [e.target.name]: e.target.value })
    }

    useEffect(() => {
        setLoaderState(true)
        awaitable(async () => {
            if (token) {
                if (Object.keys(user).length === 0) {
                    const user_server = await fromServer(`/users`, token)
                    console.log({ user_server })
                    if (!!user_server && !hasKey(user_server, "error")) {
                        setUser(user_server)
                    }
                }
                setLoaderState(false)
            } else {
                console.log("não tem token", token)
                const _user = isAuthenticated()
                if (!_user) {
                    logOut(dispatch, Router)
                } else {
                    LogIn(getToken(), _user, dispatch)
                }
            }
            setLoaderState(false)
        })
        setLoaderState(false)
    }, [token])
    useEffect(() => {
        setLoaderState(true)
        console.log("user was changed", { user }, user?.provider)
        console.log({ token, user, state })
        if (OBJ.hasKey(user, "provider")) {
            setState({
                state_register: user.provider.state_register,
                fantasy_name: user.provider.fantasy_name,
                reason_social: user.provider.reason_social,
                county_register: user.provider.county_register,
                telephone_commercial: user.provider.telephone_commercial
                    .replace("(", "")
                    .replace(")", "")
                    .replace(/[^\d|^\(|^\)|^\-|^\s]/g, ""),
                telephone_whatsapp: user.provider.telephone_whatsapp
                    .replace("(", "")
                    .replace(")", "")
                    .replace(/[^\d|^\(|^\)|^\-|^\s]/g, ""),
                data_fundacao: user.provider.data_fundacao,
                qty_funcionarios: user.provider.qty_funcionarios,
                site: user.provider.site,
                modalidade: user.provider.modalidade,
                name: user.provider.responsible[0]?.name,
                cpf: user.provider.responsible[0]?.cpf,
                email: user.provider.responsible[0]?.email,
                telefone: user.provider.responsible[0]?.telephone
                    .replace("(", "")
                    .replace(")", "")
                    .replace(/[^\d|^\(|^\)|^\-|^\s]/g, ""),
                cargo: user.provider.responsible[0]?.cargo,
                provider_ramos: user.provider.provider_ramos.map(
                    (ramo, idx) => ({
                        id: idx,
                        name: ramo.name,
                    })
                ),
            })
            setProvider_ramos_server(
                user.provider.provider_ramos.map((ramo, idx) => ({
                    id: idx,
                    name: ramo.name,
                }))
            )
            const fields = [
                <>
                    <Col xs="12" style={{ marginBottom: "2rem" }}>
                        <h1>Informações da empresa</h1>
                    </Col>
                    <Col
                        xs="12"
                        className="allignerCenter"
                        style={{ flexDirection: "column" }}
                    >
                        <PhotoUploader
                            className={"mb-4"}
                            label={"Logo da empresa"}
                            listener={(imageList) => {
                                if (!!imageList[0]?.dataURL) {
                                    setThumb(imageList[0].dataURL)
                                } else {
                                    setThumb("")
                                }
                            }}
                            defaultValue={
                                !!thumb.length
                                    ? [{ dataURL: thumb }]
                                    : !!user?.provider?.avatar?.url
                                    ? [{ dataURL: user.provider.avatar.url }]
                                    : []
                            }
                            maxNumber={1}
                            maxFileSize={5 * 1024 * 1024}
                            acceptType={["jpg", "png", "jpeg"]}
                        />
                    </Col>
                    <Col md="6">
                        <InputDefault
                            className={"mb-4"}
                            name={"fantasy_name"}
                            type={"text"}
                            has_label={true}
                            label={"Nome Fantasia"}
                            onChange={defaultListener}
                            initial_value={user.provider.fantasy_name}
                        />
                    </Col>
                    <Col md="6">
                        <InputDefault
                            className={"mb-4"}
                            name={"reason_social"}
                            type={"text"}
                            has_label={true}
                            label={"Razão Social"}
                            onChange={defaultListener}
                            initial_value={user.provider.reason_social}
                        />
                    </Col>
                    <Col md="6">
                        <InputDefault
                            className={"mb-4"}
                            name={"county_register"}
                            type={"text"}
                            has_label
                            label={"Inscrição Municipal"}
                            onChange={defaultListener}
                            initial_value={user.provider.county_register}
                        />
                    </Col>
                    <Col md="6">
                        <InputDefault
                            className={"mb-4"}
                            name={"state_register"}
                            type={"text"}
                            has_label
                            label={"Inscrição Estadual"}
                            onChange={defaultListener}
                            initial_value={user.provider.state_register}
                        />
                    </Col>
                    <Col md="6">
                        <InputDefault
                            className={"mb-4"}
                            name={"telephone_commercial"}
                            type={"tel"}
                            has_label={true}
                            label={"Telefone Comercial"}
                            onChange={defaultListener}
                            initial_value={user.provider.telephone_commercial
                                .replace("(", "")
                                .replace(")", "")
                                .replace(/[^\d|^\(|^\)|^\-|^\s]/g, "")}
                            onKeyUp={onKeyUpTelephone}
                        />
                    </Col>
                    <Col md="6">
                        <InputDefault
                            className={"mb-4"}
                            name={"telephone_whatsapp"}
                            type={"tel"}
                            has_label={true}
                            label={"Telefone Whatsapp"}
                            onKeyUp={onKeyUpTelephone}
                            onChange={defaultListener}
                            initial_value={user.provider.telephone_whatsapp
                                .replace("(", "")
                                .replace(")", "")
                                .replace(/[^\d|^\(|^\)|^\-|^\s]/g, "")}
                            options={{
                                delimiters: [" ", "-"],
                                blocks: [2, 5, 4],
                            }}
                        />
                    </Col>
                    <Col md="6">
                        <InputDefault
                            className={"mb-4"}
                            name={"data_fundacao"}
                            type="text"
                            has_label
                            // placeholder={"XX/XX/XXXX"}
                            onBlur={async (e) => {
                                const flag = await dateChecker(e)
                                if (flag) {
                                    setState({
                                        ...state,
                                        data_fundacao: elm(
                                            "[name=data_fundacao]"
                                        ).value,
                                    })
                                } else {
                                    setState({ ...state, data_fundacao: "" })
                                }
                                return flag
                            }}
                            onKeyUp={birthday}
                            initial_value={user.provider.data_fundacao}
                            label="Data de fundação"
                            options={{
                                delimiters: ["/", "/"],
                                blocks: [2, 2, 4],
                            }}
                        />
                    </Col>
                    <Col md="6">
                        <InputDefault
                            className={"mb-4"}
                            name={"qty_funcionarios"}
                            type={"text"}
                            has_label
                            onChange={defaultListener}
                            initial_value={user.provider.qty_funcionarios}
                            onKeyUp={onlyNumber}
                            label={"Quantidade de funcionários"}
                        />
                    </Col>
                    <Col md="6">
                        <InputDefault
                            className={"mb-4"}
                            id={"site"}
                            name={"site"}
                            type={"text"}
                            has_label
                            onChange={defaultListener}
                            initial_value={user.provider.site}
                            onBlur={site_blur}
                            label={"Site"}
                        />
                    </Col>
                    <Col md="6">
                        <SelectDefault
                            className={"mb-4"}
                            name={"modalidade"}
                            id={"modalidade"}
                            has_label
                            label={"Modalidade de comércio"}
                            listener={(e) => {
                                setModalidade({
                                    name: e.target?.selectedOptions[0]?.text,
                                })
                            }}
                            initial_value={
                                modalidades.find(
                                    (r) =>
                                        String(r.name).toUpperCase() ===
                                        String(
                                            user.provider.modalidade
                                        ).toUpperCase()
                                )?.id
                            }
                            values={modalidades}
                            _required
                        />
                    </Col>
                    <Col md="12">
                        <FormGroup>
                            <Label for={"provider_ramos"}>
                                {"Canal de vendas"}
                            </Label>
                            <SelectCustom
                                id={"provider_ramos"}
                                name={"provider_ramos"}
                                values={ramos}
                                onSelect={onSelectRamos}
                                onRemove={onSelectRamos}
                                selectedValues={user.provider.provider_ramos.map(
                                    (ramo, idx) => ({
                                        id: idx,
                                        name: ramo.name,
                                    })
                                )}
                            />
                        </FormGroup>
                    </Col>
                    <Col xs="12" style={{ marginTop: "2rem" }}>
                        <h1>Informações do responsável</h1>
                    </Col>
                    <Col xs="12" md="6">
                        <InputDefault
                            className={"mb-4"}
                            name={"name"}
                            type={"text"}
                            has_label
                            _required
                            onChange={defaultListener}
                            initial_value={user.provider.responsible[0]?.name}
                            label={"Nome"}
                        />
                    </Col>
                    <Col xs="12" md="6">
                        <InputDefault
                            className={"mb-4"}
                            name={"cpf"}
                            type={"text"}
                            has_label
                            _required
                            onChange={defaultListener}
                            initial_value={user.provider.responsible[0]?.cpf}
                            label={"CPF"}
                            onBlur={cpf_blur}
                            onKeyUp={cpf_rg}
                        />
                    </Col>
                    <Col xs="12" md="6">
                        <InputDefault
                            className={"mb-4"}
                            name={"email_responsible"}
                            type={"email"}
                            has_label
                            _required
                            onChange={defaultListener}
                            initial_value={user.provider.responsible[0]?.email}
                            label={"E-mail"}
                            onBlur={email_blur}
                        />
                    </Col>
                    <Col xs="12" md="6">
                        <InputDefault
                            className={"mb-4"}
                            name={"telephone_responsible"}
                            type={"tel"}
                            has_label={true}
                            label={"Telefone do responsável"}
                            onChange={defaultListener}
                            initial_value={user.provider.responsible[0]?.telephone
                                .replace("(", "")
                                .replace(")", "")
                                .replace(/[^\d|^\(|^\)|^\-|^\s]/g, "")}
                            onKeyUp={onKeyUpTelephone}
                            options={{
                                delimiters: [" ", "-"],
                                blocks: [2, 5, 4],
                            }}
                            _required
                        />
                    </Col>
                    <Col xs="12" md="6">
                        <InputDefault
                            className={"mb-4"}
                            name={"cargo"}
                            type={"text"}
                            has_label={true}
                            label={"Cargo"}
                            _required
                            onChange={defaultListener}
                            initial_value={user.provider.responsible[0]?.cargo}
                        />
                    </Col>
                    <Col xs="12" md="6">
                        <InputDefault
                            className={"mb-4"}
                            has_label
                            label={"Senha"}
                            type="password"
                            name="password"
                            id="password"
                            _minLength={"6"}
                            onChange={defaultListener}
                        />
                    </Col>
                </>,
            ]
            setFields(fields)
            setLoaderState(false)
        } else {
            console.log("not ok", user)
            setLoaderState(false)
        }
        setLoaderState(false)
    }, [user])

    return (
        <form onSubmit={save} autoComplete="off">
            {loaderState && (
                <div className="spinnerLoader">
                    <ClipLoader
                        size={150}
                        color={"var(--red-1)"}
                        loading={loaderState}
                    />
                </div>
            )}
            <Row className="allinerCenter mb-2">
                {fields}
                <Col xs="12" md="6">
                    <InputDefault
                        className={"mb-4"}
                        has_label
                        label={"Senha atual"}
                        type="password"
                        name="password_atual"
                        id="password_atual"
                        onChange={defaultListener}
                    />
                </Col>
                <Col xs="12" className="mt-4">
                    <Button
                        size="lg"
                        type={"submit"}
                        className={`btn-save ${styles.buttonSave}`}
                        onClick={save}
                    >
                        SALVAR
                    </Button>
                </Col>
            </Row>
        </form>
    )
}

export default MyAccount
