/*global FormData*/
import React, {useCallback, useContext, useEffect, useMemo, useState,} from "react"
import {Button, Col, Row} from "reactstrap"
import styles from "./MyAccount.module.css"
import {InputDefault} from "../.."
import {useRouter} from "next/router"
import {
    awaitable,
    birthday,
    cpf_blur,
    cpf_rg,
    dataURItoBlob,
    dateChecker,
    elm,
    email_blur,
    fromServer,
    hasKey,
    onKeyUpTelephone,
    tryCatch,
} from "../../../assets/helpers"
import {Context, dispatchFunctions} from "../../../store"
import {getToken, isAuthenticated, LogIn, logOut} from "../../../assets/auth"
import PhotoUploader from "./../../photoUploader"
import {MySwal} from "../../../assets/sweetalert"
import ClipLoader from "react-spinners/ClipLoader"

const MyAccount = () => {
    const Router = useRouter()
    const [user, setUser] = useState({})
    const [{token}, dispatch] = useContext(Context)
    const [state, setState] = useState({})
    const [thumb, setThumb] = useState("")
    const [loaderState, setLoaderState] = useState(false)

    const save = useCallback(
        async (e) => {
            e.preventDefault()
            const btn = elm("button.btn-save")
            btn.disabled = true
            btn.innerHTML = `Carregando ... <div class="spinner-border text-light" role="status"><span class="sr-only">Loading...</span></div>`
            let payload = {
                data: {
                    name: elm("[name=name]").value,
                    telephone: elm("[name=telephone]").value,
                    birthday: elm("[name=birthday]").value,
                },
            }
            if (!!thumb.length) {
                let formData = new FormData()
                let file = await dataURItoBlob(thumb)
                formData.append("file", file)
                console.log({file, thumb}, formData.getAll('file'), formData.get('file'))
                const avatar = await fromServer("/files", false, "POST", formData)
                console.log("AFTER", {avatar, formData})
                !avatar || !!avatar?.error?.message &&
                (await MySwal.fire(avatar?.error?.message, "", "error"))
                if (!!avatar && !hasKey(avatar, 'error')) {
                    payload = {...payload, fk_avatar: avatar.id}
                }
            } else if (user?.users?.avatar?.id) {
                console.log(user?.users?.avatar?.id)
                payload = {
                    ...payload,
                    fk_avatar: user?.users?.avatar?.id,
                }
            }
            console.log({state, user})
            payload = {
                ...payload,
                type: false,
            }
            if (!!state?.password?.length) {
                payload = {
                    ...payload,
                    password: state.password,
                    confirmPassword: state.password,
                    oldPassword: elm("[name=password_atual]").value,
                }
            }
            console.log({payload}, JSON.stringify(payload))
            const saveData = await fromServer(`/users`, token, "put", payload)
            console.log("saveData", {saveData})
            if (!hasKey(saveData, "error") && saveData && saveData.success) {
                await MySwal.fire(
                    "Dados atualizados com sucesso",
                    ``,
                    "success"
                )
                tryCatch(async () => {
                    dispatch(dispatchFunctions.navbar_props(await fromServer("/users", token)))
                })
            } else {
                await MySwal.fire(
                    "Não foi possível atualizar seus dados, tente novamente",
                    ``,
                    "error"
                )
                console.log("fail to load addresses")
            }
            btn.innerHTML = " SALVAR"
            btn.disabled = false
        },
        [state, thumb, user]
    )

    function defaultListener(e) {
        e.preventDefault()
        //console.log(e.target.name, e.target.value)
        setState({...state, [e.target.name]: e.target.value})
    }

    useEffect(() => {
        setLoaderState(true)
        awaitable(async () => {
            if (token) {
                if (Object.keys(user).length === 0) {
                    const user_server = await fromServer(`/users`, token)
                    console.log({user_server})
                    if (!!user_server && !hasKey(user_server, "error")) {
                        setUser(user_server)
                        setLoaderState(false)
                    }
                    setLoaderState(false)
                }
            } else {
                console.log("não tem token", token)
                const _user = isAuthenticated()
                if (!_user) {
                    logOut(dispatch, Router)
                } else {
                    LogIn(getToken(), _user, dispatch)
                }
            }
        })
    }, [token])

    useEffect(() => {
        setLoaderState(true)
        console.log("user was changed", {user}, !!user && !user?.error)
        if (!!user && !user?.error) {
            const newState = {
                birthday: user?.birthday || "",
                name: user?.name || "",
                cpf: user?.cpf || "",
                email: user?.users?.email || "",
                telephone:
                    user?.telephone
                        ?.replace("(", "")
                        .replace(")", "")
                        .replace(/[^\d|^\(|^\)|^\-|^\s]/g, "") || "",
            }
            console.log({newState})
            setState(newState)
        }
        setLoaderState(false)
    }, [user])

    return useMemo(
        () =>
            !!user.name && (
                <form onSubmit={save}>
                    {loaderState && (
                        <div className="spinnerLoader">
                            <ClipLoader
                                size={150}
                                color={"var(--red-1)"}
                                loading={loaderState}
                            />
                        </div>
                    )}
                    <Row className="allinerCenter mb-2">
                        <>
                            <Col xs="12" style={{marginBottom: "2rem"}}>
                                <h1>Informações cadastrais</h1>
                            </Col>
                            <Col
                                xs="12"
                                className="allignerCenter"
                                style={{flexDirection: "column"}}
                            >
                                <PhotoUploader
                                    className={"mb-4"}
                                    label={"Imagem de perfil "}
                                    listener={(imageList) => {
                                        if (!!imageList[0]?.dataURL) {
                                            setThumb(imageList[0].dataURL)
                                        } else {
                                            setThumb("")
                                        }
                                    }}
                                    defaultValue={
                                        !!thumb.length
                                            ? [{dataURL: thumb}]
                                            : !!user?.users?.avatar?.url
                                            ? [
                                                {
                                                    dataURL:
                                                    user?.users?.avatar
                                                        ?.url,
                                                },
                                            ]
                                            : []
                                    }
                                    maxNumber={1}
                                    maxFileSize={5 * 1024 * 1024}
                                    acceptType={["jpg", "png", "jpeg"]}
                                />
                            </Col>
                            <Col md="6">
                                <InputDefault
                                    className={"mb-4"}
                                    name={"name"}
                                    type={"text"}
                                    has_label={true}
                                    label={"Nome"}
                                    onChange={defaultListener}
                                    initial_value={user.name}
                                />
                            </Col>
                            <Col md="6">
                                <InputDefault
                                    className={"mb-4"}
                                    name={"telephone"}
                                    type={"tel"}
                                    has_label={true}
                                    label={"Telefone"}
                                    onKeyUp={onKeyUpTelephone}
                                    onChange={defaultListener}
                                    initial_value={
                                        user?.telephone
                                            ?.replace("(", "")
                                            .replace(")", "")
                                            .replace(
                                                /[^\d|^\(|^\)|^\-|^\s]/g,
                                                ""
                                            ) || ""
                                    }
                                    options={{
                                        delimiters: [" ", "-"],
                                        blocks: [2, 5, 4],
                                    }}
                                />
                            </Col>
                            <Col md="6">
                                <InputDefault
                                    className={"mb-4"}
                                    name={"birthday"}
                                    type="text"
                                    has_label
                                    // placeholder={"XX/XX/XXXX"}
                                    onBlur={async (e) => {
                                        const flag = await dateChecker(e)
                                        if (flag) {
                                            setState({
                                                ...state,
                                                birthday: elm("[name=birthday]")
                                                    .value,
                                            })
                                        } else {
                                            setState({...state, birthday: ""})
                                            elm("[name=birthday]").value = ""
                                        }
                                        return flag
                                    }}
                                    onKeyUp={birthday}
                                    initial_value={user?.birthday || ""}
                                    label="Data de nascimento"
                                    options={{
                                        delimiters: ["/", "/"],
                                        blocks: [2, 2, 4],
                                    }}
                                />
                            </Col>
                            <Col xs="12" md="6">
                                <InputDefault
                                    className={"mb-4"}
                                    name={"cpf"}
                                    type={"text"}
                                    has_label
                                    _required
                                    onChange={defaultListener}
                                    initial_value={user?.cpf || ""}
                                    label={"CPF"}
                                    onBlur={cpf_blur}
                                    onKeyUp={cpf_rg}
                                />
                            </Col>
                            <Col xs="12" md="6">
                                <InputDefault
                                    className={"mb-4"}
                                    name={"email"}
                                    type={"email"}
                                    has_label
                                    _required
                                    onChange={defaultListener}
                                    initial_value={user?.users?.email || ""}
                                    label={"E-mail"}
                                    onBlur={email_blur}
                                />
                            </Col>
                            <Col xs="12" md="6">
                                <InputDefault
                                    className={"mb-4"}
                                    has_label
                                    label={"Senha"}
                                    type="password"
                                    name="password"
                                    id="password"
                                    _minLength={"6"}
                                    onChange={defaultListener}
                                />
                            </Col>
                        </>
                        <Col xs="12" md="6">
                            <InputDefault
                                className={"mb-4"}
                                has_label
                                label={"Senha atual"}
                                type="password"
                                name="password_atual"
                                id="password_atual"
                                onChange={defaultListener}
                            />
                        </Col>
                        <Col xs="12" className="mt-4">
                            <Button
                                size="lg"
                                type={"submit"}
                                className={`btn-save ${styles.buttonSave}`}
                            >
                                SALVAR
                            </Button>
                        </Col>
                    </Row>
                </form>
            ),
        [user, token, thumb, loaderState]
    )
}

export default MyAccount
