import React from "react"
import { Row, Form, Button } from "reactstrap"
import InputDefault from "../../InputDefault"
const index = () => {
  return (
    <>
      <Row className={"m-0 pt-0 mb-3"}>
        <h1 className="page_title h1 m-0 mr-auto">{"Historico de Vendas"}</h1>
      </Row>
      <Row className={"mx-0 mb-4 pt-0 w-100"}>
        <Form className={"d-flex flex-row w-100 p-0"}>
          <InputDefault
            styleInput={{ height: 36 }}
            id={"input_search_boarded"}
            className={"h-100 m-0 w-100"}
            type={"search"}
            placeholder={"Pesquisar pedidos"}
          />
          <Button
            style={{
              fontFamily: "Roboto",
              fontSize: "1.5rem",
              fontWeight: "bold",
              fontStretch: "normal",
              fontStyle: "normal",
              lineHeight: "1.33",
              letterSpacing: "normal",
              color: "#ffffff",
            }}
            className={
              "btn my-2 my-sm-0 btn-lg h-100 btn-custom rounded-0 border-0 px-5 py-3 actively"
            }
          >
            Pesquisar
          </Button>
        </Form>
      </Row>
      <Row className={"m-0 pt-0  mb-4"}>
        {"<ProductList products={pag(products, page)} />"}
      </Row>
    </>
  )
}

index.propTypes = {}

export default index
