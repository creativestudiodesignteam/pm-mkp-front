import React, { useState } from "react"
import propTypes from "prop-types"
import { Collapse } from "reactstrap"

const Component = ({ title, _key, collapseOpen = false, children }) => {
    const [collapse, setCollapse] = useState(collapseOpen)
    const [status, setStatus] = useState(false)
    //const onEntering = () => setStatus('Opening...');
    const onEntered = () => setStatus(true)
    //const onExiting = () => setStatus('Closing...');
    const onExited = () => setStatus(false)
    const toggle = () => setCollapse(!collapse)
    const toggle_svg_status = (open) => (open ? "8.216" : "4.719")
    const toggle_svg_transform = (open) =>
        open ? "translate(.08)" : "rotate(-90.97 4.11 4.038)"
    return (
        <div className="justify-content-center" style={{ cursor: "pointer" }}>
            <h2
                id={`${title}-id-${_key}`}
                onClick={toggle}
                className="h2 w-100 border-bottom"
            >
                <svg
                    xmlns="http://www.w3.org/2000/svg"
                    width={toggle_svg_status(status)}
                    height={toggle_svg_status(!status)}
                    viewBox={`0 0 ${(status && "8.216 4.719") ||
                        "4.719 8.216"}`}
                >
                    <g
                        id="prefix__orientation"
                        transform={toggle_svg_transform(status)}
                    >
                        <path
                            id="prefix__Caminho_3"
                            fill="#232323"
                            d="M7.987 64.1a.489.489 0 0 0-.358-.151H.509a.489.489 0 0 0-.358.151.5.5 0 0 0 0 .715l3.56 3.56a.5.5 0 0 0 .715 0l3.56-3.56a.5.5 0 0 0 0-.715z"
                            data-name="Caminho 3"
                            transform="translate(0 -63.953)"
                        />
                    </g>
                </svg>
                {title}
            </h2>
            <Collapse
                className={""}
                isOpen={collapse}
                // onEntering={onEntering}
                onEntered={onEntered}
                // onExiting={onExiting}
                onExited={onExited}
            >
                {children}
            </Collapse>
        </div>
    )
}

Component.propTypes = {
    title: propTypes.string,
    _key: propTypes.any,
    children: propTypes.node,
}

export default Component
