import React from 'react';
import SemiContainer from "./SemiContainer";
import {verifyAllAttrs} from '../assets/helpers'
import {Img, Link} from "./index";

const Index = ({data = [], classNames = ['', '', ''], over = false, overClassNames = ['', '', ''], children, ...props}) => {
    const [cl_first, cl_second, cl_third] = over ? overClassNames : classNames
    const _data = (data && data[0] && verifyAllAttrs(data[0], ['zoom_href', 'href', 'img', 'alt', 'node']) ? data : [
        {
            zoom_href: '/',
            href: '/',
            img: 'image_slide_top_1-min.png',
            alt: 'Image Shore',
            node: <p className="uppercase bold">Mobile</p>
        },
        {
            zoom_href: '/',
            href: '/',
            img: 'image_slide_top_2-min.png',
            alt: 'Image Shore',
            node: <p className="uppercase bold">Accessory</p>
        },
        {
            zoom_href: '/',
            href: '/',
            img: 'image_slide_top_3-min.png',
            alt: 'Image Shore',
            node: <p className="uppercase bold">Camera</p>
        },
        {
            zoom_href: '/',
            href: '/',
            img: 'image_shore.png',
            alt: 'Image Shore',
            node: <p className="uppercase bold">Shoes</p>
        },
        {
            zoom_href: '/',
            href: '/',
            img: 'image_slide_top_4-min.png',
            alt: 'Image Shore',
            node: <p className="uppercase bold">Bags</p>
        }
    ]).map(({alt, zoom_href, href, img, node}, i) => (
        <div key={i} className={over ? cl_third : `category-image-slide relative full-width ${cl_third}`}>
            <div
                className="clearfix effect-hover-zoom overfollow-hidden img-categorys-slide center-vertical-image relative">
                <Img className="animate-default" image_name_with_extension={img} alt={alt}/>
                <Link _href={zoom_href}/>
            </div>
            <Link _href={href}>
                {node}
            </Link>
        </div>
    ))
    return (
        <SemiContainer over={true} overClassNames={over ? [cl_first, cl_second] : [
            `category-image top-margin-15-default left-margin-15-default float-left border ${cl_first}`,
            `owl-carousel owl-theme ${cl_second}`
        ]}>
            {_data}
        </SemiContainer>
    );

};
export default Index;