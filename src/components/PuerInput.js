/* eslint-disable react/display-name */
import React from 'react';
import propTypes from 'prop-types'
const Component = (
    {
        OnClick = () => false,
        OnBlur = () => false,
        OnChange = () => false,
        OnKeyUp = () => false,
        placeholder = "",
        className = "",
        type = "text",
        maxLength = '100',
        required = true,
        id,
        name = false
    }) => {
    return (
        <input
            id={id}
            name={`${name || `${id}-default-${Math.floor(Math.random() * 10000)}`}`}
            onBlur={OnBlur}
            onChange={OnChange}
            onKeyUp={OnKeyUp}
            className={className}
            type={type}
            placeholder={placeholder}
            aria-label={placeholder}
            onClick={OnClick}
            maxLength={maxLength}
            required={required}
        />
    );
}

Component.propTypes = {
    OnClick: propTypes.func,
    OnBlur: propTypes.func,
    OnChange: propTypes.func,
    OnKeyUp: propTypes.func,
    placeholder: propTypes.string,
    className: propTypes.string,
    type: propTypes.string,
    maxLength: propTypes.any,
    required: propTypes.bool,
    id: propTypes.string.isRequired,
    name: propTypes.string
}
export default Component;
