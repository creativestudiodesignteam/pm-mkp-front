import React, {useCallback, useContext, useEffect, useState} from "react"
import {Col, Row} from "reactstrap"
import Delivery from "./delivery"
import CartDetails from "./cartDetails"
import PaymentMethods from "./paymentMethods"
import {Context} from "../../../store"
import {awaitable, fromServer, hasKey, tryCatch} from "../../../assets/helpers"
import {getToken} from "../../../assets/auth"
import ClipLoader from "react-spinners/ClipLoader"
import useForceUpdate from "use-force-update"

const Overall = ({
                     cart, cards, setCards, address, setAddress,
                     installments,
                     setInstallments,
                     installmentsValue,
                     setInstallmentsValue,
                     serverToken
                 }) => {
    const [shipping, setShipping] = useState({selected: "", options: []})
    const [loaderState, setLoaderState] = useState(false)
    const [{token}] = useContext(Context)
    const [timeoutFrete, setTimeoutFrete] = useState(false)
    const forceUpdate = useForceUpdate()
    const safeToken = useCallback(() => tryCatch(() => token || serverToken || getToken(), () => '', true), [token || serverToken || getToken])
    useEffect(() => {
        awaitable(async () => {
            if (shipping.selected !== '') {
                const _installments = await fromServer('/installments', safeToken())
                _installments && !hasKey(_installments, 'error') && setInstallments(_installments)
                console.log("[shipping.selected])",{shipping, installments,_installments})
            } else {
                console.log({shipping, installments})
            }
        })
    }, [shipping.selected])
    const [total, setTotal] = useState(
        Array(...cart).reduce(
            (accum, curr) =>
                accum + Number(Number(curr.price) * Number(curr.amount)),
            0
        )
    )

    function _selectedValueShipping() {
        if (!!shipping.selected.length) {
            try {
                const {selected, options} = shipping
                const [selectedOption] = options.filter(
                    (option) => option.type === shipping.selected
                )
                return selectedOption?.price
            } catch (e) {
                console.log("selectedValueShipping", e)
                return 0
            }
        }
        return 0
    }

    const [shippingValue, setShippingValue] = useState(_selectedValueShipping())
    useEffect(() => {
        setShippingValue(_selectedValueShipping())
    }, [shipping])
    useEffect(() => {
        setTotal(
            Array(...cart).reduce(
                (accum, curr) =>
                    accum + Number(Number(curr.price) * Number(curr.amount)),
                0
            )
        )
    }, [cart])
    useEffect(() => {
        awaitable(async () => {
            setLoaderState(true)
            if (!shipping.options.length) {
                setTimeout(() => {
                    if (!shipping_types || hasKey(shipping_types, "error")) {
                        setTimeoutFrete(true)
                        setLoaderState(false)
                    }
                }, 15000)
                const shipping_types = await fromServer(
                    `/shipping`,
                    safeToken()
                )
                if (!!shipping_types && !hasKey(shipping_types, "error")) {
                    setTimeoutFrete(false)
                    setShipping({
                        ...shipping,
                        options: shipping_types.map(({price, ...rest}) => ({
                            ...rest,
                            price: price / 100,
                        })),
                    })
                    setLoaderState(false)
                } else {
                    setTimeoutFrete(true)
                }
                setLoaderState(false)
            }
            setLoaderState(false)
        })
    }, [])
    return (
        <>
            {loaderState && (
                <div className="spinnerLoader">
                    <ClipLoader
                        size={150}
                        color={"var(--red-1)"}
                        loading={loaderState}
                    />
                </div>
            )}
            <Row>
                <Col lg="8">
                    <Delivery
                        setInstallments={setInstallments}
                        token={safeToken()}
                        address={address}
                        setAddress={setAddress}
                        shipping={shipping}
                        setShipping={setShipping}
                        timeoutFrete={timeoutFrete}
                        setTimeoutFrete={setTimeoutFrete}
                    />
                    <PaymentMethods
                        installments={installments}
                        setInstallmentsValue={setInstallmentsValue}
                        cart={cart}
                        shippingValue={shippingValue}
                        address={address}
                        cards={cards}
                        setCards={setCards}
                        token={safeToken()}
                        shipping={shipping}
                    />
                </Col>
                <Col lg="4">
                    <CartDetails
                        installmentsValue={installmentsValue}
                        token={safeToken()}
                        cart={cart}
                        address={address}
                        setAddress={setAddress}
                        shipping={shipping}
                        setShipping={setShipping}
                    />
                </Col>
            </Row>
        </>
    )
}

export default Overall
