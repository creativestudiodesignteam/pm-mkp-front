import React, {useContext, useEffect, useState} from "react"
import {Alert, Button, Col, Input, Label, Row} from "reactstrap"
import styles from "./PaymentMethods.module.css"
import {useRouter} from "next/router"
import {
    birthday,
    disable_btn_and_put_innerHTML,
    elm,
    enable_btn_and_put_innerHTML,
    fromServer,
    hasKey,
    onlyNumber,
} from "../../../../assets/helpers"
import {MySwal, Toast} from "../../../../assets/sweetalert"
import {getToken} from "../../../../assets/auth"
import FormGroup from "@material-ui/core/FormGroup"
import InputDefault from "../../../InputDefault"
import Cards from "react-credit-cards"
import {Context, dispatchFunctions} from "../../../../store"
import {SelectDefault} from "../../../index";

const PaymentMethods = ({
                            cart,
                            cards,
                            setCards,
                            shipping,
                            token,
                            address,
                            shippingValue,
                            installments,
                            setInstallmentsValue
                        }) => {
    const [card, setCard] = useState({
        card_holder: "",
        card_number: "",
        expiration_date: "",
        security_code: "",
        focus: "",
    })
    const [context, dispatch] = useContext(Context)
    const [usarCartao, set_usarCartao] = useState(false)
    const [newcard, setNewCard] = useState(false)
    const [paymentType, setPaymentType] = useState(1)
    const [alert, setAlert] = useState(false)
    const toggle = () => setAlert(!alert)
    const router = useRouter()
    const window = require("global/window")
    const handleInputFocus = (e) => {
        setCard({...card, focus: e.target.name})
    }
    const handleInputChange = (e) => {
        const {name, value} = e.target
        setCard({...card, [name]: value})
    }
    const goToCheckout = async (e) => {
        e.preventDefault()
        disable_btn_and_put_innerHTML("#go")
        const {id: fk_addresses} = address.find((a) => !!a.select_at)
        const total = Array(...cart).reduce(
            (accum, curr) =>
                accum + Number(Number(curr.price) * Number(curr.amount)),
            0
        )
        let linkProps = {
            pathname: "/checkout",
            query: {
                paymentType,
                total,
                shipping:
                    shipping?.options?.find(
                        ({type}) => type === shipping?.selected
                    )?.price || 0,
                address: JSON.stringify(
                    address.find((a) => !!a.select_at) || "{}"
                ),
            },
        }
        if (!!fk_addresses) {
            if (!usarCartao && paymentType === 2) {
                linkProps.query = {
                    ...linkProps,
                    query: {
                        ...linkProps.query,
                        ...card,
                    },
                }
                if (
                    card.card_number &&
                    card.card_holder &&
                    card.expiration_date &&
                    card.security_code
                ) {
                    const salvar = await fromServer(
                        "/creditcard",
                        token,
                        "post",
                        {
                            card_holder: card.card_holder,
                            card_number: card.card_number,
                            expiration_date: card.expiration_date,
                            security_code: card.security_code,
                            type: 2,
                        }
                    )
                    // console.log({ salvar }, elm("#saveCard"))
                    if (
                        !!salvar &&
                        !hasKey(salvar, "error") &&
                        hasKey(salvar, "id")
                    ) {
                        const checkout = await fromServer(
                            "/checkout",
                            token,
                            "POST",
                            {
                                payment_type: 2,
                                fk_creditcard: salvar.id,
                                fk_addresses,
                                qty_installments: elm('[name=installment]').value
                            }
                        )
                        //console.log({ checkout, salvar })
                        if (!elm("#saveCard")?.checked) {
                            const delete_card = await fromServer(
                                `/creditcard/${salvar.id}`,
                                token || getToken(),
                                "delete"
                            )
                            !!delete_card.error &&
                            (await MySwal.fire(
                                "Falha ao deletar cartao",
                                "Apos a compra, exclua o cartao",
                                "warning"
                            ))
                        }
                        if (!checkout || hasKey(checkout, "error")) {
                            await MySwal.fire(
                                "Não foi possivel realizar a compra",
                                checkout?.error?.message,
                                "error"
                            )
                        } else {
                            await MySwal.fire(
                                "Compra efetuada com sucesso",
                                "Confira seu email para mais informações",
                                "success"
                            ) //address,shippingValue,total
                            dispatch(dispatchFunctions.cart_length(0))
                            if (!elm("#saveCard")?.checked) {
                                await router.push({
                                    pathname: "/checkout",
                                    query: {
                                        paymentType,
                                        total,
                                        shipping:
                                            shipping?.options?.find(
                                                ({type}) =>
                                                    type === shipping?.selected
                                            )?.price || 0,
                                        address: JSON.stringify(
                                            address.find(
                                                (a) => !!a.select_at
                                            ) || "{}"
                                        ),
                                        card: JSON.stringify({
                                            card_holder: card.card_holder,
                                            card_number: `${
                                                card.card_number
                                            }`.slice(0, 4),
                                        }),
                                    },
                                })
                            } else {
                                await router.push({
                                    pathname: "/checkout",
                                    query: {
                                        paymentType,
                                        total,
                                        shipping:
                                            shipping?.options?.find(
                                                ({type}) =>
                                                    type === shipping?.selected
                                            )?.price || 0,
                                        address: JSON.stringify(
                                            address.find(
                                                (a) => !!a.select_at
                                            ) || "{}"
                                        ),
                                        card: JSON.stringify({
                                            card_holder: card.card_holder,
                                            card_number: `${
                                                card.card_number
                                            }`.slice(0, 4),
                                        }),
                                    },
                                })
                            }
                        }
                    } else {
                        await MySwal.fire("Ops", salvar.error?.message, "error")
                    }
                }
            } else if (usarCartao && paymentType === 2) {
                const {id: fk_creditcard} = cards?.options?.find(
                    (a) => !!a.selected
                )
                if (!!fk_creditcard) {
                    const data = await fromServer(
                        "/checkout",
                        token,
                        "POST",
                        {
                            payment_type: 2,
                            fk_addresses,
                            fk_creditcard,
                            qty_installments: elm('[name=installment]').value,
                            shipping: shipping.id,
                        }
                    )
                    if (!!data && !hasKey(data, "error")) {
                        console.log({data})
                        await MySwal.fire(
                            "Compra efetuada com sucesso",
                            "Confira seu email para mais informações",
                            "success"
                        )
                        dispatch(dispatchFunctions.cart_length(0))
                        await router.push({
                            ...linkProps,
                            query: {
                                ...linkProps.query,
                                card_id: fk_creditcard,
                            },
                        })
                    } else {
                        console.log(data.error)
                        await MySwal.fire(
                            "Não foi possivel realizar a compra",
                            data?.error?.message,
                            "error"
                        )
                    }
                } else {
                    await MySwal.fire(
                        "Ops",
                        "Nao encontramos o seu cartao",
                        "info"
                    )
                }
                //console.log({ fk_creditcard })
            } else {
                const data = await fromServer(
                    "/checkout",
                    token,
                    "POST",
                    {
                        payment_type: 1,
                        fk_addresses,
                    shipping: shipping.id
                    }
                )
                if (!data || hasKey(data, "error")) {
                    console.log(data.error)
                    await MySwal.fire(
                        "Não foi possivel realizar a compra",
                        data?.error?.message,
                        "error"
                    )
                } else {
                    /*console.log(
                        "BOLETOOOOO",
                        { data },
                        data?.payment?.BankSlipUrl,
                        data?.payment,
                        data
                    )*/
                    await MySwal.fire(
                        "Compra efetuada com sucesso",
                        "Confira seu email para mais informações",
                        "success"
                    )
                    dispatch(dispatchFunctions.cart_length(0))
                    await router.push({
                        pathname: "/checkout",
                        query: {
                            paymentType,
                            total,
                            shipping:
                                shipping?.options?.find(
                                    ({type}) => type === shipping?.selected
                                )?.price || 0,
                            url: data?.payment?.BankSlipUrl,
                            address: JSON.stringify(
                                address.find((a) => !!a.select_at) || "{}"
                            ),
                        },
                    })
                }
            }
        } else {
            await MySwal.fire("Ops", "Nao encontramos o seu endereço", "info")
        }
        //console.log({ fk_addresses })
        enable_btn_and_put_innerHTML("#go", "CONCLUIR PAGAMENTO")
        return false
    }
    useEffect(() => {
        if (cards.options.length < 1) {
            setAlert(true)
        }
        //console.log({ cards })
    }, [cards])
    return (
        <>
            <Row className={"mb-4"}>
                <Col xs="12">
                    <h1>Formas de pagamento</h1>
                </Col>
            </Row>

            <Row className={"m-0 mb-4 colSavedCard"}>
                <Col xs="6" className="p-0">
                    <Button
                        onClick={(e) => {
                            e.preventDefault()
                            setPaymentType(1)
                        }}
                        className={`btn btn-lg ${
                            styles.deliveryType
                        } ${paymentType === 1 && styles.active}`}
                    >
                        BOLETO
                    </Button>
                </Col>
                <Col xs="6" className="p-0">
                    <Button
                        onClick={(e) => {
                            e.preventDefault()
                            setPaymentType(2)
                        }}
                        className={`btn btn-lg ${
                            styles.deliveryType
                        } ${paymentType === 2 && styles.active}`}
                        defaultChecked="true"
                    >
                        CARTÃO DE CRÉDITO
                    </Button>
                </Col>
            </Row>
            {paymentType === 2 && (
                <div className="">
                    <Row form className="mb-4 rowUseCard">
                        <Col xs="12" className="colSavedCard">
                            <Button
                                className={`mb-4 ${
                                    styles.paymentType
                                } ${usarCartao && styles.active}`}
                                onClick={(e) => {
                                    e.preventDefault()
                                    set_usarCartao(true)
                                    setNewCard(false)
                                    setCard({
                                        card_holder: " ",
                                        card_number: " ",
                                        expiration_date: " ",
                                        security_code: " ",
                                        focus: " ",
                                    })
                                }}
                            >
                                Comprar com cartão salvo
                            </Button>
                            <Button
                                className={` mb-4 ${
                                    styles.paymentType
                                } ${newcard && styles.active}`}
                                onClick={(e) => {
                                    e.preventDefault()
                                    setNewCard(true)
                                    set_usarCartao(false)
                                }}
                            >
                                Adicionar novo cartão
                            </Button>
                        </Col>
                        <hr className="showTabletAndLess" />
                        {usarCartao && cards?.options?.length > 0 && (
                            <Col xs="12" className="allignerCenter">
                                <h1>Selecione um cartão</h1>
                            </Col>
                        )}
                        <Col sm={"12"} className={"mb-4"}>
                            {usarCartao && cards?.options?.length < 1 && (
                                <Alert
                                    color="primary"
                                    isOpen={alert}
                                    toggle={toggle}
                                    style={{
                                        opacity: 1,
                                        color: "white",
                                        backgroundColor: "var(--red-1)",
                                        padding: "2rem",
                                        fontSize: "16px",
                                        textAlign: "center",
                                    }}
                                >
                                    Você ainda não possui cartões cadastrados!
                                </Alert>
                            )}

                            {usarCartao &&
                            cards?.options?.map(
                                ({
                                     id,
                                     first_digits,
                                     fk_users,
                                     holder_name,
                                     selected,
                                     status,
                                     type,
                                 }) => (
                                    <Row
                                        key={id}
                                        className="allignerCenter"
                                    >
                                        <Col
                                            sm={"12"}
                                            md={"4"}
                                            className={
                                                "mr-2 mb-4 allignerCenter"
                                            }
                                        >
                                            <Cards
                                                name={holder_name}
                                                number={first_digits}
                                                acceptedCards={[
                                                    "visa",
                                                    "mastercard",
                                                    "elo",
                                                    "hipercard",
                                                    "amex",
                                                    "dinersclub",
                                                    "discover",
                                                ]}
                                                placeholders={{
                                                    name: "nome no cartão",
                                                    valid: "valido até",
                                                }}
                                                locale={{
                                                    valid: "valido até",
                                                }}
                                            />
                                        </Col>
                                        <Col
                                            xs="12"
                                            className="allignerCenter mb-4"
                                            onClick={async (e) => {
                                                e.preventDefault()
                                                if (!selected) {
                                                    const data = await fromServer(
                                                        "/creditcard/select",
                                                        getToken(),
                                                        "PUT",
                                                        {id}
                                                    )
                                                    const creditcard = await fromServer(
                                                        "/creditcard",
                                                        getToken()
                                                    )
                                                    /*console.log({
                                                    data,
                                                    creditcard,
                                                })*/
                                                    if (
                                                        !hasKey(
                                                            data,
                                                            "error"
                                                        ) &&
                                                        !hasKey(
                                                            creditcard,
                                                            "error"
                                                        )
                                                    ) {
                                                        setCards({
                                                            selected: id,
                                                            options: creditcard,
                                                        })
                                                    }
                                                    await Toast.fire(
                                                        "Cartão selecionado",
                                                        "",
                                                        "success"
                                                    )
                                                } else {
                                                    await Toast.fire(
                                                        "Cartão já selecionado",
                                                        "",
                                                        "info"
                                                    )
                                                }
                                            }}
                                        >
                                            {selected && (
                                                <Button className="selectedCard">
                                                    Cartão selecionado
                                                </Button>
                                            )}
                                            {!selected && (
                                                <Button className="buttonSave50">
                                                    Selecionar cartão
                                                </Button>
                                            )}
                                        </Col>
                                    </Row>
                                )
                            )}
                        </Col>
                        {newcard && (
                            <>
                                <Col xs="12">
                                    <h1>Adicionar novo cartão de crédito</h1>
                                </Col>
                                <Col xs="12" lg="6">
                                    <InputDefault
                                        id={"card_number"}
                                        name={"card_number"}
                                        type={"text"}
                                        has_label={true}
                                        label={"Número do cartão *"}
                                        onKeyUp={onlyNumber}
                                        onChange={handleInputChange}
                                        onFocus={handleInputFocus}
                                        _minLength={"16"}
                                        _maxLength={"16"}
                                        _required
                                    />

                                    <InputDefault
                                        id={"expiration_date"}
                                        name={"expiration_date"}
                                        type={"text"}
                                        has_label={true}
                                        label={"Validade*"}
                                        onChange={handleInputChange}
                                        onFocus={handleInputFocus}
                                        onKeyUp={birthday}
                                        options={{
                                            delimiters: ["/"],
                                            blocks: [2, 6],
                                        }}
                                        _minLength={"7"}
                                        _maxLength={"7"}
                                        _required
                                    />

                                    <InputDefault
                                        id={"card_holder"}
                                        name={"card_holder"}
                                        type={"text"}
                                        has_label={true}
                                        label={"Nome do titular do cartão*"}
                                        onChange={handleInputChange}
                                        onFocus={handleInputFocus}
                                        _required
                                    />

                                    <InputDefault
                                        id={"security_code"}
                                        name={"security_code"}
                                        type={"text"}
                                        has_label={true}
                                        label={"CVV*"}
                                        onChange={handleInputChange}
                                        onFocus={handleInputFocus}
                                        onKeyUp={onlyNumber}
                                        placeholder={"XXX"}
                                        _minLength={"3"}
                                        _maxLength={"4"}
                                        _required
                                    />
                                </Col>
                                <Col
                                    xs="12"
                                    lg="6"
                                    className={"mb-4 allignerCenter"}
                                >
                                    <Cards
                                        cvc={card.security_code}
                                        expiry={card.expiration_date}
                                        focused={card.focus}
                                        name={card.card_holder}
                                        number={card.card_number}
                                        acceptedCards={[
                                            "visa",
                                            "mastercard",
                                            "elo",
                                            "hipercard",
                                            "amex",
                                            "dinersclub",
                                            "discover",
                                        ]}
                                        placeholders={{
                                            name: "nome no cartão",
                                            valid: "valido até",
                                        }}
                                        locale={{ valid: "valido até" }}
                                    />
                                </Col>
                            </>
                        )}
                    </Row>
                    {!usarCartao && (
                        <Row className={"mb-4"}>
                            <Col xs="12" className="mt-4">
                                <FormGroup>
                                    <Label check>
                                        <Input
                                            type="checkbox"
                                            id="saveCard"
                                            className="relative"
                                        />{" "}
                                        Salvar cartão para transações futuras
                                    </Label>
                                </FormGroup>
                            </Col>
                        </Row>
                    )}
                </div>
            )}
            {
                paymentType === 2 && <Row className={"mb-4"}>
                    <Col xs={"12"} sm={'5'} md={'4'} lg={'3'}>
                        <SelectDefault
                            has_label
                            label={'Parcelamento:'}
                            id='installment'
                            name='installment'
                            placeholder={installments.length ? "" : 'Selecione uma modalidade de entrega'}
                            listener={
                                e => {
                                    e.preventDefault();
                                    const id = Number(e.target.value || elm('[name=installment]'))
                                    const _installments = installments.find(({Installments: installmentsId}) => installmentsId === id)
                                    if (!!_installments) {
                                        setInstallmentsValue(_installments.TotalValue)//
                                    }
                                    console.log("Parcelamento", {id, installments, _installments})

                                }}
                            initial_value={1}
                            values={installments?.map(({InstallmentValue: name, Installments: id}) => ({
                                id,
                                name: `${id}x de ${name}`
                            })) || []}
                        />
                    </Col>
                </Row>
            }
            <Row className={"mb-4"}>
                <Col xs={"12"}>
                    {paymentType === 1 && (
                        <Button
                            size="lg"
                            id={"go"}
                            className={styles.confirmPayment}
                            onClick={goToCheckout}
                            disabled={!shipping.selected}
                        >
                            CONCLUIR PAGAMENTO
                        </Button>
                    )}
                    {paymentType === 2 && (
                        <Button
                            size="lg"
                            id="go"
                            className={styles.confirmPayment}
                            onClick={goToCheckout}
                            disabled={
                                !elm('[name=installment]')?.value ||
                                !shipping.selected ||
                                (!newcard &&
                                    (cards.selected === "" ||
                                        !cards.selected)) ||
                                (newcard &&
                                    (card.security_code === "" ||
                                        card.expiration_date === "" ||
                                        card.card_holder === "" ||
                                        card.card_number === ""))
                            }
                        >
                            CONCLUIR PAGAMENTO
                        </Button>
                    )}
                    <small id={`helper`} className="form-text text-muted mt-2">
                        Pagamento só poderá ser feito com um frete selecionado
                    </small>
                </Col>
            </Row>
        </>
    )
}
export default PaymentMethods
