import React, {useEffect, useMemo} from "react"
import {Button, Col, Jumbotron, Row} from "reactstrap"
import {Img} from "../../.."
import styles from "./CartDetails.module.css"
import {brlFormat} from "../../../../assets/helpers"

const CartDetails = ({cart, shipping, setShipping, token, installmentsValue}) => {
    const total = Array(...cart).reduce(
        (accum, curr) =>
            accum + Number(Number(curr.price) * Number(curr.amount)),
        0
    )
    useEffect(() =>
        console.log("CartDetails", {cart, shipping, setShipping, total})
    )

    function selectedValueShipping() {
        console.log("selectedValueShipping", {shipping})
        if (shipping.selected.length) {
            try {
                const {selected, options} = shipping
                const [selectedOption] = options.filter(
                    (option) => option.type === shipping.selected
                )
                console.log({selected, options, selectedOption})
                return selectedOption?.price
            } catch (e) {
                console.log("selectedValueShipping", e)
                return 0
            }
        }
        return 0
    }

    return useMemo(
        () => (
            <>
                <h3>Seu pedido</h3>
                {cart?.map(({id, url, name, price, amount = 0}, i, arr) => (
                    <Row key={id} className="showTabletAndLess">
                        <Col xs="12">
                            <Row>
                                <Col
                                    xs="12"
                                    className={`${
                                        styles.colNamePrice
                                    } allignerCenter`}
                                >
                                    <Button size="lg" className={styles.count}>
                                        {name?.length > 20 && name.slice(0, 20)}
                                        {name?.length > 20 && (
                                            <small className={"text-muted"}>
                                                {" "}
                                                ...{" "}
                                            </small>
                                        )}
                                        {name?.length <= 20 && name}
                                    </Button>
                                    <Button outline className={styles.price}>
                                        R$ {price} x {amount}
                                    </Button>
                                </Col>
                            </Row>
                            {arr.length - 1 !== i && <hr/>}
                        </Col>
                    </Row>
                ))}
                {cart?.map(({id, url, name, price}, i, arr) => (
                    <Row key={id} className="showAboveTablet">
                        <Col xs="12">
                            <Row>
                                <Col xs="5">
                                    <Img
                                        image_name_with_extension={
                                            url || "placeholder/default.png"
                                        }
                                        outSide={!!url}
                                        alt={name}
                                        className={styles.imgCart}
                                    />
                                </Col>

                                <Col xs="7" className={styles.colNamePrice}>
                                    <Button
                                        size="lg"
                                        className={styles.nameProduct}
                                    >
                                        {name?.length > 20 && name.slice(0, 20)}
                                        {name?.length > 20 && (
                                            <small className={"text-muted"}>
                                                {" "}
                                                ...{" "}
                                            </small>
                                        )}
                                        {name?.length <= 20 && name}
                                    </Button>
                                    <Button
                                        outline
                                        className={styles.priceDetails}
                                    >
                                        R$ {price}
                                    </Button>
                                </Col>
                            </Row>
                            {arr.length - 1 !== i && <hr/>}
                        </Col>
                    </Row>
                ))}
                <hr/>
                <h3>Total do carrinho</h3>
                <Row>
                    <Col xs="12">
                        <Jumbotron className={styles.jumbo}>
                            <Row>
                                <Col xs="12" className={styles.alignerAround}>
                                    <Button size="lg" className={styles.count}>
                                        Subtotal
                                    </Button>
                                    <Button outline className={styles.price}>
                                        {brlFormat(!!installmentsValue ? installmentsValue  : total)}
                                    </Button>
                                </Col>
                            </Row>
                            <hr/>
                            <Row>
                                <Col xs="12">
                                    <Button size="lg" className={styles.count}>
                                        Frete {shipping?.selected}
                                    </Button>
                                </Col>
                                <Col xs="12" className={styles.alignerAround}>
                                    <Button
                                        outline
                                        className={styles.priceDelivery}
                                    >
                                        {brlFormat(
                                            Number(selectedValueShipping())
                                        )}
                                    </Button>
                                </Col>
                            </Row>
                            <hr/>
                            <Row>
                                <Col xs="12" className={styles.alignerAround}>
                                    <Button size="lg" className={styles.total}>
                                        Total
                                    </Button>
                                    <Button outline className={styles.price}>
                                        {brlFormat(
                                            Number(installmentsValue||total) +
                                            Number(selectedValueShipping())
                                        )}
                                    </Button>
                                </Col>
                            </Row>
                        </Jumbotron>
                    </Col>
                </Row>
            </>
        ),
        [cart, shipping, setShipping, token]
    )
}

export default CartDetails
