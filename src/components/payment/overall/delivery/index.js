import React, {useCallback, useEffect, useState} from "react"
import {Alert, Button, Card, CardLink, CardText, Col, Container, Modal, ModalBody, ModalHeader, Row,} from "reactstrap"
import {Img, LinkTo} from "../../.."
import styles from "./Delivery.module.css"
import {
    awaitable,
    brlFormat,
    disable_btn_and_put_innerHTML,
    elm,
    elmAll,
    enable_btn_and_put_innerHTML,
    fromServer,
    hasKey,
} from "../../../../assets/helpers"
import {useRouter} from "next/router"
import {getToken} from "../../../../assets/auth"
import {ClipLoader} from "react-spinners"
import {MySwal} from "../../../../assets/sweetalert"

const Delivery = ({
                      setInstallments,
                      address,
                      setAddress,
                      shipping,
                      setShipping,
                      token,
                      timeoutFrete,
                      setTimeoutFrete,
                  }) => {
    const [modal, setModal] = useState(false)
    const router = useRouter()
    const [loaderState, setLoaderState] = useState(false)
    useEffect(() => {
        console.log("useEffect:[address]", {address})
        hydrateSelectedAddress()
        if (timeoutFrete) {
            setLoaderState(false)
        }
    }, [address])
    useEffect(() => {
        hydrateSelectedAddress()
        if (timeoutFrete) {
            setLoaderState(false)
        }
    }, [])
    useEffect(() => {
        if (timeoutFrete) {
            setLoaderState(false)
        }
    }, [timeoutFrete])
    const toggle = () => setModal(!modal)
    const [alert, setAlert] = useState(true)
    const toggleAlert = () => setAlert(!alert)
    const toggleAlertFrete = () => setTimeoutFrete(!timeoutFrete)
    const add = async (e) => {
        e.preventDefault()
        setLoaderState(true)
        await router.push("/account/[feature]", "/account/newaddress")
    }
    const [selectedAddress, setSelectedAddress] = useState({
        id: 0,
        name: false,
        postcode: false,
        state: false,
        city: false,
        neighborhood: false,
        street: false,
        number: false,
        complement: false,
        references: false,
        status: false,
        select_at: false,
    })
    const hydrateSelectedAddress = useCallback(() => {
        try {
            if (address?.length) {
                const selected = address.filter(
                    (address_) => address_.select_at === true
                )
                !!selected?.length && setSelectedAddress(selected[0])
                setAlert(false)
            }
        } catch (e) {
            console.log("hydrateSelectedAddress :catch", e, {address})
        }
    }, [address])
    return (
        <>
            {!!loaderState && (
                <div className="spinnerLoader">
                    <ClipLoader
                        size={150}
                        color={"var(--red-1)"}
                        loading={loaderState}
                    />
                </div>
            )}
            <Row className="mb-4">
                <Col xs="12">
                    <Modal
                        isOpen={modal}
                        toggle={toggle}
                        fade={false}
                        className={`modal-lg modal-dialog modal-dialog-centered ${
                            styles.modalAddress
                        }`}
                    >
                        <Img
                            image_name_with_extension="Mary-modal.png"
                            alt="Good Deal Today"
                            className={`showAboveTablet ${styles.mary}`}
                        />
                        <ModalHeader
                            className={styles.modalHeader}
                            toggle={toggle}
                        />
                        <ModalBody>
                            <Container>
                                <Row style={{marginBottom: "3rem"}}>
                                    <Col xs="12" lg={{size: 8, offset: 2}}>
                                        <h1>Escolha seu endereço</h1>
                                    </Col>
                                    <Col xs="12" lg={{size: 10, offset: 2}}>
                                        <div className={styles.cardWrap}>
                                            {address?.map(
                                                ({
                                                     id,
                                                     name,
                                                     state,
                                                     city,
                                                     neighborhood,
                                                     street,
                                                     number,
                                                     complement,
                                                     postcode,
                                                     select_at,
                                                 }) => (
                                                    <Card
                                                        key={id}
                                                        body
                                                        className={`${
                                                            styles.cardModal
                                                        } card-${id}`}
                                                    >
                                                        <Col
                                                            xs="12"
                                                            className="mb-4"
                                                            style={{
                                                                height: "11rem",
                                                            }}
                                                            {...(!select_at
                                                                ? {
                                                                    onClick: async (
                                                                        e
                                                                    ) => {
                                                                        e.preventDefault()
                                                                        setLoaderState(
                                                                            true
                                                                        )
                                                                        const btn = elm(
                                                                            `.card-${id}`
                                                                        )
                                                                        btn.style.cursor =
                                                                            "not-allowed !important"
                                                                        const data = await fromServer(
                                                                            "/addresses/select",
                                                                            token ||
                                                                            getToken(),
                                                                            "PUT",
                                                                            {
                                                                                fk_new_addresses: id,
                                                                            }
                                                                        )
                                                                        console.log(
                                                                            "BEFORE --/addresses/select",
                                                                            {
                                                                                data,
                                                                            }
                                                                        )
                                                                        ///carts/addresses/{{id}}
                                                                        const carts_addresses = await fromServer(
                                                                            `/carts/addresses/${id}`,
                                                                            token ||
                                                                            getToken(),
                                                                            "PUT"
                                                                        )
                                                                        console.log(
                                                                            `AFTER --/carts/addresses/${id}`,
                                                                            {
                                                                                carts_addresses,
                                                                            }
                                                                        )
                                                                        setTimeout(
                                                                            () => {
                                                                                if (
                                                                                    !shipping_types ||
                                                                                    hasKey(
                                                                                        shipping_types,
                                                                                        "error"
                                                                                    )
                                                                                ) {
                                                                                    awaitable(
                                                                                        async () => {
                                                                                            setTimeoutFrete(
                                                                                                true
                                                                                            )
                                                                                            setLoaderState(
                                                                                                false
                                                                                            )
                                                                                            await MySwal.fire(
                                                                                                "Erro no servidor dos correios",
                                                                                                "Tente novamente mais tarde",
                                                                                                "error"
                                                                                            )
                                                                                            toggle()
                                                                                        }
                                                                                    )
                                                                                    console.log(
                                                                                        "timeout 15s"
                                                                                    )
                                                                                }
                                                                            },
                                                                            15000
                                                                        )
                                                                        const shipping_types = await fromServer(
                                                                            `/shipping`,
                                                                            token ||
                                                                            getToken()
                                                                        )

                                                                        console.log(
                                                                            `AFTER --/shipping`,
                                                                            {
                                                                                shipping_types,
                                                                            }
                                                                        )

                                                                        if (
                                                                            !!shipping_types &&
                                                                            !hasKey(
                                                                                shipping_types,
                                                                                "error"
                                                                            )
                                                                        ) {
                                                                            setShipping(
                                                                                {
                                                                                    ...shipping,
                                                                                    options: shipping_types.map(
                                                                                        ({
                                                                                             price,
                                                                                             ...rest
                                                                                         }) => ({
                                                                                            ...rest,
                                                                                            price:
                                                                                                price /
                                                                                                100,
                                                                                        })
                                                                                    ),
                                                                                }
                                                                            )
                                                                        }
                                                                        const has_error =
                                                                            hasKey(
                                                                                data,
                                                                                "error"
                                                                            ) ||
                                                                            hasKey(
                                                                                carts_addresses,
                                                                                "error"
                                                                            ) ||
                                                                            hasKey(
                                                                                shipping_types,
                                                                                "error"
                                                                            )
                                                                        has_error &&
                                                                        console.log(
                                                                            "fail to update selected address",
                                                                            data?.error,
                                                                            carts_addresses?.error,
                                                                            shipping_types?.error
                                                                        )
                                                                        if (
                                                                            !has_error
                                                                        ) {
                                                                            setAddress(
                                                                                address.map(
                                                                                    (
                                                                                        addr
                                                                                    ) => {
                                                                                        if (
                                                                                            addr.select_at
                                                                                        ) {
                                                                                            return {
                                                                                                ...addr,
                                                                                                select_at: false,
                                                                                            }
                                                                                        }
                                                                                        if (
                                                                                            addr.id ===
                                                                                            id
                                                                                        ) {
                                                                                            return data
                                                                                        }
                                                                                        return addr
                                                                                    }
                                                                                )
                                                                            )
                                                                            setSelectedAddress(
                                                                                data
                                                                            )
                                                                        }
                                                                        btn.style.cursor =
                                                                            "pointer"
                                                                        setLoaderState(
                                                                            false
                                                                        )
                                                                        return true
                                                                    },
                                                                    style: {
                                                                        cursor:
                                                                            "pointer",
                                                                    },
                                                                }
                                                                : {})}
                                                        >
                                                            {select_at && (
                                                                <span
                                                                    className={
                                                                        styles.checkmark
                                                                    }
                                                                >
                                                                    <div
                                                                        className={
                                                                            styles.checkmark_stem
                                                                        }
                                                                    />
                                                                    <div
                                                                        className={
                                                                            styles.checkmark_kick
                                                                        }
                                                                    />
                                                                </span>
                                                            )}
                                                            <CardText
                                                                className={
                                                                    styles.cardName
                                                                }
                                                            >
                                                                {name}
                                                            </CardText>
                                                            <CardText
                                                                className={
                                                                    styles.cardText
                                                                }
                                                            >
                                                                {street &&
                                                                `${street}`}
                                                                {number &&
                                                                `, ${number} `}
                                                                {complement}
                                                            </CardText>
                                                            <CardText
                                                                className={
                                                                    styles.cardText
                                                                }
                                                            >
                                                                {neighborhood}
                                                            </CardText>
                                                            <CardText
                                                                className={
                                                                    styles.cardText
                                                                }
                                                            >
                                                                {city &&
                                                                `${city} `}
                                                                {state &&
                                                                `- ${state}`}
                                                            </CardText>
                                                            <CardText
                                                                className={
                                                                    styles.cardText
                                                                }
                                                            >
                                                                {postcode &&
                                                                `CEP: ${postcode}`}
                                                            </CardText>
                                                        </Col>
                                                        <LinkTo
                                                            _href="/account/[feature]"
                                                            as={
                                                                "/account/alteraddress"
                                                            }
                                                            query={{
                                                                name: name,
                                                                street: street,
                                                                number: number,
                                                                neighborhood: neighborhood,
                                                                city: city,
                                                                state: state,
                                                                postcode: postcode,
                                                                id: id,
                                                            }}
                                                        >
                                                            <Button
                                                                size="lg"
                                                                className={
                                                                    styles.editAddressModal
                                                                }
                                                            >
                                                                EDITAR
                                                            </Button>
                                                        </LinkTo>
                                                    </Card>
                                                )
                                            )}
                                        </div>
                                    </Col>
                                </Row>
                                <Row style={{marginBottom: "10rem"}}>
                                    <Col
                                        xs="12"
                                        lg={{size: 8, offset: 4}}
                                        style={{cursor: "pointer"}}
                                        onClick={add}
                                    >
                                        <Card body>
                                            <Col
                                                xs="12"
                                                className={styles.cardAddress}
                                            >
                                                <Img
                                                    image_name_with_extension="address.svg"
                                                    alt="Good Deal Today"
                                                />
                                                <CardLink
                                                    className={styles.cardName}
                                                >
                                                    Adicionar novo endereço
                                                </CardLink>
                                            </Col>
                                        </Card>
                                    </Col>
                                </Row>
                            </Container>
                        </ModalBody>
                    </Modal>
                </Col>
                <Col xs="12" className="mb-4">
                    <Card body className={styles.cardBody}>
                        <Col lg="12" className="mb-4">
                            <h1>Endereço de Entrega</h1>
                        </Col>
                        <Col lg="12" className="mb-4">
                            {!selectedAddress.status && (
                                <Alert
                                    color="primary"
                                    isOpen={alert}
                                    toggle={toggleAlert}
                                    style={{
                                        opacity: 1,
                                        color: "white",
                                        backgroundColor: "var(--red-1)",
                                        padding: "2rem",
                                        fontSize: "16px",
                                        textAlign: "center",
                                    }}
                                >
                                    Você ainda não possui endereço selecionado
                                    para entrega!
                                </Alert>
                            )}
                            {selectedAddress.status && (
                                <>
                                    <CardText className={styles.cardName}>
                                        {selectedAddress.name}
                                    </CardText>
                                    <CardText className={styles.cardText}>
                                        {selectedAddress.street}
                                        {selectedAddress.street &&
                                        selectedAddress.number &&
                                        ` - ${selectedAddress.number}`}
                                        {selectedAddress.complement && `, `}
                                        {selectedAddress.complement}
                                    </CardText>
                                    <CardText className={styles.cardText}>
                                        {selectedAddress.neighborhood}
                                    </CardText>
                                    <CardText className={styles.cardText}>
                                        {selectedAddress.city}
                                        {selectedAddress.city &&
                                        selectedAddress.state &&
                                        ` - ${selectedAddress.state}`}
                                    </CardText>
                                    <CardText className={styles.cardText}>
                                        {selectedAddress.postcode &&
                                        `CEP: ${selectedAddress.postcode}`}
                                    </CardText>
                                </>
                            )}
                        </Col>
                        <Col>
                            <Button
                                size="lg"
                                className={styles.editAddress}
                                onClick={toggle}
                            >
                                ALTERAR
                            </Button>
                        </Col>
                    </Card>
                </Col>
                <Col lg="12" className="mb-4">
                    <h1>Opções de frete</h1>
                    <h4 className="text-muted h5">
                        <b>Selecione</b> uma das opções abaixo para prosseguir
                        com a compra!
                    </h4>
                </Col>
                <Col lg="12">
                    <Row>
                        {shipping?.options?.map(
                            (
                                {
                                    price: valor,
                                    shipping_time: prazo,
                                    type: tipo,
                                },
                                index
                            ) => (
                                <Col key={index} lg={"6"} className={"mb-4"}>
                                    {prazo > 0 && valor > 0 && (
                                        <button
                                            id={`shipping-${index}-${tipo}`}
                                            className={`${
                                                styles.deliveryType
                                            } shipping btn btn-lg ${
                                                tipo === shipping.selected
                                                    ? "shipping_selected"
                                                    : ""
                                            }`}
                                            onClick={async (e) => {
                                                e.preventDefault()
                                                disable_btn_and_put_innerHTML(
                                                    `#shipping-${index}-${tipo}`
                                                )
                                                for (const btn of elmAll(
                                                    ".shipping"
                                                )) {
                                                    btn.disabled = true
                                                }
                                                const data = await fromServer(
                                                    "/shipping",
                                                    token || getToken(),
                                                    "POST",
                                                    {
                                                        type: tipo,
                                                        shipping_time: prazo,
                                                        price: valor * 100,
                                                    }
                                                )
                                                if (
                                                    !data ||
                                                    !hasKey(data, "error")
                                                ) {
                                                    setShipping({
                                                        ...shipping,
                                                        selected: tipo,
                                                        id: data.id,
                                                    })
                                                }
                                                for (const btn of elmAll(
                                                    ".shipping"
                                                )) {
                                                    btn.disabled = false
                                                }
                                                enable_btn_and_put_innerHTML(
                                                    `#shipping-${index}-${tipo}`,
                                                    `${tipo.toUpperCase()} | Receber em até ${prazo} dias úteis por ${" "} ${brlFormat(
                                                        valor
                                                    )}`
                                                )
                                            }}
                                        >
                                            <b>{tipo.toUpperCase()}</b> |
                                            Receber em até <b>{prazo}</b> dias
                                            úteis por
                                            <b>{" " + brlFormat(valor)}</b>
                                        </button>
                                    )}
                                </Col>
                            )
                        )}
                        {shipping.options.length < 1 && (
                            <Col xs="12">
                                <Alert
                                    color="primary"
                                    isOpen={alert}
                                    toggle={toggleAlert}
                                    style={{
                                        opacity: 1,
                                        color: "white",
                                        backgroundColor: "var(--red-1)",
                                        padding: "2rem",
                                        fontSize: "16px",
                                        textAlign: "center",
                                    }}
                                >
                                    Selecione um endereço para entrega logo
                                    acima
                                </Alert>
                            </Col>
                        )}
                        {timeoutFrete && (
                            <Col xs="12">
                                <Alert
                                    color="primary"
                                    isOpen={timeoutFrete}
                                    toggle={toggleAlertFrete}
                                    style={{
                                        opacity: 1,
                                        color: "white",
                                        backgroundColor: "var(--red-1)",
                                        padding: "2rem",
                                        fontSize: "16px",
                                        textAlign: "center",
                                    }}
                                >
                                    Erro no servidor dos Correios, tente
                                    novamente mais tarde
                                </Alert>
                            </Col>
                        )}
                    </Row>
                </Col>
            </Row>
        </>
    )
}

export default Delivery
