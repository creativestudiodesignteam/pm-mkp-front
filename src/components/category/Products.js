import React, { useContext } from "react"
import { Img, LinkTo } from "../../components/"
import {
    TabContent,
    TabPane,
    Pagination,
    PaginationItem,
    PaginationLink,
    Row,
    Col,
} from "reactstrap"
import styles from "../../../pages/category/Category.module.css"
import { brlFormat, toFixed } from "../../assets/helpers"
import { Context } from "../../store"
import ProductCard from "../home/ProductCard"

const Products = ({
    categories,
    activeTab,
    currentPage,
    setCurrentPage,
    pageSize,
    pagesCount,
    products,
    subcategoryName,
    showSub,
}) => {
    const [{ token }] = useContext(Context)
    const handleClick = (e, index) => {
        e.preventDefault()
        setCurrentPage(index)
    }
    return (
        <>
            <TabContent activeTab={activeTab}>
                {categories.map((category) => {
                    return (
                        <TabPane key={category.id} tabId={category.id}>
                            <Col lg="12" className={styles.barCategory}>
                                <Row>
                                    <Col lg="6">
                                        {!showSub && (
                                            <p className={styles.titleCategory}>
                                                {category.name}
                                            </p>
                                        )}
                                        {showSub && (
                                            <p className={styles.titleCategory}>
                                                {subcategoryName}
                                            </p>
                                        )}
                                    </Col>
                                    <Col
                                        lg="6"
                                        className={styles.rightCategoryBar}
                                    >
                                        <br />
                                        <p className=" float-left">
                                            Mostrando{" "}
                                            {currentPage * pageSize + 1}–
                                            {(currentPage + 1) * pageSize >
                                            products.length
                                                ? products.length
                                                : (currentPage + 1) *
                                                  pageSize}{" "}
                                            de {products.length} resultados
                                        </p>
                                    </Col>
                                </Row>
                            </Col>
                            <Row>
                                {products
                                    .slice(
                                        currentPage * pageSize,
                                        (currentPage + 1) * pageSize
                                    )
                                    .map(
                                        (
                                            {
                                                id,
                                                thumb,
                                                name,
                                                infos: { minPrice, maxPrice },
                                            },
                                            idx
                                        ) => {
                                            return (
                                                <Col
                                                    key={idx}
                                                    xs="12"
                                                    lg="4"
                                                    className="paddingZero"
                                                >
                                                    <ProductCard
                                                        id={id}
                                                        thumb={thumb}
                                                        name={name}
                                                        infos={{
                                                            minPrice,
                                                            maxPrice,
                                                        }}
                                                        little={false}
                                                    />
                                                </Col>
                                            )
                                        }
                                    )}
                            </Row>
                        </TabPane>
                    )
                })}
                <Row>
                    <Col lg="12" className={styles.allignCenter}>
                        <Pagination aria-label="Page navigation example">
                            <PaginationItem disabled={currentPage <= 0}>
                                <PaginationLink
                                    className={styles.buttonPagination}
                                    first
                                    onClick={(e) => handleClick(e, 0)}
                                    href="#"
                                >
                                    Começo
                                </PaginationLink>
                            </PaginationItem>

                            {[...Array(pagesCount)].map((page, i) => (
                                <PaginationItem
                                    active={i === currentPage}
                                    key={i}
                                >
                                    <PaginationLink
                                        className={
                                            i === currentPage
                                                ? `${
                                                      styles.buttonPaginationActive
                                                  }`
                                                : `${styles.buttonPagination}`
                                        }
                                        onClick={(e) => handleClick(e, i)}
                                        href="#"
                                    >
                                        {i + 1}
                                    </PaginationLink>
                                </PaginationItem>
                            ))}

                            <PaginationItem
                                disabled={currentPage >= pagesCount - 1}
                            >
                                <PaginationLink
                                    className={styles.buttonPagination}
                                    onClick={(e) =>
                                        handleClick(e, pagesCount - 1)
                                    }
                                    href="#"
                                >
                                    Fim
                                </PaginationLink>
                            </PaginationItem>
                        </Pagination>
                    </Col>
                </Row>
            </TabContent>
        </>
    )
}

export default Products
