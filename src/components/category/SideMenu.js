import React, { useState } from "react"
import { Button, Col, Collapse, Nav, NavItem, NavLink } from "reactstrap"
import classnames from "classnames"
import {
    FaRegCaretSquareDown,
    FaRegCaretSquareUp,
    FaChevronDown,
    FaChevronUp,
} from "react-icons/fa"

const SideMenu = ({
    setPage,
    page,
    categories,
    activeTab,
    toggle,
    nameCategory,
    getProductsCat,
    getSubs,
    setSubCategoryName,
    setCategoryName,
}) => {
    const [isOpen, setIsOpen] = useState(false)
    const toggleCat = () => setIsOpen(!isOpen)
    return (
        <>
            <Col
                md="12"
                className="sider-bar-category border bottom-margin-default"
            >
                <p className="title-siderbar bold">
                    {nameCategory}{" "}
                    <Button
                        onClick={toggleCat}
                        style={{
                            color: "var(--red-1)",
                            backgroundColor: "transparent",
                            border: "none",
                        }}
                    >
                        {isOpen && <FaChevronDown />}
                        {!isOpen && <FaChevronUp />}
                    </Button>
                </p>
                <Collapse isOpen={isOpen}>
                    <Nav vertical className="clear-margin list-siderbar">
                        {categories.map((category, idx) => {
                            return (
                                <NavItem key={idx}>
                                    <NavLink
                                        href="#"
                                        style={{
                                            display: "contents",
                                            padding: "0",
                                        }}
                                        className={`${classnames({
                                            active: activeTab === idx,
                                        })}`}
                                        onClick={() => {
                                            getProductsCat(category.id)
                                            toggle(idx)
                                            getSubs(category.id)
                                            setSubCategoryName("")
                                            setCategoryName(category.name)
                                        }}
                                    >
                                        {category.name}
                                    </NavLink>
                                </NavItem>
                            )
                        })}

                        {(page.categories.length > 1 && (
                            <button
                                className="btn btn-outline-dark btn-lg btn-block"
                                onClick={(e) => {
                                    e.preventDefault()
                                    setPage({ ...page, categories: [0] })
                                }}
                            >
                                Ver mais
                                <FaRegCaretSquareDown className="ml-3" />
                            </button>
                        )) || (
                            <button
                                className="btn btn-outline-dark btn-lg btn-block"
                                onClick={(e) => {
                                    e.preventDefault()
                                    setPage({ ...page, categories: [0, 10] })
                                }}
                            >
                                Ver menos
                                <FaRegCaretSquareUp className="ml-3" />
                            </button>
                        )}
                    </Nav>
                </Collapse>
            </Col>
        </>
    )
}

export default SideMenu
