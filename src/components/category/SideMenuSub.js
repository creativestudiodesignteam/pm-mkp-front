import React, { useState } from "react"
import { Button, Col, Collapse, Nav, NavItem, NavLink } from "reactstrap"
import classnames from "classnames"
import {
    FaChevronDown,
    FaChevronUp,
    FaRegCaretSquareDown,
    FaRegCaretSquareUp,
} from "react-icons/fa"

const SideMenuSub = ({
    setPage,
    page,
    activeTab,
    nameCategory,
    subcategories,
    setSubCategoryName,
    getProductsSub,
    toggle,
    setCategoryName,
}) => {
    const [isOpen, setIsOpen] = useState(false)
    const toggleCat = () => setIsOpen(!isOpen)
    return (
        <>
            <Col
                md="12"
                className="sider-bar-category border bottom-margin-default"
            >
                <p className="title-siderbar bold">
                    {(window.innerWidth < 767.99 ||
                        window.innerWidth > 991.98) &&
                        nameCategory}
                    {window.innerWidth >= 767.99 &&
                        window.innerWidth <= 991.98 &&
                        "SubCat."}{" "}
                    <Button
                        onClick={toggleCat}
                        style={{
                            color: "var(--red-1)",
                            backgroundColor: "transparent",
                            border: "none",
                        }}
                    >
                        {isOpen && <FaChevronDown />}
                        {!isOpen && <FaChevronUp />}
                    </Button>
                </p>
                <Collapse isOpen={isOpen}>
                    <Nav vertical className="clear-margin list-siderbar">
                        {subcategories.map((category, idx) => {
                            return (
                                <NavItem key={idx}>
                                    <NavLink
                                        href="#"
                                        style={{
                                            display: "contents",
                                            padding: "0",
                                        }}
                                        className={`${classnames({
                                            active: activeTab === idx,
                                        })}`}
                                        onClick={() => {
                                            toggle(idx)
                                            getProductsSub(category.id)
                                            setSubCategoryName(category.name)
                                            setCategoryName("")
                                        }}
                                    >
                                        {category.name}
                                    </NavLink>
                                </NavItem>
                            )
                        })}
                        {(page.subcategories.length > 1 && (
                            <button
                                className="btn btn-outline-dark btn-lg btn-block"
                                onClick={(e) => {
                                    e.preventDefault()
                                    setPage({ ...page, subcategories: [0] })
                                }}
                            >
                                Ver mais
                                <FaRegCaretSquareDown className="ml-3" />
                            </button>
                        )) || (
                            <button
                                className="btn btn-outline-dark btn-lg btn-block"
                                onClick={(e) => {
                                    e.preventDefault()
                                    setPage({ ...page, subcategories: [0, 10] })
                                }}
                            >
                                {" "}
                                Ver menos
                                <FaRegCaretSquareUp className="ml-3" />
                            </button>
                        )}
                    </Nav>
                </Collapse>
            </Col>
        </>
    )
}

export default SideMenuSub
