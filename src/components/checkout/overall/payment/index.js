import React, {useEffect, useMemo, useState} from "react"
import {Card, Col, Row} from "reactstrap"
import styles from "./Payment.module.css"
import Cards from "react-credit-cards"
import {awaitable, fromServer, hasKey} from "../../../../assets/helpers";

const Payment = ({url = false, card_id = false, card: serverCard = false, context: {token}, ...props}) => {
    const [payment, setpayment] = useState({
        type: 2,
        card_number: "",
        card_holder: "",
    })
    useEffect(() => {
        awaitable(async () => {
            if (!!card_id && payment.card_holder === '') {
                const card = await fromServer(`/creditcard/find/${card_id}`, token)
                if (!!card && !hasKey(card, 'error')) {
                    setpayment(({...payment, card_holder: card.holder_name, card_number: card.first_digits}))
                }
                //console.log("card was changed", {card, ...props})
            } else if (!!serverCard && payment.card_holder === '') {
                //console.log({serverCard})
                const _card = JSON.parse(serverCard || `{}`)
                setpayment({
                    ...payment,
                    card_holder: _card?.card_holder, card_number: _card?.card_number
                })
            }
        })
    }, [card_id, serverCard])
    return useMemo(() =>
            <>
                <Card body className={styles.cardBody}>
                    <Row>
                        <Col lg="12" className="mb-4">
                            <h1>Pagamento</h1>
                            <h4>
                                {!!card_id || !!serverCard
                                    ? "Cartão de crédito"
                                    : "Boleto"}
                            </h4>
                        </Col>

                        <Col lg="12" className="allignerCenter">
                            {
                                (payment.card_holder !== '') && <Cards
                                    cvc={""}
                                    expiry={"XX-XXXX"}
                                    focused={""}
                                    name={payment.card_holder}
                                    number={payment.card_number}
                                    acceptedCards={[
                                        "visa",
                                        "mastercard",
                                        "elo",
                                        "hipercard",
                                        "amex",
                                        "dinersclub",
                                        "discover",
                                    ]}
                                    placeholders={{
                                        name: "nome no cartão",
                                        valid: "valido até",
                                    }}
                                    locale={{valid: "valido até"}}
                                />
                                ||
                                <a
                                    className="text_red pointer"
                                    target="_blank"
                                    href={url}
                                >
                                    Clique aqui para visualizar
                                </a>

                            }

                        </Col>
                    </Row>
                </Card>
            </>
        , [payment, card_id, serverCard]);
}

export default Payment
