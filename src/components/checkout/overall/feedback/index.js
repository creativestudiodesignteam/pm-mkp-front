import React from "react"
import { Row, Col, Button } from "reactstrap"
import Img from "../../../Img"
import styles from "./Feedback.module.css"
import { useRouter } from "next/router"

const Feedback = ({...props}) => {
    const router = useRouter()

    return (
        <>
            <Row className="mb-4 w-100 mt-5 m-0">
                <Col xs="12" className={styles.cardPrincipal}>
                    <Row className="mb-4 mt-4">
                        <Col xs={{ size: 12 }}>
                            <Img
                                className={styles.imgMary}
                                image_name_with_extension="Mary-newsign.png"
                                alt="Finish"
                            />
                        </Col>
                    </Row>
                    <Row className="mb-4 mt-4">
                        <Col
                            xs={{ size: 8, offset: 3 }}
                            className={styles.aligner}
                        >
                            <h1 className={styles.textOne}>
                                Sua compra foi realizada com sucesso!
                            </h1>
                            <h3 className={styles.textTwo}>
                                Estamos esperando a aprovação da sua operadora
                                para liberação do pedido. Você pode acompanhar
                                esse processo na aba Pedidos em seu perfil
                            </h3>
                        </Col>
                    </Row>
                    <Row className="mb-5 m-0">
                        <Col
                            xs={{ size: 8, offset: 3 }}
                            className={styles.alignBottom}
                        >
                            <Button
                                className={styles.buttonLeft}
                                size="lg"
                                onClick={(e) => {
                                    e.preventDefault()
                                    router.push("/account/request")
                                }}
                            >
                                ACOMPANHAR PEDIDO
                            </Button>
                            <Button
                                className={styles.buttonRight}
                                size="lg"
                                onClick={(e) => {
                                    e.preventDefault()
                                    router.push("/")
                                }}
                            >
                                CONTINUAR COMPRANDO
                            </Button>
                        </Col>
                    </Row>
                </Col>
            </Row>
        </>
    )
}

export default Feedback
