import React, {useContext, useEffect, useState} from "react"
import {Card, CardText, Col, Row} from "reactstrap"
import styles from "./Delivery.module.css"
import {getToken, userByToken} from "../../../../assets/auth";
import {Context} from "../../../../store";

const Delivery = (props) => {
    const [{token},] = useContext(Context)
    const [deliveryAddress, setdeliveryAddress] = useState({
        name: (userByToken(token || getToken()))?.name || '',
        street: props?._address.street,
        number: props?._address.number,
        complement: props?._address.complement,
        neighborhood: props?._address.neighborhood,
        city: props?._address.city,
        state: props?._address.state,
        zipCode: props?._address.postcode,
    })
    return (
        <>
            <Card body className={styles.cardBody}>
                <Row>
                    <Col lg="12" className="mb-4">
                        <h1>Endereço de Entrega</h1>
                    </Col>
                </Row>
                <Row>
                    <Col lg="12" className="mt-4">
                        <CardText className={styles.cardName}>
                            {deliveryAddress?.name}
                        </CardText>
                        <CardText className={styles.cardText}>
                            {deliveryAddress?.street}, {deliveryAddress?.number}{" "}
                            {deliveryAddress?.complement?.length > 0 &&
                            "," + deliveryAddress?.complement}
                        </CardText>
                        <CardText className={styles.cardText}>
                            {deliveryAddress?.neighborhood}
                        </CardText>
                        <CardText className={styles.cardText}>
                            {deliveryAddress?.city} - {deliveryAddress?.state}
                        </CardText>
                        <CardText className={styles.cardText}>
                            CEP: {deliveryAddress?.zipCode}
                        </CardText>
                    </Col>
                </Row>
            </Card>
        </>
    )
}

export default Delivery
