import React from "react"
import {Button, Card, Col, Row} from "reactstrap"
import styles from "./Total.module.css"
import {brlFormat} from "../../../../assets/helpers";

const Total = ({...props}) => {
    return (
        <>
            <Card body className={styles.cardBody}>
                <Row>
                    <Col xs="12" className="mb-2">
                        <h1>Total</h1>
                    </Col>
                </Row>
                <Row>
                    <Col xs="12" className={styles.alignerAround}>
                        <Button size="lg" className={styles.total}>
                            Subtotal
                        </Button>

                        <Button
                            outline
                            className={styles.price}
                        >{brlFormat(Number((props?.total)))}
                        </Button>
                    </Col>
                    <Col xs="12" className={styles.alignerAround}>
                        <Button size="lg" className={styles.total}>
                            Frete
                        </Button>
                        <Button outline
                                className={styles.price}>{brlFormat(Number((props?.shipping)))}</Button>
                    </Col>
                </Row>
                <hr
                    style={{
                        marginTop: "0.5rem",
                        marginBottom: "0.5rem",
                    }}
                />
                <Row>
                    <Col xs="12" className={styles.alignerAround}>
                        <Button size="lg" className={styles.total}>
                            Total
                        </Button>
                        <Button outline
                                className={styles.price}>{brlFormat((Number((props?.total || 0)) + Number((props?.shipping || 0))))}</Button>
                    </Col>
                </Row>
            </Card>
        </>
    )
}

export default Total
