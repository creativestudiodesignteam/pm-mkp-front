import React, {useEffect} from "react"
import {Col, Row} from "reactstrap"
import Delivery from "./delivery"
import Total from "./total"
import Feedback from "./feedback"
import Payment from "./payment";

const Overall = ({...props}) => {
    useEffect(() => {
        console.log('Overall', {...props})
    }, [props])
    return (
        <>
            <Row>
                <Col lg="4">
                    <Payment  {...props}/>
                </Col>
                <Col lg="4">
                    <Total  {...props}/>
                </Col>
                <Col lg="4">
                    <Delivery  {...props}/>
                </Col>
            </Row>
            <Row>
                <Col lg="12">
                    <Feedback {...props}/>
                </Col>
            </Row>
        </>
    )
}

export default Overall
