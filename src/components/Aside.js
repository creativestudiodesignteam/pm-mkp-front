import React, { useContext, useEffect, useState } from "react"
import { Img } from "../../src/components"
import LinkTo from "./LinkTo"
import { Context } from "../store"
import { useRouter } from "next/router"
import ClipLoader from "react-spinners/ClipLoader"

const Aside = ({
    user,
    id = "dashboard_pj",
    links = [],
    feature,
    setAsideToggle,
    asideToggle,
}) => {
    const [{ token }] = useContext(Context)
    const router = useRouter()
    const [loaderState, setLoaderState] = useState(false)

    useEffect(() => {
        setLoaderState(false)
    }, [router])

    const checkRoute = (next) => {
        if (router.pathname !== next) {
            setLoaderState(true)
        }
    }

    return (
        <>
            {loaderState && (
                <div className="spinnerLoader">
                    <ClipLoader
                        size={150}
                        color={"var(--red-1)"}
                        loading={loaderState}
                    />
                </div>
            )}
            <aside
                id={id}
                className={`${
                    asideToggle ? "w-100" : ""
                } border shadow-sm mr-5 mb-4 flex-shrink-0`}
            >
                <ul
                    className={`list-group  border-0 mx-4 bg-white border-0 mx-4 shadow-none paddingBottomAside`}
                >
                    {links &&
                        links.map(([key, value], index) => {
                            const actived = feature === key ? "text_red " : ""
                            const classs = `${actived} ${!index &&
                                "marginTopLinkAside"} ${(index ===
                                links.length - 1 &&
                                "mb-7") ||
                                "mb-0"}`
                            return (
                                <li
                                    key={index}
                                    className={`list-group-item border-0  mx-4 ${classs} ${
                                        key === "alteraddress" ||
                                        key === "newaddress" ||
                                        key === "newcreditcard" ||
                                        key === "newoption" ||
                                        key === "alteroption"
                                            ? "d-none"
                                            : ""
                                    }
                ${
                    (key === "sales" ||
                        key === "product" ||
                        key === "options" ||
                        key === "sales") &&
                    !user.type
                        ? "d-none"
                        : ""
                }`}
                                >
                                    <LinkTo
                                        _href={`/account/[feature]`}
                                        query={{ token }}
                                        as={`/account/${key}`}
                                    >
                                        <p
                                            className="m-0 p-0 titleAsideMobile"
                                            onClick={() => {
                                                if (asideToggle) {
                                                    setAsideToggle(false)
                                                }
                                                checkRoute(`/account/${key}`)
                                            }}
                                        >
                                            {value}
                                        </p>
                                    </LinkTo>
                                </li>
                            )
                        })}
                </ul>
                {
                    <footer
                        id="aside_footer"
                        className="border-0 bg_red text-white p-4 mt-5 showAboveTablet"
                    >
                        <Img
                            style={{
                                position: "absolute",
                                bottom: 0,
                                left: "-65px",
                                height: "23rem",
                            }}
                            image_name_with_extension={"Mary-2.png"}
                            alt={"Seja Bem vindo"}
                        />
                        <div className="pl-2">
                            <Img
                                className="mb-2"
                                style={{
                                    position: "relative",
                                    left: 0,
                                    height: "2rem",
                                    width: "2rem",
                                }}
                                image_name_with_extension="speech-balloon.png"
                            />
                            <p className="mb-0">Olá!</p>
                            <p className="mb-0">Seja bem-vindo(a) ao</p>
                            <p className="mb-0 bold">Portal da Maria!</p>
                        </div>
                    </footer>
                }
            </aside>
        </>
    )
}

export default Aside
