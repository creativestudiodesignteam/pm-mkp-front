import { Container } from "reactstrap"
import React, { useEffect, useState } from "react"
import { FaArrowCircleUp } from "react-icons/fa"
import ClipLoader from "react-spinners/ClipLoader"

const Main = ({ children, _BreadCrumb = [{}], _className = "", astro }) => {
    let window = require("global/window")
    const [showArrow, setShowArrow] = useState(false)
    const scrollTop = () => {
        window.scrollTo({ top: 0, behavior: "smooth" })
    }

    const handleScroll = () => {
        if (window.pageYOffset > 200) {
            setShowArrow(true)
        } else {
            setShowArrow(false)
        }
    }

    useEffect(() => {
        window.addEventListener("scroll", handleScroll)
    }, [])

    return (
        <div
            className={`w-100 relative contentApp ${
                children[0]?.props?.className === "top-product-detail relative"
                    ? "noOverflow"
                    : ""
            }`}
        >
            <Container id="main" className={`mb-10 ${_className}`}>
                {children}
                <FaArrowCircleUp
                    color="#e3171b"
                    className={`${showArrow ? "scrollTop" : "d-none"}`}
                    onClick={scrollTop}
                />
            </Container>
        </div>
    )
}
export default Main
