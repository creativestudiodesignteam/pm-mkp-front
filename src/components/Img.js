import React from "react"

export default function Component({
    outSide = false,
    image_name_with_extension,
    alt,
    ...props
}) {
    return (
        <img
            src={
                outSide
                    ? image_name_with_extension
                    : `/img/${image_name_with_extension}`
            }
            alt={alt}
            {...props}
        />
    )
}
