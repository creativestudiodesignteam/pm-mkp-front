import React from "react";
import { Img, Link } from "./index";
import { verifyAllAttrs } from "./../assets/helpers";
import propTypes from "prop-types";

const ContentProductBox = ({ data = [] }) => {
  const _data = data.map((row, i) => (
    <div key={i} className="clearfix ">
      {row &&
        row[0] &&
        verifyAllAttrs(row[0], ["_class", "img", "alt", "href"]) &&
        row.map(({ _class, img, alt, href }, index) => (
          <div key={index} className={_class}>
            <Img image_name_with_extension={img} alt={alt} />
            <Link _href={href} />
          </div>
        ))}
    </div>
  ));

  return (
    <div className="clearfix content-product-box bottom-margin-default full-width deal-hot-v2">
      {_data}
    </div>
  );
};
ContentProductBox.propTypes = {
  data: propTypes.array,
};
export default ContentProductBox;
