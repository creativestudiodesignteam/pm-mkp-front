import React from "react"
import { Spinner, Container, Row, Col } from "reactstrap"
import styles from "./index.module.css"

function Index({
    type = "border",
    size = "lg",
    color = "primary",
    className = styles.Spinner,
    children = "...",
}) {
    return (
        <Container className={styles.containerSpinner}>
            <Row className="allignerCenter w-100 h-100">
                <Col xs="12" className="allignerCenter">
                    <Spinner
                        type={type}
                        size={size}
                        color={color}
                        className={className}
                        children={children}
                    />
                </Col>
            </Row>
        </Container>
    )
}

export default Index
