/* eslint-disable no-undef */
/**global localStorage */
import React, { useEffect, useState } from "react"
import { Badge, Col, Container, Row } from "reactstrap"
import { Img, LinkTo } from "./../components"
import { useRouter } from "next/router"
import { fromServer, tryCatch, tryInitializeDataStore } from "../assets/helpers"
import { _storage } from "../assets/auth"
import { cart_length } from "../store"
import { FaList } from "react-icons/fa"
import ReactMegaMenu from "react-mega-menu"
import ClipLoader from "react-spinners/ClipLoader"

function FormSearch({ _onSubmit = () => false, checkRoute }) {
    const [state, setState] = useState("")
    return (
        <form
            id="navbar_search"
            className="form-inline my-2 my-lg-0 d-flex flex-row flex-nowrap h-100 mx-auto mb-4"
            onSubmit={(e) => {
                e.preventDefault()
                return false
            }}
        >
            <input
                onChange={(event) => {
                    event.preventDefault()
                    setState(event.target.value)
                }}
                value={state}
                id="input_navbar_search"
                className="form-control h-100 w-100"
                type="search"
                placeholder="Pesquise produto aqui..."
                aria-label="Search"
            />
            <LinkTo _href={"/found"} query={{ text: state || "" }}>
                <button
                    id={"btn_search"}
                    type="button"
                    className="btn my-2 my-sm-0 btn-lg h-100 btn-custom rounded-0 border-0 px-5 py-3 actively text-white"
                    onClick={(e) => checkRoute("/found")}
                >
                    Buscar
                </button>
            </LinkTo>
        </form>
    )
}

const Component = ({ context, dispatch }) => {
    const router = useRouter()
    const [active, setActive] = useState(null)
    const [menuCat, setMenuCat] = useState(false)
    const [toggleMenu, setToggleMenu] = useState(false)
    const { categories, subCat } = context
    const [loaderState, setLoaderState] = useState(false)
    const toggle = () => setToggleMenu(!toggleMenu)
    const toggleActive = (sub) => {
        if (active === sub) {
            setActive(null)
        } else {
            setActive(sub)
        }
    }
    const goTo = () => {
        setLoaderState(true)
    }
    const checkRoute = (next) => {
        if (router.pathname !== next) {
            setLoaderState(true)
        }
    }

    useEffect(
        tryInitializeDataStore(
            {
                context,
                dispatch,
                cb: () => {
                    setLoaderState(true)
                    console.log("before useEffect []", context.navbar_props)
                    return () => {
                        setLoaderState(false)
                        console.log("after useEffect []", context.navbar_props)
                    }
                },
            },
            true
        ),
        []
    )
    useEffect(
        tryInitializeDataStore(
            {
                context,
                dispatch,
                cb: () => {
                    setLoaderState(true)
                    console.log(
                        "before useEffect [context.token]",
                        context.navbar_props
                    )
                    return () => {
                        setLoaderState(false)
                        console.log(
                            "after useEffect [context.token]",
                            context.navbar_props
                        )
                    }
                },
            },
            true
        ),
        [context.token]
    )
    useEffect(() => {
        console.log(
            "useEffect [router.pathname]",
            { context },
            context.navbar_props
        )
        if (router.pathname.substring(0, 9) === "/category") {
            setMenuCat(false)
        } else {
            setMenuCat(true)
        }
        setToggleMenu(false)
        if (router.pathname !== "/account/out") {
            tryCatch(async () => {
                if (context.token) {
                    const { length } = await fromServer("/carts", context.token)
                    dispatch(cart_length(length))
                    setLoaderState(false)
                } else {
                    const storage = _storage()
                    if (!!storage && !!storage.getItem("cart")) {
                        const { length } = JSON.parse(storage.getItem("cart"))
                        dispatch(cart_length(length))
                        setLoaderState(false)
                    } else {
                        setLoaderState(false)
                    }
                }
            })
        } else {
            setLoaderState(false)
        }
        setTimeout(() => {
            setLoaderState(false)
        }, 500)
    }, [router.pathname])
    useEffect(() => {
        if (toggleMenu === false) {
            toggleActive(null)
        }
    }, [toggleMenu])
    useEffect(() => {
        const { categories, subCat } = context
        console.log({ categories, subCat })
    }, [context])
    const categoriesMenu = [
        {
            label: "Ar-condicionado e aquecedores",
            key: "Category1",
            items: (
                <div
                    style={{
                        width: "90.4rem",
                        height: "57.5rem",
                        backgroundColor: "#fff",
                        marginTop: "1rem",
                        borderRight: "1px solid #f6f6f6",
                        borderBottom: "1px solid #f6f6f6",
                    }}
                >
                    <Row>
                        <Col md="12" style={{ padding: "3rem" }}>
                            <h1
                                className="bg-red text-white p-1"
                                style={{ width: "max-content" }}
                            >
                                Ar-condicionado e aquecedores
                            </h1>
                        </Col>
                    </Row>
                    <Row>
                        <Col md="4" className="bannerMenuDrop">
                            {subCat?.ar?.slice(0, 6).map((sub, idx) => {
                                return (
                                    <>
                                        <LinkTo
                                            key={idx}
                                            _href="/category"
                                            query={{
                                                id_sub: sub.id,
                                                id_cat: 1,
                                            }}
                                        >
                                            <div onClick={goTo}>{sub.name}</div>
                                        </LinkTo>

                                        <li
                                            className={`bannerMenuBottom ${
                                                idx === 5 ? "d-none" : ""
                                            }`}
                                        />
                                    </>
                                )
                            })}
                        </Col>
                        <Col md="4" className="bannerMenuDrop">
                            {subCat?.ar?.slice(6, 12).map((sub, idx) => {
                                return (
                                    <>
                                        <LinkTo
                                            key={idx}
                                            _href="/category"
                                            query={{
                                                id_sub: sub.id,
                                                id_cat: 1,
                                            }}
                                        >
                                            <div onClick={goTo}>{sub.name}</div>
                                        </LinkTo>
                                        <li
                                            className={`bannerMenuBottom ${
                                                idx === 5 ? "d-none" : ""
                                            }`}
                                        />
                                    </>
                                )
                            })}
                        </Col>
                        <Col md="4" className="bannerMenuDrop">
                            {subCat?.ar?.slice(12).map((sub, idx) => {
                                return (
                                    <>
                                        <LinkTo
                                            key={idx}
                                            _href="/category"
                                            query={{
                                                id_sub: sub.id,
                                                id_cat: 1,
                                            }}
                                        >
                                            <div onClick={goTo}>{sub.name}</div>
                                        </LinkTo>
                                        <li
                                            className={`bannerMenuBottom ${
                                                idx === subCat?.ar.length - 1
                                                    ? "d-none"
                                                    : ""
                                            }`}
                                        />
                                    </>
                                )
                            })}
                        </Col>
                    </Row>
                    <Row style={{ marginTop: "2rem" }}>
                        <Col md="12" className="bannerAllSub">
                            <LinkTo
                                _href="/category"
                                query={{
                                    id_cat: 1,
                                }}
                            >
                                Ver todas as subcategorias
                            </LinkTo>
                        </Col>
                    </Row>
                </div>
            ),
        },
        {
            label: "Automotivo",
            key: "Category2",
            items: (
                <div
                    style={{
                        width: "90.4rem",
                        height: "57.5rem",
                        backgroundColor: "#fff",
                        marginTop: "1rem",
                        borderRight: "1px solid #f6f6f6",
                        borderBottom: "1px solid #f6f6f6",
                    }}
                >
                    <Row>
                        <Col md="12" style={{ padding: "3rem" }}>
                            <h1
                                className="bg-red text-white p-1"
                                style={{ width: "max-content" }}
                            >
                                Automotivo
                            </h1>
                        </Col>
                    </Row>
                    <Row>
                        <Col md="4" className="bannerMenuDrop">
                            {subCat?.automotivo?.slice(0, 6).map((sub, idx) => {
                                return (
                                    <>
                                        <LinkTo
                                            key={idx}
                                            _href="/category"
                                            query={{
                                                id_sub: sub.id,
                                                id_cat: 4,
                                            }}
                                        >
                                            <div onClick={goTo}>{sub.name}</div>
                                        </LinkTo>
                                        <li
                                            className={`bannerMenuBottom ${
                                                idx === 5 ? "d-none" : ""
                                            }`}
                                        />
                                    </>
                                )
                            })}
                        </Col>
                        <Col md="4" className="bannerMenuDrop">
                            {subCat?.automotivo
                                ?.slice(6, 12)
                                .map((sub, idx) => {
                                    return (
                                        <>
                                            <LinkTo
                                                key={idx}
                                                _href="/category"
                                                query={{
                                                    id_sub: sub.id,
                                                    id_cat: 4,
                                                }}
                                            >
                                                <div onClick={goTo}>
                                                    {sub.name}
                                                </div>
                                            </LinkTo>
                                            <li
                                                className={`bannerMenuBottom ${
                                                    idx === 5 ? "d-none" : ""
                                                }`}
                                            />
                                        </>
                                    )
                                })}
                        </Col>
                        <Col md="4" className="bannerMenuDrop">
                            {subCat?.automotivo?.slice(12).map((sub, idx) => {
                                return (
                                    <>
                                        <LinkTo
                                            key={idx}
                                            _href="/category"
                                            query={{
                                                id_sub: sub.id,
                                                id_cat: 4,
                                            }}
                                        >
                                            <div onClick={goTo}>{sub.name}</div>
                                        </LinkTo>
                                        <li
                                            className={`bannerMenuBottom ${
                                                idx ===
                                                subCat?.automotivo.length - 1
                                                    ? "d-none"
                                                    : ""
                                            }`}
                                        />
                                    </>
                                )
                            })}
                        </Col>
                    </Row>
                    <Row style={{ marginTop: "2rem" }}>
                        <Col md="12" className="bannerAllSub">
                            <LinkTo
                                _href="/category"
                                query={{
                                    id_cat: 4,
                                }}
                            >
                                Ver todas as subcategorias
                            </LinkTo>
                        </Col>
                    </Row>
                </div>
            ),
        },
        {
            label: "Bebês",
            key: "Category3",
            items: (
                <div
                    style={{
                        width: "90.4rem",
                        height: "57.5rem",
                        backgroundColor: "#fff",
                        marginTop: "1rem",
                        borderRight: "1px solid #f6f6f6",
                        borderBottom: "1px solid #f6f6f6",
                    }}
                >
                    <Row>
                        <Col md="12" style={{ padding: "3rem" }}>
                            <h1
                                className="bg-red text-white p-1"
                                style={{ width: "max-content" }}
                            >
                                Bebês
                            </h1>
                        </Col>
                    </Row>
                    <Row>
                        <Col md="4" className="bannerMenuDrop">
                            {subCat?.bebes?.slice(0, 5).map((sub, idx) => {
                                return (
                                    <>
                                        <LinkTo
                                            key={idx}
                                            _href="/category"
                                            query={{
                                                id_sub: sub.id,
                                                id_cat: 7,
                                            }}
                                        >
                                            <div onClick={goTo}>{sub.name}</div>
                                        </LinkTo>
                                        <li
                                            className={`bannerMenuBottom ${
                                                idx === 4 ? "d-none" : ""
                                            }`}
                                        />
                                    </>
                                )
                            })}
                        </Col>
                        <Col md="4" className="bannerMenuDrop">
                            {subCat?.bebes?.slice(5, 10).map((sub, idx) => {
                                return (
                                    <>
                                        <LinkTo
                                            key={idx}
                                            _href="/category"
                                            query={{
                                                id_sub: sub.id,
                                                id_cat: 7,
                                            }}
                                        >
                                            <div onClick={goTo}>{sub.name}</div>
                                        </LinkTo>
                                        <li
                                            className={`bannerMenuBottom ${
                                                idx === 4 ? "d-none" : ""
                                            }`}
                                        />
                                    </>
                                )
                            })}
                        </Col>
                        <Col md="4" className="bannerMenuDrop">
                            {subCat?.bebes?.slice(10).map((sub, idx) => {
                                return (
                                    <>
                                        <LinkTo
                                            key={idx}
                                            _href="/category"
                                            query={{
                                                id_sub: sub.id,
                                                id_cat: 7,
                                            }}
                                        >
                                            <div onClick={goTo}>{sub.name}</div>
                                        </LinkTo>
                                        <li
                                            className={`bannerMenuBottom ${
                                                idx === subCat?.bebes.length - 1
                                                    ? "d-none"
                                                    : ""
                                            }`}
                                        />
                                    </>
                                )
                            })}
                        </Col>
                    </Row>
                    <Row style={{ marginTop: "2rem" }}>
                        <Col md="12" className="bannerAllSub">
                            <LinkTo
                                _href="/category"
                                query={{
                                    id_cat: 7,
                                }}
                            >
                                Ver todas as subcategorias
                            </LinkTo>
                        </Col>
                    </Row>
                </div>
            ),
        },
        {
            label: "Beleza e Perfumaria",
            key: "Category4",
            items: (
                <div
                    style={{
                        width: "90.4rem",
                        height: "57.5rem",
                        backgroundColor: "#fff",
                        marginTop: "1rem",
                        borderRight: "1px solid #f6f6f6",
                        borderBottom: "1px solid #f6f6f6",
                    }}
                >
                    <Row>
                        <Col md="12" style={{ padding: "3rem" }}>
                            <h1
                                className="bg-red text-white p-1"
                                style={{ width: "max-content" }}
                            >
                                Beleza e Perfumaria
                            </h1>
                        </Col>
                    </Row>
                    <Row>
                        <Col md="4" className="bannerMenuDrop">
                            {subCat?.beleza?.slice(0, 5).map((sub, idx) => {
                                return (
                                    <>
                                        <LinkTo
                                            key={idx}
                                            _href="/category"
                                            query={{
                                                id_sub: sub.id,
                                                id_cat: 6,
                                            }}
                                        >
                                            <div onClick={goTo}>{sub.name}</div>
                                        </LinkTo>
                                        <li
                                            className={`bannerMenuBottom ${
                                                idx === 4 ? "d-none" : ""
                                            }`}
                                        />
                                    </>
                                )
                            })}
                        </Col>
                        <Col md="4" className="bannerMenuDrop">
                            {subCat?.beleza?.slice(5, 10).map((sub, idx) => {
                                return (
                                    <>
                                        <LinkTo
                                            key={idx}
                                            _href="/category"
                                            query={{
                                                id_sub: sub.id,
                                                id_cat: 6,
                                            }}
                                        >
                                            <div onClick={goTo}>{sub.name}</div>
                                        </LinkTo>
                                        <li
                                            className={`bannerMenuBottom ${
                                                idx === 4 ? "d-none" : ""
                                            }`}
                                        />
                                    </>
                                )
                            })}
                        </Col>
                        <Col md="4" className="bannerMenuDrop">
                            {subCat?.beleza?.slice(10).map((sub, idx) => {
                                return (
                                    <>
                                        <LinkTo
                                            key={idx}
                                            _href="/category"
                                            query={{
                                                id_sub: sub.id,
                                                id_cat: 6,
                                            }}
                                        >
                                            <div onClick={goTo}>{sub.name}</div>
                                        </LinkTo>
                                        <li
                                            className={`bannerMenuBottom ${
                                                idx ===
                                                subCat?.beleza.length - 1
                                                    ? "d-none"
                                                    : ""
                                            }`}
                                        />
                                    </>
                                )
                            })}
                        </Col>
                    </Row>
                    <Row style={{ marginTop: "2rem" }}>
                        <Col md="12" className="bannerAllSub">
                            <LinkTo
                                _href="/category"
                                query={{
                                    id_cat: 6,
                                }}
                            >
                                Ver todas as subcategorias
                            </LinkTo>
                        </Col>
                    </Row>
                </div>
            ),
        },
        {
            label: "Brinquedos e Jogos",
            key: "Category5",
            items: (
                <div
                    style={{
                        width: "90.4rem",
                        height: "57.5rem",
                        backgroundColor: "#fff",
                        marginTop: "1rem",
                        borderRight: "1px solid #f6f6f6",
                        borderBottom: "1px solid #f6f6f6",
                    }}
                >
                    <Row>
                        <Col md="12" style={{ padding: "3rem" }}>
                            <h1
                                className="bg-red text-white p-1"
                                style={{ width: "max-content" }}
                            >
                                Brinquedos e Jogos
                            </h1>
                        </Col>
                    </Row>
                    <Row>
                        <Col md="4" className="bannerMenuDrop">
                            {subCat?.brinquedos?.slice(0, 5).map((sub, idx) => {
                                return (
                                    <>
                                        <LinkTo
                                            key={idx}
                                            _href="/category"
                                            query={{
                                                id_sub: sub.id,
                                                id_cat: 8,
                                            }}
                                        >
                                            <div onClick={goTo}>{sub.name}</div>
                                        </LinkTo>
                                        <li
                                            className={`bannerMenuBottom ${
                                                idx === 4 ? "d-none" : ""
                                            }`}
                                        />
                                    </>
                                )
                            })}
                        </Col>
                        <Col md="4" className="bannerMenuDrop">
                            {subCat?.brinquedos
                                ?.slice(5, 11)
                                .map((sub, idx) => {
                                    return (
                                        <>
                                            <LinkTo
                                                key={idx}
                                                _href="/category"
                                                query={{
                                                    id_sub: sub.id,
                                                    id_cat: 8,
                                                }}
                                            >
                                                <div onClick={goTo}>
                                                    {sub.name}
                                                </div>
                                            </LinkTo>
                                            <li
                                                className={`bannerMenuBottom ${
                                                    idx === 4 ? "d-none" : ""
                                                }`}
                                            />
                                        </>
                                    )
                                })}
                        </Col>
                        <Col md="4" className="bannerMenuDrop">
                            {subCat?.brinquedos?.slice(11).map((sub, idx) => {
                                return (
                                    <>
                                        <LinkTo
                                            key={idx}
                                            _href="/category"
                                            query={{
                                                id_sub: sub.id,
                                                id_cat: 8,
                                            }}
                                        >
                                            <div onClick={goTo}>{sub.name}</div>
                                        </LinkTo>
                                        <li
                                            className={`bannerMenuBottom ${
                                                idx ===
                                                subCat?.brinquedos.length - 1
                                                    ? "d-none"
                                                    : ""
                                            }`}
                                        />
                                    </>
                                )
                            })}
                        </Col>
                    </Row>
                    <Row style={{ marginTop: "2rem" }}>
                        <Col md="12" className="bannerAllSub">
                            <LinkTo
                                _href="/category"
                                query={{
                                    id_cat: 8,
                                }}
                            >
                                Ver todas as subcategorias
                            </LinkTo>
                        </Col>
                    </Row>
                </div>
            ),
        },
        {
            label: "Construção",
            key: "Category6",
            items: (
                <div
                    style={{
                        width: "90.4rem",
                        height: "57.5rem",
                        backgroundColor: "#fff",
                        marginTop: "1rem",
                        borderRight: "1px solid #f6f6f6",
                        borderBottom: "1px solid #f6f6f6",
                    }}
                >
                    <Row>
                        <Col md="12" style={{ padding: "3rem" }}>
                            <h1
                                className="bg-red text-white p-1"
                                style={{ width: "max-content" }}
                            >
                                Construção
                            </h1>
                        </Col>
                    </Row>
                    <Row>
                        <Col md="4" className="bannerMenuDrop">
                            {subCat?.construcao?.slice(0, 6).map((sub, idx) => {
                                return (
                                    <>
                                        <LinkTo
                                            key={idx}
                                            _href="/category"
                                            query={{
                                                id_sub: sub.id,
                                                id_cat: 9,
                                            }}
                                        >
                                            <div onClick={goTo}>{sub.name}</div>
                                        </LinkTo>
                                        <li
                                            className={`bannerMenuBottom ${
                                                idx === 5 ? "d-none" : ""
                                            }`}
                                        />
                                    </>
                                )
                            })}
                        </Col>
                        <Col md="4" className="bannerMenuDrop">
                            {subCat?.construcao
                                ?.slice(6, 12)
                                .map((sub, idx) => {
                                    return (
                                        <>
                                            <LinkTo
                                                key={idx}
                                                _href="/category"
                                                query={{
                                                    id_sub: sub.id,
                                                    id_cat: 9,
                                                }}
                                            >
                                                <div onClick={goTo}>
                                                    {sub.name}
                                                </div>
                                            </LinkTo>
                                            <li
                                                className={`bannerMenuBottom ${
                                                    idx === 5 ? "d-none" : ""
                                                }`}
                                            />
                                        </>
                                    )
                                })}
                        </Col>
                        <Col md="4" className="bannerMenuDrop">
                            {subCat?.construcao?.slice(12).map((sub, idx) => {
                                return (
                                    <>
                                        <LinkTo
                                            key={idx}
                                            _href="/category"
                                            query={{
                                                id_sub: sub.id,
                                                id_cat: 9,
                                            }}
                                        >
                                            <div onClick={goTo}>{sub.name}</div>
                                        </LinkTo>
                                        <li
                                            className={`bannerMenuBottom ${
                                                idx ===
                                                subCat?.construcao.length - 1
                                                    ? "d-none"
                                                    : ""
                                            }`}
                                        />
                                    </>
                                )
                            })}
                        </Col>
                    </Row>
                    <Row style={{ marginTop: "2rem" }}>
                        <Col md="12" className="bannerAllSub">
                            <LinkTo
                                _href="/category"
                                query={{
                                    id_cat: 9,
                                }}
                            >
                                Ver todas as subcategorias
                            </LinkTo>
                        </Col>
                    </Row>
                </div>
            ),
        },
        {
            label: "Cuidados Pessoais",
            key: "Category7",
            items: (
                <div
                    style={{
                        width: "90.4rem",
                        height: "57.5rem",
                        backgroundColor: "#fff",
                        marginTop: "1rem",
                        borderRight: "1px solid #f6f6f6",
                        borderBottom: "1px solid #f6f6f6",
                    }}
                >
                    <Row>
                        <Col md="12" style={{ padding: "3rem" }}>
                            <h1
                                className="bg-red text-white p-1"
                                style={{ width: "max-content" }}
                            >
                                Cuidados Pessoais
                            </h1>
                        </Col>
                    </Row>
                    <Row>
                        <Col md="4" className="bannerMenuDrop">
                            {subCat?.cuidados?.slice(0, 5).map((sub, idx) => {
                                return (
                                    <>
                                        <LinkTo
                                            key={idx}
                                            _href="/category"
                                            query={{
                                                id_sub: sub.id,
                                                id_cat: 10,
                                            }}
                                        >
                                            <div onClick={goTo}>{sub.name}</div>
                                        </LinkTo>
                                        <li
                                            className={`bannerMenuBottom ${
                                                idx === 5 ? "d-none" : ""
                                            }`}
                                        />
                                    </>
                                )
                            })}
                        </Col>
                        <Col md="4" className="bannerMenuDrop">
                            {subCat?.cuidados?.slice(5, 12).map((sub, idx) => {
                                return (
                                    <>
                                        <LinkTo
                                            key={idx}
                                            _href="/category"
                                            query={{
                                                id_sub: sub.id,
                                                id_cat: 10,
                                            }}
                                        >
                                            <div onClick={goTo}>{sub.name}</div>
                                        </LinkTo>
                                        <li
                                            className={`bannerMenuBottom ${
                                                idx === 11 ? "d-none" : ""
                                            }`}
                                        />
                                    </>
                                )
                            })}
                        </Col>
                        <Col md="4" className="bannerMenuDrop">
                            {subCat?.cuidados?.slice(12).map((sub, idx) => {
                                return (
                                    <>
                                        <LinkTo
                                            key={idx}
                                            _href="/category"
                                            query={{
                                                id_sub: sub.id,
                                                id_cat: 10,
                                            }}
                                        >
                                            <div onClick={goTo}>{sub.name}</div>
                                        </LinkTo>
                                        <li
                                            className={`bannerMenuBottom ${
                                                idx ===
                                                subCat?.cuidados.length - 1
                                                    ? "d-none"
                                                    : ""
                                            }`}
                                        />
                                    </>
                                )
                            })}
                        </Col>
                    </Row>
                    <Row style={{ marginTop: "2rem" }}>
                        <Col md="12" className="bannerAllSub">
                            <LinkTo
                                _href="/category"
                                query={{
                                    id_cat: 10,
                                }}
                            >
                                Ver todas as subcategorias
                            </LinkTo>
                        </Col>
                    </Row>
                </div>
            ),
        },
        {
            label: "Eletrodomésticos",
            key: "Category8",
            items: (
                <div
                    style={{
                        width: "90.4rem",
                        height: "57.5rem",
                        backgroundColor: "#fff",
                        marginTop: "1rem",
                        borderRight: "1px solid #f6f6f6",
                        borderBottom: "1px solid #f6f6f6",
                    }}
                >
                    <Row>
                        <Col md="12" style={{ padding: "3rem" }}>
                            <h1
                                className="bg-red text-white p-1"
                                style={{ width: "max-content" }}
                            >
                                Eletrodomésticos
                            </h1>
                        </Col>
                    </Row>
                    <Row>
                        <Col md="4" className="bannerMenuDrop">
                            {subCat?.eletrodomesticos
                                ?.slice(0, 5)
                                .map((sub, idx) => {
                                    return (
                                        <>
                                            <LinkTo
                                                key={idx}
                                                _href="/category"
                                                query={{
                                                    id_sub: sub.id,
                                                    id_cat: 12,
                                                }}
                                            >
                                                <div onClick={goTo}>
                                                    {sub.name}
                                                </div>
                                            </LinkTo>
                                            <li
                                                className={`bannerMenuBottom ${
                                                    idx === 4 ? "d-none" : ""
                                                }`}
                                            />
                                        </>
                                    )
                                })}
                        </Col>
                        <Col md="4" className="bannerMenuDrop">
                            {subCat?.eletrodomesticos
                                ?.slice(5, 10)
                                .map((sub, idx) => {
                                    return (
                                        <>
                                            <LinkTo
                                                key={idx}
                                                _href="/category"
                                                query={{
                                                    id_sub: sub.id,
                                                    id_cat: 12,
                                                }}
                                            >
                                                <div onClick={goTo}>
                                                    {sub.name}
                                                </div>
                                            </LinkTo>
                                            <li
                                                className={`bannerMenuBottom ${
                                                    idx === 4 ? "d-none" : ""
                                                }`}
                                            />
                                        </>
                                    )
                                })}
                        </Col>
                        <Col md="4" className="bannerMenuDrop">
                            {subCat?.eletrodomesticos
                                ?.slice(10)
                                .map((sub, idx) => {
                                    return (
                                        <>
                                            <LinkTo
                                                key={idx}
                                                _href="/category"
                                                query={{
                                                    id_sub: sub.id,
                                                    id_cat: 12,
                                                }}
                                            >
                                                <div onClick={goTo}>
                                                    {sub.name}
                                                </div>
                                            </LinkTo>
                                            <li
                                                className={`bannerMenuBottom ${
                                                    idx ===
                                                    subCat?.eletrodomesticos
                                                        .length -
                                                        1
                                                        ? "d-none"
                                                        : ""
                                                }`}
                                            />
                                        </>
                                    )
                                })}
                        </Col>
                    </Row>
                    <Row style={{ marginTop: "2rem" }}>
                        <Col md="12" className="bannerAllSub">
                            <LinkTo
                                _href="/category"
                                query={{
                                    id_cat: 12,
                                }}
                            >
                                Ver todas as subcategorias
                            </LinkTo>
                        </Col>
                    </Row>
                </div>
            ),
        },
        {
            label: "Eletrônicos",
            key: "Category10",
            items: (
                <div
                    style={{
                        width: "90.4rem",
                        height: "57.5rem",
                        backgroundColor: "#fff",
                        marginTop: "1rem",
                        borderRight: "1px solid #f6f6f6",
                        borderBottom: "1px solid #f6f6f6",
                    }}
                >
                    <Row>
                        <Col md="12" style={{ padding: "3rem" }}>
                            <h1
                                className="bg-red text-white p-1"
                                style={{ width: "max-content" }}
                            >
                                Eletrônicos
                            </h1>
                        </Col>
                    </Row>
                    <Row>
                        <Col md="4" className="bannerMenuDrop">
                            {subCat?.eletronicos
                                ?.slice(0, 5)
                                .map((sub, idx) => {
                                    return (
                                        <>
                                            <LinkTo
                                                key={idx}
                                                _href="/category"
                                                query={{
                                                    id_sub: sub.id,
                                                    id_cat: 13,
                                                }}
                                            >
                                                <div onClick={goTo}>
                                                    {sub.name}
                                                </div>
                                            </LinkTo>
                                            <li
                                                className={`bannerMenuBottom ${
                                                    idx === 4 ? "d-none" : ""
                                                }`}
                                            />
                                        </>
                                    )
                                })}
                        </Col>
                        <Col md="4" className="bannerMenuDrop">
                            {subCat?.eletronicos
                                ?.slice(5, 11)
                                .map((sub, idx) => {
                                    return (
                                        <>
                                            <LinkTo
                                                key={idx}
                                                _href="/category"
                                                query={{
                                                    id_sub: sub.id,
                                                    id_cat: 13,
                                                }}
                                            >
                                                <div onClick={goTo}>
                                                    {sub.name}
                                                </div>
                                            </LinkTo>
                                            <li
                                                className={`bannerMenuBottom ${
                                                    idx === 4 ? "d-none" : ""
                                                }`}
                                            />
                                        </>
                                    )
                                })}
                        </Col>
                        <Col md="4" className="bannerMenuDrop">
                            {subCat?.eletronicos?.slice(11).map((sub, idx) => {
                                return (
                                    <>
                                        <LinkTo
                                            key={idx}
                                            _href="/category"
                                            query={{
                                                id_sub: sub.id,
                                                id_cat: 13,
                                            }}
                                        >
                                            <div onClick={goTo}>{sub.name}</div>
                                        </LinkTo>
                                        <li
                                            className={`bannerMenuBottom ${
                                                idx ===
                                                subCat?.eletronicos.length - 1
                                                    ? "d-none"
                                                    : ""
                                            }`}
                                        />
                                    </>
                                )
                            })}
                        </Col>
                    </Row>
                    <Row style={{ marginTop: "2rem" }}>
                        <Col md="12" className="bannerAllSub">
                            <LinkTo
                                _href="/category"
                                query={{
                                    id_cat: 13,
                                }}
                            >
                                Ver todas as subcategorias
                            </LinkTo>
                        </Col>
                    </Row>
                </div>
            ),
        },
        {
            label: "Esportes",
            key: "Category11",
            items: (
                <div
                    style={{
                        width: "90.4rem",
                        height: "57.5rem",
                        backgroundColor: "#fff",
                        marginTop: "1rem",
                        borderRight: "1px solid #f6f6f6",
                        borderBottom: "1px solid #f6f6f6",
                    }}
                >
                    <Row>
                        <Col md="12" style={{ padding: "3rem" }}>
                            <h1
                                className="bg-red text-white p-1"
                                style={{ width: "max-content" }}
                            >
                                Esportes
                            </h1>
                        </Col>
                    </Row>
                    <Row>
                        <Col md="4" className="bannerMenuDrop">
                            {subCat?.esportes?.slice(0, 6).map((sub, idx) => {
                                return (
                                    <>
                                        <LinkTo
                                            key={idx}
                                            _href="/category"
                                            query={{
                                                id_sub: sub.id,
                                                id_cat: 16,
                                            }}
                                        >
                                            <div onClick={goTo}>{sub.name}</div>
                                        </LinkTo>
                                        <li
                                            className={`bannerMenuBottom ${
                                                idx === 5 ? "d-none" : ""
                                            }`}
                                        />
                                    </>
                                )
                            })}
                        </Col>
                        <Col md="4" className="bannerMenuDrop">
                            {subCat?.esportes?.slice(6, 12).map((sub, idx) => {
                                return (
                                    <>
                                        <LinkTo
                                            key={idx}
                                            _href="/category"
                                            query={{
                                                id_sub: sub.id,
                                                id_cat: 16,
                                            }}
                                        >
                                            <div onClick={goTo}>{sub.name}</div>
                                        </LinkTo>
                                        <li
                                            className={`bannerMenuBottom ${
                                                idx === 5 ? "d-none" : ""
                                            }`}
                                        />
                                    </>
                                )
                            })}
                        </Col>
                        <Col md="4" className="bannerMenuDrop">
                            {subCat?.esportes?.slice(12).map((sub, idx) => {
                                return (
                                    <>
                                        <LinkTo
                                            key={idx}
                                            _href="/category"
                                            query={{
                                                id_sub: sub.id,
                                                id_cat: 16,
                                            }}
                                        >
                                            <div onClick={goTo}>{sub.name}</div>
                                        </LinkTo>
                                        <li
                                            className={`bannerMenuBottom ${
                                                idx ===
                                                subCat?.esportes.length - 1
                                                    ? "d-none"
                                                    : ""
                                            }`}
                                        />
                                    </>
                                )
                            })}
                        </Col>
                    </Row>
                    <Row style={{ marginTop: "2rem" }}>
                        <Col md="12" className="bannerAllSub">
                            <LinkTo
                                _href="/category"
                                query={{
                                    id_cat: 16,
                                }}
                            >
                                Ver todas as subcategorias
                            </LinkTo>
                        </Col>
                    </Row>
                </div>
            ),
        },
        {
            label: "Filmes",
            key: "Category12",
            items: (
                <div
                    style={{
                        width: "90.4rem",
                        height: "57.5rem",
                        backgroundColor: "#fff",
                        marginTop: "1rem",
                        borderRight: "1px solid #f6f6f6",
                        borderBottom: "1px solid #f6f6f6",
                    }}
                >
                    <Row>
                        <Col md="12" style={{ padding: "3rem" }}>
                            <h1
                                className="bg-red text-white p-1"
                                style={{ width: "max-content" }}
                            >
                                Filmes
                            </h1>
                        </Col>
                    </Row>
                    <Row>
                        <Col md="4" className="bannerMenuDrop">
                            {subCat?.filmes?.slice(0, 5).map((sub, idx) => {
                                return (
                                    <>
                                        <LinkTo
                                            key={idx}
                                            _href="/category"
                                            query={{
                                                id_sub: sub.id,
                                                id_cat: 18,
                                            }}
                                        >
                                            <div onClick={goTo}>{sub.name}</div>
                                        </LinkTo>
                                        <li
                                            className={`bannerMenuBottom ${
                                                idx === 2 ? "d-none" : ""
                                            }`}
                                        />
                                    </>
                                )
                            })}
                        </Col>
                        <Col md="4" className="bannerMenuDrop">
                            {subCat?.filmes?.slice(5, 11).map((sub, idx) => {
                                return (
                                    <>
                                        <LinkTo
                                            key={idx}
                                            _href="/category"
                                            query={{
                                                id_sub: sub.id,
                                                id_cat: 18,
                                            }}
                                        >
                                            <div onClick={goTo}>{sub.name}</div>
                                        </LinkTo>
                                        <li
                                            className={`bannerMenuBottom ${
                                                idx === 4 ? "d-none" : ""
                                            }`}
                                        />
                                    </>
                                )
                            })}
                        </Col>
                        <Col md="4" className="bannerMenuDrop">
                            {subCat?.filmes?.slice(11).map((sub, idx) => {
                                return (
                                    <>
                                        <LinkTo
                                            key={idx}
                                            _href="/category"
                                            query={{
                                                id_sub: sub.id,
                                                id_cat: 18,
                                            }}
                                        >
                                            <div onClick={goTo}>{sub.name}</div>
                                        </LinkTo>
                                        <li
                                            className={`bannerMenuBottom ${
                                                idx ===
                                                subCat?.filmes.length - 1
                                                    ? "d-none"
                                                    : ""
                                            }`}
                                        />
                                    </>
                                )
                            })}
                        </Col>
                    </Row>
                    <Row style={{ marginTop: "2rem" }}>
                        <Col md="12" className="bannerAllSub">
                            <LinkTo
                                _href="/category"
                                query={{
                                    id_cat: 18,
                                }}
                            >
                                Ver todas as subcategorias
                            </LinkTo>
                        </Col>
                    </Row>
                </div>
            ),
        },
        {
            label: "Games",
            key: "Category13",
            items: (
                <div
                    style={{
                        width: "90.4rem",
                        height: "57.5rem",
                        backgroundColor: "#fff",
                        marginTop: "1rem",
                        borderRight: "1px solid #f6f6f6",
                        borderBottom: "1px solid #f6f6f6",
                    }}
                >
                    <Row>
                        <Col md="12" style={{ padding: "3rem" }}>
                            <h1
                                className="bg-red text-white p-1"
                                style={{ width: "max-content" }}
                            >
                                Games
                            </h1>
                        </Col>
                    </Row>
                    <Row>
                        <Col md="4" className="bannerMenuDrop">
                            {subCat?.games?.slice(0, 6).map((sub, idx) => {
                                return (
                                    <>
                                        <LinkTo
                                            key={idx}
                                            _href="/category"
                                            query={{
                                                id_sub: sub.id,
                                                id_cat: 19,
                                            }}
                                        >
                                            <div onClick={goTo}>{sub.name}</div>
                                        </LinkTo>
                                        <li
                                            className={`bannerMenuBottom ${
                                                idx === 4 ? "d-none" : ""
                                            }`}
                                        />
                                    </>
                                )
                            })}
                        </Col>
                        <Col md="4" className="bannerMenuDrop">
                            {subCat?.games?.slice(6, 12).map((sub, idx) => {
                                return (
                                    <>
                                        <LinkTo
                                            key={idx}
                                            _href="/category"
                                            query={{
                                                id_sub: sub.id,
                                                id_cat: 19,
                                            }}
                                        >
                                            <div onClick={goTo}>{sub.name}</div>
                                        </LinkTo>
                                        <li
                                            className={`bannerMenuBottom ${
                                                idx === 4 ? "d-none" : ""
                                            }`}
                                        />
                                    </>
                                )
                            })}
                        </Col>
                        <Col md="4" className="bannerMenuDrop">
                            {subCat?.games?.slice(12).map((sub, idx) => {
                                return (
                                    <>
                                        <LinkTo
                                            key={idx}
                                            _href="/category"
                                            query={{
                                                id_sub: sub.id,
                                                id_cat: 19,
                                            }}
                                        >
                                            <div onClick={goTo}>{sub.name}</div>
                                        </LinkTo>
                                        <li
                                            className={`bannerMenuBottom ${
                                                idx === subCat?.games.length - 1
                                                    ? "d-none"
                                                    : ""
                                            }`}
                                        />
                                    </>
                                )
                            })}
                        </Col>
                    </Row>
                    <Row style={{ marginTop: "2rem" }}>
                        <Col md="12" className="bannerAllSub">
                            <LinkTo
                                _href="/category"
                                query={{
                                    id_cat: 19,
                                }}
                            >
                                Ver todas as subcategorias
                            </LinkTo>
                        </Col>
                    </Row>
                </div>
            ),
        },
        {
            label: "Móveis",
            key: "Category14",
            items: (
                <div
                    style={{
                        width: "90.4rem",
                        height: "57.5rem",
                        backgroundColor: "#fff",
                        marginTop: "1rem",
                        borderRight: "1px solid #f6f6f6",
                        borderBottom: "1px solid #f6f6f6",
                    }}
                >
                    <Row>
                        <Col md="12" style={{ padding: "3rem" }}>
                            <h1
                                className="bg-red text-white p-1"
                                style={{ width: "max-content" }}
                            >
                                Móveis
                            </h1>
                        </Col>
                    </Row>
                    <Row>
                        <Col md="4" className="bannerMenuDrop">
                            {subCat?.moveis?.slice(0, 6).map((sub, idx) => {
                                return (
                                    <>
                                        <LinkTo
                                            key={idx}
                                            _href="/category"
                                            query={{
                                                id_sub: sub.id,
                                                id_cat: 29,
                                            }}
                                        >
                                            <div onClick={goTo}>{sub.name}</div>
                                        </LinkTo>
                                        <li
                                            className={`bannerMenuBottom ${
                                                idx === 5 ? "d-none" : ""
                                            }`}
                                        />
                                    </>
                                )
                            })}
                        </Col>
                        <Col md="4" className="bannerMenuDrop">
                            {subCat?.moveis?.slice(6, 12).map((sub, idx) => {
                                return (
                                    <>
                                        <LinkTo
                                            key={idx}
                                            _href="/category"
                                            query={{
                                                id_sub: sub.id,
                                                id_cat: 29,
                                            }}
                                        >
                                            <div onClick={goTo}>{sub.name}</div>
                                        </LinkTo>
                                        <li
                                            className={`bannerMenuBottom ${
                                                idx === 5 ? "d-none" : ""
                                            }`}
                                        />
                                    </>
                                )
                            })}
                        </Col>
                        <Col md="4" className="bannerMenuDrop">
                            {subCat?.moveis?.slice(12).map((sub, idx) => {
                                return (
                                    <>
                                        <LinkTo
                                            key={idx}
                                            _href="/category"
                                            query={{
                                                id_sub: sub.id,
                                                id_cat: 29,
                                            }}
                                        >
                                            <div onClick={goTo}>{sub.name}</div>
                                        </LinkTo>
                                        <li
                                            className={`bannerMenuBottom ${
                                                idx ===
                                                subCat?.moveis.length - 1
                                                    ? "d-none"
                                                    : ""
                                            }`}
                                        />
                                    </>
                                )
                            })}
                        </Col>
                    </Row>
                    <Row style={{ marginTop: "2rem" }}>
                        <Col md="12" className="bannerAllSub">
                            <LinkTo
                                _href="/category"
                                query={{
                                    id_cat: 29,
                                }}
                            >
                                Ver todas as subcategorias
                            </LinkTo>
                        </Col>
                    </Row>
                </div>
            ),
        },
        {
            label: "Petshop",
            key: "Category15",
            items: (
                <div
                    style={{
                        width: "90.4rem",
                        backgroundColor: "#fff",
                        marginTop: "1rem",
                        borderRight: "1px solid #f6f6f6",
                        borderBottom: "1px solid #f6f6f6",
                        height: "57.5rem",
                    }}
                >
                    <Row>
                        <Col md="12" style={{ padding: "3rem" }}>
                            <h1
                                className="bg-red text-white p-1"
                                style={{ width: "max-content" }}
                            >
                                Petshop
                            </h1>
                        </Col>
                    </Row>
                    <Row>
                        <Col md="4" className="bannerMenuDrop">
                            {subCat?.petshop?.slice(0, 5).map((sub, idx) => {
                                return (
                                    <>
                                        <LinkTo
                                            key={idx}
                                            _href="/category"
                                            query={{
                                                id_sub: sub.id,
                                                id_cat: 31,
                                            }}
                                        >
                                            <div onClick={goTo}>{sub.name}</div>
                                        </LinkTo>
                                        <li
                                            className={`bannerMenuBottom ${
                                                idx === 4 ? "d-none" : ""
                                            }`}
                                        />
                                    </>
                                )
                            })}
                        </Col>
                        <Col md="4" className="bannerMenuDrop">
                            {subCat?.petshop?.slice(5, 11).map((sub, idx) => {
                                return (
                                    <>
                                        <LinkTo
                                            key={idx}
                                            _href="/category"
                                            query={{
                                                id_sub: sub.id,
                                                id_cat: 31,
                                            }}
                                        >
                                            <div onClick={goTo}>{sub.name}</div>
                                        </LinkTo>
                                        <li
                                            className={`bannerMenuBottom ${
                                                idx === 4 ? "d-none" : ""
                                            }`}
                                        />
                                    </>
                                )
                            })}
                        </Col>
                        <Col md="4" className="bannerMenuDrop">
                            {subCat?.petshop?.slice(11).map((sub, idx) => {
                                return (
                                    <>
                                        <LinkTo
                                            key={idx}
                                            _href="/category"
                                            query={{
                                                id_sub: sub.id,
                                                id_cat: 31,
                                            }}
                                        >
                                            <div onClick={goTo}>{sub.name}</div>
                                        </LinkTo>
                                        <li
                                            className={`bannerMenuBottom ${
                                                idx ===
                                                subCat?.petshop.length - 1
                                                    ? "d-none"
                                                    : ""
                                            }`}
                                        />
                                    </>
                                )
                            })}
                        </Col>
                    </Row>
                    <Row style={{ marginTop: "2rem" }}>
                        <Col md="12" className="bannerAllSub">
                            <LinkTo
                                _href="/category"
                                query={{
                                    id_cat: 31,
                                }}
                            >
                                Ver todas as subcategorias
                            </LinkTo>
                        </Col>
                    </Row>
                </div>
            ),
        },
        {
            label: "Relógios, Joias e Bijouterias",
            key: "Category16",
            items: (
                <div
                    style={{
                        width: "90.4rem",
                        height: "57.5rem",
                        backgroundColor: "#fff",
                        marginTop: "1rem",
                        borderRight: "1px solid #f6f6f6",
                        borderBottom: "1px solid #f6f6f6",
                    }}
                >
                    <Row>
                        <Col md="12" style={{ padding: "3rem" }}>
                            <h1
                                className="bg-red text-white p-1"
                                style={{ width: "max-content" }}
                            >
                                Relógios, Joias e Bijouterias
                            </h1>
                        </Col>
                    </Row>
                    <Row>
                        <Col md="4" className="bannerMenuDrop">
                            {subCat?.relogios?.slice(0, 6).map((sub, idx) => {
                                return (
                                    <>
                                        <LinkTo
                                            key={idx}
                                            _href="/category"
                                            query={{
                                                id_sub: sub.id,
                                                id_cat: 33,
                                            }}
                                        >
                                            <div onClick={goTo}>{sub.name}</div>
                                        </LinkTo>
                                        <li
                                            className={`bannerMenuBottom ${
                                                idx === 5 ? "d-none" : ""
                                            }`}
                                        />
                                    </>
                                )
                            })}
                        </Col>
                        <Col md="4" className="bannerMenuDrop">
                            {subCat?.relogios?.slice(6, 11).map((sub, idx) => {
                                return (
                                    <>
                                        <LinkTo
                                            key={idx}
                                            _href="/category"
                                            query={{
                                                id_sub: sub.id,
                                                id_cat: 33,
                                            }}
                                        >
                                            <div onClick={goTo}>{sub.name}</div>
                                        </LinkTo>
                                        <li
                                            className={`bannerMenuBottom ${
                                                idx === 5 ? "d-none" : ""
                                            }`}
                                        />
                                    </>
                                )
                            })}
                        </Col>
                        <Col md="4" className="bannerMenuDrop">
                            {subCat?.relogios?.slice(11).map((sub, idx) => {
                                return (
                                    <>
                                        <LinkTo
                                            key={idx}
                                            _href="/category"
                                            query={{
                                                id_sub: sub.id,
                                                id_cat: 33,
                                            }}
                                        >
                                            <div onClick={goTo}>{sub.name}</div>
                                        </LinkTo>
                                        <li
                                            className={`bannerMenuBottom ${
                                                idx ===
                                                subCat?.relogios.length - 1
                                                    ? "d-none"
                                                    : ""
                                            }`}
                                        />
                                    </>
                                )
                            })}
                        </Col>
                    </Row>
                    <Row style={{ marginTop: "2rem" }}>
                        <Col md="12" className="bannerAllSub">
                            <LinkTo
                                _href="/category"
                                query={{
                                    id_cat: 33,
                                }}
                            >
                                Ver todas as subcategorias
                            </LinkTo>
                        </Col>
                    </Row>
                </div>
            ),
        },
        {
            label: "Saúde",
            key: "Category17",
            items: (
                <div
                    style={{
                        width: "90.4rem",
                        height: "57.5rem",
                        backgroundColor: "#fff",
                        marginTop: "1rem",
                        borderRight: "1px solid #f6f6f6",
                        borderBottom: "1px solid #f6f6f6",
                    }}
                >
                    <Row>
                        <Col md="12" style={{ padding: "3rem" }}>
                            <h1
                                className="bg-red text-white p-1"
                                style={{ width: "max-content" }}
                            >
                                Saúde
                            </h1>
                        </Col>
                    </Row>
                    <Row>
                        <Col md="4" className="bannerMenuDrop">
                            {subCat?.saude?.slice(0, 6).map((sub, idx) => {
                                return (
                                    <>
                                        <LinkTo
                                            key={idx}
                                            _href="/category"
                                            query={{
                                                id_sub: sub.id,
                                                id_cat: 34,
                                            }}
                                        >
                                            <div onClick={goTo}>{sub.name}</div>
                                        </LinkTo>
                                        <li
                                            className={`bannerMenuBottom ${
                                                idx === 5 ? "d-none" : ""
                                            }`}
                                        />
                                    </>
                                )
                            })}
                        </Col>
                        <Col md="4" className="bannerMenuDrop">
                            {subCat?.saude?.slice(6, 12).map((sub, idx) => {
                                return (
                                    <>
                                        <LinkTo
                                            key={idx}
                                            _href="/category"
                                            query={{
                                                id_sub: sub.id,
                                                id_cat: 34,
                                            }}
                                        >
                                            <div onClick={goTo}>{sub.name}</div>
                                        </LinkTo>
                                        <li
                                            className={`bannerMenuBottom ${
                                                idx === 5 ? "d-none" : ""
                                            }`}
                                        />
                                    </>
                                )
                            })}
                        </Col>
                        <Col md="4" className="bannerMenuDrop">
                            {subCat?.saude?.slice(12).map((sub, idx) => {
                                return (
                                    <>
                                        <LinkTo
                                            key={idx}
                                            _href="/category"
                                            query={{
                                                id_sub: sub.id,
                                                id_cat: 34,
                                            }}
                                        >
                                            <div onClick={goTo}>{sub.name}</div>
                                        </LinkTo>
                                        <li
                                            className={`bannerMenuBottom ${
                                                idx === subCat?.saude.length - 1
                                                    ? "d-none"
                                                    : ""
                                            }`}
                                        />
                                    </>
                                )
                            })}
                        </Col>
                    </Row>
                    <Row style={{ marginTop: "2rem" }}>
                        <Col md="12" className="bannerAllSub">
                            <LinkTo
                                _href="/category"
                                query={{
                                    id_cat: 34,
                                }}
                            >
                                Ver todas as subcategorias
                            </LinkTo>
                        </Col>
                    </Row>
                </div>
            ),
        },
        {
            label: "Ver todas as categorias",
            key: "all",
        },
    ]
    return (
        <div id="navbar_nav" className="mb-5">
            {loaderState && (
                <div className="spinnerLoader">
                    <ClipLoader
                        size={150}
                        color={"var(--red-1)"}
                        loading={loaderState}
                    />
                </div>
            )}
            <Container className={"containerHeader"}>
                <nav
                    id="navbar"
                    className={`d-flex flex-row align-items-center h-100 w-100 text-black-50 px-0 pt-5 navbarHeader`}
                >
                    <LinkTo _href="/">
                        <Img
                            className="logoMaria"
                            image_name_with_extension="logo-maria.svg"
                            alt="logo Portal Maria"
                            onClick={(e) => checkRoute("/")}
                        />
                    </LinkTo>
                    <FormSearch checkRoute={checkRoute} />
                    {!context.token && (
                        <div className="displayFlex mx-auto flex-row allignerAround">
                            <div
                                className={`ml-md-auto d-flex divAvatarNotLogged mr-5 flex-row align-items-center `}
                            >
                                <Img
                                    image_name_with_extension={"user.svg"}
                                    alt={"Faça seu login e registre-se"}
                                    className={`logoUserNotLogged `}
                                />
                                <b className={`bLogoUser`}>
                                    Olá, faça seu{" "}
                                    <LinkTo _href={"/login"}>
                                        <div
                                            style={{ display: "contents" }}
                                            onClick={(e) =>
                                                checkRoute("/login")
                                            }
                                        >
                                            login
                                        </div>{" "}
                                        <br />
                                        ou{" "}
                                        <div style={{ display: "contents" }}>
                                            registre-se
                                        </div>
                                    </LinkTo>
                                </b>
                            </div>
                            <div className="showPhoneAndLess displayFlex divImageLogged mx-auto flex-row">
                                <LinkTo _href="/cart">
                                    <Img
                                        style={{ height: "3rem" }}
                                        alt={"carrinho"}
                                        image_name_with_extension={
                                            "supermarket.svg"
                                        }
                                        onClick={(e) => checkRoute("/cart")}
                                    />
                                </LinkTo>
                                <div className="counterCart">
                                    <Badge
                                        color="secondary"
                                        style={{
                                            backgroundColor: "var(--red-1)",
                                        }}
                                        pill
                                    >
                                        {context?.cart_length || 0}
                                    </Badge>
                                </div>
                            </div>
                        </div>
                    )}
                    {!!context.token && !!context.navbar_props && (
                        <div className={`d-flex allignerAround flex-row`}>
                            <div className="d-flex divImageLogged mr-5">
                                <Img
                                    className={`rounded-circle logoUser`}
                                    image_name_with_extension={
                                        (
                                            context.navbar_props?.users ||
                                            context.navbar_props?.provider
                                        )?.avatar?.url || "user.svg"
                                    }
                                    outSide={
                                        context.navbar_props.users?.avatar?.url
                                            ? context.navbar_props.users.avatar
                                                  .url
                                            : context.navbar_props.provider
                                                  ?.avatar
                                            ? context.navbar_props.provider
                                                  .avatar.url
                                            : ""
                                    }
                                    alt="logo Portal Maria"
                                    style={{
                                        objectContain: "cover!important",
                                    }}
                                />
                                <div
                                    className={`d-flex flex-column w-auto divMenuHeaderLogged`}
                                >
                                    <h4 className="bold m-0 text_dark_2 text-capitalize">
                                        {context.navbar_props?.name?.split(
                                            " "
                                        )[0] ||
                                            context.navbar_props?.provider?.fantasy_name?.split(
                                                " "
                                            )[0]}
                                    </h4>
                                    <LinkTo
                                        _href="/account"
                                        query={{ token: context?.token }}
                                    >
                                        <h6
                                            className="m-0 text_dark_2"
                                            onClick={(e) =>
                                                checkRoute("/account")
                                            }
                                        >
                                            minha conta
                                        </h6>
                                    </LinkTo>
                                </div>
                            </div>
                            <div className="showPhoneAndLess displayFlex divImageLogged flex-row">
                                <LinkTo
                                    _href="/cart"
                                    query={{ token: context.token }}
                                >
                                    <Img
                                        style={{ height: "3rem" }}
                                        alt={"carrinho"}
                                        image_name_with_extension={
                                            "supermarket.svg"
                                        }
                                        onClick={(e) => checkRoute("/cart")}
                                    />
                                </LinkTo>
                                <div className="counterCart">
                                    <Badge
                                        color="secondary"
                                        style={{
                                            backgroundColor: "var(--red-1)",
                                        }}
                                        pill
                                    >
                                        {context?.cart_length || 0}
                                    </Badge>
                                </div>
                            </div>
                        </div>
                    )}
                    {(!context.token && (
                        <div
                            className={`displayFlex divImageLogged mx-auto mb-4 flex-row showAbovePhone`}
                        >
                            <LinkTo _href="/cart">
                                <Img
                                    style={{ height: "3rem" }}
                                    alt={"carrinho"}
                                    image_name_with_extension={
                                        "supermarket.svg"
                                    }
                                    onClick={(e) => checkRoute("/cart")}
                                />
                            </LinkTo>
                            <div className="counterCart">
                                <Badge
                                    color="secondary"
                                    style={{
                                        backgroundColor: "var(--red-1)",
                                    }}
                                    pill
                                >
                                    {context?.cart_length || 0}
                                </Badge>
                            </div>
                        </div>
                    )) || (
                        <div
                            className={`displayFlex divImageLogged mx-auto mb-4 flex-row showAbovePhone`}
                        >
                            <LinkTo
                                className={"ml-5"}
                                _href="/cart"
                                query={{ token: context.token }}
                            >
                                <Img
                                    style={{ height: "3rem" }}
                                    alt={"carrinho"}
                                    image_name_with_extension={
                                        "supermarket.svg"
                                    }
                                    onClick={(e) => checkRoute("/cart")}
                                />
                            </LinkTo>

                            <div className="counterCart">
                                <Badge
                                    color="secondary"
                                    style={{
                                        backgroundColor: "var(--red-1)",
                                    }}
                                    pill
                                >
                                    {context?.cart_length || 0}
                                </Badge>
                            </div>
                        </div>
                    )}
                </nav>
            </Container>
            <Container className="px-0">
                <div
                    id="navbar_nav_list"
                    className="showPhoneAndLess displayFlex flex-row justify-content-start px-0"
                >
                    <Row className="m-0">
                        <Col
                            xs="12"
                            className="allignerCenterMenu menuTitle paddingPhone"
                        >
                            {(context.token && (
                                <>
                                    <div
                                        onClick={(e) => checkRoute("/")}
                                        className="divGoTo"
                                    >
                                        <LinkTo
                                            _href="/"
                                            query={{ token: context.token }}
                                        >
                                            Home
                                        </LinkTo>
                                    </div>
                                    <div
                                        onClick={(e) => checkRoute("/account")}
                                        className="divGoTo"
                                    >
                                        <LinkTo
                                            _href="/account"
                                            query={{ token: context.token }}
                                        >
                                            Minha conta
                                        </LinkTo>
                                    </div>
                                </>
                            )) || (
                                <>
                                    <div
                                        onClick={(e) => checkRoute("/")}
                                        className="divGoTo"
                                    >
                                        <LinkTo _href="/">Home</LinkTo>
                                    </div>
                                    <div
                                        onClick={(e) => checkRoute("/account")}
                                        className="divGoTo"
                                    >
                                        <LinkTo _href="/account">
                                            Minha conta
                                        </LinkTo>
                                    </div>
                                </>
                            )}
                            <div
                                onClick={(e) => checkRoute("/about")}
                                className="divGoTo"
                            >
                                <LinkTo _href="/about">Sobre o portal</LinkTo>
                            </div>
                            <div
                                onClick={(e) => checkRoute("/contact")}
                                className="divGoTo"
                            >
                                <LinkTo _href="/contact">Contato</LinkTo>
                            </div>
                        </Col>
                        {menuCat && (
                            <Col xs="12" className="allignerCenterMenu">
                                <a
                                    className="d-flex flex-row align-items-center buttonMenuDrop allignerCenter"
                                    onClick={() => {
                                        toggle()
                                    }}
                                >
                                    <FaList color="white" className="mr-2" />
                                    {!toggleMenu && "CATEGORIAS"}
                                    {toggleMenu && "FECHAR CATEGORIAS"}
                                </a>
                            </Col>
                        )}
                    </Row>
                </div>

                <div
                    id="navbar_nav_list"
                    className="showAbovePhone displayFlex flex-row justify-content-start menuTitle"
                >
                    {!!menuCat && (
                        <a
                            className="d-flex flex-row align-items-center buttonMenuDrop"
                            onClick={() => {
                                if (
                                    router.pathname.substring(0, 9) !==
                                    "/category"
                                ) {
                                    toggle()
                                } else {
                                    setToggleMenu(false)
                                }
                            }}
                        >
                            <FaList color="white" className="mr-2" />
                            {!toggleMenu && "CATEGORIAS"}
                            {toggleMenu && "FECHAR CATEGORIAS"}
                        </a>
                    )}
                    {(!!context.token && (
                        <>
                            <div
                                onClick={(e) => checkRoute("/")}
                                className="divGoTo"
                            >
                                <LinkTo
                                    _href="/"
                                    query={{ token: context.token }}
                                >
                                    Home
                                </LinkTo>
                            </div>
                            <div
                                onClick={(e) => checkRoute("/account")}
                                className="divGoTo"
                            >
                                <LinkTo
                                    _href="/account"
                                    query={{ token: context.token }}
                                >
                                    Minha conta
                                </LinkTo>
                            </div>
                        </>
                    )) || (
                        <>
                            <div
                                onClick={(e) => checkRoute("/")}
                                className="divGoTo"
                            >
                                <LinkTo _href="/">Home</LinkTo>
                            </div>
                            <div
                                onClick={(e) => checkRoute("/account")}
                                className="divGoTo"
                            >
                                <LinkTo _href="/account">Minha conta</LinkTo>
                            </div>
                        </>
                    )}
                    <div
                        onClick={(e) => checkRoute("/about")}
                        className="divGoTo"
                    >
                        <LinkTo _href="/about">Sobre o portal</LinkTo>
                    </div>
                    <div
                        onClick={(e) => checkRoute("/contact")}
                        className="divGoTo"
                    >
                        <LinkTo _href="/contact">Contato</LinkTo>
                    </div>
                </div>
            </Container>
            {toggleMenu && (
                <Container className="containerMenuMobile showTabletAndLess">
                    <Row className="rowMenuMobile">
                        {categories.map((cat) => {
                            return (
                                <>
                                    <Col
                                        xs="12"
                                        key={cat.idCat}
                                        className={
                                            active !== cat.idCat
                                                ? "colMenuMobile"
                                                : "colMenuMobileActive"
                                        }
                                        onClick={() => toggleActive(cat.idCat)}
                                    >
                                        {active !== cat.idCat
                                            ? cat.name
                                            : "Fechar Subcategorias"}
                                    </Col>
                                    {active === cat.idCat &&
                                        cat.subMenu.map((sub, idx) => {
                                            return (
                                                <>
                                                    {idx !==
                                                        cat.subMenu.length -
                                                            1 && (
                                                        <Col
                                                            key={sub.id}
                                                            xs="12"
                                                            className="colSubMenuMobile"
                                                            onClick={goTo}
                                                        >
                                                            <LinkTo
                                                                _href="/category"
                                                                query={{
                                                                    id_sub:
                                                                        sub.id,
                                                                    id_cat:
                                                                        cat.idCat,
                                                                }}
                                                            >
                                                                {sub.name}
                                                            </LinkTo>
                                                        </Col>
                                                    )}
                                                    {idx ===
                                                        cat.subMenu.length -
                                                            1 && (
                                                        <Col
                                                            xs="12"
                                                            className="colSubMenuMobile allSub"
                                                            onClick={goTo}
                                                        >
                                                            <LinkTo
                                                                _href="/category"
                                                                query={{
                                                                    id_cat:
                                                                        cat.idCat,
                                                                }}
                                                            >
                                                                Ver todas as
                                                                subcategorias
                                                            </LinkTo>
                                                        </Col>
                                                    )}
                                                </>
                                            )
                                        })}
                                </>
                            )
                        })}
                        <Col
                            xs="12"
                            className="colMenuMobile allSub"
                            onClick={goTo}
                        >
                            <LinkTo _href="/allcategories">
                                Ver todas as categorias
                            </LinkTo>
                        </Col>
                    </Row>
                </Container>
            )}
            {toggleMenu && (
                <Container className="px-0 showAboveTablet">
                    <ReactMegaMenu
                        data={categoriesMenu}
                        styleConfig={{
                            menuProps: {
                                style: {
                                    width: "27rem",
                                    fontSize: "14px",
                                    backgroundClip: "border-box",
                                    borderLeft: "1px solid #f6f6f6",
                                    borderRight: "1px solid #f6f6f6",
                                    borderBottom: "1px solid #f6f6f6",
                                    background: "#fff",
                                },
                            },
                            contentProps: {
                                style: {
                                    height: "100%",
                                    borderLeft: "1px solid #f6f6f6",
                                    borderRight: "1px solid #f6f6f6",
                                    borderBottom: "1px solid #f6f6f6",
                                    backgroundClip: "border-box",
                                    background: "#fff",
                                    zIndex: "100",
                                    width: "100%",
                                },
                            },
                            menuItemProps: {
                                style: {
                                    color: "#000",
                                    padding: "9.2px",
                                    paddingRight: "30px",
                                    paddingLeft: "30px",
                                    cursor: "pointer",
                                    width: "27rem",
                                    backgroundClip: "border-box",
                                    borderBottom: " 1px solid #f6f6f6",
                                    borderBottomStyle: "solid",
                                    background: "#fff",
                                },
                                onClick: async (e) => {
                                    if (
                                        e.currentTarget.innerText ===
                                        "Ver todas as categorias"
                                    ) {
                                        setLoaderState(true)
                                        await router.push("/allcategories")
                                    }
                                },
                            },
                            menuItemSelectedProps: {
                                style: {
                                    padding: "9.2px",
                                    paddingRight: "30px",
                                    paddingLeft: "30px",
                                    cursor: "pointer",
                                    backgroundClip: "border-box",
                                    borderLeft: "1px solid #f6f6f6",
                                    borderRight: "1px solid #f6f6f6",
                                    borderBottom: "1px solid #f6f6f6",
                                    background: "#fff",
                                },
                            },
                            containerProps: {
                                style: {
                                    display: "flex",
                                    flexDirection: "row",
                                    color: "#000",
                                    cursor: "pointer",
                                    width: "95%",
                                    zIndex: "100",
                                },
                            },
                        }}
                    />
                </Container>
            )}
        </div>
    )
}

export default Component
