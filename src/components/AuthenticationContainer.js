import React from "react";
import {Form} from "reactstrap";

function Index({
                   children,
                   maxW,
                   minW = 'max-content',
                   style,
                   onSubmit = (e) => {
                       e.preventDefault();
                       return false
                   },
                   ...rest
               }) {
    return (
        <Form
            onSubmit={onSubmit}
            autoComplete="off"
            style={{...style, maxWidth: maxW, minWidth: minW}}
            className={`container authentication_container p-0 needs-validation`}
            {...rest}
        >
            {children}
        </Form>
    );
}

export default Index;