/* eslint-disable react/prop-types */
import React, {useState} from "react"
import {FormGroup, Input, Label} from "reactstrap"
import Cleave from "cleave.js/react"

function Index({
                   rows = "6",
                   styleInput = {},
                   _required = false,
                   options = false,
                   has_label = false,
                   type = "text",
                   name,
                   blur = function (e) {
                       e.preventDefault()
                   },
                   id_ = false,
                   placeholder,
                   label = "",
                   listenner = function () {
                   },
                   initial_value = "",
                   _maxLength = 256,
                   _minLength = 0,
                   wrap = true,
                   small = false,
                   uncontrolled = false,
                   _value = false,
                   _handle = false,
                   min = 0,
                   ...rest
               }) {
    if (!uncontrolled) {
        const [value, setValue] = useState(initial_value)
        const handle = (e) => {
            setValue(e.target.value)
            listenner(e)
        }
        const _style_label = !has_label
            ? {display: "none"}
            : {display: "block"}
        const _id = !id_ ? `input-${name}` : id_
        return wrap ? (
            <FormGroup {...rest}>
                <Label style={_style_label} for={name}>
                    {label}
                </Label>
                {options === false ? (
                    <>
                        <Input
                            onBlur={blur}
                            rows={rows}
                            style={styleInput}
                            autoComplete="off"
                            required={_required}
                            {...(_required ? {minLength: _minLength} : {})}
                            maxLength={_maxLength}
                            value={value}
                            onChange={handle}
                            className={`pl-4 form-control  w-100`}
                            type={type}
                            name={name}
                            id={_id}
                            placeholder={placeholder}
                            aria-describedby={`helper-${_id}`}
                            min={min}
                        />

                        {small ? (
                            <small
                                id={`helper-${_id}`}
                                className="form-text text-muted"
                            >
                                {small}
                            </small>
                        ) : (
                            ""
                        )}
                    </>
                ) : (
                    <>
                        <Cleave
                            onBlur={blur}
                            style={styleInput}
                            autoComplete={"off"}
                            required={_required}
                            options={options}
                            {...(_required ? {minLength: _minLength} : {})}
                            maxLength={_maxLength}
                            value={value}
                            onChange={handle}
                            className={`pl-4 form-control w-100`}
                            type={type}
                            name={name}
                            id={_id}
                            placeholder={placeholder}
                        />
                        {small ? <small>{small}</small> : ""}
                    </>
                )}
            </FormGroup>
        ) : (
            <>
                <Label style={_style_label} for={name}>
                    {label}
                </Label>
                {options === false ? (
                    <Input
                        onBlur={blur}
                        rows={rows}
                        style={styleInput}
                        autoComplete={"off"}
                        required={_required}
                        {...(_required ? {minLength: _minLength} : {})}
                        maxLength={_maxLength}
                        value={value}
                        onChange={handle}
                        className={`pl-4 form-control  w-100`}
                        type={type}
                        name={name}
                        id={_id}
                        placeholder={placeholder}
                    />
                ) : (
                    <Cleave
                        onBlur={blur}
                        style={styleInput}
                        autoComplete={"off"}
                        required={_required}
                        options={options}
                        {...(_required ? {minLength: _minLength} : {})}
                        maxLength={_maxLength}
                        value={value}
                        onChange={handle}
                        className={`pl-4 form-control w-100`}
                        type={type}
                        name={name}
                        id={_id}
                        placeholder={placeholder}
                    />
                )}
            </>
        )
    } else {
        const _style_label = !has_label
            ? {display: "none"}
            : {display: "block"}
        const _id = !id_ ? `input-${name}` : id_
        return wrap ? (
            <FormGroup {...rest}>
                <Label style={_style_label} for={name}>
                    {label}
                </Label>
                {options === false ? (
                    <>
                        <Input
                            onBlur={blur}
                            rows={rows}
                            style={styleInput}
                            autoComplete={"off"}
                            required={_required}
                            {...(_required ? {minLength: _minLength} : {})}
                            maxLength={_maxLength}
                            value={_value}
                            onChange={_handle}
                            className={`pl-4 form-control  w-100`}
                            type={type}
                            name={name}
                            id={_id}
                            placeholder={placeholder}
                            aria-describedby={`helper-${_id}`}
                        />

                        {small ? (
                            <small
                                id={`helper-${_id}`}
                                className="form-text text-muted"
                            >
                                {small}
                            </small>
                        ) : (
                            ""
                        )}
                    </>
                ) : (
                    <>
                        <Cleave
                            onBlur={blur}
                            style={styleInput}
                            autoComplete={"off"}
                            required={_required}
                            options={options}
                            {...(_required ? {minLength: _minLength} : {})}
                            maxLength={_maxLength}
                            value={_value}
                            onChange={_handle}
                            className={`pl-4 form-control w-100`}
                            type={type}
                            name={name}
                            id={_id}
                            placeholder={placeholder}
                        />
                        {small ? <small>{small}</small> : ""}
                    </>
                )}
            </FormGroup>
        ) : (
            <>
                <Label style={_style_label} for={name}>
                    {label}
                </Label>
                {options === false ? (
                    <Input
                        onBlur={blur}
                        rows={rows}
                        style={styleInput}
                        autoComplete={"off"}
                        required={_required}
                        {...(_required ? {minLength: _minLength} : {})}
                        maxLength={_maxLength}
                        value={_value}
                        onChange={_handle}
                        className={`pl-4 form-control  w-100`}
                        type={type}
                        name={name}
                        id={_id}
                        placeholder={placeholder}
                    />
                ) : (
                    <Cleave
                        onBlur={blur}
                        style={styleInput}
                        autoComplete={"off"}
                        required={_required}
                        options={options}
                        {...(_required ? {minLength: _minLength} : {})}
                        maxLength={_maxLength}
                        value={_value}
                        onChange={_handle}
                        className={`pl-4 form-control w-100`}
                        type={type}
                        name={name}
                        id={_id}
                        placeholder={placeholder}
                    />
                )}
            </>
        )
    }
}

export default Index;