import React from 'react'
import {Img} from "./index";
import {brlFormat} from "../assets/helpers";


const Index = ({img, alt, href, node_text, price, spread = '(x1)'}) => {
    return (
        <div className="product-cart-son clearfix">
            <div className="image-product-cart float-left center-vertical-image ">
                <a href="#">
                    <Img
                        image_name_with_extension={img}
                        alt={alt}
                    />
                </a>
            </div>
            <div className="info-product-cart float-left">
                <p className="title-product title-hover-black">
                    <a className="animate-default" href={href}>
                        {node_text}
                    </a>
                </p>
                <p className="clearfix price-product">
                    {`$ ${brlFormat(Number(price))}`}
                    <span className="total-product-cart-son">{spread}</span>
                </p>
            </div>
        </div>
    );
}
export default Index;