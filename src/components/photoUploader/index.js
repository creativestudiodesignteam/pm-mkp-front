import React from "react"
import PropTypes from "prop-types"
import ImageUploading from "react-images-uploading"
import { autoRemoveNode } from "../../assets/helpers"
import { Button, Col, Row } from "reactstrap"
import styles from "../product/gridOption/components/PhotoUpload/index.module.css"

const Index = ({
    listener,
    defaultValue = [],
    maxNumber,
    maxFileSize = 5 * 1024 * 1024,
    acceptType = ["jpg", "png", "jpeg"],
    label = false,
}) => {
    return (
        <>
            {label !== false && (
                <label htmlFor={"thumb"} className="mb-4">
                    {label}
                </label>
            )}
            <ImageUploading
                name={"thumb"}
                onChange={listener}
                defaultValue={defaultValue}
                maxNumber={maxNumber}
                maxFileSize={maxFileSize}
                acceptType={acceptType}
            >
                {({ imageList, onImageUpload, errors }) => (
                    <>
                        {!!errors.acceptType && (
                            <>
                                <p
                                    className="d-block alert bg_red relative text-white-50  pointer p-4 mx-auto my-2 alert-dismissable acceptType"
                                    onClick={(event) =>
                                        autoRemoveNode(event, ".acceptType")
                                    }
                                >
                                    A imagem deve ser do formato JPG, JPEG ou
                                    PNG{" "}
                                    <spam
                                        className=" absolute text-white"
                                        style={{ right: 8 }}
                                    >
                                        X
                                    </spam>
                                </p>
                            </>
                        )}
                        {!!errors.maxFileSize && (
                            <>
                                <p
                                    className="alert bg_red relative text-white-50  pointer p-4 mx-auto my-2 alert-dismissable maxFileSize"
                                    onClick={(event) =>
                                        autoRemoveNode(event, ".maxFileSize")
                                    }
                                >
                                    A imagem ultrapassa limite de 5mb
                                    <spam
                                        className=" absolute text-white"
                                        style={{ right: 8 }}
                                    >
                                        X
                                    </spam>
                                </p>
                            </>
                        )}
                        {!!errors.maxNumber && (
                            <>
                                <p
                                    className="alert bg_red relative text-white-50  pointer p-4 mx-auto my-2 alert-dismissable maxNumber"
                                    onClick={(event) =>
                                        autoRemoveNode(event, ".maxNumber")
                                    }
                                >
                                    Upload máximo de {maxNumber} foto(s)
                                    <spam
                                        className=" absolute text-white"
                                        style={{ right: 8 }}
                                    >
                                        X
                                    </spam>
                                </p>
                            </>
                        )}
                        <Row
                            className="w-100 p-0 m-0"
                            style={{
                                display: "flex",
                                justifyContent: "center",
                            }}
                        >
                            {!imageList.length && (
                                <div
                                    className={`${
                                        styles.colUpload
                                    } w-100 mb-4 col-12`}
                                >
                                    <Button
                                        outline
                                        onClick={onImageUpload}
                                        className={styles.buttonUpload}
                                    >
                                        <img
                                            src={"/img/ar-camera.svg"}
                                            alt=""
                                        />
                                        <span>
                                            <p className="mt-3">
                                                Adicionar foto(s)
                                            </p>
                                        </span>
                                    </Button>
                                </div>
                            )}
                            <Row className={styles.photoGallery}>
                                {imageList.map((image) => (
                                    <Col
                                        xs="12"
                                        className={styles.photoUpload}
                                        key={image.key}
                                    >
                                        <img alt={""} src={image.dataURL} />
                                        <span className="inline">&nbsp;</span>
                                        <Row className={styles.aligner}>
                                            <Col xs="12">
                                                <Button
                                                    outline
                                                    onClick={image.onRemove}
                                                    className={
                                                        "redButtonOutline"
                                                    }
                                                >
                                                    Remover
                                                </Button>
                                            </Col>
                                        </Row>
                                    </Col>
                                ))}
                            </Row>
                        </Row>
                    </>
                )}
            </ImageUploading>
            <p className="text-muted m-0 mb-4 text-center">
                as imagens devem ser jpg, jpeg ou png,
                <br />
                com tamanho máximo de 5 Mb
                <br />e número máximo de imagens são {maxNumber}
            </p>
        </>
    )
}

Index.propTypes = {
    listener: PropTypes.func.isRequired,
    defaultValue: PropTypes.array,
    maxNumber: PropTypes.number.isRequired,
    maxFileSize: PropTypes.number,
    acceptType: PropTypes.array,
}

export default Index
