import React, {createContext, useReducer} from "react"
import propTypes from "prop-types"
import {_storage} from "../assets/auth";
import {hasKey} from "../assets/helpers";

export const INITIAL_STATE = {
    user: {
        id: 0,
        name: "",
        email: "",
        type: false,
    },
    token: "",
    cart: [],
    navbar_props: false,
    cart_length: 0,
    categories: [],
    subCat: {
        ar: [],
        automotivo: [],
        bebes: [],
        beleza: [],
        brinquedos: [],
        construcao: [],
        cuidados: [],
        eletrodomesticos: [],
        eletronicos: [],
        esportes: [],
        filmes: [],
        games: [],
        moveis: [],
        petshop: [],
        relogios: [],
        saude: [],
    }
}
export const partialState = {
    user: {
        id: 0,
        name: "",
        email: "",
        type: false,
    },
    token: "",
    cart: [],
    navbar_props: false
}
const Types = {
    init: "initialize",
    login: "login",
    cart_init: "cart_init",
    navbar_props: "avatar",
    cart_length: 'cart_length',
    categories: 'categories',
    subCat: 'subCat',
    raw: 'raw'
}
export const Context = createContext(INITIAL_STATE)
export const dispatchFunctions = {
    raw: (payload) => ({type: Types.raw, payload}),
    init: (payload) => ({type: Types.init, payload}),
    login: (payload) => ({type: Types.login, payload}),
    create: (payload) => ({type: Types.create, payload}),
    update: (payload) => ({type: Types.update, payload}),
    delete_by_id: (payload) => ({type: Types.delete, payload}),
    cart_init: (payload) => ({type: Types.cart_init, payload}),
    navbar_props: (payload) => ({type: Types.navbar_props, payload}),
    cart_length: (payload) => ({type: Types.cart_length, payload}),
    categories: (payload) => ({type: Types.categories, payload}),
    subCat: (payload) => ({type: Types.subCat, payload})
}
export const {init, navbar_props, cart_length, update, delete_by_id, create, login, cart_init, categories, subCat, raw} = dispatchFunctions

function reducer(state = INITIAL_STATE, {type, payload}) {
    const storage = _storage()
    let newState = {}
    const {init: _init, login: _login, cart_init: _cart_init, navbar_props: _navbar_props, cart_length: _cart_length, categories: _categories, raw: _raw, subCat: _subCat} = Types
    switch (type) {
        case _raw:
            newState = {...state, ...payload}
            !!storage &&
            storage.setItem('store', JSON.stringify(newState))
            console.log({_raw, newState})
            return newState
        case _init:
            if (hasKey(payload, 'categories')) {
                if (hasKey(payload, 'subCat')) {
                    const {categories, subCat, ...withoutCategories} = payload
                    newState = {...state, ...withoutCategories}
                } else {
                    const {categories, ...withoutCategories} = payload
                    newState = {...state, ...withoutCategories}
                }
            } else {
                newState = {...state, ...payload}
            }
            !!storage &&
            storage.setItem('store', JSON.stringify(newState))
            console.log({_init, newState})
            return newState
        case _login:
            newState = {...state, user: payload?.user, token: payload?.token}
            !!storage &&
            storage.setItem('store', JSON.stringify(newState))
            console.log({_login, newState})
            return newState
        case _cart_init:
            newState = {...state, cart: payload}
            !!storage &&
            storage.setItem('store', JSON.stringify(newState))
            console.log({_cart_init, newState})
            return newState
        case _navbar_props:
            newState = {...state, navbar_props: payload}
            !!storage &&
            storage.setItem('store', JSON.stringify(newState))
            console.log({_navbar_props, newState})
            return newState
        case _cart_length:
            newState = {...state, cart_length: Number(payload)}
            !!storage &&
            storage.setItem('store', JSON.stringify(newState))
            console.log({_cart_length, newState})
            return newState
        case _categories:
            newState = {...state, categories: payload}
            !!storage &&
            storage.setItem('store', JSON.stringify(newState))
            console.log({_categories, newState})
            return newState
        case _subCat:
            newState = {...state, subCat: payload}
            !!storage &&
            storage.setItem('store', JSON.stringify(newState))
            console.log({_subCat, newState})
            return newState
        default:
            throw new Error(`Unknown action: ${type}`)
    }
}

function Store({children}) {
    const {Provider} = Context
    const [store, dispatch] = useReducer(
        reducer,
        INITIAL_STATE
    )
    return <Provider value={[store, dispatch]}>{children}</Provider>
}

Store.propTypes = {
    children: propTypes.any.isRequired,
}
export default Store
