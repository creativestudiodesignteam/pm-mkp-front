/*global localStorage, window*/
import * as jwt from "jsonwebtoken"
import {dispatchFunctions, INITIAL_STATE} from "../store"
import {hasKey, tryCatch} from "./helpers"

export const TOKEN_KEY = process.env._key || ""
export const userByToken = (token) => {
    const key = `${process.env._key}`
    try {
        return jwt.verify(token, key)
    } catch (error) {
        console.log(`Nao foi possivel decodificar o token :${(token || "").slice(0, 10)}...`, error, {token})
        return false
    }
}

export function _storage() {
    return tryCatch(
        () => {
            const window = require('global/window')
            return window.localStorage
        },
        () => {
            console.log('Não foi possivel localizar o LOCALSTORAGE')
            return false;
        },
        true
    )
}

export function __storage(cb, _default = false) {
    const storage = _storage()
    if (!!storage) {
        return tryCatch(() => cb(storage), (error) => {
            console.log('__storage', error)
            return _default
        }, true)
    } else {
        console.log('Não foi possivel localizar o LOCALSTORAGE', {cb, _default})
        return _default;
    }
}

export const isAuthenticated = (_token) => (
    __storage(
        (storage) => userByToken(_token && _token !== "" ? _token : storage.getItem("token"))
    )
)
/**
 * @return {string}
 * */
export const getToken = () => __storage(
    (storage) => storage.getItem("token"),
    ''
)

export const login = (token) => {
    const storage = _storage()
    if (!!storage) {
        storage.setItem("token", token)
    }
}
export const logout = () => {
    const storage = _storage()
    if (!!storage) {
        storage.removeItem(TOKEN_KEY)
    }
}
export const remember = (_remember, user) => {
    const storage = _storage()
    if (!!storage) {
        if (!!_remember) {
            !!hasKey(user, "email") &&
            storage.setItem("user", JSON.stringify(user))
        } else {
            try {
                if (hasKey(user, "email")) {
                    const _user = JSON.parse(storage.getItem("user") || "{}")
                    if (_user?.email === user?.email) {
                        storage.setItem(
                            "user",
                            JSON.stringify({
                                email: user.email,
                                password: "",
                                type: user.type,
                            })
                        )
                    }
                }
            } catch (e) {
                console.error("remember", e)
            }
        }
        return true
    }
    return false;
}
export const _logOut = () => {
    const storage = _storage()
    if (!!storage) {
        storage.setItem("token", "")
    }
}

/***
 *
 * @param {function} dispatch
 * @param {import('next/router').NextRouter} router
 */
export const logOut = (dispatch, router) => {
    const storage = _storage()
    if (!!storage) {
        storage.setItem("token", "")
    }
    console.log('logOut')
    dispatch(dispatchFunctions.init(INITIAL_STATE))
    router.push("/login").then(() => console.log("log out"))
}
/***
 * @see localStorage.setItem
 * @see dispatch(dispatchFunctions.init({...}));
 * @param {String} token
 * @param {Object} user
 * @param {Function} dispatch
 * @param {Object} payload
 * @returns {boolean} true
 */
export const LogIn = (token, user, dispatch, payload = {}) => __storage((storage) => {
    storage.setItem("token", "")
    storage.setItem("token", token)
    if (Object.keys(payload).length) {
        dispatch(dispatchFunctions.raw({user, token, ...payload}))
    } else {
        dispatch(dispatchFunctions.login({user, token}))
    }
    return true;
})

/***
 * redirectIfIsLogged
 * @param {String} token
 * @param {import('next/router').NextRouter} router
 * @param {String} target
 * @param {Boolean} passTokenToTarget
 */
export function redirectIfIsLogged(token, router, target, passTokenToTarget = false) {
    const user = !!token && userByToken(token)
    console.log({user, token})
    if (user) {
        router.push(passTokenToTarget ? `${target}?token=${token}` : target)
    }
}