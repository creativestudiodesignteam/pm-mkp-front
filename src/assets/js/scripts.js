(function($){
	"use strict";
	$(document).ready(function($){
		//Fixed Header
    $(window).scroll(function() {
    	const header_top = $('.header-ontop')
        if(header_top.length>0){
            if($(window).width()>812){
				const ht = $('header').height();
				const st = $(window).scrollTop();
				if(st>ht){
					header_top.addClass('fixed-ontop');
                }else{
					header_top.removeClass('fixed-ontop');
                }
            }
        }
    });

		// Push menu home 5
		const menuLeft = $('.pushmenu-left');
		const menuHome6 = $('.menu-home5');
		const nav_list = $('.open-cart');
		const nav_click = $('.icon-pushmenu');
		nav_list.on("click", function(event) {
	        event.stopPropagation();
	        $(this).toggleClass('active');
	        $('body').toggleClass('pushmenu-push-toright-cart');
	        menuLeft.toggleClass('pushmenu-open');
	        $(".container").toggleClass("canvas-container");
	    });
	    nav_click.on("click", function(event) {
	        event.stopPropagation();
	        $(this).toggleClass('active');
	        $('body').toggleClass('pushmenu-push-toleft');
	        menuHome6.toggleClass('pushmenu-open');
	        $('.menu-mobile-left-content').hide();
	        $('#slide-bar-category').hide();
	    });
	    $(".wrappage").on("click", function() {
	        $(this).removeClass('active');
	        $('body').removeClass('pushmenu-push-toright-cart').removeClass('pushmenu-push-toleft');
	        menuLeft.removeClass('pushmenu-open');
	        menuHome6.removeClass('pushmenu-open');
	    });
	    $(".close-left").on("click", function() {
	        $(this).removeClass('active');
	        $('body').removeClass('pushmenu-push-toright-cart');
	        menuLeft.removeClass('pushmenu-open');
	    });
	    $(".close-left").on("click", function() {
	        $('body').removeClass('pushmenu-push-toleft');
	        menuHome6.removeClass('pushmenu-open');
	    });
	    // Open menu dropdown home 5
	    $(".js-menubar li .icon-sub-menu").on("click", function() {

	        $(this).toggleClass('up-icon');
	        $(this).parent().find(".js-open-menu").slideToggle('fast', function() {
	            $(this).next().stop(true).toggleClass('open', $(this).is(":visible"));
	        });
	    });
		$('#quickview').modal('show');
		/* Count animate numeric */
		$('.Count').each(function () {
		  var $this = $(this);
		  jQuery({ Counter: 0 }).animate({ Counter: $this.text() }, {
		    duration: 2000,
		    easing: 'swing',
		    step: function (now) {
			  $this.text(Math.ceil(now));
			}
		  });
		});
		if($('.grid-box-content').length){
			$('.grid-box-content').masonry({
			    itemSelector:'.card',
			    gutter: 30,
			    horizontalOrder:true,
			    originLeft: true,
			    transitionDuration: '0.5s',
			    resize:true,
			    stagger:30
			});
		}
		if($('.range-slider').length){
			var sliderSections = document.getElementsByClassName("range-slider")[0];
			var sliders = sliderSections.getElementsByTagName("input");
			for( var y = 0; y < sliders.length; y++ ){
				if( sliders[y].type ==="range" ){
					sliders[y].oninput = getValsRanger;
					// Manually trigger event first time to display values
					sliders[y].oninput();
				}
			}
		}
		changeIconButtonSearch();
		$('.category-box').on('click',function(){
			var check = $('.search-box').find('.show-box-category-seach').length;
			if(check){
				$('.box-category-search').removeClass('show-box-category-seach');
			}else{
				$('.box-category-search').addClass('show-box-category-seach');
			}
		});
		$('.form-placeholde-animate').find('input, textarea').on('keyup blur focus', function (e) {

			var $this = $(this),
			label = $this.prev('label');

			if (e.type === 'keyup') {
				if ($this.val() === '') {
					label.removeClass('active highlight');
				} else {
					label.addClass('active highlight');
				}
			} else if (e.type === 'blur') {
				if( $this.val() === '' ) {
					label.removeClass('active highlight'); 
				} else {
					label.removeClass('highlight');   
				}   
			} else if (e.type === 'focus') {

				if( $this.val() === '' ) {
					label.removeClass('highlight'); 
				} 
				else if( $this.val() !== '' ) {
					label.addClass('highlight');
				}
			}

		});
		if($('#owl-big-slide').length){
			$('#owl-big-slide').slick({
				slidesToShow: 1,
				slidesToScroll: 1,
				arrows: false,
				fade: true,
				asNavFor: '#owl-thumbnail-slide'
			});
			$('#owl-thumbnail-slide').slick({
				slidesToShow: 3,
				slidesToScroll: 1,
				asNavFor: '#owl-big-slide',
				dots: false,
				autoplay: true,
		  		autoplaySpeed: 5000,
				focusOnSelect: true,
				responsive: [
					{
				      breakpoint: 1170,
				      settings: {
				        slidesToShow: 3
				      }
				    },
				    {
				      breakpoint: 768,
				      settings: {
				        slidesToShow: 3
				      }
				    },
				    {
				      breakpoint: 480,
				      settings: {
				        slidesToShow: 2
				      }
				    },
				    {
				      breakpoint: 320,
				      settings: {
				        slidesToShow: 1
				      }
				    }
				]
			});
		}
	$('.tab a').on('click', function (e) {
	  
	  e.preventDefault();
	  
	  $(this).parent().addClass('active');
	  $(this).parent().siblings().removeClass('active');
	  
	  target = $(this).attr('href');

	  $('.tab-content > div').not(target).hide();
	  
	  $(target).fadeIn(600);
	  
	});
		$('.category-image > div').owlCarousel({
			loop:true,
			nav:false,
			dots:false,
		    autoplay:true,
		    autoplayTimeout:5500,
		    autoplayHoverPause:true,
		    responsive:{
		    	320:{
		    		items:2
		    	},
		    	480:{
		    		items:3
		    	},
		    	760:{
		    		items:5
		    	}
		    }
		});
		$('.slide-related-product .owl-theme').each(function(k){
			var id_btn,data_items,items_owl;
			id_btn = k+1
			data_items = $(this).attr('data-items');
			items_owl = data_items.split(',');
			$(this).owlCarousel({
				loop:true,
				nav:true,
				dots:false,
			    autoplay:true,
			    autoplayTimeout:5500,
			    autoplayHoverPause:true,
			    navContainer:'#btn-slide-'+id_btn,
			    margin:30,
			    navText:['<i class="fa fa-angle-left" aria-hidden="true"></i>','<i class="fa fa-angle-right" aria-hidden="true"></i>'],
			    responsive : {
			    	320:{
			    		items:items_owl[0]
			    	},
			    	480:{
			    		items:items_owl[1]
			    	},
			    	768:{
			    		items:items_owl[2]
			    	},
			    	1170:{
			    		items:items_owl[3]
			    	}
			    }
			});
		});
		$('.good-deal-product .owl-theme').each(function(){
			$(this).owlCarousel({
				loop:true,
				nav:false,
				dots:false,
			    autoplay:true,
			    autoplayTimeout:5500,
			    autoplayHoverPause:true,
			    margin:30,
			    responsive:{
			    	320:{
			    		items:1
			    	},
			    	480:{
			    		items:2
			    	},
			    	768:{
			    		items:4
			    	}
			    }
			});
		});
		$('.slide-product-category .owl-theme').each(function(){
			$(this).owlCarousel({
				loop:true,
				nav:false,
				dots:false,
			    autoplay:true,
			    autoplayTimeout:5500,
			    autoplayHoverPause:true,
			    responsive:{
			    	320:{
			    		items:1
			    	},
			    	480:{
			    		items:2
			    	},
			    	768:{
			    		items:4
			    	}
			    },
			});
		});
		$('.slide-on-sale-sidebar .owl-theme').owlCarousel({
			loop:true,
			nav:true,
			dots:false,
		    autoplay:true,
		    autoplayTimeout:5500,
		    autoplayHoverPause:true,
		    items:1,
		    navContainerClass:"btn-slide-on-sale"
		});
		$(window).resize(function(){
			changeIconButtonSearch();
		});
		$('.slide-home').owlCarousel({
		    loop:true,
		    nav:false,
		    autoplay:true,
		    autoplayTimeout:5000,
		    autoplayHoverPause:true,
		    items:1,
		    dotsClass:'dots-slide',
		    dotClass:'dot-slide-home'
		});
		$('.slide-logo-brand').owlCarousel({
			loop:true,
		    nav:true,
		    dots:false,
		    autoplay:true,
		    autoplayTimeout:5000,
		    autoplayHoverPause:true,
		    navText:['<i class="fa fa-angle-left" aria-hidden="true"></i>','<i class="fa fa-angle-right" aria-hidden="true"></i>'],
		    navContainer:'.nav-next,.nav-prev',
		    responsive:{
		    	0:{
		    		items:2
		    	},
		    	480:{
		    		items:2
		    	},
		    	760:{
		    		items:4
		    	},
		    	980:{
		    		items:4
		    	},
		    	1170:{
		    		items:6
		    	}
		    }
		});
	});

})(jQuery)