import Swal from 'sweetalert2'
import withReactContent from 'sweetalert2-react-content'

export const opt = {
    showCloseButton: true,
    showConfirmButton: false,
    timer: 3500
}
export const MySwal = (withReactContent(Swal)).mixin(opt)
export const Toast = (
    withReactContent(Swal)
).mixin({
    toast: true,
    position: 'top-right',
    showConfirmButton: false,
    timer: 2000
})