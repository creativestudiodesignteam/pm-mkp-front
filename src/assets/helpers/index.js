/* eslint-disable no-useless-escape */
/*global localStorage, document, Blob, atob*/
import axios from "axios"
import {__storage, _storage, isAuthenticated, login, userByToken,} from "../auth"
import {dispatchFunctions, INITIAL_STATE, raw} from "../../store"
import {valid_path} from "../../components/account/store"
import cep from "cep-promise"
import {MySwal} from "../sweetalert"

export const baseUrl = "http://198.199.90.249:3336/"
export const timeout = 20000
export const clientHTTP = axios.create({
    timeout,
    baseURL: baseUrl,
    headers: {
        "Content-Type": "application/json",
    },
})
export const deliveryTypes = [
    {
        id: 1,
        name: "Formato caixa/pacote",
    },
    {
        id: 2,
        name: "Formato rolo/prisma",
    },
    {
        id: 3,
        name: "Envelope",
    },
]

/**
 *
 * @param {Object} data
 * @param {Array} checker
 */
export const verifyAllAttrs = (data, checker = []) => {
    let ok = true
    for (const check of checker) {
        ok = data && data[check] ? ok : false
    }
    return ok
}
export const isValidEmail = (mail) =>
    /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(mail)
export const elm = (target) => document.querySelector(target)

export const elmAll = (target) => document.querySelectorAll(target)

export const hasEmptyNode = (target) => {
    let ok = false
    for (const node of elmAll(target)) {
        ok = node.value === "" ? true : ok
    }

    return ok
}
export const hasEmptyNodeForm = (target) => {
    let ok = false
    for (const node of elmAll(target)) {
        if (node.value === "") {
            node.placeholder = "Campo obrigatório"
            node.classList.add("invalid")
        }
        ok = node.value === "" ? true : ok
    }

    return ok
}

export async function cepBlur(
    {target: {value}},
    {
        street = "street",
        neighborhood = "neighborhood",
        city = "city",
        state_address = "state_address",
    }
) {
    try {
        const _cep_ = await cep(String(value))
        _cep_?.street && add_valid(elm(`[name=${street}]`))
        _cep_?.neighborhood && add_valid(elm(`[name=${neighborhood}]`))
        _cep_?.city && add_valid(elm(`[name=${city}]`))
        _cep_?.state && add_valid(elm(`[name=${state_address}]`))
        !_cep_?.street && add_invalid(elm(`[name=${street}]`), "", "")
        !_cep_?.neighborhood &&
        add_invalid(elm(`[name=${neighborhood}]`), "", "")
        !_cep_?.city && add_invalid(elm(`[name=${city}]`), "", "")
        !_cep_?.state && add_invalid(elm(`[name=${state_address}]`), "", "")
        return _cep_
    } catch (error) {
        return false
    }
}

export const _start = (i) => i * 6
export const _end = (i) => _start(i) + 6

export function pag(arr, page) {
    return arr.slice(_start(page), _end(page))
}

export function BearerHeaderAuthorization(token) {
    return {headers: {Authorization: `Bearer ${token}`}}
}

export function autoRemoveNode(event, querySelector, callback = false) {
    elm(querySelector).classList.remove("d-block")
    elm(querySelector).classList.add("d-none")
    if (!!callback) {
        callback(event)
    }
    event.preventDefault()
}

export function faker(length = 100) {
    let a = 1
    let element = []
    for (let index = 0; index < length; index++) {
        element = [
            ...element,
            {
                id: a,
                price: (a++).toFixed(2),
                amount: "0",
                cod: "#0000000000",
                exibition_name: "Tênis Nike Preto",
            },
        ]
    }
    return element
}

export function validarCPF(cpf) {
    cpf = cpf.replace(/[^\d]+/g, "")
    if (cpf === "") return false
    // Elimina CPFs invalidos conhecidos
    if (
        cpf.length !== 11 ||
        cpf === "00000000000" ||
        cpf === "11111111111" ||
        cpf === "22222222222" ||
        cpf === "33333333333" ||
        cpf === "44444444444" ||
        cpf === "55555555555" ||
        cpf === "66666666666" ||
        cpf === "77777777777" ||
        cpf === "88888888888" ||
        cpf === "99999999999"
    )
        return false
    // Valida 1o digito
    let add = 0
    let i = 0
    for (i = 0; i < 9; i++) add += parseInt(cpf.charAt(i)) * (10 - i)
    let rev = Number(11 - (Number(add) % 11))
    if (rev === 10 || rev === 11) rev = 0
    if (rev !== parseInt(cpf.charAt(9))) return false
    // Valida 2o digito
    add = 0
    for (i = 0; i < 10; i++) add += parseInt(cpf.charAt(i)) * (11 - i)
    rev = 11 - (add % 11)
    if (rev === 10 || rev === 11) rev = 0
    return rev === parseInt(cpf.charAt(10))
}

export function validURL(str) {
    const pattern = new RegExp(
        "^(https?:\\/\\/)?" + // protocol
        "((([a-z\\d]([a-z\\d-]*[a-z\\d])*)\\.)+[a-z]{2,}|" + // domain name
        "((\\d{1,3}\\.){3}\\d{1,3}))" + // OR ip (v4) address
        "(\\:\\d+)?(\\/[-a-z\\d%_.~+]*)*" + // port and path
        "(\\?[;&a-z\\d%_.~+=-]*)?" + // query string
        "(\\#[-a-z\\d_]*)?$",
        "i"
    ) // fragment locator
    return !!pattern.test(str)
}

export function validarCNPJ(cnpj) {
    cnpj = String(cnpj.replace(/[^\d]+/g, ""))

    if (String(cnpj) === "") return false

    if (cnpj.length !== 14) return false

    // Elimina CNPJs invalidos conhecidos
    if (
        cnpj === "00000000000000" ||
        cnpj === "11111111111111" ||
        cnpj === "22222222222222" ||
        cnpj === "33333333333333" ||
        cnpj === "44444444444444" ||
        cnpj === "55555555555555" ||
        cnpj === "66666666666666" ||
        cnpj === "77777777777777" ||
        cnpj === "88888888888888" ||
        cnpj === "99999999999999"
    )
        return false

    // Valida DVs
    let tamanho = cnpj.length - 2
    let numeros = cnpj.substring(0, tamanho)
    let digitos = cnpj.substring(tamanho)
    let soma = 0
    let pos = tamanho - 7
    let i = 0
    for (i = tamanho; i >= 1; i--) {
        soma += numeros.charAt(tamanho - i) * pos--
        if (pos < 2) pos = 9
    }
    let resultado = String(soma % 11 < 2 ? 0 : 11 - (soma % 11))
    if (resultado !== digitos.charAt(0)) return false

    tamanho = tamanho + 1
    numeros = cnpj.substring(0, tamanho)
    soma = 0
    pos = tamanho - 7
    for (i = tamanho; i >= 1; i--) {
        soma += numeros.charAt(tamanho - i) * pos--
        if (pos < 2) pos = 9
    }
    resultado = soma % 11 < 2 ? 0 : 11 - (soma % 11)
    if (resultado != digitos.charAt(1)) return false

    return true
}

export const onlyNumber = ({target}) => {
    if (target && target.value) {
        const result = target.value.replace(/[^\d]/g, "")
        target.value = target.value.replace(/[^\d]/g, "")
        return target.value
    }
}
export const _onlyNumber = ({target}) => {
    if (target && target.value) {
        return target.value.replace(/[^\d]/g, "")
    }
}
export const cnpj = ({target}) => {
    if (target && target.value)
        target.value = target.value.replace(/[^\d|^\.|^/|^\-]/g, "")
}
export const cpf_rg = ({target}) => {
    if (target && target.value)
        target.value = target.value.replace(/[^\d|^\.|^\-]/g, "")
}
export const birthday = ({target}) => {
    if (target && target.value)
        target.value = target.value
            .replace(/[A-z]/g, "")
            .replace(/[^\d|^/]/g, "")
}
export const onKeyUpTelephone = ({target}) => {
    if (target && target.value)
        target.value = target.value.replace(/[^\d|^\(|^\)|^\-|^\s]/g, "")
}
export const postcode = ({target}) => {
    if (target && target.value)
        target.value = target.value.replace(/[^\d|^\-]/g, "")
}
export const set_value_node = (node, v = "") => {
    node.value = v
    elm(`[name=${node.name}]`).value = v
}

export const add_invalid = (node, fb, v = "") => {
    set_value_node(node, v)
    node.placeholder = fb
    node.classList.add("invalid")
    node.classList.remove("valid")
}
export const add_valid = (target) => {
    target.classList.add("valid")
    target.classList.remove("invalid")
}
export const site_blur = async ({target}) => {
    if (target && target.value)
        if (!validURL(target.value)) {
            add_invalid(target, "URL invalida")
        } else {
            add_valid(target)
            set_value_node(
                target,
                `//${target.value
                    .replace("//", "")
                    .replace("http://", "")
                    .replace("https://", "")}`
            )
        }
}
export const cnpj_blur = ({target}) => {
    if (target && target.value)
        if (!validarCNPJ(target.value)) {
            add_invalid(target, "CNPJ invalido")
        } else {
            add_valid(target)
        }
}
export const cpf_blur = ({target}) => {
    if (target && target.value)
        if (!validarCPF(target.value)) {
            add_invalid(target, "CPF invalido")
        } else {
            add_valid(target)
        }
}
export const email_blur = ({target}) => {
    if (target && target.value)
        if (!isValidEmail(target.value)) {
            add_invalid(target, "Email invalido")
        } else {
            add_valid(target)
        }
}
export const _password_blur = ({target}) => {
    if (target && target.value)
        if (!(target.value === elm("[name=password]").value)) {
            add_invalid(target, "As senhas não coincidem")
        } else {
            add_valid(target)
            add_valid(elm("[name=password]"))
        }
}

export const obrigatory_blur = ({target}, min, max, demilimiters = 0) => {
    if (target && !target.value) {
        add_invalid(target, "Campo obrigatório")
    } else if (
        target &&
        min &&
        max &&
        min === max &&
        (target.value.length < min || target.value.length > max)
    ) {
        add_invalid(
            target,
            `Tamanho exato de ${min - demilimiters} caracteres necessários`
        )
    } else if (target && min && target.value.length < min) {
        add_invalid(
            target,
            `Tamanho mínimo de ${min - demilimiters} caracteres`
        )
    } else if (target && max && target.value.length > max) {
        add_invalid(target, `Tamanho max de ${max - demilimiters} caracteres`)
    } else {
        add_valid(target)
    }
}

export const size_blur = ({target}, min, max, demilimiters = 0) => {
    if (target && min && target.value.length < min) {
        add_invalid(target, `Tamanho mínimo de ${min} caracteres`)
    } else if (
        target &&
        min &&
        max &&
        min === max &&
        (target.value.length < min || target.value.length > max)
    ) {
        add_invalid(
            target,
            `Tamanho exato de ${min - demilimiters} caracteres necessários`
        )
    } else if (target && min && target.value.length < min) {
        add_invalid(
            target,
            `Tamanho mínimo de ${min - demilimiters} caracteres`
        )
    } else if (target && max && target.value.length > max) {
        add_invalid(target, `Tamanho max de ${max - demilimiters} caracteres`)
    } else {
        add_valid(target)
    }
}
/**
 * Fire focus on element and preventDefault event
 * @param {function} callback
 * @param {HTMLElement} nodeRef
 * @param {boolean} before
 **/
export const focusOnMouseOver = ({
                                     nodeRef,
                                     callback = (_) => _.preventDefault(),
                                     before = false,
                                 }) => (e) => {
    before && nodeRef.focus()
    before && callback(e)
    !before && callback(e)
    !before && nodeRef.focus()
}

/**
 * Check if string contain only number.
 * @param {string} val
 **/
export function onlyNumbers(val) {
    return /^\d+$/.test(val)
}

export const getOrEmpty = ({value, target, _default = ""}) => {
    const _value = target
    if (value) {
        if ("object" === typeof value) {
            return !Array.isArray(value)
                ? Object.prototype.hasOwnProperty.call(value, _value)
                    ? value[_value]
                    : _default
                : value.includes(_value)
                    ? value[_value]
                    : _default
        }
        if ("string" === typeof value) {
            return (
                value.substr(value.search(`${_value}`), `${_value}`.length) ||
                _default
            )
        }
    }
    return _default
}
export const getAttrOrDefault = (_props, target, _default) =>
    Object.prototype.hasOwnProperty.call(_props, target)
        ? _props[target]
        : _default

export const awaitable = (
    callback,
    params = {},
    returns = false,
    inside = false
) => {
    if (inside) {
        return function () {
            ;(async ({...rest}) => {
                try {
                    return await callback(rest)
                } catch (e) {
                    return e
                }
            })(params)
        }
    } else if (returns) {
        return (async ({...rest}) => {
            try {
                return await callback(rest)
            } catch (e) {
                return e
            }
        })(params)
    } else {
        ;(async ({...rest}) => {
            try {
                return await callback(rest)
            } catch (e) {
            }
        })(params)
    }
}

export function disable_btn_and_put_innerHTML(
    target,
    feedback = `Carregando ... <div class="spinner-border text-light" role="status"><span class="sr-only">Loading...</span></div>`
) {
    try {
        const btn = elm(target)
        if (!!btn) {
            btn.disabled = true
            btn.innerHTML = feedback
        }
    } catch (e) {
        console.error(e)
    }
}

export function enable_btn_and_put_innerHTML(target, feedback = `SALVAR`) {
    try {
        const btn = elm(target)
        if (!!btn) {
            btn.disabled = false
            btn.innerHTML = feedback
        }
    } catch (e) {
        console.error(e)
    }
}

export async function fromServer(
    endPoint,
    token,
    method = "get",
    data = {},
    config = false,
    silent = true
) {
    !silent &&
    console.log({
        endPoint,
        token,
        method,
        data,
        config,
        silent,
    })
    try {
        switch (method.toLowerCase()) {
            case "get":
                return (await clientHTTP.get(
                    endPoint,
                    config || BearerHeaderAuthorization(token)
                )).data
            case "patch":
                return (await clientHTTP.patch(
                    endPoint,
                    data,
                    config || BearerHeaderAuthorization(token)
                )).data
            case "put":
                return (await clientHTTP.put(
                    endPoint,
                    data,
                    config || BearerHeaderAuthorization(token)
                )).data
            case "delete":
                return (await clientHTTP.delete(
                    endPoint,
                    config || BearerHeaderAuthorization(token)
                )).data
            default:
                return (await clientHTTP.post(
                    endPoint,
                    data,
                    config || BearerHeaderAuthorization(token)
                )).data
        }
    } catch ({message, ...error}) {
        !silent &&
        console.error(
            `fromServer ${JSON.stringify({
                clientHTTP,
                endPoint,
                token,
                config,
                method,
            })} ${message}`,
            error
        )
        return error?.response?.data
    }
}

export async function _fromServer({
                                      endPoint,
                                      token,
                                      method = "get",
                                      data = {},
                                      config = false,
                                      silent = false,
                                  }) {
    // !silent && console.log({ endPoint, method })
    try {
        switch (method.toLowerCase()) {
            case "get":
                return (await clientHTTP.get(
                    endPoint,
                    config || BearerHeaderAuthorization(token)
                )).data
            default:
                return (await clientHTTP.post(
                    endPoint,
                    data,
                    config || BearerHeaderAuthorization(token)
                )).data
        }
    } catch ({message, ...error}) {
        !silent &&
        console.error(
            `fromServer ${JSON.stringify({
                clientHTTP,
                endPoint,
                token,
                config,
                method,
            })} ${message}`,
            error
        )
        return error?.response?.data
    }
}

/**
 *
 * If LocalStorage contain a valid token, redirect to daschboar page
 * @param {Object} context authentication string, that u get from /auth
 * @param {function} dispatch
 * @param {import('next/router').NextRouter} router
 */
export const redirectIfIsLoddeg = ({router, dispatch, context}) => {
    const user =
        context.token !== ""
            ? isAuthenticated(context.token)
            : isAuthenticated()
    console.log(router.pathname)
    console.log("load", user, context.token)
    const isAuthPage =
        router.pathname === "/account" || router.pathname === "/login"
    const sameUser = context.email === user.email
    if (user && !sameUser) {
        dispatch(dispatchFunctions.login(user))
        !isAuthPage && router.push("/login")
    }
}

/***
 * @param {function} dispatch
 * @param {import('next/router').NextRouter} router
 * @param {boolean} redirect true
 */
export const logOut = (dispatch, router, redirect = true) => {
    /*global localStorage*/
    if (!!localStorage) {
        localStorage.setItem("token", "")
    } else {
        const window = require("global/window")
        if (!!window && !!window?.localStorage) {
            window.localStorage.setItem("token", "")
        }
    }
    console.log('logOut')
    dispatch(dispatchFunctions.init(INITIAL_STATE))
    if (redirect) {
        router.push("/login").then(() => console.log("log out"))
    }
}

export function dataURItoBlob(dataURI) {
    // convert base64/URLEncoded data component to raw binary data held in a string
    let byteString
    if (dataURI.split(",")[0].indexOf("base64") >= 0)
        byteString = atob(dataURI.split(",")[1])
    else byteString = unescape(dataURI.split(",")[1])
    // separate out the mime component
    const mimeString = dataURI
        .split(",")[0]
        .split(":")[1]
        .split(";")[0]
    // write the bytes of the string to a typed array
    const ia = new Uint8Array(byteString.length)
    for (let i = 0; i < byteString.length; i++) {
        ia[i] = byteString.charCodeAt(i)
    }
    return new Blob([ia], {type: mimeString})
}

/***
 * @param {String} value
 * @param patter
 * @returns {Number}
 */
export function formatPriceToServer(value, patter = [".", ","]) {
    if (value && value.length) {
        const [_unit, _cents] = value.split(patter[0])
        const unit = Number(_unit.replace(patter[1], "")) * 100
        const cents = Number(_cents || 0)
        return Number(unit + cents)
    }
    return Number(value || 0)
}

/***
 * isObject
 * @returns {Boolean} true | false
 */
function isObject(val) {
    if (val === null) {
        return false
    }
    return val instanceof Object || typeof val === "object"
}

/***
 * hasKey
 * @example objHasKey({a:'2'}, 'a'): true
 * @see Object.prototype.hasOwnProperty.call;
 * @param {Object} obj
 * @param {String} key
 * @returns {Boolean} true | false
 */
export function hasKey(obj, key) {
    if (isObject(obj)) {
        try {
            return Object.prototype.hasOwnProperty.call(obj, key)
        } catch (e) {
            console.log("hasKey", e)
        }
    }
    return false
}

/***
 * keys
 * @example keys({...}, (key)=>{...})): Array
 * @param {function} callback
 * @param {Array|Boolean} _valid_paths false
 * @param {String} iteration map
 * @returns {Array} true | false
 */
export function keys(callback, _valid_paths = false, iteration = "map") {
    if (!Object.is) {
        Object.is = function (x, y) {
            return x === y ? x !== 0 || 1 / x === 1 / y : x !== x && y !== y
        }
    }
    const it = iteration.toLowerCase()
    const object = Object(_valid_paths || valid_path)
    const keys = Array(Object.keys(object))
    switch (it) {
        case "reduce": {
            return keys.reduce(callback)
        }
        case "filter": {
            return keys.filter(callback)
        }
        default: {
            return keys.map(callback)
        }
    }
}

export const OBJ = {
    isObject,
    keys,
    hasKey,
}

export const cartParseFromServer = (item) => {
    if (hasKey(item, "grids")) {
        return {
            id: item.id, // id item in cart
            id_grid: item.grids.id, //id grid
            name: item.grids.products.name, // product name
            price: Number(Number(item.grids.price) / 100).toFixed(2), // product price
            amount: item.amount, // amount in cart
            amount_stock: item.grids.amount, // amount in stock of the grid
            url: item?.grids?.products?.file?.url || null,
        }
    }
    return item
}
let staticPrice = 1
export const productsParseFromServer = (item) => ({
    id: item.id || "-1",
    price: `${Number(Number(item?.infos?.minPrice || "0") / 100).toFixed(2)}`,
    amount: item?.infos?.amount_stock || "-1",
    cod: `#${item.sku || "-1"}`,
    exibition_name: item.name || "-1",
    url: item?.thumb?.url || "-1",
    selected: false,
})

/***
 *@param {Number} number 0
 * @param {Boolean} cents
 * */
export function brlFormat(number = 0, cents = false) {
    return (cents ? Number(number) / 100 : Number(number)).toLocaleString(
        "pt-br",
        {
            style: "currency",
            currency: "BRL",
        }
    )
}

export function toFixed(number, digits = 2) {
    return `${Number(Number(number) / 100).toFixed(digits)}`.replace(".", ",")
}

export const fetchAsBlob = (url) =>
    fetch(url).then((response) => response.blob())

export const convertBlobToBase64 = (blob) =>
    new Promise((resolve, reject) => {
        const reader = new FileReader()
        reader.onerror = reject
        reader.onload = () => {
            resolve(reader.result)
        }
        reader.readAsDataURL(blob)
    })

export function compareBy(by) {
    return (a, b) => {
        let comparison = 0
        if (Object.keys(a).includes(by) && Object.keys(b).includes(by)) {
            const bandA = a[by].toUpperCase()
            const bandB = b[by].toUpperCase()
            if (bandA > bandB) {
                comparison = 1
            } else if (bandA < bandB) {
                comparison = -1
            }
        }
        return comparison
    }
}

export function arrayDivideBy(items, n = 1) {
    return new Array(Math.ceil(items.length / n))
        .fill()
        .map((_) => items.splice(0, n))
}

export const verifyEmail = async (email) => {
    const result = await fromServer(`/check_email`, "", "post", {
        email,
    })
    console.log({result})
    if (!result || hasKey(result, "error")) {
        await MySwal.fire("E-mail inválido", "", "error")
        return false
    } else if (hasKey(result, "status")) {
        if (result.status) {
            await MySwal.fire("E-mail já cadastrado", "", "error")
            return false
        } else {
            return true
        }
    }
}
export const verifyCNPJ = async (_target) => {
    const target = elm(_target)
    const response = await fromServer("/check_cnpj", false, "POST", {
        cnpj: target.value,
    })
    if (hasKey(response, "error")) {
        target.value = ""
        target.placeholder = "Cnpj inválido!"
        await MySwal.fire("Cnpj inválido!", "", "error")
    } else {
        if (!response.success.status) {
            target.value = ""
            target.placeholder = "CNPJ já cadastrado"
            await MySwal.fire("CNPJ já cadastrado", "", "error")
        } else {
            target.classList.add("is-valid", "valid")
            return true
        }
    }
    return false
}

/**
 *
 * @param setState {function}
 * @param state
 */
export const defaultListener = (setState, state) => {
    return (e) => {
        e.preventDefault()
        setState({...state, [e.target.name]: e.target.value})
    }
}

export function optionsParseFromSever(data) {
    try {
        let _gridOptions = []
        for (const grid of data) {
            for (const {
                fk_options,
                name_options,
                fk_options_names,
                name_options_names,
            } of grid.options) {
                if (
                    !_gridOptions.length ||
                    !_gridOptions.filter(
                        (_gridOption) => _gridOption.fk_options === fk_options
                    ).length
                ) {
                    _gridOptions = [
                        ..._gridOptions,
                        {
                            fk_options,
                            name_options,
                            names: [
                                {
                                    fk_options_names,
                                    name_options_names,
                                },
                            ],
                        },
                    ]
                } else {
                    _gridOptions = _gridOptions.map((_gridOption) =>
                        _gridOption.fk_options === fk_options &&
                        !_gridOption.names.find(
                            (n) => n.fk_options_names === fk_options_names
                        )
                            ? {
                                fk_options,
                                name_options,
                                names: [
                                    ..._gridOption.names,
                                    {
                                        fk_options_names,
                                        name_options_names,
                                    },
                                ],
                            }
                            : _gridOption
                    )
                }
            }
        }
        return _gridOptions.sort(compareBy("name_options"))
    } catch (e) {
        console.error(e)
    }
}

/**
 * getMaxValueOnArrayOfObjectBy
 * @example [{value:10, other_number_target:102},...].reduce(getMaxValueOnArrayOfObjectBy('other_number_target'), 0):Number
 * @param target {String}
 */
export function getMaxValueOnArrayOfObjectBy(target) {
    return (acc, c) => (c[`${target}`] > acc ? c[`${target}`] : acc)
}

export async function dateChecker({target}) {
    const campo = target
    const {value: valor} = target
    const date = valor

    let ardt
    const ExpReg = new RegExp(
        "(0[1-9]|[12][0-9]|3[01])/(0[1-9]|1[012])/[12][0-9]{3}"
    )
    ardt = date.split("/")
    let error = false
    if (Number(date.search(ExpReg)) === -1) {
        error = true
    } else if (
        (Number(ardt[1]) === 4 ||
            Number(ardt[1]) === 6 ||
            Number(ardt[1]) === 9 ||
            Number(ardt[1]) === 11) &&
        Number(ardt[0]) > 30
    )
        error = true
    else if (Number(ardt[1]) === 2) {
        if (Number(ardt[0]) > 28 && Number(ardt[2]) % 4 !== 0) error = true
        if (Number(ardt[0]) > 29 && Number(ardt[2]) % 4 === 0) error = true
    }
    if (error) {
        add_invalid(target, "Data inválida")
        campo.value = ""
        return false
    } else {
        add_valid(target)
    }
    return true
}

/**
 * @param {Function} cb
 * @param {Function} error
 * @param {Boolean} returnable
 * */
export function tryCatch(
    cb,
    error = function (error) {
        console.trace(error)
        return false
    },
    returnable = false
) {
    if (!returnable) {
        try {
            cb()
        } catch (e) {
            error(e)
        }
    } else {
        try {
            return cb()
        } catch (e) {
            return error(e)
        }
    }
}

function check({
                   ar,
                   automotivo,
                   bebes,
                   beleza,
                   brinquedos,
                   construcao,
                   cuidados,
                   eletrodomesticos,
                   eletronicos,
                   esportes,
                   filmes,
                   games,
                   moveis,
                   petshop,
                   relogios,
                   saude
               }) {
    return !ar ||
        hasKey(ar, "error") ||
        !automotivo ||
        hasKey(automotivo, "error") ||
        !bebes ||
        hasKey(bebes, "error") ||
        !beleza ||
        hasKey(beleza, "error") ||
        !brinquedos ||
        hasKey(brinquedos, "error") ||
        !construcao ||
        hasKey(construcao, "error") ||
        !cuidados ||
        hasKey(cuidados, "error") ||
        !eletrodomesticos ||
        hasKey(eletrodomesticos, "error") ||
        !eletronicos ||
        hasKey(eletronicos, "error") ||
        !esportes ||
        hasKey(esportes, "error") ||
        !filmes ||
        hasKey(filmes, "error") ||
        !games ||
        hasKey(games, "error") ||
        !moveis ||
        hasKey(moveis, "error") ||
        !petshop ||
        hasKey(petshop, "error") ||
        !relogios ||
        hasKey(relogios, "error") ||
        !saude ||
        hasKey(saude, "error")
}

export function tryInitializeDataStore(
    {context, dispatch, cb = false},
    useEffect = false
) {
    function _tryInitializeDataStore() {
        let finalCB = function () {
            console.log('empty finalCB')
        }
        if (!!cb) {
            finalCB = cb()
        }
        tryCatch(
            async () => {
                const _token =
                    context?.token ||
                    __storage(
                        (storage) => storage.getItem("token"),
                        (error) => {
                            console.log(
                                "tryInitializeDataStore _tryInitializeDataStore",
                                error
                            )
                            return ""
                        },
                        true
                    )
                const user = userByToken(_token)
                let {categories, subCat} = INITIAL_STATE
                if (!context.categories.length || !context.subCat.ar.length) {
                    console.log('categories e subCat vazios')
                    const payload = __storage(storage => {
                        if (storage.getItem('store')) {
                            console.log('tem cache')
                            const _state = JSON.parse(storage.getItem('store'))
                            const {categories: _categories, subCat: _subCat} = _state
                            return {categories: _categories, subCat: _subCat}
                        }
                        return false
                    })
                    if (!!payload && !!payload?.categories?.length > 0 && !!payload?.subCat?.ar?.length > 0) {
                        console.log('tem no cache', {payload})
                        categories = payload.categories
                        subCat = payload.subCat
                    } else {
                        const [
                            ar,
                            automotivo,
                            bebes,
                            beleza,
                            brinquedos,
                            construcao,
                            cuidados,
                            eletrodomesticos,
                            eletronicos,
                            esportes,
                            filmes,
                            games,
                            moveis,
                            petshop,
                            relogios,
                            saude,
                        ] = [
                            await fromServer(`/subcategories/1`),
                            await fromServer(`/subcategories/4`),
                            await fromServer(`/subcategories/7`),
                            await fromServer(`/subcategories/6`),
                            await fromServer(`/subcategories/8`),
                            await fromServer(`/subcategories/9`),
                            await fromServer(`/subcategories/10`),
                            await fromServer(`/subcategories/12`),
                            await fromServer(`/subcategories/13`),
                            await fromServer(`/subcategories/16`),
                            await fromServer(`/subcategories/18`),
                            await fromServer(`/subcategories/19`),
                            await fromServer(`/subcategories/29`),
                            await fromServer(`/subcategories/31`),
                            await fromServer(`/subcategories/33`),
                            await fromServer(`/subcategories/34`),
                        ]

                        if (
                            check({
                                ar,
                                automotivo,
                                bebes,
                                beleza,
                                brinquedos,
                                construcao,
                                cuidados,
                                eletrodomesticos,
                                eletronicos,
                                esportes,
                                filmes,
                                games,
                                moveis,
                                petshop,
                                relogios,
                                saude,
                            })
                        ) {
                            console.log("fail to load", {
                                ar,
                                automotivo,
                                bebes,
                                beleza,
                                brinquedos,
                                construcao,
                                cuidados,
                                eletrodomesticos,
                                eletronicos,
                                esportes,
                                filmes,
                                games,
                                moveis,
                                petshop,
                                relogios,
                                saude,
                            })
                        } else {
                            categories = [
                                {
                                    name: "Ar-condicionado e aquecedores",
                                    idCat: 1,
                                    subMenu: ar,
                                },
                                {
                                    name: "Automotivo",
                                    idCat: 4,
                                    subMenu: automotivo,
                                },
                                {
                                    name: "Bebês",
                                    idCat: 7,
                                    subMenu: bebes,
                                },
                                {
                                    name: "Beleza e Perfumaria",
                                    idCat: 6,
                                    subMenu: beleza,
                                },
                                {
                                    name: "Brinquedos e Jogos",
                                    idCat: 8,
                                    subMenu: brinquedos,
                                },
                                {
                                    name: "Construção",
                                    idCat: 9,
                                    subMenu: construcao,
                                },
                                {
                                    name: "Cuidados Pessoais",
                                    idCat: 10,
                                    subMenu: cuidados,
                                },
                                {
                                    name: "Eletrodomésticos",
                                    idCat: 12,
                                    subMenu: eletrodomesticos,
                                },
                                {
                                    name: "Eletrônicos",
                                    idCat: 13,
                                    subMenu: eletronicos,
                                },
                                {
                                    name: "Esportes",
                                    idCat: 16,
                                    subMenu: esportes,
                                },
                                {
                                    name: "Filmes",
                                    idCat: 18,
                                    subMenu: filmes,
                                },
                                {
                                    name: "Games",
                                    idCat: 19,
                                    subMenu: games,
                                },
                                {
                                    name: "Móveis",
                                    idCat: 29,
                                    subMenu: moveis,
                                },
                                {
                                    name: "Petshop",
                                    idCat: 31,
                                    subMenu: petshop,
                                },
                                {
                                    name: "Relógios, Joias e Bijouterias",
                                    idCat: 33,
                                    subMenu: relogios,
                                },
                                {
                                    name: "Saúde",
                                    idCat: 34,
                                    subMenu: saude,
                                },
                            ]
                            subCat = {
                                ar,
                                automotivo,
                                bebes,
                                beleza,
                                brinquedos,
                                construcao,
                                cuidados,
                                eletrodomesticos,
                                eletronicos,
                                esportes,
                                filmes,
                                games,
                                moveis,
                                petshop,
                                relogios,
                                saude,
                            }
                            console.log('pegou do servidor', {categories, subCat})
                        }
                    }
                } else {
                    console.log('tem categoria ja carregada')
                }
                if (!!user) {
                    console.log('user valido')
                    await tryCatch(async () => {
                        const cart = await fromServer("/carts", _token)
                        !cart ||
                        (hasKey(cart, "error") &&
                            console.log(
                                "tryInitializeDataStore !!user '/carts' fail",
                                cart?.error?.message
                            ))
                        let data = context.navbar_props;
                        if (data === false) {
                            console.log("try set navbar_props")
                            const response = await fromServer("/users", _token)
                            if (!!response && !hasKey(response, 'error')) {
                                data = response
                            } else {
                                console.log("falha ao carregarr /users", _token)
                            }
                        } else {
                            console.log("not try set navbar_props", data)
                        }
                        const backup = JSON.parse(_storage() && _storage().getItem('store') || '{}')
                        console.log('before set state', {
                            data,
                            categories: categories.length > 0 ? categories : backup?.categories,
                            subCat: subCat?.ar?.length > 0 ? subCat : backup?.subCat,
                        })
                        login(_token)
                        dispatch(
                            raw({
                                ...context,
                                categories: categories.length > 0 ? categories : backup.categories,
                                subCat: subCat?.ar?.length > 0 ? subCat : backup.subCat,
                                navbar_props: data,
                                cart:
                                    !!cart && !hasKey(cart, "error")
                                        ? cart.map(cartParseFromServer)
                                        : INITIAL_STATE.cart,
                                cart_length:
                                    !!cart && !hasKey(cart, "error")
                                        ? cart.length
                                        : INITIAL_STATE.cart_length,
                                user,
                                token: _token,
                            })
                        )
                    })
                } else {
                    console.log('user invalido')
                    tryCatch(() => {
                        if (!!_storage().getItem("store")) {
                            console.log('com cache')
                            const {
                                cart_length
                            } = JSON.parse(_storage().getItem("store"))
                            _storage().setItem("token", "")
                            console.log({
                                categories,
                                subCat,
                                cart_length
                            })
                            dispatch(
                                raw({
                                    ...INITIAL_STATE,
                                    categories,
                                    subCat,
                                    cart_length,
                                })
                            )
                        } else {
                            console.log('sem cache')
                            _storage().setItem("token", "")
                            console.log({
                                categories,
                                subCat
                            })
                            dispatch(
                                raw({
                                    ...INITIAL_STATE,
                                    categories,
                                    subCat
                                })
                            )
                        }

                    })
                }
                tryCatch(() => {
                    !!cb && !!finalCB && finalCB()
                })
            },
            (error) => {
                console.log("store fail:", error)
                tryCatch(() => {
                    !!cb && !!finalCB && finalCB()
                })
            }
        )

    }

    if (useEffect) {
        return () => {
            _tryInitializeDataStore()
        }
    } else {
        _tryInitializeDataStore()
    }
}
export function re() {
    
}
export function forceFormat(v) {
    if(v.length>14){
        return v.slice(0,14)
    }
    if (v) {
        const raw = v.replace(/[^\d]/g, "")
        console.log({raw})
        const cents = raw.slice(v.length - 2, v.length)
        console.log({cents})
        const rest = raw.slice(0, v.length - 2)
        console.log({rest})
        if(!rest&&!cents){
            return ''
        }
        const finalValue = (!rest) ? `${brlFormat(Number(`0.${cents.length > 1 ? cents : `0${cents}`}`))}`.replace(/[\s|R$]/g, "") :
            `${brlFormat(Number(`${rest}.${cents}`))}`.replace(/[\s|R$]/g, "")
        console.log({finalValue,v})
        return finalValue;
    }
    return ""
}

export const ramos = [
    {id: 0, name: "vendas online"},
    {id: 1, name: "vendas offline"},
    {id: 2, name: "vendas porta a porta"},
]
export const modalidades = [
    {id: "0", name: ""},
    {id: "Atacado", name: "Atacado"},
    {id: "Varejo", name: "Varejo"},
    {id: "Atacado e Varejo", name: "Atacado e Varejo"},
]
export const bank_account_types = [
    {id: "", name: ""},
    {id: "CC", name: "Conta Corrente"},
    {id: "CI", name: "Conta Poupança"},
]
export const conditions = [
    {id: 0, name: ""},
    {id: 1, name: "Novo"},
    {id: 2, name: "Usado"},
]
export const state_address = [
    {name: "", id: ""},
    {name: "Acre", id: "AC"},
    {name: "Alagoas", id: "AL"},
    {name: "Amapá", id: "AP"},
    {name: "Amazonas", id: "AM"},
    {name: "Bahia", id: "BA"},
    {name: "Ceará", id: "CE"},
    {name: "Distrito Federal", id: "DF"},
    {name: "Espírito Santo", id: "ES"},
    {name: "Goiás", id: "GO"},
    {name: "Maranhão", id: "MA"},
    {name: "Mato Grosso", id: "MT"},
    {name: "Mato Grosso do Sul", id: "MS"},
    {name: "Minas Gerais", id: "MG"},
    {name: "Pará", id: "PA"},
    {name: "Paraíba", id: "PB"},
    {name: "Paraná", id: "PR"},
    {name: "Pernambuco", id: "PE"},
    {name: "Piauí", id: "PI"},
    {name: "Rio de Janeiro", id: "RJ"},
    {name: "Rio Grande do Norte", id: "RN"},
    {name: "Rio Grande do Sul", id: "RS"},
    {name: "Rondônia", id: "RO"},
    {name: "Roraima", id: "RR"},
    {name: "Santa Catarina", id: "SC"},
    {name: "São Paulo", id: "SP"},
    {name: "Sergipe", id: "SE"},
    {name: "Tocantins", id: "TO"},
]
export const code_banks = [
    {value: "0", label: ""},
    {
        value: "001",
        label: "Banco do Brasil S.A.",
    },
    {
        value: "003",
        label: "Banco da Amazônia S.A.",
    },
    {
        value: "004",
        label: "Banco do Nordeste do Brasil S.A.",
    },
    {
        value: "007",
        label: "Banco Nacional de Desenvolvimento Econômico e Social BNDES",
    },
    {
        value: "010",
        label: "Credicoamo Crédito Rural Cooperativa",
    },
    {
        value: "011",
        label: "Credit Suisse Hedging-Griffo Corretora de Valores S.A.",
    },
    {
        value: "012",
        label: "Banco Inbursa S.A.",
    },
    {
        value: "014",
        label: "Natixis Brasil S.A. Banco Múltiplo",
    },
    {
        value: "015",
        label:
            "UBS Brasil Corretora de Câmbio, Títulos e Valores Mobiliários S.A.",
    },
    {
        value: "016",
        label:
            "Coop de Créd. Mútuo dos Despachantes de Trânsito de SC e Rio Grande do Sul",
    },
    {
        value: "017",
        label: "BNY Mellon Banco S.A.",
    },
    {
        value: "018",
        label: "Banco Tricury S.A.",
    },
    {
        value: "021",
        label: "Banestes S.A. Banco do Estado do Espírito Santo",
    },
    {
        value: "024",
        label: "Banco Bandepe S.A.",
    },
    {
        value: "025",
        label: "Banco Alfa S.A.",
    },
    {
        value: "029",
        label: "Banco Itaú Consignado S.A.",
    },
    {
        value: "033",
        label: "Banco Santander (Brasil) S. A.",
    },
    {
        value: "036",
        label: "Banco Bradesco BBI S.A.",
    },
    {
        value: "037",
        label: "Banco do Estado do Pará S.A.",
    },
    {
        value: "040",
        label: "Banco Cargill S.A.",
    },
    {
        value: "041",
        label: "Banco do Estado do Rio Grande do Sul S.A.",
    },
    {
        value: "047",
        label: "Banco do Estado de Sergipe S.A.",
    },
    {
        value: "060",
        label: "Confidence Corretora de Câmbio S.A.",
    },
    {
        value: "062",
        label: "Hipercard Banco Múltiplo S.A.",
    },
    {
        value: "063",
        label: "Banco Bradescard S.A.",
    },
    {
        value: "064",
        label: "Goldman Sachs do Brasil  Banco Múltiplo S. A.",
    },
    {
        value: "065",
        label: "Banco AndBank (Brasil) S.A.",
    },
    {
        value: "066",
        label: "Banco Morgan Stanley S. A.",
    },
    {
        value: "069",
        label: "Banco Crefisa S.A.",
    },
    {
        value: "070",
        label: "Banco de Brasília S.A.",
    },
    {
        value: "074",
        label: "Banco J. Safra S.A.",
    },
    {
        value: "075",
        label: "Banco ABN Amro S.A.",
    },
    {
        value: "076",
        label: "Banco KDB do Brasil S.A.",
    },
    {
        value: "077",
        label: "Banco Inter S.A.",
    },
    {
        value: "078",
        label: "Haitong Banco de Investimento do Brasil S.A.",
    },
    {
        value: "079",
        label: "Banco Original do Agronegócio S.A.",
    },
    {
        value: "080",
        label: "BT Corretora de Câmbio Ltda.",
    },
    {
        value: "081",
        label: "BBN Banco Brasileiro de Negocios S.A.",
    },
    {
        value: "082",
        label: "Banco Topazio S.A.",
    },
    {
        value: "083",
        label: "Banco da China Brasil S.A.",
    },
    {
        value: "084",
        label: "Uniprime Norte do Paraná - Cooperativa de Crédito Ltda.",
    },
    {
        value: "085",
        label: "Cooperativa Central de Crédito Urbano - Cecred",
    },
    {
        value: "089",
        label: "Cooperativa de Crédito Rural da Região da Mogiana",
    },
    {
        value: "091",
        label:
            "Central de Cooperativas de Economia e Crédito Mútuo do Est RS - Unicred",
    },
    {
        value: "092",
        label: "BRK S.A. Crédito, Financiamento e Investimento",
    },
    {
        value: "093",
        label:
            "Pólocred Sociedade de Crédito ao Microempreendedor e à Empresa de Pequeno Porte",
    },
    {
        value: "094",
        label: "Banco Finaxis S.A.",
    },
    {
        value: "095",
        label: "Banco Confidence de Câmbio S.A.",
    },
    {
        value: "096",
        label: "Banco BMFBovespa de Serviços de Liquidação e Custódia S/A",
    },
    {
        value: "097",
        label:
            "Cooperativa Central de Crédito Noroeste Brasileiro Ltda - CentralCredi",
    },
    {
        value: "098",
        label: "Credialiança Cooperativa de Crédito Rural",
    },
    {
        value: "099",
        label:
            "Uniprime Central – Central Interestadual de Cooperativas de Crédito Ltda.",
    },
    {
        value: "100",
        label: "Planner Corretora de Valores S.A.",
    },
    {
        value: "101",
        label:
            "Renascença Distribuidora de Títulos e Valores Mobiliários Ltda.",
    },
    {
        value: "102",
        label:
            "XP Investimentos Corretora de Câmbio Títulos e Valores Mobiliários S.A.",
    },
    {
        value: "104",
        label: "Caixa Econômica Federal",
    },
    {
        value: "105",
        label: "Lecca Crédito, Financiamento e Investimento S/A",
    },
    {
        value: "107",
        label: "Banco Bocom BBM S.A.",
    },
    {
        value: "108",
        label: "PortoCred S.A. Crédito, Financiamento e Investimento",
    },
    {
        value: "111",
        label:
            "Oliveira Trust Distribuidora de Títulos e Valores Mobiliários S.A.",
    },
    {
        value: "113",
        label: "Magliano S.A. Corretora de Cambio e Valores Mobiliarios",
    },
    {
        value: "114",
        label:
            "Central Cooperativa de Crédito no Estado do Espírito Santo - CECOOP",
    },
    {
        value: "117",
        label: "Advanced Corretora de Câmbio Ltda.",
    },
    {
        value: "118",
        label: "Standard Chartered Bank (Brasil) S.A. Banco de Investimento",
    },
    {
        value: "119",
        label: "Banco Western Union do Brasil S.A.",
    },
    {
        value: "120",
        label: "Banco Rodobens SA",
    },
    {
        value: "121",
        label: "Banco Agibank S.A.",
    },
    {
        value: "122",
        label: "Banco Bradesco BERJ S.A.",
    },
    {
        value: "124",
        label: "Banco Woori Bank do Brasil S.A.",
    },
    {
        value: "125",
        label: "Brasil Plural S.A. Banco Múltiplo",
    },
    {
        value: "126",
        label: "BR Partners Banco de Investimento S.A.",
    },
    {
        value: "127",
        label: "Codepe Corretora de Valores e Câmbio S.A.",
    },
    {
        value: "128",
        label: "MS Bank S.A. Banco de Câmbio",
    },
    {
        value: "129",
        label: "UBS Brasil Banco de Investimento S.A.",
    },
    {
        value: "130",
        label:
            "Caruana S.A. Sociedade de Crédito, Financiamento e Investimento",
    },
    {
        value: "131",
        label: "Tullett Prebon Brasil Corretora de Valores e Câmbio Ltda.",
    },
    {
        value: "132",
        label: "ICBC do Brasil Banco Múltiplo S.A.",
    },
    {
        value: "133",
        label:
            "Confederação Nacional das Cooperativas Centrais de Crédito e Economia Familiar e",
    },
    {
        value: "134",
        label:
            "BGC Liquidez Distribuidora de Títulos e Valores Mobiliários Ltda.",
    },
    {
        value: "135",
        label:
            "Gradual Corretora de Câmbio, Títulos e Valores Mobiliários S.A.",
    },
    {
        value: "136",
        label:
            "Confederação Nacional das Cooperativas Centrais Unicred Ltda – Unicred do Brasil",
    },
    {
        value: "137",
        label: "Multimoney Corretora de Câmbio Ltda",
    },
    {
        value: "138",
        label: "Get Money Corretora de Câmbio S.A.",
    },
    {
        value: "139",
        label: "Intesa Sanpaolo Brasil S.A. - Banco Múltiplo",
    },
    {
        value: "140",
        label: "Easynvest - Título Corretora de Valores SA",
    },
    {
        value: "142",
        label: "Broker Brasil Corretora de Câmbio Ltda.",
    },
    {
        value: "143",
        label: "Treviso Corretora de Câmbio S.A.",
    },
    {
        value: "144",
        label: "Bexs Banco de Câmbio S.A.",
    },
    {
        value: "145",
        label: "Levycam - Corretora de Câmbio e Valores Ltda.",
    },
    {
        value: "146",
        label: "Guitta Corretora de Câmbio Ltda.",
    },
    {
        value: "149",
        label: "Facta Financeira S.A. - Crédito Financiamento e Investimento",
    },
    {
        value: "157",
        label:
            "ICAP do Brasil Corretora de Títulos e Valores Mobiliários Ltda.",
    },
    {
        value: "159",
        label: "Casa do Crédito S.A. Sociedade de Crédito ao Microempreendedor",
    },
    {
        value: "163",
        label: "Commerzbank Brasil S.A. - Banco Múltiplo",
    },
    {
        value: "169",
        label: "Banco Olé Bonsucesso Consignado S.A.",
    },
    {
        value: "172",
        label: "Albatross Corretora de Câmbio e Valores S.A",
    },
    {
        value: "173",
        label: "BRL Trust Distribuidora de Títulos e Valores Mobiliários S.A.",
    },
    {
        value: "174",
        label:
            "Pernambucanas Financiadora S.A. Crédito, Financiamento e Investimento",
    },
    {
        value: "177",
        label: "Guide Investimentos S.A. Corretora de Valores",
    },
    {
        value: "180",
        label:
            "CM Capital Markets Corretora de Câmbio, Títulos e Valores Mobiliários Ltda.",
    },
    {
        value: "182",
        label:
            "Dacasa Financeira S/A - Sociedade de Crédito, Financiamento e Investimento",
    },
    {
        value: "183",
        label: "Socred S.A. - Sociedade de Crédito ao Microempreendedor",
    },
    {
        value: "184",
        label: "Banco Itaú BBA S.A.",
    },
    {
        value: "188",
        label: "Ativa Investimentos S.A. Corretora de Títulos Câmbio e Valores",
    },
    {
        value: "189",
        label: "HS Financeira S/A Crédito, Financiamento e Investimentos",
    },
    {
        value: "190",
        label:
            "Cooperativa de Economia e Crédito Mútuo dos Servidores Públicos Estaduais do Rio",
    },
    {
        value: "191",
        label: "Nova Futura Corretora de Títulos e Valores Mobiliários Ltda.",
    },
    {
        value: "194",
        label: "Parmetal Distribuidora de Títulos e Valores Mobiliários Ltda.",
    },
    {
        value: "196",
        label: "Fair Corretora de Câmbio S.A.",
    },
    {
        value: "197",
        label: "Stone Pagamentos S.A.",
    },
    {
        value: "204",
        label: "Banco Bradesco Cartões S.A.",
    },
    {
        value: "208",
        label: "Banco BTG Pactual S.A.",
    },
    {
        value: "212",
        label: "Banco Original S.A.",
    },
    {
        value: "213",
        label: "Banco Arbi S.A.",
    },
    {
        value: "217",
        label: "Banco John Deere S.A.",
    },
    {
        value: "218",
        label: "Banco BS2 S.A.",
    },
    {
        value: "222",
        label: "Banco Credit Agrícole Brasil S.A.",
    },
    {
        value: "224",
        label: "Banco Fibra S.A.",
    },
    {
        value: "233",
        label: "Banco Cifra S.A.",
    },
    {
        value: "237",
        label: "Banco Bradesco S.A.",
    },
    {
        value: "241",
        label: "Banco Clássico S.A.",
    },
    {
        value: "243",
        label: "Banco Máxima S.A.",
    },
    {
        value: "246",
        label: "Banco ABC Brasil S.A.",
    },
    {
        value: "249",
        label: "Banco Investcred Unibanco S.A.",
    },
    {
        value: "250",
        label: "BCV - Banco de Crédito e Varejo S/A",
    },
    {
        value: "253",
        label: "Bexs Corretora de Câmbio S/A",
    },
    {
        value: "254",
        label: "Parana Banco S. A.",
    },
    {
        value: "260",
        label: "Nu Pagamentos S.A.",
    },
    {
        value: "265",
        label: "Banco Fator S.A.",
    },
    {
        value: "266",
        label: "Banco Cédula S.A.",
    },
    {
        value: "268",
        label: "Barigui Companhia Hipotecária",
    },
    {
        value: "269",
        label: "HSBC Brasil S.A. Banco de Investimento",
    },
    {
        value: "271",
        label: "IB Corretora de Câmbio, Títulos e Valores Mobiliários Ltda.",
    },
    {
        value: "300",
        label: "Banco de la Nacion Argentina",
    },
    {
        value: "318",
        label: "Banco BMG S.A.",
    },
    {
        value: "320",
        label: "China Construction Bank (Brasil) Banco Múltiplo S/A",
    },
    {
        value: "341",
        label: "Itaú Unibanco  S.A.",
    },
    {
        value: "366",
        label: "Banco Société Générale Brasil S.A.",
    },
    {
        value: "370",
        label: "Banco Mizuho do Brasil S.A.",
    },
    {
        value: "376",
        label: "Banco J. P. Morgan S. A.",
    },
    {
        value: "389",
        label: "Banco Mercantil do Brasil S.A.",
    },
    {
        value: "394",
        label: "Banco Bradesco Financiamentos S.A.",
    },
    {
        value: "399",
        label: "Kirton Bank S.A. - Banco Múltiplo",
    },
    {
        value: "412",
        label: "Banco Capital S. A.",
    },
    {
        value: "422",
        label: "Banco Safra S.A.",
    },
    {
        value: "456",
        label: "Banco MUFG Brasil S.A.",
    },
    {
        value: "464",
        label: "Banco Sumitomo Mitsui Brasileiro S.A.",
    },
    {
        value: "473",
        label: "Banco Caixa Geral - Brasil S.A.",
    },
    {
        value: "477",
        label: "Citibank N.A.",
    },
    {
        value: "479",
        label: "Banco ItauBank S.A.",
    },
    {
        value: "487",
        label: "Deutsche Bank S.A. - Banco Alemão",
    },
    {
        value: "488",
        label: "JPMorgan Chase Bank, National Association",
    },
    {
        value: "492",
        label: "ING Bank N.V.",
    },
    {
        value: "494",
        label: "Banco de La Republica Oriental del Uruguay",
    },
    {
        value: "495",
        label: "Banco de La Provincia de Buenos Aires",
    },
    {
        value: "505",
        label: "Banco Credit Suisse (Brasil) S.A.",
    },
    {
        value: "545",
        label: "Senso Corretora de Câmbio e Valores Mobiliários S.A.",
    },
    {
        value: "600",
        label: "Banco Luso Brasileiro S.A.",
    },
    {
        value: "604",
        label: "Banco Industrial do Brasil S.A.",
    },
    {
        value: "610",
        label: "Banco VR S.A.",
    },
    {
        value: "611",
        label: "Banco Paulista S.A.",
    },
    {
        value: "612",
        label: "Banco Guanabara S.A.",
    },
    {
        value: "613",
        label: "Omni Banco S.A.",
    },
    {
        value: "623",
        label: "Banco Pan S.A.",
    },
    {
        value: "626",
        label: "Banco Ficsa S. A.",
    },
    {
        value: "630",
        label: "Banco Intercap S.A.",
    },
    {
        value: "633",
        label: "Banco Rendimento S.A.",
    },
    {
        value: "634",
        label: "Banco Triângulo S.A.",
    },
    {
        value: "637",
        label: "Banco Sofisa S. A.",
    },
    {
        value: "641",
        label: "Banco Alvorada S.A.",
    },
    {
        value: "643",
        label: "Banco Pine S.A.",
    },
    {
        value: "652",
        label: "Itaú Unibanco Holding S.A.",
    },
    {
        value: "653",
        label: "Banco Indusval S. A.",
    },
    {
        value: "654",
        label: "Banco A. J. Renner S.A.",
    },
    {
        value: "655",
        label: "Banco Votorantim S.A.",
    },
    {
        value: "707",
        label: "Banco Daycoval S.A.",
    },
    {
        value: "712",
        label: "Banco Ourinvest S.A.",
    },
    {
        value: "719",
        label: "Banif - Bco Internacional do Funchal (Brasil) S.A.",
    },
    {
        value: "735",
        label: "Banco Neon S.A.",
    },
    {
        value: "739",
        label: "Banco Cetelem S.A.",
    },
    {
        value: "741",
        label: "Banco Ribeirão Preto S.A.",
    },
    {
        value: "743",
        label: "Banco Semear S.A.",
    },
    {
        value: "745",
        label: "Banco Citibank S.A.",
    },
    {
        value: "746",
        label: "Banco Modal S.A.",
    },
    {
        value: "747",
        label: "Banco Rabobank International Brasil S.A.",
    },
    {
        value: "748",
        label: "Banco Cooperativo Sicredi S. A.",
    },
    {
        value: "751",
        label: "Scotiabank Brasil S.A. Banco Múltiplo",
    },
    {
        value: "752",
        label: "Banco BNP Paribas Brasil S.A.",
    },
    {
        value: "753",
        label: "Novo Banco Continental S.A. - Banco Múltiplo",
    },
    {
        value: "754",
        label: "Banco Sistema S.A.",
    },
    {
        value: "755",
        label: "Bank of America Merrill Lynch Banco Múltiplo S.A.",
    },
    {
        value: "756",
        label: "Banco Cooperativo do Brasil S/A - Bancoob",
    },
    {
        value: "757",
        label: "Banco Keb Hana do Brasil S. A.",
    },
]
