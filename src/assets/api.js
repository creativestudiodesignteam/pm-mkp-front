import axios from 'axios';
import {baseUrl, timeout} from "./helpers";

export const clientHTTP = axios.create({
    timeout,
    baseURL: baseUrl,
    headers: {
        'Content-Type': 'application/json'
    }
});


