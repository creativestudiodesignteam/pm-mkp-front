const path = require("path")
const withImages = require("next-images")
module.exports = {
  env: {
    _key: "b6a0d19386d0171a557c11fac4113118",
  },
  ...withImages(),
}
